importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  'messagingSenderId': '264919211030'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();


// self.addEventListener('notificationclick', function(event) {
//   event.notification.close();

//   var promise = new Promise(function(resolve) {
//     setTimeout(resolve, 1000);
//   }).then(function() {
//     return clients.openWindow(event.data.locator);
//   });
//   console.log("Inside push event");

//   event.waitUntil(promise);
// });





messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
 
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: 'favicon.png'
  };

  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});

self.addEventListener('push', function(event) {

  console.log("Inside push event");

}); 