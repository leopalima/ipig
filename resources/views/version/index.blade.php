@extends('esquema.esquema')

@section('libreriascss')

@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Control de versión</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tr>
                                                    <th># Versión</th>
                                                    <th>Fecha</th>
                                                    <th>Autor</th>
                                                    <th>Cambios</th>
                                                    <th></th>
                                                </tr>
                                                @foreach($version as $v)
                                                <tr>
                                                    <td>{{$v->version_num}}</td>
                                                    <td>{{Carbon\Carbon::parse($v->fecha)->format('d/m/Y')}}</td>
                                                    <td>{{$v->autor}}</td>
                                                    <td>{{$v->cambios}}</td>
                                                    <td>
                                                        <button name="btnEditar" id="btnEditar" type="button" class="btn btn-info btnEditar" data-version_id="{{$v->id}}" >Editar</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                            {{ $version->links() }}
                                            <div>
                                                <button type="button" id="btnNuevo" class="btn btn-warning m-b-5 m-t-5">Nuevo</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
@endsection

@section('libreriasjavascript')
<!-- Large Modal -->
<div id="largeModal" class="modal fade">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header pd-x-20">
                <h6 class="modal-title" id="modalTitulo">Título</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <form name="frmVersion" id="frmVersion" action="" method="POST" class="form-horizontal" >
                    @csrf
                    <input type="hidden" id="_method" name="_method" value="">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Número de versión</label>
                        <div class="col-md-9">
                            <input id="txtNumero" name="txtNumero" type="text" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">S.O.</label>
                        <div class="col-md-9">
                            <select name="so" id="so" class="form-control">
                                <option value="android">Android</option>
                                <option value="ios">iOS</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="example-email">Fecha</label>
                        <div class="col-md-9">
                            <input id="txtFecha" name="txtFecha" type="text" class="form-control" placeholder="" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Autor</label>
                        <div class="col-md-9">
                            <input id="txtAutor" name="txtAutor" type="text" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Cambios realizados</label>
                        <div class="col-md-9">
                            <textarea id="txtCambios" name="txtCambios" class="form-control" rows="6"></textarea>
                        </div>
                    </div>
                </form>
            </div><!-- modal-body -->
            <div class="modal-footer">
                <button type="button" value="" id="btnGuardar" class="btn btn-primary" value="">Guardar</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {

            $("#btnNuevo").click(function(){
                $("#frmVersion")[0].reset();
                $("#largeModal").appendTo("body");
                $("#largeModal").modal("show");
                $("#modalTitulo").text("Control de versión - nuevo");
                $("#btnGuardar").val("nuevo");
                $("#_method").val("");
                $frm_url = "{{route('version.index')}}";
                $("#frmVersion").attr("action", $frm_url);
            });

            $.each($("#btnEditar"), function(){
                $("table").delegate("#btnEditar", "click", function(){
                    $id = $(this).data("version_id");
                    $url = "{{route('version.index')}}/"+ $id + "/edit";
                    $frm_url = "{{route('version.index')}}/"+ $id;
                    $("#frmVersion")[0].reset();
                    $("#largeModal").appendTo("body");
                    $("#largeModal").modal("show");
                    $("#modalTitulo").text("Control de versión - Editar");
                    $("#btnGuardar").val("editar");
                    console.log($(this).data("version_id"))
                    
                    $.get($url)
                        .done(function(d){
                            console.log(d);
                            if(d.estatus == "success"){
                                $("#_method").val("PUT");
                                $("#frmVersion").attr("action", $frm_url);
                                $("#txtNumero").val(d.data.version_num);
                                $("#txtFecha").val(d.data.fecha);
                                $("#txtAutor").val(d.data.autor);
                                $("#txtCambios").val(d.data.cambios);
                                $("#so").val(d.data.so);
                                $("#btnGuardar").val("editar");
                            }

                            if(d.estatus == "error"){
                                console.log(d.data);
                            }
                        });
                });
            });

            $("#btnGuardar").click(function(){
                $accion= $(this).val();
                $formulario = $("#frmVersion");
                $url = $("#frmVersion").attr('action');
                $data = $formulario.serialize();

                if($accion=="editar"){
                    $.ajax({
                        url: $url,
                        data: $data,
                        method: "POST",
                        dataType: "JSON",
                        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    })
                    .done(function(d){
                        switch (d.estatus) {
                            case "success":
                                notificacion("Se editó la versión", "success");
                                cerrarModal();
                                reloadPagina();
                              break;
                            case "error":
                                notificacion(d.data, "error");
                              break;
                            default:
                                notificacion("Algo no salió bien", "info");
                              break;
                          }
                    });
                }

                if($accion=="nuevo"){
                    $.ajax({
                        url: $url,
                        data: $data,
                        method: "POST",
                        dataType: "JSON",
                        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    })
                    .done(function(d){
                        console.log(d);
                        switch (d.estatus) {
                            case "success":
                                notificacion("Se guardó la versión", "success");
                                cerrarModal();
                                reloadPagina();
                              break;
                            case "error":
                                notificacion(d.data, "error");
                              break;
                            default:
                                notificacion("Algo no salió bien", "info");
                              break;
                          }
                    });
                }
            });


        });

        function cerrarModal(){
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function() {
                $("#largeModal").modal('hide');//ocultamos el modal
                $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
                $('.modal-backdrop').remove();//eliminamos el backdrop del modal
              }, delayInMilliseconds);
        }
        function reloadPagina(){
            var delayInMilliseconds = 2500; //1 second
            setTimeout(function() {
                window.location = self.location.href;
              }, delayInMilliseconds);
        }


    </script>
@endsection