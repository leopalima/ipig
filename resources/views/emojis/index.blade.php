@extends('esquema.esquema')

@section('libreriascss')

@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Control de versión</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tr>
                                                    <th>id</th>
                                                    <th>Clasificacion</th>
                                                    <th>Emoji</th>
                                                    <th></th>
                                                </tr>
                                                @foreach($emojis as $e)
                                                <tr>
                                                    <td>{{$e->id}}</td>
                                                    <td>{{$e->clasificacion}}</td>
                                                    <td>{{$e->emoji}}</td>
                                                    <td>
                                                        <button name="btnEditar" id="btnEditar" type="button" class="btn btn-info btnEditar" data-emoji_id="{{$e->id}}" >Editar</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                            {{ $emojis->links() }}
                                            <div>
                                                <button type="button" id="btnNuevo" class="btn btn-warning m-b-5 m-t-5">Nuevo</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
@endsection

@section('libreriasjavascript')
<!-- Large Modal -->
<div id="largeModal" class="modal fade">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header pd-x-20">
                <h6 class="modal-title" id="modalTitulo">Título</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <form name="frmEmoji" id="frmEmoji" action="" method="POST" class="form-horizontal" >
                    @csrf
                    <input type="hidden" id="_method" name="_method" value="">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Clasificación</label>
                        <div class="col-md-9">
                            <select name="clasificacion" id="clasificacion" class="form-control">
                                @foreach ($clasificacion as $item)   
                                <option value="{{$item->clasificacion}}">{{$item->clasificacion}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="example-email">Nueva Clasif</label>
                        <div class="col-md-9">
                            <input id="nvaClasif" name="nvaClasif" type="text" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Emoji</label>
                        <div class="col-md-9">
                            <input id="emoji" name="emoji" type="text" class="form-control" value="">
                        </div>
                    </div>
                </form>
            </div><!-- modal-body -->
            <div class="modal-footer">
                <button type="button" value="" id="btnGuardar" class="btn btn-primary" value="">Guardar</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {

            $("#btnNuevo").click(function(){
                $("#frmEmoji")[0].reset();
                $("#largeModal").appendTo("body");
                $("#largeModal").modal("show");
                $("#modalTitulo").text("Control de versión - nuevo");
                $("#btnGuardar").val("nuevo");
                $("#_method").val("");
                $frm_url = "{{route('emojis.index')}}";
                $("#frmEmoji").attr("action", $frm_url);
            });

            $.each($("#btnEditar"), function(){
                $("table").delegate("#btnEditar", "click", function(){
                    $id = $(this).data("emoji_id");
                    $url = "{{route('emojis.index')}}/"+ $id + "/edit";
                    $frm_url = "{{route('emojis.index')}}/"+ $id;
                    $("#frmEmoji")[0].reset();
                    $("#largeModal").appendTo("body");
                    $("#largeModal").modal("show");
                    $("#modalTitulo").text("Control de versión - Editar");
                    $("#btnGuardar").val("editar");
                    console.log($(this).data("emoji_id"))
                    
                    $.get($url)
                        .done(function(d){
                            console.log(d);
                            if(d.estatus == "success"){
                                $("#_method").val("PUT");
                                $("#frmEmoji").attr("action", $frm_url);
                                $("#clasificacion").val(d.data.clasificacion);
                                $("#emoji").val(d.data.emoji);
                                $("#btnGuardar").val("editar");
                            }

                            if(d.estatus == "error"){
                                console.log(d.data);
                            }
                        });
                });
            });

            $("#btnGuardar").click(function(){
                $accion= $(this).val();
                $formulario = $("#frmEmoji");
                $url = $("#frmEmoji").attr('action');
                $data = $formulario.serialize();

                if($accion=="editar"){
                    $.ajax({
                        url: $url,
                        data: $data,
                        method: "POST",
                        dataType: "JSON",
                        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    })
                    .done(function(d){
                        console.log(d)
                        switch (d.estatus) {
                            case "success":
                                notificacion("Se editó la versión", "success");
                                cerrarModal();
                              break;
                            case "error":
                                notificacion(d.data, "error");
                              break;
                            default:
                                notificacion("Algo no salió bien", "info");
                              break;
                          }
                    });
                }

                if($accion=="nuevo"){
                    $.ajax({
                        url: $url,
                        data: $data,
                        method: "POST",
                        dataType: "JSON",
                        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    })
                    .done(function(d){
                        console.log(d);
                        switch (d.estatus) {
                            case "success":
                                notificacion("Se guardó la versión", "success");
                                cerrarModal();
                                $fila = `
                                <tr>
                                        <td>${d.data.id}</td>
                                        <td>${d.data.clasificacion}</td>
                                        <td>${d.data.emoji}</td>
                                        <td>
                                            <button name="btnEditar" id="btnEditar" type="button" class="btn btn-info btnEditar" data-emoji_id="${d.data.id}" >Editar</button>
                                        </td>
                                    </tr>
                                `;
                                $('table tr:first').after($fila);
                              break;
                            case "error":
                                notificacion(d.data, "error");
                              break;
                            default:
                                notificacion("Algo no salió bien", "info");
                              break;
                          }
                    });
                }
            });


        });

        function cerrarModal(){
            var delayInMilliseconds = 1000; //1 second
            setTimeout(function() {
                $("#largeModal").modal('hide');//ocultamos el modal
                $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
                $('.modal-backdrop').remove();//eliminamos el backdrop del modal
              }, delayInMilliseconds);
        }
        function reloadPagina(){
            var delayInMilliseconds = 2500; //1 second
            setTimeout(function() {
                window.location = self.location.href;
              }, delayInMilliseconds);
        }


    </script>
@endsection