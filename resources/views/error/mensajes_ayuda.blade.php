@extends('esquema.esquema')

@section('libreriascss')

@endsection

@section('css')

<style>

    .textoM{
        max-width: 100px;
        overflow: hidden; 
        text-overflow: ellipsis; 
        white-space: nowrap;
    }


</style>

@endsection
@section('contenido')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Mensajes</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive" id="user_table">
                                            <table class="table table-bordered table-hover mb-0 ">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Usuario</th>
                                                    <th>Email</th>
                                                    <th>Mensaje</th>
                                                    <th>Fecha</th>
                                                    <th>Estatus</th>
                                                    <th>Detalles</th>
                                                </tr>
                                                @foreach($mensajesAyuda as $a)
                                                <tr>
                                                    <td>{{$a->id}}</td>
                                                    <td>{{$a->cliente->name}}</td>
                                                    <td>{{$a->cliente->email}}</td>
                                                    <td class="textoM">{{$a->mensaje}}</td>
                                                    <td>{{$a->created_at->format('d-m-Y H:i')}}</td>
                                                    <td class="estatusStyle">
                                                        <span class=" @if( $a->estatus == 'cerrado') badge bg-success mb-1 @else badge bg-danger mb-1 @endif " valule="">
                                                            {{$a->estatus}}
                                                        </span>
                                                    </td>
                                                    <td>
                                                    <button type="button" id="{{$a->id}}" data-error_id=""  data-target="#modalError" data-toggle="modal" class="btn editar btn-primary m-b-5 m-t-5">Detalles</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                        {{ $mensajesAyuda->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">New message</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
              <span id="form_result"></span>
              <form  id="sample_form" class="form-horizontal validation" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Recipient:</label>
                      <input type="text" class="form-control" id="recipient-name" readonly>
                      <input type="hidden" id="idCliente" value="">
                    </div>
                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Message:</label>
                      <textarea class="form-control" id="message-text"readonly></textarea>
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      <button type="submit" id="guardarAyuda" class="btn btn-primary">Ayudar</button>
                    </div>
              </form>
        </div>
        
    </div>
  </div>
</div>
<input type="hidden" value="{{ csrf_token()}}" name="_token">
@endsection

@section('libreriasjavascript')


@endsection

@section('javascript')

<script>

$(document).ready(function() {


    //editar
        $(document).on('click', '.editar', function(){

            var id = $(this).attr('id');
            $('#form_result').html('');
            $('#modalError').appendTo("body");
	    	$('#modalError').modal('show')
            $.ajax({
                url:"/mensaje/"+id,
                dataType:"json",
                success:function(html){
                    
                    if(html.data.estatus == 'cerrado')
                    {
                        document.getElementById("guardarAyuda").disabled = true;
                        $('#guardarAyuda').text("Ayudado");
                    }else{
                        document.getElementById("guardarAyuda").disabled = false;
                        $('#guardarAyuda').text("Ayudar");
                    }
                    $('#recipient-name').val(html.cliente.name);
                    $('#idCliente').val(html.data.id);
                    $('#message-text').text(html.data.mensaje);
                    

                }
            })
        });

    //editar

    //editar guardar
    $('#sample_form').on('submit', function(event){
        event.preventDefault();
        
        var _token = $('input[name="_token"]').val();
        
            var id = $('#idCliente').val();
            $.ajax({
                url:"/mensaje/"+id,
                method:"POST",
                data:new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType:"json",
                success:function(data)
                {
                   
                    $("#user_table").load(" #user_table")
                    $('#modalError').modal('hide')
                    if(data.success){

					    notificacion('Configuración guardada correctamente', 'success');
					}
               
                }
            });
        

    });
    //editar guardar










});

</script>
    

@endsection