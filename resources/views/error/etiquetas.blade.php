@extends('esquema.esquema')

@section('libreriascss')
 

@endsection

@section('css')

<style>

    .textoM{
        max-width: 100px;
        overflow: hidden; 
        text-overflow: ellipsis; 
        white-space: nowrap;
    }


</style>

@endsection
@section('contenido')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div style="display:scroll; position:fixed;bottom:100px;right:50px;">
                                        <button type="button" href="" id="guardarEtiqueta" class="btn btn-success m-b-5 m-t-5">Nuevo</button>
                                    </div>
                                    <div class="card-header">
                                        <h4>Etiquetas</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive" id="user_table">
                                            <table class="table table-bordered table-hover mb-0 " >
                                                <tr>
                                                    <th>#</th>
                                                    <th>Etiqueta</th>
                                                    <th>Fecha</th>
                                                    <th></th>
                                                </tr>
                                            
                                               @foreach($etiquetas as $et)
                                                <tr>
                                                    <td>{{$et->id}}</td>
                                                    <td>{{$et->etiqueta}}</td>
                                                    <td >{{$et->created_at->format('d-m-Y H:i')}}</td>
                                                    <td>
                                                    <button type="button" id="{{$et->id}}" data-error_id=""  data-target="#modalError" data-toggle="modal" class="btn editar btn-primary m-b-5 m-t-5">Editar</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                        {{ $etiquetas->links() }}
                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
              <span id="form_result"></span>
              <form  id="sample_form" class="form-horizontal validation" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Recipient:</label>
                      <input type="text" class="form-control" name="etiquetaNombre" id="etiquetaNombre" >
                      <input type="hidden" id="idEtiqueta" value="">
                    </div>

                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      <input type="submit" id="guardarAyuda" class="btn btn-primary" value="">
                    </div>
              </form>
        </div>
        
    </div>
  </div>
</div>
<input type="hidden" value="{{ csrf_token()}}" name="_token">
@endsection

@section('libreriasjavascript')


@endsection

@section('javascript')

<script>

$(document).ready(function() {

    $(document).on('click', '#guardarEtiqueta', function(){


        $('#modalError').appendTo("body");
        $('#modalError').modal('show')
        $('#guardarAyuda').val('Guardar');
        $('#exampleModalLabel').text('Nueva etiqueta');
        $('#etiquetaNombre').val(''); 
        $('#idEtiqueta').val('');
        
    });


    //editar
        $(document).on('click', '.editar', function(){

            var id = $(this).attr('id');
            $('#form_result').html('');
            $('#modalError').appendTo("body");
            $('#modalError').modal('show')
            $('#guardarAyuda').val('Editar');
            $('#exampleModalLabel').text('Editar etiqueta');
            $.ajax({
                url:"/etiqueta/"+id,
                dataType:"json",
                success:function(html){
                    
                    $('#etiquetaNombre').val(html.data.etiqueta); 
                    $('#idEtiqueta').val(html.data.id);           

                }
            })
        });

    //editar

    //editar crear
    $('#sample_form').on('submit', function(event){
        event.preventDefault();
        
        var _token = $('input[name="_token"]').val();
        //crear guardar       
            if($('#guardarAyuda').val() == 'Guardar')
            {

                $.ajax({
                 url:"{{ route('etiquetas.store') }}",
                 method:"POST",
                 data: new FormData(this),
                 contentType: false,
                 cache:false,
                 processData: false,
                 dataType:"json",
                      success:function(data)
                      {

                        if(data.errors){

                            notificacion('El nombre de la etiqueta es obligatorio', 'error');
                        }
                        if(data.success){
                            $("#user_table").load(" #user_table")
                            $('#etiquetaNombre').val(''); 
                            $('#idEtiqueta').val('');
                            notificacion('Configuración guardada correctamente', 'success');
                        }

                      }
                })
            };
        //crear guardar 

        //editar guardar
            if($('#guardarAyuda').val() == 'Editar')
            {
            
                var id = $('#idEtiqueta').val();
                $.ajax({
                    url:"/etiqueta/"+id,
                    method:"POST",
                    data:new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    dataType:"json",
                    success:function(data)
                    {

                        if(data.errors){

                            notificacion('El nombre de la etiqueta es obligatorio', 'error');
                        }
                    
                        if(data.success){
                            $("#user_table").load(" #user_table")
                            $('#modalError').modal('hide')
                            notificacion('Configuración guardada correctamente', 'success');
                        }
                    
                    }
                });
            
            }
        //editar guardar

    });
    //editar crear










});

</script>
    

@endsection