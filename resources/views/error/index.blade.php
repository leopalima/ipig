@extends('esquema.esquema')

@section('libreriascss')

@endsection

@section('css')

<style>

.modal-error
{
    max-width: 80% !important;
    margin: 5% auto auto auto !important;
}

.tipoError
{
    width: 100%;
    margin-left: 0.2%;
}

#fechaId
{
    padding-right: 6%;
    width: 15%;
}


</style>

@endsection
@section('contenido')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Error</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover mb-0 ">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Usuario</th>
                                                    <th>Código</th>
                                                    <th>Mensaje</th>
                                                    <th id="fechaId"> Fecha</th>
                                                    <th>Detalles</th>
                                                </tr>
                                                @foreach($listaError as $e)
                                                <tr>
                                                    <td>{{$e->id}}</td>
                                                    <td>{{isset($e->usuario->email) ? $e->usuario->email : "-"}}</td>
                                                    <td>{{$e->code}}</td>
                                                    <td>{{$e->message}}</td>
                                                    <td>{{$e->created_at->format('d-m-Y H:i')}}</td>
                                                    <td>
                                                    <button type="button" id="btnError" data-error_id="{{$e->id}}" data-target="#modalError" data-toggle="modal" class="btn btn-primary m-b-5 m-t-5">Detalles</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                            {{ $listaError->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
@endsection

@section('libreriasjavascript')
<!--Scrolling Modal-->
<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-error modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detalles del error</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped mb-0">
                        <tr>
                            <th colspan="2">Mensaje</th>
                        </tr>
                        <tr>
                            <td colspan="2" id="tdMensaje">1</td>
                        </tr>
                        <tr>
                            <th>Archivo</th>
                            <th>Línea</th>
                        </tr>
                        <tr>
                            <td id="tdArchivo">1</td>
                            <td id="tdLinea">1</td>
                        </tr>
                        <tr>
                            <th colspan="2">Trace</th>
                        </tr>
                        <tr>
                            <td class="tdTextarea" colspan="2"><textarea class="tipoError" id="tdTrace" rows="100" cols="100" name="textarea">Write something here</textarea></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                {{--
                <button type="button" class="btn btn-primary">Eliminar</button>
                <!-- falta implementar eliminar -->
                --}}
                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            $.each($("#btnError"), function(){
                $("table").delegate("#btnError", "click", function(){
                    buscarError($(this).data("error_id"));
                });
            });
        });

function buscarError($id){
    $url = "{{route('error.buscar')}}/" + $id;

    $.ajax({
        url: $url,
        dataType: "json",
        headers: {'X-CSRF-TOKEN':$("input[name=_token]").val()},
    })
        .done(function(d){
            $("#tdMensaje").html(d.message);
            $("#tdArchivo").html(d.file);
            $("#tdLinea").html(d.line);
            $("#tdTrace").html(d.trace);

        });
}
</script>

@endsection