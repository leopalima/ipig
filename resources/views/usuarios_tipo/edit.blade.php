@extends('esquema.esquema')

@section('libreriascss')
    {{-- Formularios --}}
        <!--Morris css-->
        <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">

    {{-- Notificaciones emergentes --}}
        <!--Toastr css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">


@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
							<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header">
										<h4>Editar tipo usuario</h4>
									</div>
									<div class="card-body">
										<form class="form-horizontal" method="POST" action="{{action('UsuariosTipoController@update', ['id' => $tipo->id])}}">
                                            @csrf
                                            @method('PUT')
											<div class="form-group row">
												<label class="col-md-3 col-form-label">Nombre</label>
												<div class="col-md-9">
													<input type="text" id="name" name="name" class="form-control" value="{{old('name') ? old('name') : $tipo->name}}">
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 col-form-label" for="email">Descripción</label>
												<div class="col-md-9">
													<input type="text" id="descripcion" name="descripcion" class="form-control" value="{{old('descripcion') ? old('descripcion') : $tipo->descripcion}}" placeholder="Descripción">
												</div>
											</div>
											<div class="form-group mb-0 mt-2 row justify-content-end">
												<div class="col-md-9">
													<button type="submit" name="btnGuardar" id="btnGuardar" class="btn btn-info">Guardar</button>
													<a href="{{route('tipos.index')}}" class="btn btn-warning">Volver</a>
												</div>
                                            </div>
										</form>
									</div>
								</div>
							</div>
                        </div>
                        
@if(session('actualizado'))
	<div style="display: none;" id="actualizado" name="actualizado" class="alert alert-success" data-mensaje="{{session('actualizado')}}"></div>
@endif
@if(session('error'))
	<div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{session('error')}}"></div>
@endif
@if($errors->has('name'))
	<div style="display: none;" id="errorName" name="errorName" class="alert alert-success" data-mensaje="{{$errors->first('name')}}"></div>
@endif
@if($errors->has('descripcion'))
	<div style="display: none;" id="errorDescripcion" name="errorDescripcion" class="alert alert-success" data-mensaje="{{$errors->first('descripcion')}}"></div>
@endif

@endsection

@section('libreriasjavascript')
        
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            if($('#actualizado').length){
                $mensaje = $('#actualizado').data('mensaje');
                notificacion($mensaje, 'success');
            }
            if($('#error').length){
                $mensaje = $('#error').data('mensaje');
                notificacion($mensaje, 'error');
            }
            if($('#errorName').length){
                $mensaje = $('#errorName').data('mensaje');
                notificacion($mensaje, 'error');
            }
            if($('#errorDescripcion').length){
                $mensaje = $('#errorDescripcion').data('mensaje');
                notificacion($mensaje, 'error');
            }
        });
    </script>
@endsection