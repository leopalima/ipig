@extends('esquema.esquema')

@section('libreriascss')

@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div style="display:scroll; position:fixed;bottom:100px;right:50px;">
                                        <a href="{{route('tipos.create')}}" class="btn btn-warning m-b-5 m-t-5">Nuevo</a>
                                    </div>
                                    <div class="card-header">
                                        <h4>Tipos de usuarios</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Nombre</th>
                                                    <th>Descripción</th>
                                                    <th></th>
                                                </tr>
                                                @foreach($tipos as $tipo)
                                                <tr>
                                                    <td></td>
                                                    <td>{{$tipo->name}}</td>
                                                    <td>{{Str::limit($tipo->descripcion, 25)}}</td>
                                                    <td>
                                                        <a class="btn btn-info" href="{{action('UsuariosTipoController@edit', ['id' => $tipo->id])}}">Editar</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                            {{ $tipos->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
@endsection

@section('libreriasjavascript')

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            
        });
    </script>
@endsection