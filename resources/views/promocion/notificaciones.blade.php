@extends('esquema.esquema')

@section('libreriascss')

@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Control de versión</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tr>
                                                    <th>id</th>
                                                    <th>Título</th>
                                                    <th>Mensaje</th>
                                                    <th></th>
                                                </tr>
                                                @foreach($notificaciones as $n)
                                                <tr>
                                                    <td>{{$n->id}}</td>
                                                    <td>{{$n->titulo}}</td>
                                                    <td>{{$n->mensaje}}</td>
                                                    <td>
                                                        {{-- <button name="btnEditar" id="btnEditar" type="button" class="btn btn-info btnEditar" data-notificacion_id="{{$n->id}}" >Editar</button> --}}
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                            {{ $notificaciones->links() }}
                                            <div>
                                                <button type="button" id="btnNuevo" class="btn btn-warning m-b-5 m-t-5">Nuevo</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
@endsection

@section('libreriasjavascript')
<!-- Large Modal -->
<div id="largeModal" class="modal fade">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header pd-x-20">
                <h6 class="modal-title" id="modalTitulo">Título</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <form name="frmNotificaciones" id="frmNotificaciones" action="" method="POST" class="form-horizontal" >
                    @csrf
                    <input type="hidden" id="_method" name="_method" value="">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="example-email">Titulo</label>
                        <div class="col-md-9">
                            <input id="titulo" name="titulo" type="text" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Mensaje</label>
                        <div class="col-md-9">
                            <input id="mensaje" name="mensaje" type="text" class="form-control" value="">
                        </div>
                    </div>
                </form>
            </div><!-- modal-body -->
            <div class="modal-footer">
                <button type="button" value="" id="btnEnviar" class="btn btn-primary" value="">Enviar</button>
                <button type="button" class="btn btn-info" data-dismiss="modal">Cancelar</button>
            </div>
            <div class="modal-body pd-20">
                @php
                    $clasificacion="";
                @endphp
                @foreach ($emojis as $e)
                    @if($e->clasificacion != $clasificacion)
                        <div style="font-weight: bold;">
                            {{$e->clasificacion}}
                        </div>
                        @php
                            $clasificacion = $e->clasificacion;
                        @endphp
                    @endif
                    <div style="display: inline-block">
                        {{$e->emoji}}
                    </div>
                @endforeach
            </div>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {

            $("#btnNuevo").click(function(){
                $("#frmNotificaciones")[0].reset();
                $("#largeModal").appendTo("body");
                $("#largeModal").modal("show");
                $("#modalTitulo").text("Notificación - nueva");
                $("#btnEnviar").val("nuevo");
                $("#_method").val("");
                $frm_url = "{{route('notificaciones.index')}}";
                $("#frmNotificaciones").attr("action", $frm_url);
            });

            $.each($("#btnEditar"), function(){
                $("table").delegate("#btnEditar", "click", function(){
                    $id = $(this).data("notificacion_id");
                    $url = "{{route('notificaciones.index')}}/"+ $id + "/edit";
                    $frm_url = "{{route('notificaciones.index')}}/"+ $id;
                    $("#frmNotificaciones")[0].reset();
                    $("#largeModal").appendTo("body");
                    $("#largeModal").modal("show");
                    $("#modalTitulo").text("Control de versión - Editar");
                    $("#btnEnviar").val("editar");
                    console.log($(this).data("notificacion_id"))
                    
                    $.get($url)
                        .done(function(d){
                            console.log(d);
                            if(d.estatus == "success"){
                                $("#_method").val("PUT");
                                $("#frmNotificaciones").attr("action", $frm_url);
                                $("#titulo").val(d.data.titulo);
                                $("#mensaje").val(d.data.mensaje);
                                $("#btnEnviar").val("editar");
                            }

                            if(d.estatus == "error"){
                                console.log(d.data);
                            }
                        });
                });
            });

            $("#btnEnviar").click(function(){
                $accion= $(this).val();
                $formulario = $("#frmNotificaciones");
                $url = $("#frmNotificaciones").attr('action');
                $data = $formulario.serialize();

                if($accion=="editar"){
                    $.ajax({
                        url: $url,
                        data: $data,
                        method: "POST",
                        dataType: "JSON",
                        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    })
                    .done(function(d){
                        console.log(d)
                        switch (d.estatus) {
                            case "success":
                                notificacion("Se editó la versión", "success");
                                cerrarModal();
                              break;
                            case "error":
                                notificacion(d.data, "error");
                              break;
                            default:
                                notificacion("Algo no salió bien", "info");
                              break;
                          }
                    });
                }

                if($accion=="nuevo"){
                    $.ajax({
                        url: $url,
                        data: $data,
                        method: "POST",
                        dataType: "JSON",
                        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    })
                    .done(function(d){
                        console.log(d);
                        switch (d.estatus) {
                            case "success":
                                notificacion("Se guardó la versión", "success");
                                cerrarModal();
                                $fila = `
                                <tr>
                                        <td>${d.data.id}</td>
                                        <td>${d.data.titulo}</td>
                                        <td>${d.data.mensaje}</td>
                                        <td>
                                            <button name="btnEditar" id="btnEditar" type="button" class="btn btn-info btnEditar" data-notificacion_id="" >Editar</button>
                                        </td>
                                    </tr>
                                `;
                                $('table tr:first').after($fila);
                              break;
                            case "error":
                                notificacion(d.data, "error");
                              break;
                            default:
                                notificacion("Algo no salió bien", "info");
                              break;
                          }
                    });
                }
            });


        });

        function cerrarModal(){
            var delayInMilliseconds = 1000; //1 second
            setTimeout(function() {
                $("#largeModal").modal('hide');//ocultamos el modal
                $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
                $('.modal-backdrop').remove();//eliminamos el backdrop del modal
              }, delayInMilliseconds);
        }
        function reloadPagina(){
            var delayInMilliseconds = 2500; //1 second
            setTimeout(function() {
                window.location = self.location.href;
              }, delayInMilliseconds);
        }


    </script>
@endsection