@extends('esquema.esquema')

@section('libreriascss')
    {{-- Formularios --}}
        <!--Morris css-->
        <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
    {{-- Formularios avanzados --}}
        <!--Bootstrap-daterangepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">

        <!--Bootstrap-datepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}">

        <!--iCheck css-->
        <link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}">

        <!--Bootstrap-colorpicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}">

        <!--Bootstrap-timepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">

        <!--Select2 css-->
        <link rel="stylesheet" href="{{url('assets/plugins/select2/select2.css')}}">

    {{-- Wysiwing Editor --}}
        <!--Bootstrap-wysihtml5 css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    {{-- Notificaciones emergentes --}}
        <!--Toastr css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">


@endsection

@section('css')
@endsection

@section('contenido')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4>Promoción - Banner</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover mb-0 text-nowrap">
                        <tr>
                            <th></th>
                            <th>Thumbnail</th>
                            <th>Fecha</th>
                            <th>Tienda</th>
                            <th></th>
                        </tr>
                        @foreach($banners as $b)
                        <tr>
                            <td>
                                @if($b->estatus)
                                <span class="badge badge-success">I</span>
                                @else
                                <span class="badge badge-danger">O</span>
                                @endif
                            </td>
                            <td><img src="{{$b->imagen_url}}" alt="" width="50"></td>
                            <td>{{$b->updated_at}}</td>
                            <td>{{!empty($b->tienda->name) ? $b->tienda->name : "iPig"}}</td>
                            <td>
                                <button id="btnEditar" data-banner_id="{{$b->id}}" data-toggle="modal" data-target="#largeModal" class="btn btn-warning">Editar</button>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                    <div>
                        {{ $banners->links() }}
                    </div>
                    <div>
                        <button id="btnNuevo" data-toggle="modal" data-target="#largeModal" class="btn btn-success m-b-5 m-t-5">Nuevo</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
        
@endsection

@section('libreriasjavascript')

    <div id="largeModal" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header pd-x-20">
                    <h6 class="modal-title">Message Preview</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <form  enctype="multipart/form-data" method="POST" name="frmBanner" id="frmBanner" action="" class="form-horizontal">
                        @csrf
                        <input type="hidden" id="_method" name="_method" value="">
                        <input type="hidden" id="banner_id" name="banner_id" value="">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label"></label>
                            <div class="col-md-9">
                                <img id="imagen_url" name="imagen_url" src="" alt="" width="400px">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Imagen</label>
                            <div class="col-md-9">
                                <input type="file" class="form-control" accept="image/x-png,image/jpeg" name="image" id="image">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label" for="comercio">Comercio</label>
                            <div class="col-md-9">
                                <select id="comercio" name="comercio" class="form-control">
                                    <option>iPig</option>
                                    @foreach($tiendas as $t)
                                    <option value="{{$t->id}}">{{$t->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Estatus</label>
                            <div class="col-md-9">
                                <select id="estatus" name="estatus" class="form-control">
                                    <option value="1">Activo</option>
                                    <option value="0">Pausado</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div><!-- modal-body -->
                <div class="modal-footer">
                    <button type="button" id="btnGuardar" name="btnGuardar" class="btn btn-primary" value="">Guardar</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    {{-- Formularios avanzados --}}
        <!--Select2 js-->
        <script src="{{url('assets/plugins/select2/select2.full.js')}}"></script>
        <!--Inputmask js-->
        <script src="{{url('assets/plugins/inputmask/jquery.inputmask.js')}}"></script>
        <!--Moment js-->
        <script src="{{url('assets/plugins/moment/moment.min.js')}}"></script>
        <!--Bootstrap-daterangepicker js-->
        <script src="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
        <!--Bootstrap-datepicker js-->
        <script src="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
        <!--Bootstrap-colorpicker js-->
        <script src="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
        <!--Bootstrap-timepicker js-->
        <script src="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
        <!--iCheck js-->
        <script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
        <!--forms js-->
        <script src="{{url('assets/js/forms.js')}}"></script>

    {{-- Wysiwing Editor --}}
        <!--ckeditor js-->
        <script src="{{url('assets/plugins/tinymce/tinymce.min.js')}}"></script>
        <!--Scripts js-->
        <script src="{{url('assets/js/formeditor.js')}}"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            $("#btnNuevo").click(function(){
                $(".modal-title").html("Nuevo Banner");
                $("#btnGuardar").val("guardar");
                $("#imagen_url").attr("src", "");
                $("#_method").val("");
                $("#frmBanner")[0].reset();
                $frm_url = "{{route('banner.index')}}";
                $("#frmBanner").attr("action", $frm_url);

            });

            $("#btnGuardar").click(function(){
                $formulario = $("#frmBanner");
                $url = $("#frmBanner").attr('action');
                $data = new FormData(jQuery("#frmBanner")[0]);
                $accion = $(this).val();

                if($accion == "editar"){
                    $.ajax({
                        url: $url,
                        data: $data,
                        method: "POST",
                        dataType: "JSON",
                        cache: false,
                        contentType: false,
                        processData: false,
                        async: true,
                        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    })
                    .done(function(d){
                        switch (d.estatus) {
                            case "success":
                                notificacion("Se editó la promoción", "success");
                                cerrarModal();
                                reloadPagina();
                              break;
                            case "error":
                                notificacion("Error al guardar los cambios", "error");
                              break;
                            default:
                                notificacion("Algo no salió bien", "info");
                              break;
                          }
                    });
                }

                if($accion == "guardar"){
                    $.ajax({
                        url: $url,
                        data: $data,
                        method: "POST",
                        dataType: "JSON",
                        cache: false,
                        contentType: false,
                        processData: false,
                        async: true,
                        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() },
                    })
                    .done(function(d){
                        switch (d.estatus) {
                            case "success":
                                notificacion("Se guardó la promoción", "success");
                                cerrarModal();
                                reloadPagina();
                              break;
                            case "error":
                                notificacion("Error al guardar los cambios", "error");
                              break;
                            default:
                                notificacion("Algo no salió bien", "info");
                              break;
                          }
                    });
                }

            });

            $.each($("#btnEditar"), function(){
                $("table").delegate("#btnEditar", "click", function(){
                    $id = $(this).data("banner_id");
                    $url = "{{route('banner.index')}}/"+$id+"/edit";
                    $frm_url = "{{route('banner.index')}}/"+$id;
                    $(".modal-title").html("Editar Banner");
                    $("#banner_id").val($id);
                    $("#btnGuardar").val("editar")
                    console.log($id);

                    $.get($url)
                        .done(function(d){
                            console.log(d);
                            if(d.estatus == "success"){
                                $("#_method").val("PUT");
                                $("#frmBanner").attr("action", $frm_url);
                                $("#comercio").val(d.data.tienda_id);
                                $("#estatus").val(d.data.estatus);
                                $("#imagen_url").attr('src', d.data.imagen_url);
                            }

                            if(d.estatus == "error"){
                                console.log(d.data);
                            }
                        });
                })
            });
        });

        function cerrarModal(){
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function() {
                $("#largeModal").modal('hide');//ocultamos el modal
                $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
                $('.modal-backdrop').remove();//eliminamos el backdrop del modal
              }, delayInMilliseconds);
        }
        function reloadPagina(){
            var delayInMilliseconds = 2500; //1 second
            setTimeout(function() {
                window.location = self.location.href;
              }, delayInMilliseconds);
        }
    </script>
@endsection