@extends('esquema.esquema')

@section('libreriascss')

@endsection

@section('css')
<script type="text/javascript">
    function mostrarTiempo($tiempoFin, $elementoId){

        // Set the date we're counting down to
        var countDownDate = new Date($tiempoFin).getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();
                
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
                
            // Time calculations for days, hours, minutes and seconds
            // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            // var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                
            // Output the result in an element with id="demo"
            document.getElementById($elementoId).innerHTML = minutes + "m " + seconds + "s ";
                
            // If the count down is over, write some text 
            if (distance < 0) {
                clearInterval(x);
                document.getElementById($elementoId).innerHTML = "--:--:--";
            }
        }, 1000);
    }
</script>
<style>
.unColor{
    font-weight: 1000;

}
.limiteComentario{
    max-width: 55%;
}
/* #comentario{
    box-shadow: 0 2px 17px 2px rgb(242, 114, 114);
} */

</style>
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Pedidos</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="tablaId" class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tr>
                                                    @usuario('root')
                                                    <th>Administrador</th>
                                                    @endusuario()
                                                    <th>Detalles</th>
                                                    <th>tiempo</th>
                                                    <th>#</th>
                                                    @if(Auth::user()->tipo->name==="root")
                                                    <th>Tienda</th>
                                                    @endif
                                                    <th>Cliente</th>
                                                    <th>Fecha</th>
                                                    <th>Tipo de pago</th>
                                                    <th>Total S/.</th>
                                                    <th>Comision S/.</th>
                                                    <th>IGV S/.</th>
                                                    <th>Total a <br /> depositar S/.</th>
                                                    
                                                </tr>
                                                
                                                @foreach($pedidos as $pedido)
                                                <tr>
                                                    @usuario('root')
                                                    <td>
                                                        <button id="btnAdministrarPedido" name="btnAdministrarPedido" class="btn btn-danger" data-pedido_id="{{$pedido->id}}" data-url="{{route('administrar.pedidos')}}" data-toggle="modal" data-target="#modalAdministrador">Ver</button>
                                                    </td>
                                                    @endusuario
                                                       <td>

                                                    @switch ($pedido->estatus)
                                                        @case ('recibido')
                                                            @php
                                                                $btnBadgeTitulo= "Pendiente";
                                                                $tiempoFin = $pedido->created_at->addMinutes(5);
                                                                $btnBadgeClase= "badge-warning";
                                                            @endphp
                                                            @break
                                                        @case ('aceptado')
                                                            @php
                                                                $btnBadgeTitulo= "Aceptado";
                                                                $tiempoFin = Carbon\Carbon::parse($pedido->aceptado)->addMinutes($pedido->tiempoentrega);
                                                                $btnBadgeClase= "badge-success";
                                                           @endphp
                                                            @break
                                                        @case ('listo')
                                                            @php
                                                                $btnBadgeTitulo= "Aceptado";
                                                                $btnBadgeClase= "badge-info";
                                                                $tiempoFin = $pedido->created_at->addMinutes(5);
                                                           @endphp
                                                           @break
                                                           @case ('buscando')
                                                            @php
                                                                $btnBadgeTitulo= "Aceptado";
                                                                $btnBadgeClase= "badge-info";
                                                                $tiempoFin = $pedido->created_at->addMinutes(5);
                                                           @endphp
                                                           @break
                                                           @case ('encamino')
                                                            @php
                                                                $btnBadgeTitulo= "Entregando";
                                                                $btnBadgeClase= "badge-secondary";
                                                                $tiempoFin = $pedido->created_at->addMinutes(5);
                                                           @endphp
                                                            @break
                                                        @case ('cancelado')
                                                            @php
                                                                $btnBadgeTitulo= "Cancelado";
                                                                $btnBadgeClase= "badge-danger";
                                                                $tiempoFin = $pedido->created_at->addMinutes(5);
                                                           @endphp
                                                            @break
                                                        @case ('entregado')
                                                            @php
                                                                $btnBadgeTitulo= "Entregado";
                                                                $btnBadgeClase= "badge-primary";
                                                                $tiempoFin = $pedido->created_at->addMinutes(5);
                                                           @endphp
                                                            @break
                                                        @default
                                                            @php
                                                                $btnBadgeTitulo= "---";
                                                                $btnBadgeClase= "badge-info";
                                                                $tiempoFin = $pedido->created_at->addMinutes(5);
                                                           @endphp
                                                            @break
                                                    @endswitch
                                                    <button class="btn btn-app mt-2" id="btnDetalles" name="btnDetalles" data-url="{{route('tienda.pedidos.pedido', ['id' => $pedido->id])}}" data-pedido_id="{{$pedido->id}}" data-pedido="{{$pedido->id}}" class="btn btn-info m-b-5 m-t-5" data-toggle="modal" >
                                                        <span id={{$pedido->id}} class="badge {{$btnBadgeClase}}">{{$btnBadgeTitulo}}</span>Detalles
                                                    </button>
                                                    </td>
                                                    <td id="tiempo-{{$pedido->id}}">
                                                    00:00:00:00
                                                    </td>
                                                    
                                                    <script type="text/javascript">
                                                    mostrarTiempo("{{$tiempoFin}}", "tiempo-{{$pedido->id}}")
                                                    </script>
                                                    <td style='display: none;' id="{{$pedido->cliente_id}}"></td>
                                                    <td style='display: none;' id="{{$pedido->id}}"></td>
                                                    <td>{{$pedido->codigo}}</td>
                                                    @if(Auth::user()->tipo->name==="root")
                                                    <td>{{$pedido->tienda->name}}</td>
                                                    @endif
                                                    <td>{{$pedido->cliente->name}}</td>
                                                    <td>{{\Carbon\Carbon::parse($pedido->created_at)->format('d/m/Y h:i A')}}</td>
                                                    <td>{{$pedido->tipo_pago == "culqi" ? "online" : $pedido->tipo_pago}}</td>
                                                    <td style='text-align: right;'>
                                                        @php
                                                        $montoTotal = $pedido->detalles->sum(function($d){
                                                                            return $d->precio * $d->cantidad;
                                                                        });
                                                        $montoTotal = $montoTotal + +$pedido->costoenvio;
                                                        $comision = $montoTotal * ($pedido->prctjcomision/100);
                                                        $igv = $comision * 1.18;
                                                        $totalDepositar = $montoTotal - ($igv);
                                                        @endphp
                                                        {{
                                                            number_format($montoTotal,2)
                                                        }}
                                                    </td>
                                                    <td style='text-align: right;'>
                                                            {{number_format($comision,2)}}
                                                    </td>
                                                    <td style='text-align: right;'>
                                                            {{number_format($igv,2)}}
                                                    </td>
                                                    <td style='text-align: right;'>
                                                            {{number_format($totalDepositar,2)}}
                                                    </td>
                                                        </tr>
                                                @endforeach
                                            </table>
                                            {{ $pedidos->links()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
@endsection

@section('libreriasjavascript')
<!-- Large Modal -->
<div id="largeModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header pd-x-20">
                <h6 class="modal-title" id="modalTitulo"></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="modalContenido">
            </div>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->

<div id="divContenidoDetalles" hidden>
<div class="modal-body pd-20" id="modalCuerpo">
    <div class="row card-body">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="pull-left m-t-5 m-b-5">
                    <h6 class="unColor">Información del Cliente:</h6>
                    <address id="cliente" class="mb-0">
                    </address>
                </div>
            </div>
            <div class="col-md-12">
                <div class="pull-left m-t-5 m-b-5">
                    <h6 class="unColor">Detalles de pago:</h6>
                    <p class="mb-1"><span class="text-muted">Tipo de pago: </span> <span id="tipoPago"><span></p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="pull-left m-t-5 m-b-5">
                    <h6 class="unColor">Dirección de entrega:</h6>
                    <p class="mb-1" id="direccion"><span class="text-muted">Tipo de pago: </span> <span id="direccion"><span></p>
                </div>
            </div>
            <div class="pull-right m-t-5 m-b-5 card">
                <div class="card-header"><h6 class="unColor limiteComentario">Comentario:</h6></div>
                <div class="card-body"><address id="comentario" class="mb-0 ">
                </address></div>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table border table-bordered text-nowrap">
            <thead>
                <tr>
                    <th class="border-0 text-uppercase  font-weight-bold" colspan="2">Pedido</th>
                    <th class="border-0 text-uppercase  font-weight-bold">Cant.</th>
                    <th class="border-0 text-uppercase  font-weight-bold">P. Unit.</th>
                    <th class="border-0 text-uppercase  font-weight-bold">Total</th>
                </tr>
            </thead>
            <tbody id="tablaProductos">
                <tr>
                    <td>Software</td>
                    <td></td>
                    <td>21</td>
                    <td>$321</td>
                    <td>$3452</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Software</td>
                    <td>21</td>
                    <td>$321</td>
                    <td>$3452</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="col-xl-4 col-12 offset-xl-8">
        <!-- <p class="text-right"><strong>Sub-total:</strong> $32,432</p>
        <p class="text-right">Discout: 10%</p> -->
        <hr>
        <h5 class="text-right">Total :<strong id="granTotal"></strong></h5>
    </div>
</div><!-- modal-body -->
<div class="modal-footer" id="modalFooter">
    <button id="btnAceptar" data-url="{{route('tienda.pedidos.pedido')}}" data-pedido_id="" type="button" data-toggle="modal" data-target="#modalEmergente" class="btn btn-primary" hidden>Aceptar</button>
    <button id="btnEntregar" data-url="{{route('tienda.pedidos.pedido')}}" data-pedido_id="" type="button" class="btn btn-primary" hidden>Entregar</button>
    <button id="btnListo" data-url="{{route('tienda.pedidos.pedido')}}" data-pedido_id="" type="button" class="btn btn-success" hidden>Listo enviar</button>
    <button id="btnCancelar" data-url="{{route('tienda.pedidos.pedido')}}" data-pedido_id="" type="button" data-toggle="modal" data-target="#modalEmergente" class="btn btn-warning" hidden>Cancelar</button>
    <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
</div>
</div>



<!-- Message Modal -->
<div class="modal fade" id="modalEmergente" tabindex="-1" role="dialog"  aria-hidden="true" >
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header" style="background-color:#f27272">
                <h5 class="modal-title" id="modalETitulo"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="modalEContenido">
            </div>
        </div>
    </div>
</div>

<div id="divForm" hidden>
    <div class="modal-body">
        <form class="form-horizontal" id="frmAceptarPedido">
            <div class="form-group row">
                <label class="col-md-3 col-form-label">Tiempo de Preparación</label>
                <div class="col-md-9">
                    <input type="text" id="tiempoentrega" name="tiempoentrega" class="form-control" value="Typing.....">
                </div>
            </div>
            {{--
            <div class="form-group row">
                <label class="col-md-3 col-form-label">Costo de envío</label>
                <div class="col-md-9">
                    <input type="text" id="costoenvio" name="costoenvio" class="form-control" value="Typing.....">
                </div>
            </div>
            --}}
        </form>
    </div>
    <div class="modal-footer">
        <button id="btnAceptarPedido" data-pedido_id="" type="button" class="btn btn-primary">Aceptar</button>
        <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
    </div>
</div>

<div id="divDetalles" hidden>
    <div class="row card-body">
        <div class="col-md-12">
            <div class="pull-left m-t-5 m-b-5">
                <h6 class="unColor">Información del Cliente:</h6>
                <address id="cliente" class="mb-0 ">
                    John Doe, Mrs Emma Downson<br>Acme Inc<br>
                    Berlin, Germany <br>6781 45P
                </address><br>
                <h6 class="unColor" >Detalles de pago:</h6>
                <p class="mb-1"><span class="text-muted">Tipo de pago: </span> <span id="tipoPago"> Root<span></p>
            </div>
            <div class="pull-right m-t-5 m-b-5 card limiteComentario" style="width:350px">
                <div class="card-header"><h6 class="unColor limiteComentario">Comentario:</h6></div>
                <div class="card-body"><address id="comentario" class="mb-0">
                    John Doe, Mrs Emma Downson<br>Acme Inc<br>
                    Berlin, Germany <br>6781 45P
                </address></div>
            </div> 
        </div> 
        <div class="col-md-12">
            <div class="pull-left m-t-5 m-b-5">
                <h6 class="unColor" >Dirección de entrega:</h6>
                <p class="mb-1" id="direccion"><span class="text-muted">Tipo de pago: </span> <span> Root<span></p>
                <p class="mb-1"><a id="verMapa" class="btn btn-primary mt-1 mb-0" href="https://www.google.com/maps/place/-12.0463731,-77.042754" target="_blank">[Ver mapa]</a></p>
            </div>
        </div><!-- end col -->
    </div>

    <div class="table-responsive">
        <table class="table border table-bordered text-nowrap">
            <thead>
                <tr>
                    <th class="border-0 text-uppercase  font-weight-bold" colspan="2">Pedido</th>
                    <th class="border-0 text-uppercase  font-weight-bold">Cant.</th>
                    <th class="border-0 text-uppercase  font-weight-bold">P. Unit.</th>
                    <th class="border-0 text-uppercase  font-weight-bold">Total</th>
                </tr>
            </thead>
            <tbody id="tablaProductos">
            </tbody>
        </table>
    </div>

    <div class="col-xl-4 col-12 offset-xl-8">
        <p class="text-right"><strong>Sub-total: </strong><span id="subTotal">$32,432</span></p>
        <p class="text-right ocultarDescuento"><strong>Descuento: <span id="descuento"></span> </strong><span id="descontado">$32,432</span></p>
        <p class="text-right ocultarEnvio"><strong>Envío: </strong><span id="costoEnvio">$32,432</span></p>
        <hr>
        <h5 class="text-right">Total: <strong id="granTotal"> $234,79</strong></h5>
    </div>
</div><!-- modal-body -->

@usuario('root')
<div id="modalAdministrador" class="modal fade">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header pd-x-20">
                <h6 class="modal-title">Administrador de pedido</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <form id="admFormPedido" name="admFormPedido" class="form-horizontal" >
                    @csrf
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Id</label>
                        <div class="col-md-9">
                            <input id="admId" name="admId" type="text" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Tienda</label>
                        <div class="col-md-9">
                            <input id="admTienda" name="admTienda" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Cliente</label>
                        <div class="col-md-9">
                            <input id="admCliente" name="admCliente" type="text" class="form-control" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Estatus</label>
                        <div class="col-md-9">
                            <select id="admEstatus" name="admEstatus" class="form-control">
                                    <option value="aceptado">Aceptado</option>
                                    <option value="buscando">Ir al comercio</option>
                                    <option value="encamino">Ir al cliente</option>
                                    <option value="entregado">Entregado</option>
                                    <option value="cancelado">Cancelado</option>
                                </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Tiempo de entrega</label>
                        <div class="col-md-9">
                            <input id="admTiempo" name="admTiempo" type="text" class="form-control">
                        </div>
                    </div>    
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Motorizado</label>
                        <div class="col-md-9">
                            <select id="admMotorizado" name="admMotorizado" class="form-control">
                            </select>
                        </div>
                    </div>    
                </form>
            </div><!-- modal-body -->
            <div class="modal-footer">
                <button id="admBtnModificar" name="admBtnModificar" type="button" data-url="{{route('administrar.pedidos.modificar')}}" class="btn btn-primary">Guardar cambios</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
@endusuario

@csrf
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            $(document).on('show.bs.modal', '.modal', function () {
                    var zIndex = 1040 + (10 * $('.modal:visible').length);
                    $(this).css('z-index', zIndex);
                    setTimeout(function() {
                        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                    }, 0);  
                });

                var urlLoca=window.location.search
                if(urlLoca=="?activos"){
                    var replaceP =window.location.origin;
                    var tablaDato= document.getElementById("tablaId");

                    for($i=0; $i<tablaDato.rows.length; $i++){
                        if(tablaDato.rows[$i].cells[0].children.length > 0) { 
                            if(tablaDato.rows[$i].cells[0].children[0].children[0].innerHTML == 'Aceptar'){
                                var clienteId=tablaDato.rows[$i].cells[2].id;
                                var pedidoId=tablaDato.rows[$i].cells[3].id;
                                var urlPedido="{{route('tienda.pedidos.pedido')}}/"+pedidoId;
                                obtenerPedido(urlPedido,clienteId);
                                history.pushState(null, "", "activos");
                                break;
                            }
                        }
                    }  

                }

            $.each($("#btnAdministrarPedido"), function(){
                $("table").delegate("#btnAdministrarPedido", "click", function(){
                    administrarPedidoObtener($(this).data("pedido_id"), $(this).data("url"));
                })
            });

            $("#admBtnModificar").click(function(){
                administrarPedidoModificar($(this).data("url"));
            });
            

            $.each($("#btnEstatus"), function(){
                $("table").delegate("#btnEstatus", "click", function(){

                    if($(this).hasClass('btn-primary')){
                        $boton = $(this);
                        modalAceptar($boton);
                    }
                    if($(this).hasClass('btn-success')){
                        $boton = $(this);
                        modalEnviar($boton);
                    }
                })
            });

            $.each($("#btnDetalles"), function(){
                
                $("table").delegate("#btnDetalles", "click", function(event){
                   
                    $('#largeModal').modal("show");
                    $("#divContenidoDetalles").attr("hidden", false);
                    $("#modalContenido").html($("#divContenidoDetalles"));
                    $url = $(this).data('url');
                    $pedido = $(this).data('pedido_id');
                   obtenerPedido($url, $pedido)
                });
            });
        });

function obtenerPedido($url,$pedido){
    $('#largeModal').modal("show");
    $("#divContenidoDetalles").attr("hidden", false);
    $("#modalContenido").html($("#divContenidoDetalles"));
    $url = $url;
    $pedido = $pedido;
    $("#modalCuerpo").html("Obteniendo datos...");
    $("#cliente").html("");
    $("#tipoPago").html("");
    $("#modalTitulo").html("");
    $("#comentario").html("");
    $.get($url, {pedido:$pedido})
        .done(function(d){
            $("#modalCuerpo").html($("#divDetalles").html());
            
            $("#cliente").html(d.cliente.name + "<br>" + d.cliente.email + "<br>" + d.cliente.phone);
            $("#tipoPago").html(d.tipo_pago);
            $("#modalTitulo").html(d.id);
            $("#comentario").html(d.comentario);
            $coordenadasMapa = "https://www.google.com/maps/place/"+ d.cliente.latitud +","+ d.cliente.longitud +""
            $("#verMapa").attr("href", $coordenadasMapa);
            $("#direccion").html(d.direccion);
            $cant_detalles = Object.keys(d.detalles).length;
            $producto = "";
            $granTotal = 0;
            for($i=0; $i < $cant_detalles; $i++){
                $total = d.detalles[$i].cantidad * d.detalles[$i].precio;
                $granTotal = $total + $granTotal;
                if(d.detalles[$i].producto != null){
                    $producto = $producto + "<tr><td><b>"+ d.detalles[$i].producto.name+"</b></td><td></td><td>"+ d.detalles[$i].cantidad +"</td><td> S/ "+ d.detalles[$i].precio +"</td><td> S/ "+ $total.toFixed(2) +"</td></tr>"
                }
                if(d.detalles[$i].adicional != null){
                    $producto = $producto + "<tr><td></td><td>"+ d.detalles[$i].adicional.nombre+"</td><td>"+ d.detalles[$i].cantidad +"</td><td> S/ "+ d.detalles[$i].precio +"</td><td> S/ "+ $total.toFixed(2) +"</td></tr>"
                }
            }
            $("#btnAceptar").attr("hidden", true);
            $("#btnListo").attr("hidden", true);
            $("#btnCancelar").attr("hidden", true);
            $("#btnEntregar").attr("hidden", true);

            $("#btnAceptar").data("pedido_id", d.id);
            $("#btnAceptar").click(function(){
                modalAceptar($(this), d.id);
            });

            $("#btnListo").data("pedido_id", d.id);
            $("#btnListo").click(function(){
                fnListo(d.id);
            });
            
            $("#btnCancelar").data("pedido_id", d.id);
            $("#btnCancelar").click(function(){
                modalCancelar($(this), d.id);
            });

            $("#btnEntregar").data("pedido_id", d.id);
            $("#btnEntregar").click(function(){
                fnEntregar(d.id);
            });

            if(d.estatus=="recibido"){
                $("#btnAceptar").attr("hidden", false);
                $("#btnListo").attr("hidden", true);
                $("#btnCancelar").attr("hidden", false);
            }
            if(d.estatus=="aceptado"){
                $("#btnAceptar").attr("hidden", true);
                $("#btnListo").attr("hidden", false);
                $("#btnCancelar").attr("hidden", false);
            }
            if(d.estatus=="encamino"){
                $("#btnAceptar").attr("hidden", true);
                $("#btnListo").attr("hidden", true);
                $("#btnEntregar").attr("hidden", false);
                $("#btnCancelar").attr("hidden", false);
            }
            $subTotal = $granTotal;
            $costoDeEnvio = 0;
            if(d.motospropias)
                $costoDeEnvio = d.costoenvio;

            $descuento = 0;
            $descontado = 0;
            $(".ocultarDescuento").hide();
            if(d.prctjdescuento){
                $descuento =  d.prctjdescuento;
                $descontado = $subTotal * ($descuento / 100);
                $(".ocultarDescuento").show();
            }
            $granTotal=($granTotal - $descontado)+$costoDeEnvio;
            $("#tablaProductos").html($producto);
            $("#subTotal").html("S/. " + $subTotal.toFixed(2));
            $("#descuento").html($descuento.toFixed(2) + "%");
            $("#descontado").html("S/. " + $descontado.toFixed(2));
            $("#costoEnvio").html("S/. " + $costoDeEnvio.toFixed(2));
            $("#granTotal").html("S/. " + $granTotal.toFixed(2));
        });
        $(document).on('show.bs.modal', '.modal', function () {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);  
        });
}



function eventFire(el, etype){
  if (el.fireEvent) {
    $("#divContenidoDetalles").attr("hidden", false);
    el.fireEvent('on' + etype);
  } else {
    $("#divContenidoDetalles").attr("hidden", false);
    var evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}

function modalAceptar($boton, id){
    $url = $boton.data('url') + "/" + id;
    $pedido = $boton.data('pedido_id');
    $.get($url, {pedido:$pedido})
        .done(function(d){

            // $("#cliente").html(d.cliente.name + "<br>" + d.cliente.email);
            // $("#tipoPago").html(d.tipo_pago);
            $("#modalETitulo").html(d.id);
            $("#btnAceptarPedido").data("pedido_id", d.id);
            $("#modalEContenido").html($("#divForm").html());

            $("#tiempoentrega").val(d.tiempoentrega);
            $("#costoenvio").val(d.costoenvio);

            $("#btnAceptarPedido").click(function(){
                    aceptar(d.id);
            });
        });
}

function modalCancelar($boton, id){
    $url = $boton.data('url') + "/" + id;
    $pedido = $boton.data('pedido_id');
    $.get($url, {pedido:$pedido})
        .done(function(d){

            // $("#cliente").html(d.cliente.name + "<br>" + d.cliente.email);
            // $("#tipoPago").html(d.tipo_pago);
            $("#modalETitulo").html(d.id);
            $("#btnAceptarPedido").data("pedido_id", d.id);
            $("#modalEContenido").html($("#divForm").html());
            
            $contenido = `
            <div>
                <div class="modal-body">
                    <form class="form-horizontal" id="frmCancelarPedido">
                        <div class="form-group row">
                            <div class="col-md-9">
                                <div class="pull-left m-t-5 m-b-5">
                                    <h4 class="unColor">¿Desea cancelar el pedido?</h4>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9">
                                <div class="pull-left m-t-5 m-b-5">
                                    <h6 class="unColor">Información del Cliente:</h6>
                                    <address id="cancelarCliente" class="mb-0">
                                    </address>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-9">
                                <div class="pull-left m-t-5 m-b-5">
                                    <h6 class="unColor">Detalles de Pago:</h6>
                                    <address id="cancelarDetallePago" class="mb-0">
                                    </address>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="btnAceptarPedido" data-pedido_id="" type="button" class="btn btn-primary">Aceptar</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
            `

            $("#modalEContenido").html($contenido);
            $("#cancelarCliente").html(d.cliente.name);
            $("#cancelarDetallePago").html(d.tipo_pago);
            console.log(d);
            $("#tiempoentrega").val(d.tiempoentrega);
            $("#costoenvio").val(d.costoenvio);

            $("#btnAceptarPedido").click(function(){
                    cancelar(d.id);
            });
        });
}

function aceptar(pedido){
    $url = "{{route('tienda.pedidos.aceptar')}}/" + pedido;
    $data = {tiempoentrega:$("#tiempoentrega").val()};

    $.ajax({
        url: $url,
        dataType: "json",
        data:{data:$data},
        headers: {'X-CSRF-TOKEN':$("input[name=_token]").val()},
    })
        .done(function(d){
            if(d.error){
                notificacion(d.error, 'error');
            }

            if(d.exito){
                $("#" + pedido).html("Aceptado");
                $("#" + pedido).removeClass("badge-warning");
                $("#" + pedido).addClass("badge-success");
                notificacion(d.exito, 'success');
                $(".modal").modal("hide");
            }
        });
}

function cancelar(pedido){
    $url = "{{route('tienda.pedidos.cancelar')}}/" + pedido;
    $data = {tiempoentrega:$("#tiempoentrega").val()};

    $.ajax({
        url: $url,
        dataType: "json",
        data:{data:$data},
        headers: {'X-CSRF-TOKEN':$("input[name=_token]").val()},
    })
        .done(function(d){
            console.log(d);
            if(d.error){
                notificacion(d.error, 'error');
            }

            if(d.exito){
                $("#" + pedido).html("Cancelado");
                $("#" + pedido).removeClass("badge-warning");
                $("#" + pedido).addClass("badge-danger");
                notificacion(d.exito, 'success');
                $(".modal").modal("hide");
            }
        });
}

function fnListo(pedido){
    $url = "{{route('tienda.pedidos.enviar')}}/" + pedido;
    $data = {tiempoentrega:$("#tiempoentrega").val(), costoenvio:$("#costoenvio").val()};

    $.ajax({
        url: $url,
        dataType: "json",
        data:{data:$data},
        headers: {'X-CSRF-TOKEN':$("input[name=_token]").val()},
    })
        .done(function(d){
            if(d.error){
                notificacion(d.error, 'error');
            }

            if(d.exito){
                $("#" + pedido).html("Entregando");
                $("#" + pedido).removeClass("badge-success");
                $("#" + pedido).addClass("badge-secondary");
                notificacion(d.exito, 'success');
                $(".modal").modal("hide");
            }
        });
}

function fnEntregar(pedido){
    $url = "{{route('tienda.pedidos.entregar')}}/" + pedido;
    $data = {tiempoentrega:$("#tiempoentrega").val(), costoenvio:$("#costoenvio").val()};

    $.ajax({
        url: $url,
        dataType: "json",
        data:{data:$data},
        headers: {'X-CSRF-TOKEN':$("input[name=_token]").val()},
    })
        .done(function(d){
            if(d.error){
                notificacion(d.error, 'error');
            }

            if(d.exito){
                $("#" + pedido).html("Entregado");
                $("#" + pedido).removeClass("badge-secondary");
                $("#" + pedido).addClass("badge-primary");
                notificacion(d.exito, 'success');
                $(".modal").modal("hide");
            }
        });
}

function administrarPedidoObtener(pedido_id, url){
    $.get(url, {pedido:pedido_id})
                        .done(function(d){
                            if(d.estatus=="success"){
                                $("#admFormPedido").trigger("reset");
                                $("#admMotorizado option").remove();
                                $("#admId").val(d.data.pedido.id);
                                $("#admTienda").val(d.data.pedido.tienda.name);
                                $("#admCliente").val(d.data.pedido.cliente.name);
                                $("#admEstatus").val(d.data.pedido.estatus);
                                $("#admTiempo").val(d.data.pedido.tiempoentrega);

                                $countMotorizados = Object.keys(d.data.motorizados).length;
                                for($i=0; $i<$countMotorizados; $i++){
                                    $("#admMotorizado").append(new Option(d.data.motorizados[$i].name, d.data.motorizados[$i].id));
                                }
                                $("#admMotorizado").val(d.data.pedido.motorizado_id);
                            }
                            if(d.estatus=="error"){
                            }
                        })
}

function administrarPedidoModificar(url){
    $formulario = $("#admFormPedido").serialize();
    $.post(url, $formulario)
        .done(function(d){
            if(d.estatus == "success"){
                window.location = self.location.href;
            }
            if(d.estatus == "error"){
                window.location = self.location.href;
            }
        });
}

</script>

<script type="text/javascript">
    function idleTimer() {
        var t;
        //window.onload = resetTimer;
        window.onmousemove = resetTimer; // catches mouse movements
        window.onmousedown = resetTimer; // catches mouse movements
        window.onclick = resetTimer;     // catches mouse clicks
        window.onscroll = resetTimer;    // catches scrolling
        window.onkeypress = resetTimer;  //catches keyboard actions
    
        function logout() {
            window.location.href = '/action/logout';  //Adapt to actual logout script
        }
    
       function reload() {
              window.location = self.location.href;  //Reloads the current page
       }
    
       function resetTimer() {
            clearTimeout(t);
            //t = setTimeout(logout, 1800000);  // time is in milliseconds (1000 is 1 second)
            t= setTimeout(reload, 60000);  // time is in milliseconds (1000 is 1 second)
        }
    }
    idleTimer();
    </script>
@endsection