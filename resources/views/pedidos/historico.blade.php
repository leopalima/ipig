@extends('esquema.esquema')

@section('libreriascss')

@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Pedidos</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Cliente</th>
                                                    <th>Fecha</th>
                                                    <th>Tipo de pago</th>
                                                    <th>Total S/.</th>
                                                    <th>Detalles</th>
                                                </tr>
                                                @foreach($pedidos as $pedido)
                                                <tr>
                                                    <td>{{$pedido->codigo}}</td>
                                                    <td>{{$pedido->cliente->name}}</td>
                                                    <td>{{\Carbon\Carbon::parse($pedido->created_at)->format('d/m/Y')}}</td>
                                                    <td>{{$pedido->tipo_pago}}</td>
                                                    <td>
                                                        {{
                                                            number_format(
                                                            $pedido->detalles->sum(function($d){
                                                                $descuento= 0.13*($d->precio * $d->cantidad);
                                                                $total= ($d->precio * $d->cantidad)-$descuento;
                                                                return $total;
                                                            })
                                                            ,2)
                                                        }}
                                                    </td>
                                                    <td>
                                                        @switch ($pedido->estatus)
                                                            @case ('recibido')
                                                                @php
                                                                    $btnBadgeTitulo= "Aceptar";
                                                                    $btnBadgeClase= "badge-info";
                                                                @endphp
                                                                @break
                                                            @case ('aceptado')
                                                                @php
                                                                    $btnBadgeTitulo= "Enviar";
                                                                    $btnBadgeClase= "badge-success";
                                                               @endphp
                                                                @break
                                                            @case ('enviado')
                                                                @php
                                                                    $btnBadgeTitulo= "Enviado";
                                                                    $btnBadgeClase= "badge-primary";
                                                               @endphp
                                                                @break
                                                            @case ('cancelado')
                                                                @php
                                                                    $btnBadgeTitulo= "Cancelado";
                                                                    $btnBadgeClase= "badge-danger";
                                                               @endphp
                                                                @break
                                                            @case ('entregado')
                                                                @php
                                                                    $btnBadgeTitulo= "Entregado";
                                                                    $btnBadgeClase= "badge-danger";
                                                               @endphp
                                                                @break
                                                            @default
                                                                @php
                                                                    $btnBadgeTitulo= "---";
                                                                    $btnBadgeClase= "badge-info";
                                                               @endphp
                                                                @break
                                                        @endswitch
                                                        <button class="btn btn-app mt-2" id="btnDetalles" name="btnDetalles" data-url="{{route('tienda.pedidos.pedido', ['id' => $pedido->id])}}" data-pedido_id="{{$pedido->id}}" data-pedido="{{$pedido->id}}" class="btn btn-info m-b-5 m-t-5" data-toggle="modal" data-target="#largeModal"><span id={{$pedido->id}} class="badge {{$btnBadgeClase}}">{{$btnBadgeTitulo}}</span>Detalles</button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                            {{ $pedidos->links()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
@endsection

@section('libreriasjavascript')
<!-- Large Modal -->
<div id="largeModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header pd-x-20">
                <h6 class="modal-title" id="modalTitulo"></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="modalContenido">
            </div>
        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->

<div id="divContenidoDetalles" hidden>
<div class="modal-body pd-20" id="modalCuerpo">
    <div class="row card-body">
        <div class="col-md-12">
            <div class="pull-left m-t-5 m-b-5">
                <h6>Información del Cliente:</h6>
                <address id="cliente" class="mb-0">
                </address>
            </div>
            <div class="pull-right m-t-5 m-b-5">
                <h6>Detalles de pago:</h6>
                <p class="mb-1"><span class="text-muted">Tipo de pago: </span> <span id="tipoPago"><span></p>
            </div>
        </div><!-- end col -->
        <div class="col-md-12">
            <div class="pull-left m-t-5 m-b-5">
                <h6>Comentario:</h6>
                <address id="comentario" class="mb-0">
                </address>
            </div>
            <div class="pull-right m-t-5 m-b-5">
                <h6>Dirección de entrega:</h6>
                <p class="mb-1" id="direccion"><span class="text-muted">Tipo de pago: </span> <span id="direccion"><span></p>
            </div>
        </div><!-- end col -->
    </div>

    <div class="table-responsive">
        <table class="table border table-bordered text-nowrap">
            <thead>
                <tr>
                    <th class="border-0 text-uppercase  font-weight-bold" colspan="2">Pedido</th>
                    <th class="border-0 text-uppercase  font-weight-bold">Cant.</th>
                    <th class="border-0 text-uppercase  font-weight-bold">P. Unit.</th>
                    <th class="border-0 text-uppercase  font-weight-bold">Total</th>
                </tr>
            </thead>
            <tbody id="tablaProductos">
                <tr>
                    <td>Software</td>
                    <td></td>
                    <td>21</td>
                    <td>$321</td>
                    <td>$3452</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Software</td>
                    <td>21</td>
                    <td>$321</td>
                    <td>$3452</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="col-xl-4 col-12 offset-xl-8">
        <!-- <p class="text-right"><strong>Sub-total:</strong> $32,432</p>
        <p class="text-right">Discout: 10%</p> -->
        <hr>
        <h5 class="text-right">Total :<strong id="granTotal"></strong></h5>
    </div>
</div><!-- modal-body -->
<div class="modal-footer" id="modalFooter">
    <button id="btnAceptar" data-url="{{route('tienda.pedidos.pedido')}}" data-pedido_id="" type="button" data-toggle="modal" data-target="#modalEmergente" class="btn btn-primary" hidden>Aceptar</button>
    <button id="btnEnviar" data-url="{{route('tienda.pedidos.pedido')}}" data-pedido_id="" type="button" class="btn btn-success" hidden>Enviar</button>
    {{--  <button id="btnRechazar" data-url="{{route('tienda.pedidos.pedido')}}" data-pedido_id="" type="button" data-toggle="modal" data-target="#modalEmergente" class="btn btn-primary" hidden>Rechazar</button>  --}}
    <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
</div>
</div>



<!-- Message Modal -->
<div class="modal fade" id="modalEmergente" tabindex="-1" role="dialog"  aria-hidden="true" style="text-align: center; padding: 0!important;">
    <div class="modal-dialog" role="document" style="height: 100%;width: 100%;display: flex;align-items: center;">
        <div class="modal-content">
            <div class="modal-header" style="background-color:#f27272">
                <h5 class="modal-title" id="modalETitulo"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="modalEContenido">
            </div>
        </div>
    </div>
</div>

<div id="divForm" hidden>
<div class="modal-body">
    <form class="form-horizontal" id="frmAceptarPedido">
        <div class="form-group row">
            <label class="col-md-3 col-form-label">Tiempo de entrega</label>
            <div class="col-md-9">
                <input type="text" id="tiempoentrega" name="tiempoentrega" class="form-control" value="Typing.....">
            </div>
        </div>
        {{--
        <div class="form-group row">
            <label class="col-md-3 col-form-label">Costo de envío</label>
            <div class="col-md-9">
                <input type="text" id="costoenvio" name="costoenvio" class="form-control" value="Typing.....">
            </div>
        </div>
        --}}
    </form>
</div>
<div class="modal-footer">
    <button id="btnAceptarPedido" data-pedido_id="" type="button" class="btn btn-primary">Aceptar</button>
    <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
</div>
</div>

<div id="divDetalles" hidden>
    <div class="row card-body">
        <div class="col-md-12">
            <div class="pull-left m-t-5 m-b-5">
                <h6>Información del Cliente:</h6>
                <address id="cliente" class="mb-0">
                    John Doe, Mrs Emma Downson<br>Acme Inc<br>
                    Berlin, Germany <br>6781 45P
                </address>
            </div>
            <div class="pull-right m-t-5 m-b-5">
                <h6>Detalles de pago:</h6>
                <p class="mb-1"><span class="text-muted">Tipo de pago: </span> <span id="tipoPago"> Root<span></p>
            </div>
        </div><!-- end col -->
        <div class="col-md-12">
            <div class="pull-left m-t-5 m-b-5">
                <h6>Comentario:</h6>
                <address id="comentario" class="mb-0">
                    John Doe, Mrs Emma Downson<br>Acme Inc<br>
                    Berlin, Germany <br>6781 45P
                </address>
            </div>
            <div class="pull-right m-t-5 m-b-5">
                <h6>Dirección de entrega:</h6>
                <p class="mb-1" id="direccion"><span class="text-muted">Tipo de pago: </span> <span> Root<span></p>
            </div>
        </div><!-- end col -->
    </div>

    <div class="table-responsive">
        <table class="table border table-bordered text-nowrap">
            <thead>
                <tr>
                    <th class="border-0 text-uppercase  font-weight-bold" colspan="2">Pedido</th>
                    <th class="border-0 text-uppercase  font-weight-bold">Cant.</th>
                    <th class="border-0 text-uppercase  font-weight-bold">P. Unit.</th>
                    <th class="border-0 text-uppercase  font-weight-bold">Total</th>
                </tr>
            </thead>
            <tbody id="tablaProductos">
            </tbody>
        </table>
    </div>

    <div class="col-xl-4 col-12 offset-xl-8">
        <!-- <p class="text-right"><strong>Sub-total:</strong> $32,432</p>
        <p class="text-right">Discout: 10%</p> -->
        <hr>
        <h5 class="text-right">Total :<strong id="granTotal"> $234,79</strong></h5>
    </div>
</div><!-- modal-body -->


@csrf
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {

            $.each($("#btnEstatus"), function(){
                $("table").delegate("#btnEstatus", "click", function(){

                    if($(this).hasClass('btn-primary')){
                        $boton = $(this);
                        modalAceptar($boton);
                    }
                    if($(this).hasClass('btn-success')){
                        $boton = $(this);
                        modalEnviar($boton);
                    }
                })
            });

            $.each($("#btnDetalles"), function(){
                $("table").delegate("#btnDetalles", "click", function(event){
                    $("#divContenidoDetalles").attr("hidden", false);
                    $("#modalContenido").html($("#divContenidoDetalles"));
                    $url = $(this).data('url');
                    $pedido = $(this).data('pedido_id');
                    // console.log($pedido);
                    $.get($url, {pedido:$pedido})
                        .done(function(d){
                            $("#modalCuerpo").html($("#divDetalles").html());
                            // console.log(d);
                            $("#cliente").html(d.cliente.name + "<br>" + d.cliente.email);
                            $("#tipoPago").html(d.tipo_pago);
                            $("#modalTitulo").html(d.id);
                            $("#comentario").html(d.comentario);
                            $("#direccion").html(d.direccion);
                            $cant_detalles = Object.keys(d.detalles).length;
                            $producto = "";
                            $granTotal = 0;
                            for($i=0; $i < $cant_detalles; $i++){
                                $total = d.detalles[$i].cantidad * d.detalles[$i].precio;
                                $granTotal = $total + $granTotal;
                                if(d.detalles[$i].producto != null){
                                    $producto = $producto + "<tr><td><b>"+ d.detalles[$i].producto.name+"</b></td><td></td><td>"+ d.detalles[$i].cantidad +"</td><td> S/ "+ d.detalles[$i].precio +"</td><td> S/ "+ $total +"</td></tr>"
                                }
                                if(d.detalles[$i].adicional != null){
                                    $producto = $producto + "<tr><td></td><td>"+ d.detalles[$i].adicional.nombre+"</td><td>"+ d.detalles[$i].cantidad +"</td><td> S/ "+ d.detalles[$i].precio +"</td><td> S/ "+ $total +"</td></tr>"
                                }
                            }
                            $("#btnAceptar").attr("hidden", true);
                            $("#btnEnviar").attr("hidden", true);
                            //$("#btnRechazar").attr("hidden", true);

                            $("#btnAceptar").data("pedido_id", d.id);
                            $("#btnAceptar").click(function(){
                                modalAceptar($(this), d.id);
                            });

                            $("#btnEnviar").data("pedido_id", d.id);
                            $("#btnEnviar").click(function(){
                                console.log($(this).data("pedido_id"));
                                fnEnviar(d.id);
                            });
                            
                            //$("#btnRechazar").data("pedido_id", d.id);
                            //$("#btnRechazar").click(function(){});

                            if(d.estatus=="recibido"){
                                $("#btnAceptar").attr("hidden", false);
                                $("#btnEnviar").attr("hidden", true);
                                //$("#btnRechazar").attr("hidden", false);
                            }
                            if(d.estatus=="aceptado"){
                                $("#btnAceptar").attr("hidden", true);
                                $("#btnEnviar").attr("hidden", false);
                                //$("#btnRechazar").attr("hidden", false);
                            }


                            // $('body').on("click", "#btnModalAceptar", function(){

                            // $("#modalFooter").html($contentFooter);

                            $("#tablaProductos").html($producto);
                            $("#granTotal").html($granTotal.toFixed(2));
                            // console.log($producto);
                        });
                    });
                });
            });

            

function modalAceptar($boton, id){
    $url = $boton.data('url') + "/" + id;
    // console.log($url);
    $pedido = $boton.data('pedido_id');
    $.get($url, {pedido:$pedido})
        .done(function(d){

            // $("#cliente").html(d.cliente.name + "<br>" + d.cliente.email);
            // $("#tipoPago").html(d.tipo_pago);
            $("#modalETitulo").html(d.id);
            $("#btnAceptarPedido").data("pedido_id", d.id);
            console.log($("#btnAceptarPedido").data("pedido_id") + "---------");
            // $("#divForm").attr("hidden", false);
            $("#modalEContenido").html($("#divForm").html());

            $("#tiempoentrega").val(d.tiempoentrega);
            // $("#costoenvio").val(d.costoenvio);

            $("#btnAceptarPedido").click(function(){
                    aceptar(d.id);
            });


            // $contentFooter = "";
            // $contentFooter = $contentFooter + "<button id='btnAceptar' type='button' class='btn btn-success'>Aceptar</button>";
            // $contentFooter = $contentFooter + "<button id='btnCancelar' type='button' class='btn btn-danger'>Rechazar</button>";
            // $contentFooter = $contentFooter + "<button type='button' class='btn btn-info' data-dismiss='modal'>Close</button>";

            // $("#modalFooter").html($contentFooter);

            // $("#btnAceptar").click(function(){
            //     aceptar(d.id, $boton);
            // })
        });
}

function aceptar(pedido){
    console.log(pedido);
    $url = "{{route('tienda.pedidos.aceptar')}}/" + pedido;
    $data = {tiempoentrega:$("#tiempoentrega").val()};

    $.ajax({
        url: $url,
        dataType: "json",
        data:{data:$data},
        headers: {'X-CSRF-TOKEN':$("input[name=_token]").val()},
    })
        .done(function(d){
            if(d.error){
                console.log(d);
                notificacion(d.error, 'error');
            }

            if(d.exito){
                console.log(d);
                $("#" + pedido).html("Enviar");
                $("#" + pedido).removeClass("badge-warning");
                $("#" + pedido).addClass("badge-success");
                notificacion(d.exito, 'success');
                $(".modal").modal("hide");
            }
        });
}
function fnEnviar(pedido){
    console.log(pedido);
    $url = "{{route('tienda.pedidos.enviar')}}/" + pedido;
    $data = {tiempoentrega:$("#tiempoentrega").val(), costoenvio:$("#costoenvio").val()};

    $.ajax({
        url: $url,
        dataType: "json",
        data:{data:$data},
        headers: {'X-CSRF-TOKEN':$("input[name=_token]").val()},
    })
        .done(function(d){
            if(d.error){
                console.log(d);
                notificacion(d.error, 'error');
            }

            if(d.exito){
                console.log(d);
                $("#" + pedido).html("Enviado");
                $("#" + pedido).removeClass("badge-success");
                $("#" + pedido).addClass("badge-primary");
                notificacion(d.exito, 'success');
                $(".modal").modal("hide");
            }
        });
}
    </script>

    <script type="text/javascript">
        function idleTimer() {
            var t;
            //window.onload = resetTimer;
            window.onmousemove = resetTimer; // catches mouse movements
            window.onmousedown = resetTimer; // catches mouse movements
            window.onclick = resetTimer;     // catches mouse clicks
            window.onscroll = resetTimer;    // catches scrolling
            window.onkeypress = resetTimer;  //catches keyboard actions
        
            function logout() {
                window.location.href = '/action/logout';  //Adapt to actual logout script
            }
        
           function reload() {
                  window.location = self.location.href;  //Reloads the current page
           }
        
           function resetTimer() {
                clearTimeout(t);
                //t = setTimeout(logout, 1800000);  // time is in milliseconds (1000 is 1 second)
                t= setTimeout(reload, 300000);  // time is in milliseconds (1000 is 1 second)
            }
        }
        idleTimer();
        </script>
    
@endsection