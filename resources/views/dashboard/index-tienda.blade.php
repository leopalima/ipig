@extends('esquema.esquema')

@section('css')

<link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">


@endsection

@section('contenido')
					<div class="row">
                        <div class="col-lg-6 col-xl-6 col-md-12 col-sm-12 col-12">
                            <div class="card bg-dark text-white">
                                <img class="card-img" src="{{auth()->user()->foto}}" alt="{{auth()->user()->name}}">
                                <div class="card-img-overlay">
                                    <h5 class="display-4">{{auth()->user()->name}}</h5>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-xl-6 col-md-12 col-sm-12 col-12">
                            <div class="list-group">
                                <span class="list-group-item list-group-item-action active">
                                    Acceso rápido
                                </span>
                                <a href="{{route('perfil.edit', ['id' =>  Auth::user()->id])}}" class="list-group-item list-group-item-action">&nbsp; &nbsp; <i class="side-menu__icon fa fa-home"></i> Configurar comercio</a>
                                <a href="{{route('tienda.categorias.index')}}" class="list-group-item list-group-item-action">&nbsp; &nbsp; <i class="side-menu__icon fa fa-list"></i> Categorías</a>
                                <a href="{{route('tienda.productos.index')}}" class="list-group-item list-group-item-action">&nbsp; &nbsp; <i class="side-menu__icon fa fa-cubes"></i> Productos</a>
                                <a href="{{route('tienda.pedidos.activos')}}" class="list-group-item list-group-item-action">&nbsp; &nbsp; <i class="side-menu__icon fa fa-send-o"></i> Pedidos</a>
                            </div>
                        </div>

                    </div>
@endsection


@section('javascript')

<!-- <script src="{{url('assets/plugins/jquery-sparkline/dist/jquery.sparkline.js')}}"></script> -->
<script src="{{url('assets/plugins/othercharts/jquery.sparkline.min.js')}}"></script>
<script src="{{url('assets/plugins/morris/raphael.min.js')}}"></script>
<script src="{{url('assets/plugins/morris/morris.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<!-- <input type="hidden" value="{{ route('dashboard.graficasDatos')}}" id="url"> -->

@endsection