@extends('esquema.esquema')

@section('css')

<link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">

<style>
.botonesDias{

    padding: 15px;

}

.btn-primary:hover {
    background-color: rgb(242,134,134) !important;
    border-color: rgb(242,134,134) !important;
}

.btn-primary:active {
    background-color: #f27272 !important;
    border-color: #f27272 !important;
}
.btn-primary:focus {
    background-color: #f27272 !important;
    border-color: #f27272 !important;
}
.btn-primary:focus:active {
    background-color: #f27272 !important;
    border-color: #f27272 !important;
}

/* dias colores */
	#Lunes{
		background-color: #f27272 !important;
	    border-color: #f27272!important;
	}

	#Martes{
		background-color: rgb(90,98,104) !important;
	    border-color:rgb(90,98,104)!important;
	}

	#Miercoles{
		background-color: rgb(25,217,54) !important;
	    border-color:rgb(25,217,54)!important;
	}

	#Jueves{
		background-color: rgb(200,34,51) !important;
	    border-color: rgb(200,34,51)!important;
	}

	#Viernes{
		background-color: rgb(248,200,17) !important;
	    border-color:rgb(248,200,17)!important;
	}

	#Sabado{
		background-color: rgb(24,190,255) !important;
	    border-color:rgb(24,190,255)!important;
	}

	#Domingo{
		background-color: rgb(34,39,43) !important;
	    border-color:rgb(34,39,43)!important;
	}
/* dias colores */

</style>

@endsection

@section('contenido')
					<div class="row">
					@csrf
						    <div class="col-lg-6 col-xl-3 col-md-6 col-sm-6 col-12">
								<div class="card">
									<div class="card-body text-center">
										<h5>Producto mas vendido</h5>
										
										<div class="text-center">
											<div class="mb-3 mt-1">
												<span class="sparkline_line" ></span>
											</div>
											
											<h6 class="mb-2 text-dark">{{$productoName}}</h6>
											<!-- <span class="text-green"><i class="fa fa-arrow-up text-success"> </i>23% increase</span></i><small> last week</small> -->
										</div>
									</div>
								</div>
							</div>
							
							<div class="col-lg-6 col-xl-3 col-md-6 col-sm-6 col-12">
								<div class="card">
									<div class="card-body text-center">
										<h5>Menor tiempo en aceptar pedidos</h5>
										
										<div class="text-center">
											<div class="mb-3 mt-1">
												<span class="sparkline_bar3" ></span>
											</div>
											<h6 class="mb-2 text-dark">{{$mejorTiempoA}} M:s</h6>
											<!-- <span class="text-green"><i class="fa fa-arrow-down text-danger"> </i>10% increase</span></i><small> last week</small> -->
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-xl-3 col-md-6 col-sm-6 col-12">
								<div class="card">
									<div class="card-body text-center">
										<h5>Menor tiempo en enviar pedidos</h5>
										
										<div class="text-center">
											<div class="mb-3 mt-1">
												<span class="sparkline_bar" ></span>
											</div>
											<h6 class="mb-2 text-dark">{{$mejorTiempoB}} M:s</h6>
											<!-- <span class="text-green"><i class="fa fa-arrow-up text-success"> </i>23% increase</span></i><small> last week</small> -->
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-xl-3 col-md-6 col-sm-6 col-12">
								<div class="card">
									<div class="card-body text-center">
										<h5>Menor tiempo en entregar pedidos</h5>
										
										<div class="text-center">
											<div class="mb-3 mt-1">
												<span class="sparkline_area" ></span>
											</div>
											<h6 class="mb-2 text-dark">{{$mejorTiempoC}} M:s</h6>
											<!-- <span class=""><i class="fa fa-arrow-down text-danger"> </i>3% decrease</span></i><small> last week</small> -->
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-12 col-sm-12">
								<div class="card botonesDias">

									<div class="row" >
										<div class="col-md">
											<button type="button" id="Lunes"   class="btn btn-primary dias"  onclick = "mostrarDias(this.value)" value="Lunes">Lunes</button>
										</div>
										<div class="col-md">
											<button type="button" id="Martes"  class="btn btn-secondary dias" onclick = "mostrarDias(this.value)" value="Martes">Martes</button>
										</div>
										<div class="col-md">
											<button type="button" id="Miercoles" class="btn btn-success dias" onclick = "mostrarDias(this.value)" value="Miercoles">Miercoles</button>
										</div>
										<div class="col-md">
											<button type="button" id="Jueves"  class="btn btn-danger dias" onclick = "mostrarDias(this.value)" value="Jueves">Jueves</button>
										</div>
										<div class="col-md">
											<button type="button" id="Viernes" class="btn btn-warning dias" onclick = "mostrarDias(this.value)" value="Viernes">Viernes</button>
										</div>
										<div class="col-md">
											<button type="button" id="Sabado" class="btn btn-info dias" onclick = "mostrarDias(this.value)" value="Sabado">Sábado</button>
										</div>
										<div class="col-md">
											<button type="button" id="Domingo" class="btn btn-dark dias" onclick = "mostrarDias(this.value)" value="Domingo">Domingo</button>
										</div>

									</div>
								</div>
							</div>
									
						</div>

						<!-- graficas dias -->
							<div class="row LunesCol" >
								<div class="col-6 col-sm-6">
									<div class="card">
										<div class="card-header">
											<h4>Estadística de los pedidos por hora</h4>
										</div>
										<div class="card-body text-center">
										<canvas id="canvas2" height="480" width="1200"></canvas>
										</div>
									</div>
								</div>
							
							<div class="col-6 col-sm-6">
								<div class="card">
									<div class="card-header">
										<h4>Ventas del mes</h4>
									
										<div  class="row" >
											<div class="col-md-6">
                  								<label>Año</label>
                  								<select class="form-control dynamicAnio dynamicMes" id="anio_sel" >
													<?php $year = date("Y");  for($i=2016;$i<$year;$i++) { echo "<option value='".$i."'>".$i."</option>"; } echo "<option value='".$year."' selected>".$year."</option>" ;  ?>
                  								</select>
											</div>
											<div class="col-md-6">
											                  <label>Mes</label>
											                  <select class="form-control dynamicMes" id="mes_sel" >
											                    <option value="1">ENERO</option>
											                    <option value="2">FEBRERO</option>
											                    <option value="3">MARZO</option>
											                    <option value="4">ABRIL</option>
											                    <option value="5">MAYO</option>
											                    <option value="6">JUNIO</option>
											                    <option value="7">JULIO</option>
											                    <option value="8">AGOSTO</option>
											                    <option value="9">SEPTIEMBRE</option>
											                    <option value="10">OCTUBRE</option>
											                    <option value="11">NOVIEMBRE</option>
											                    <option value="12">DICIEMBRE</option>
											                  </select>
											</div>
										</div>
									
									</div>
									<div class="card-body text-center">
									<canvas id="canvas" height="480" width="1200"></canvas>
									</div>
								</div>
							</div>
						</div>


								
						


<input type="hidden" value="{{ csrf_token()}}" name="_token">



@endsection


@section('javascript')

<!-- <script src="{{url('assets/plugins/jquery-sparkline/dist/jquery.sparkline.js')}}"></script> -->
<script src="{{url('assets/plugins/othercharts/jquery.sparkline.min.js')}}"></script>
<script src="{{url('assets/plugins/morris/raphael.min.js')}}"></script>
<script src="{{url('assets/plugins/morris/morris.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
<!-- <input type="hidden" value="{{ route('dashboard.graficasDatos')}}" id="url"> -->


<script>
	var year = <?php echo date("m"); ?>;
	var repetido = <?php echo $repetido; ?>;
	var fechasGraficasA = <?php echo $fechasGraficasA; ?>;
	var fechasGraficasB = <?php echo $fechasGraficasB; ?>;
	var fechasGraficasC = <?php echo $fechasGraficasC; ?>;
	var diasEfectivo = <?php echo $diasEfectivo; ?>;
	var diasPos = <?php echo $diasPos; ?>;
	var nombreDias = <?php echo $nombreDias; ?>;
	nombreDias =Object.values(nombreDias)
	diasEfectivo =Object.values(diasEfectivo)
	diasPos =Object.values(diasPos)
	var horaLunes = <?php echo $horaLunes; ?>;
	var horaMartes = <?php echo $horaMartes; ?>;
	var horaMiercoles = <?php echo $horaMiercoles; ?>;
	var horaJueves = <?php echo $horaJueves; ?>;
	var horaViernes = <?php echo $horaViernes; ?>;
	var horaSabado = <?php echo $horaSabado; ?>;
	var horaDomingo = <?php echo $horaDomingo; ?>;


	
	
	//graficas pequeñas
		$(function(e){
  		'4 primeros'

				$(".sparkline_line").sparkline(repetido, {
					type: 'line',
					lineColor: '#5458b3',
					fillColor: '#a9ace6',
					width: 80,
					height: 50,
					spotColor: '#f44336',
					minSpotColor: '#f44336'
				});

				$(".sparkline_bar3").sparkline(fechasGraficasA, {
					type: 'bar',
					height: 50,
					width:120,
					colorMap: {
						'9': '#a1a1a1'
					},
					barColor: '#f47b25'
				});

				$(".sparkline_bar").sparkline(fechasGraficasB, {
					type: 'bar',
					height: 50,
					colorMap: {
						'9': '#a1a1a1'
					},
					barColor: '#5458b3'
				});
					
				$(".sparkline_area").sparkline(fechasGraficasC, {
					type: 'line',
					width: 50,
					height: 50,
					lineColor: '#ffa22b',
					fillColor: '#ffa22b',
					spotColor: '#f44336',
					minSpotColor: '#f44336',
					maxSpotColor: '#f44336',
					highlightSpotColor: '#f44336',
					highlightLineColor: '#f44336',
					spotRadius: 2.5,
					width: 85
				});

		});
	//graficas pequeñas
	
	//grafica por año
		var barChartData = {
    	    labels: [],
    	    datasets: [{
    	        label: 'Efectivo',
    	        backgroundColor: "rgba(242, 121, 121, 0.69)",
    	        data: []
    	    },{
    	        label: 'POS',
    	        backgroundColor: "rgba(0, 0, 0, 0.69)",
    	        data: []
    	    }]
    	};


    	window.onload = function() {


			document.getElementById("mes_sel").value = year;
			for(i=0; i < diasEfectivo.length; i++){
				barChartData.datasets[0].data.push(diasEfectivo[i]);
				barChartData.datasets[1].data.push(diasPos[i]);
				barChartData.labels.push(nombreDias[i]);

			}

    	    var ctx = document.getElementById("canvas").getContext("2d");
    	    window.myBar = new Chart(ctx, {
    	        type: 'line',
    	        data: barChartData,
    	        options: {
    	            elements: {
    	                rectangle: {
    	                    borderWidth: 2,
    	                    borderColor: 'rgb(0, 255, 0)',
    	                    borderSkipped: 'bottom'
    	                }
    	            },
    	            responsive: true,
    	            title: {
    	                display: true,
    	                text: 'Ventas Mensuales'
    	            }
    	        }
    	    });
			
			//iniciar graficas dia
				var hoy = new Date();
				var days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
				var d = new Date(hoy);
				var dayName = days[d.getDay()];
				mostrarDias(dayName);
			//iniciar graficas dia


    	};
	//grafica por año


	//grafica por dia de semana


			

		//graficas dias
			var barChartData2 = {
    		    labels: ["12:00 AM","1:00 AM","2:00 AM","3:00 AM","4:00 AM","5:00 AM","6:00 AM","7:00 AM","8:00 AM","9:00 AM","10:00 AM","11:00 AM"
						,"12:00 AM","1:00 PM","2:00 PM","3:00 PM","4:00 PM","5:00 PM","6:00 PM","7:00 PM","8:00 PM","9:00 PM","10:00 PM","11:00 PM"],
    		    datasets: [],
					options: { scaleLabel: function (label) { return Math.round(label.value); } } 
    		};

    		var ctx2 = document.getElementById("canvas2");
    		var canvas2 = new Chart(ctx2, {
    		    type: 'bar',
    		    data: barChartData2,
    		    options: {
					
    		        elements: {
    		            rectangle: {
    		                borderWidth: 2,
    		                borderColor: 'rgb(0, 255, 0)',
    		                borderSkipped: 'bottom'
    		            }
    		        },
					
    		        responsive: true,
    		        title: {
    		            display: true,
    		            text: 'Ventas '
    		        },

					scales: {
					    yAxes: [{
					        ticks: {
					            beginAtZero: true,
					            userCallback: function(label, index, labels) {
					                if (Math.floor(label) === label) {
					                    return label;
					                }
					            },
					        }
					    }],
					},

					//no
    		    }
    		});
		//graficas dias



	//grafica por dia de semana

	//modificar datos de grafica 
		var borrar=0;

		function mostrarDias(mostrar){
			var prefijo="hora";
		
			if(canvas2.config.data.datasets.findIndex( item => item.label == mostrar) >= 0){
			
				borrar=canvas2.config.data.datasets.findIndex( item => item.label == mostrar);
				canvas2.config.data.datasets.splice(borrar,1);	
				canvas2.update();
			}else{
			
				var barChartData = {  
						   label: mostrar,
						   backgroundColor: $("#"+mostrar).css("background-color"),
							borderColor: 'rgba(255, 99, 132,0)',
						   data: eval(prefijo+mostrar) 
					   };
				   
					canvas2.config.data.datasets.push(barChartData);	
					canvas2.update();
				   
			}
		
		};

	//modificar datos de grafica 


</script>

<script>

	
	$(document).ready(function(){
				   
		
				$('.dynamicMes').change(function(){

				 	
				 	
				 	 	var select = $(this).attr("id");
						var mes= $("#mes_sel").val();
						var anio= $(".dynamicAnio").val();
						
						var _token = $('input[name="_token"]').val();
						

				 	 	$.ajax({
				 	 	 	url:"{{route('dashboard.graficasDatos')}}",
				 	 	 	method:"POST",
				 	 	 	data:{select:select, mes:mes,anio:anio, _token:_token},

				 	 	 	success:function(result)
				 	 	 	{	
								var nombresDias=result[0];
								var pagosEfectivo=result[1];
								var pagosPos=result[2];
								nombresDias =Object.values(nombresDias)
								pagosEfectivo =Object.values(pagosEfectivo)
								pagosPos =Object.values(pagosPos)
								barChartData.datasets[0].data.length=[];
								barChartData.datasets[1].data.length=[];
								barChartData.labels.length=[];
								for(i=0; i < nombresDias.length; i++){

								   	barChartData.datasets[0].data.push(pagosEfectivo[i]);
									barChartData.datasets[1].data.push(pagosPos[i]);
									barChartData.labels.push(nombresDias[i]);
								}
								// $("#recarga").load(" #recarga");
								window.myBar.update();


								
				 	 	 	}
						   
				 	 	})
					 
					 
				});
					  
					
				 
  
   
	});


</script>



@endsection