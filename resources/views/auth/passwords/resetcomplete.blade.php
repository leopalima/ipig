<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>iPig, el delivery a favor de los restaurantes</title>
		
		<!--Favicon -->
		<link rel="icon" href="{{url('favicon.png')}}" type="image/x-icon"/>

		<!--Bootstrap.min css-->
		<link rel="stylesheet" href="{{url('assets/plugins/bootstrap/css/bootstrap.min.css')}}">

		<!--Icons css-->
		<link rel="stylesheet" href="{{url('assets/css/icons.css')}}">

		<!--Style css-->
		<link rel="stylesheet" href="{{url('assets/css/style.css')}}">

		<!--mCustomScrollbar css-->
		<link rel="stylesheet" href="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css')}}">

		<!--Sidemenu css-->
		<link rel="stylesheet" href="{{url('assets/plugins/toggle-menu/sidemenu.css')}}">

	</head>
	<style>
		.bg-primary2 {
			background-image: url('/ipig_background.jpg');
			background-size: cover;
		}
		.container2 {
			width: 100%;
			padding-right: 15px;
			padding-left: 15px;
			margin-right: auto;
			margin-left: auto
		}
		
		@media (min-width: 576px) {
			.container2 {
				max-width:540px
			}
		}
		
		@media (min-width: 768px) {
			.container2 {
				max-width:768px
			}
		}
		
		@media (min-width: 992px) {
			.container2 {
				max-width:720px;
			}
		}
		
		@media (min-width: 1200px) {
			.container2 {
				max-width: 720px;
			}
		}
		.container3 {
			overflow: hidden;
			background-color: #ff9b9b;
			position: unset; /* Set the navbar to fixed position */
			top: 0; /* Position the navbar at the top of the page */
			width: 100%; /* Full width */
			display: table;
		}
		.container3 img {
			width: 10%;
		}

		@media (max-width: 500px) {
			.container3 img {
				width: 25%;
			}
		}
	</style>
	<body class="bg-primary2">
		<div id="app">
			<section class="section section-2">
                <div class="container3">
					<a href="https://apps.apple.com/pe/app/ipig/id1467554504"><img src="/app_store.png" alt="iPig en la App Store"></a>
					<a href="https://play.google.com/store/apps/details?id=com.area51.ipig"><img src="/play_store.png" alt="iPig en la Play Store"></a>
				</div>
                <div class="container2" style="float:right;">
					<div class="row">
						<div class="single-page single-pageimage construction-bg cover-image">
							<div class="row">
								<div class="col-lg-12">
									<div class="wrapper wrapper2">
										<form class="card-body" tabindex="500">
											@csrf
											<h3>El cambio de contraseña se ha completado</h3>
											<div class="submit">
												<button type="submit" class="btn btn-primary btn-block">
                                                    {{ __('Inicia sesión en la aplicacion') }}
                                                </button>
											</div>
											{{---
											<p class="mb-2"><a href="forgot.html" >Olvidé mi contraseña</a></p>
											<p class="text-dark mb-0">No estas registrado?<a href="{{route('register')}}" class="text-primary ml-1">Registrate</a></p>
											---}}
										</form>
										<div class="card-body border-top">
										<h3>¿Eres un comercio nuevo que quiere disfrutar de iPig?</h3>
										<h3> <a href="https://forms.gle/tr6kkjZEyhoBdDfL7" target="_blank">Solicitar inscripción</a></h3>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</section>
		</div>

		<!--Jquery.min js-->
		<script src="{{url('assets/js/jquery.min.js')}}"></script>

		<!--popper js-->
		<script src="{{url('assets/js/popper.js')}}"></script>

		<!--Tooltip js-->
		<script src="{{url('assets/js/tooltip.js')}}"></script>

		<!--Bootstrap.min js-->
		<script src="{{url('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

		<!--Jquery.nicescroll.min js-->
		<script src="{{url('assets/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>

		<!--Scroll-up-bar.min js-->
		<script src="{{url('assets/plugins/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>
		
		<script src="{{url('assets/js/moment.min.js')}}"></script>

		<!--mCustomScrollbar js-->
		<script src="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

		<!--Sidemenu js-->
		<script src="{{url('assets/plugins/toggle-menu/sidemenu.js')}}"></script>

		<!--Scripts js-->
		<script src="{{url('assets/js/scripts.js')}}"></script>

	</body>
</html>