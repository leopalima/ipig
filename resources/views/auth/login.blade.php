<!DOCTYPE html>
<html lang="en">
	<head>

		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Activa tu negocio</title>
		
		<!--Favicon -->
		<link rel="icon" href="favicon.png" type="image/x-icon"/>

		<!--Bootstrap.min css-->
		<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">

		<!--Icons css-->
		<link rel="stylesheet" href="assets/css/icons.css">

		<!--Style css-->
		<link rel="stylesheet" href="assets/css/style.css">

		<!--mCustomScrollbar css-->
		<link rel="stylesheet" href="assets/plugins/scroll-bar/jquery.mCustomScrollbar.css">

		<!--Sidemenu css-->
		<link rel="stylesheet" href="assets/plugins/toggle-menu/sidemenu.css">

	</head>
	<style>
		.bg-primary2 {
			background-image: url('/ipig_background.jpg');
			background-size: cover;
		}
		.container2 {
			width: 100%;
			padding-right: 15px;
			padding-left: 15px;
			margin-right: auto;
			margin-left: auto
		}
		
		@media (min-width: 576px) {
			.container2 {
				max-width:540px
			}
		}
		
		@media (min-width: 768px) {
			.container2 {
				max-width:768px
			}
		}
		
		@media (min-width: 992px) {
			.container2 {
				max-width:720px;
			}
		}
		
		@media (min-width: 1200px) {
			.container2 {
				max-width: 720px;
			}
		}
		.container3 {
			overflow: hidden;
			background-color: #ff9b9b;
			position: unset; /* Set the navbar to fixed position */
			top: 0; /* Position the navbar at the top of the page */
			width: 100%; /* Full width */
			display: table;
		}
		.container3 img {
			width: 10%;
		}

		@media (max-width: 500px) {
			.container3 img {
				width: 25%;
			}
		}
	</style>
	<body class="bg-primary2">
		<div id="app">
			<section class="section section-2">
                <div class="container2" style="float:right;">
					<div class="row">
						<div class="single-page single-pageimage construction-bg cover-image">
							<div class="row">
								<div class="col-lg-12">
									<div class="wrapper wrapper2">
										<form id="login" class="card-body" tabindex="500" method="POST" action="{{ route('login') }}">
											@csrf
											<h3>Plataforma de ventas</h3>
											<div class="mail">
												<input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
												<label>Email</label>
											</div>
											<div class="passwd">
												<input id="password" type="password" name="password" name="password" required autocomplete="current-password">
												<label>Contraseña</label>
											</div>
											@error('email')
												<div>
													<strong class="text-danger">Email o contraseña incorrecta.</strong>
												</div>
											@enderror
											@error('password')
												<div>
													<strong>{{ $message }}</strong>
												</div>
											@enderror
											<div class="submit">
												<button type="submit" class="btn btn-primary btn-block">
                                                    {{ __('Iniciar sesión') }}
                                                </button>
											</div>
											{{---
											<p class="mb-2"><a href="forgot.html" >Olvidé mi contraseña</a></p>
											<p class="text-dark mb-0">No estas registrado?<a href="{{route('register')}}" class="text-primary ml-1">Registrate</a></p>
											---}}
										</form>
										<div class="card-body">
											<a href="https://activatunegocio.com">
												<img src="{{asset('assets/img/brand/activatunegocio.jpeg')}}" width="35%" alt="Activa tu negocio">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</section>
		</div>

		<!--Jquery.min js-->
		<script src="assets/js/jquery.min.js"></script>

		<!--popper js-->
		<script src="assets/js/popper.js"></script>

		<!--Tooltip js-->
		<script src="assets/js/tooltip.js"></script>

		<!--Bootstrap.min js-->
		<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>

		<!--Jquery.nicescroll.min js-->
		<script src="assets/plugins/nicescroll/jquery.nicescroll.min.js"></script>

		<!--Scroll-up-bar.min js-->
		<script src="assets/plugins/scroll-up-bar/dist/scroll-up-bar.min.js"></script>
		
		<script src="assets/js/moment.min.js"></script>

		<!--mCustomScrollbar js-->
		<script src="assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!--Sidemenu js-->
		<script src="assets/plugins/toggle-menu/sidemenu.js"></script>

		<!--Scripts js-->
		<script src="assets/js/scripts.js"></script>

		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5d518c83eb1a6b0be607299b/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script-->

	</body>
</html>