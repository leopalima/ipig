<!DOCTYPE html>
<html lang="es">
	<head>

		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{{Auth::user()->name}} - Activa tu negocio</title>

		<!--favicon -->
		<link rel="icon" href="{{url('favicon.png')}}" type="image/x-icon"/>

		<!--Bootstrap.min css-->
		<link rel="stylesheet" href="{{url('assets/plugins/bootstrap/css/bootstrap.min.css')}}">

		<!--Icons css-->
		<link rel="stylesheet" href="{{url('assets/css/icons.css')}}">

		<!--Style css-->
		<link rel="stylesheet" href="{{url('assets/css/style.css')}}">

		<!--mCustomScrollbar css-->
		<link rel="stylesheet" href="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.css')}}">

		<!--Sidemenu css-->
		<link rel="stylesheet" href="{{url('assets/plugins/toggle-menu/sidemenu.css')}}">
		<link rel="manifest" href="{{request()->root()}}/manifest.json">
		<link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">
		<link rel="stylesheet" href="{{url('https://cdn.jsdelivr.net/npm/sweetalert2@8/dist/sweetalert2.min.css')}}">
		
@csrf
		
		@yield('libreriascss')
        
        @yield('css')

		<!-- The core Firebase JS SDK is always required and must be listed first -->
		<script src="https://www.gstatic.com/firebasejs/6.2.0/firebase-app.js"></script>
		<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase-messaging.js"></script>
	</head>

	<body class="app ">

		<div id="spinner"></div>

		<div id="app">
			<div class="main-wrapper" >
                @include('esquema.cintillo')
                @include('esquema.menu_izquierdo')
                @include('esquema.contenido')
                @include('esquema.pie_pagina')
			</div>
		</div>

		<!--Jquery.min js-->
		<script src="{{url('assets/js/jquery.min.js')}}"></script>

		<!--popper js-->
		<script src="{{url('assets/js/popper.js')}}"></script>

		<!--Tooltip js-->
		<script src="{{url('assets/js/tooltip.js')}}"></script>

		<!--Bootstrap.min js-->
		<script src="{{url('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

		<!--Jquery.nicescroll.min js-->
		<script src="{{url('assets/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>

		<!--Scroll-up-bar.min js-->
		<script src="{{url('assets/plugins/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>

		<!--mCustomScrollbar js-->
		<script src="{{url('assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

		<!--Sidemenu js-->
		<script src="{{url('assets/plugins/toggle-menu/sidemenu.js')}}"></script>

		<!--Toastr js-->
		<script src="{{url('assets/plugins/toastr/build/toastr.min.js')}}"></script>
		<script src="{{url('assets/plugins/toaster/garessi-notif.js')}}"></script>
		
		@yield('libreriasjavascript')

		<script type="text/javascript">
			function notificacion($mensaje, $tipo){
				$proyecto = '{{config('app.name')}}';
	
				switch($tipo){
					case 'error':
						toastr.error($mensaje, $proyecto);
						break;
					case 'warning':
						toastr.warning($mensaje, $proyecto);
				
						break;
					case 'success':
						toastr.success(
							$mensaje,
							$proyecto,
							{
								onclick: function() {console.log('you clicked on the info toaster n.1')},
								timeOut: 5000
							}
							
							);
						break;
					case 'danger':
						toastr.danger($mensaje, $proyecto);
						break;
					case 'info':
						toastr.info(
							$mensaje,
							$proyecto,
							{
								onclick: function() { enlace() },
								timeOut: 5000,
								onHidden: function() { enlace() }
							}
							
							);
						break;
					default:
					toastr.info('Falta configuracion', $proyecto);
				}
			}
			function enlace(){
				Swal.fire(
            	  'Good job!',
            	  'You clicked the button!',
            	  'success'
				);
			var musica = new Audio("{{url('assets/plugins/musica/cambo01start.mp3')}}");
			musica.play();
				
				// window.location.href = "{{route('tienda.pedidos.activos')}}/activos";
			}
		</script>

		<!-- TODO: Add SDKs for Firebase products that you want to use
			https://firebase.google.com/docs/web/setup#config-web-app -->

		<script>
		// Your web app's Firebase configuration
		var firebaseConfig = {
			apiKey: "AIzaSyDVBD7JY1oysoXFlbvgzWQ5Je38v7c1rPw",
			authDomain: "ipig-1559920943412.firebaseapp.com",
			databaseURL: "https://ipig-1559920943412.firebaseio.com",
			projectId: "ipig-1559920943412",
			storageBucket: "ipig-1559920943412.appspot.com",
			messagingSenderId: "264919211030",
			appId: "1:264919211030:web:b4e9ab5c3521e18e"
		};
		// Initialize Firebase
		firebase.initializeApp(firebaseConfig);
		// Retrieve Firebase Messaging object.
		const messaging = firebase.messaging();
		messaging
			.requestPermission()
			.then(function() {
				// console.log('Notification permission granted.');
				messaging.getToken().then(function(currentToken) {
				if (currentToken) {
					// console.log(currentToken);
					var url = "{{route('recibir.token')}}";
					$.ajax({
						url: url,
						method: "POST",
						data: {data:currentToken},
						headers: {'X-CSRF-TOKEN':$("input[name=_token]").val()},
					})
					.done(function(r){
						// console.log(r);
					});
					// sendTokenToServer(currentToken);
					// updateUIForPushEnabled(currentToken);
				} else {
					// Show permission request.
					console.log('No Instance ID token available. Request permission to generate one.');
					// Show permission UI.
					updateUIForPushPermissionRequired();
					setTokenSentToServer(false);
				}
				}).catch(function(err) {
					console.log('An error occurred while retrieving token. ', err);
					showToken('Error retrieving Instance ID token. ', err);
					setTokenSentToServer(false);
				});
		}).catch(function(err) {
			console.log('Unable to get permission to notify.', err);
		});
		var URLactual = window.location.origin;
		URLactualActivo=URLactual+"/pedidos/activos/activos?activos";
		URLactualHistorico=URLactual+"/pedidos/activos/historico";

		messaging.onMessage(function(payload){
			// $payload = JSON.stringify(payload)
			$mensaje = payload['notification']['body'];
			// console.log(payload['data']['data'].latitud);
			// $x = JSON.parse(payload.data.estatus);
			//cancelado,recibido
			// console.log($x.id);
			if(payload.data.estatus == "recibido"){
				var musica = new Audio("{{url('assets/plugins/musica/cambo01start.mp3')}}");
				musica.play();
					Swal.fire({
					  	title: 'Pedido',
					  	text: "Tienes un pedido!",
					  	type: 'warning',
					  	confirmButtonColor: '#3085d6',
					  	cancelButtonColor: '#d33',
						confirmButtonText: 'Ver pedido',
						customClass: {
  						  popup: 'animated tada'
  						}  
					}).then((result) => {
					  	if (result.value) {
							musica.pause()
							window.location=URLactualActivo;
					  	}
					})
			}
			
			if(payload.data.estatus == "cancelado"){
				Swal.fire({
				  type: 'error',
				  title: 'Oops...',
				  text: 'Un pedido fue cancelado',
				}).then((result) => {
					window.location=URLactualHistorico;
					  	
					})

			}
			
				


		});

		</script>
        @yield('javascript')

		<!--Scripts js-->
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
		<script src="{{url('assets/js/scripts.js')}}"></script>
		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5d518c83eb1a6b0be607299b/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
		<!--End of Tawk.to Script-->
	</body>
</html>