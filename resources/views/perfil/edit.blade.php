@extends('esquema.esquema')

@section('libreriascss')
    {{-- Formularios --}}
        <!--Morris css-->
        <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">

    {{-- Notificaciones emergentes --}}
        <!--Toastr css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">
    {{-- Daterangepicker Tiempo --}}

    <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">

        <!--Bootstrap-datepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}">

        <!--iCheck css-->
        <link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}">

        <!--Bootstrap-colorpicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}">

        <!--Bootstrap-timepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">

        <!--Select2 css-->
        <link rel="stylesheet" href="{{url('assets/plugins/select2/select2.css')}}">

    {{-- Wysiwing Editor --}}
        <!--Bootstrap-wysihtml5 css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    <style>
    #iconoE{
        font-size: 300%;
        color: orange;
    }
    .anchoModal
    {
        width: 100%;
    }
    .anchoModal h4
    {
        color:black;

    }
    #iconoA{
        font-size: 300%;
        color: #c70b1d;
    }
    
    </style>

@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Datos de perfil</h4>
                                    </div>
                                    <div class="card-body">
                                        <form enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{action('PerfilController@update', ['id' => $usuario->id])}}">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Nombre</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="name" name="name" class="form-control" value="{{old('name') ? old('name') : $usuario->name}}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="email">Email</label>
                                                <div class="col-md-9">
                                                    <input type="email" id="email" name="email" class="form-control" value='{{$usuario->email}}' placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="phone">Teléfono</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="phone" name="phone" class="form-control" value='{{$usuario->phone}}' placeholder="Teléfono">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="descripcion">Descripción</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="descripcion" name="descripcion" class="form-control" value='{{$usuario->descripcion}}' placeholder="Descripción">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Contraseña</label>
                                                <div class="col-md-9">
                                                    <input type="password" id="password" name="password"class="form-control" placeholder="Si desea cambiarla escriba la nueva contraseña">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Foto</label>
                                                <div class="col-md-9">
                                                    <input type="file" accept="image/*" id="foto" name="foto" class="form-control">
                                                    <div id="errorFoto" name="errorFoto" class="alert alert-danger mb-lg-0" hidden>
                                                    </div>
                                                </div>
                                                <label class="col-md-3 col-form-label"></label>
                                                @if($usuario->foto)
                                                <div class="col-md-9">
                                                    <img id="imgFoto" src="{{$usuario->foto}}" alt="Smiley face" width="300">
                                                </div>
                                                @endif
                                            </div>
                                            <div class="form-group mb-0 mt-2 row justify-content-end">
                                                <div class="col-md-9">
                                                    <button type="submit" name="btnGuardar" id="btnGuardar" class="btn btn-info">Guardar</button>
                                                    <a href="{{url('/')}}" class="btn btn-warning">Volver</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        @if($usuario->tipo->name == "tienda")
                        <div class="row">
                            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Configuración</h4>
                                    </div>
                                        <div class="card-body">
                                            <form class="form-horizontal" id="frmConfig" method="POST" action="{{action('PerfilController@update', ['id' => $usuario->id])}}">
                                                @csrf
                                                @method('PUT')
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Tiempo de preparacion</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="tiempoEntrega" name="tiempoEntrega" class="form-control" value="{{$usuario->tiempoEntrega}}"  placeholder="Tiempo">
                                                        <input type="hidden" id="configName" name="configName" class="form-control" value="configName">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label" for="email">Costo de envío</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="costoEnvio" name="costoEnvio" class="form-control" value='{{$usuario->costoEnvio}}' placeholder="costo S/">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label" for="email">Whatsapp</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="whatsapp" name="whatsapp" class="form-control" value='{{$usuario->whatsapp}}' placeholder="Whatsapp">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label" for="email">Descuento</label>
                                                    <div class="col-md-9">
                                                        <select class="form-control" name="descuento" id="descuento">
                                                        @for($i=0; $i < 16; $i++)
                                                            <option value="{{$i}}" @if($i == $usuario->descuento) {{"selected"}} @endif>{{$i}}%</option>
                                                        @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label" for="email">URL términos y condiciones</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="terminos" name="terminos" class="form-control" value='{{($usuario->terminos == "https://" ? "" : $usuario->terminos)}}' placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Recojo en tienda</label>
                                                    <div class="col-md-9">
                                                        <label class="custom-switch">
                                                            <input type="checkbox" value="1" id="recojoentienda" name="recojoentienda" class="custom-switch-input" {{$usuario->recojoentienda == 1 ? 'checked': ''}}>
                                                            <span class="custom-switch-indicator"></span>
                                                        </label>
                                                    </div>
                                                </div>
{{--
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Motos propias</label>
                                                    <div class="col-md-9">
                                                        <label class="custom-switch">
                                                            <input type="checkbox" value="1" id="motospropias" name="motospropias" class="custom-switch-input" {{$usuario->motospropias == 1 ? 'checked': ''}}>
                                                            <span class="custom-switch-indicator"></span>
                                                        </label>
                                                    </div>
                                                </div>
--}}
                                                <div class="form-group row">
                                                    <label class="col-md-3 col-form-label">Pagos aceptados</label>
                                                    <div class="col-md-9">
                                                        <label class="custom-switch"> Efectivo
                                                            <input type="checkbox" value="1" id="efectivo" name="efectivo" class="custom-switch-input" {{$usuario->pagosaceptados['efectivo'] == 1 ? 'checked': ''}}>
                                                            <span class="custom-switch-indicator"></span>
                                                        </label>
                                                        <label class="custom-switch"> Visa
                                                            <input type="checkbox" value="1" id="visa" name="visa" class="custom-switch-input" {{$usuario->pagosaceptados['visa'] == 1 ? 'checked': ''}}>
                                                            <span class="custom-switch-indicator"></span>
                                                        </label>
                                                        <label class="custom-switch"> Mastercard
                                                            <input type="checkbox" value="1" id="master" name="master" class="custom-switch-input" {{$usuario->pagosaceptados['master'] == 1 ? 'checked': ''}}>
                                                            <span class="custom-switch-indicator"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group mb-0 mt-2 row justify-content-end">                                                    
                                                    <div class="col-md-9">
                                                        <button type="button" name="btnGuardarConfig" id="btnGuardarConfig" class="btn btn-info">Guardar</button>
                                                        <a href="{{url('/')}}" class="btn btn-warning">Volver</a>
                                                    </div>    
                                                </div>
                                            </div>

                                            </form>
                                        </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-lg-6 col-xl-6 col-md-6 col-sm6">
                                                <h4>Horas de trabajo</h4>
                                            </div>
                                            <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6">
                                            
                                            @if($usuario->cerradoEmergenciaActivo == "0")
                                                <label class="custom-switch"> Cerrado de emergencia
                                                        <input type="checkbox" value="1" id="cerradoEmergencia" name="cerradoEmergencia" class="custom-switch-input" >
                                                        <span class="custom-switch-indicator"></span>
                                                </label>
                                            @else
                                                <label class="custom-switch"> Cerrado de emergencia
                                                        <input type="checkbox" value="1" id="cerradoEmergencia" name="cerradoEmergencia" class="custom-switch-input" CHECKED>
                                                        <span class="custom-switch-indicator"></span>
                                                </label>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                        <div class="card-body">
                                            <form class="form-horizontal" id="TimepoConfig" method="POST" action="{{action('PerfilController@update', ['id' => $usuario->id])}}">
                                                @csrf
                                                @method('PUT')

                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label">Lunes</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="tiempoLunes" name="tiempoLunes" class="form-control tiempoDia" value="{{$usuario->tiempoLunes}}"  placeholder="">
                                                            <input type="hidden" id="configTiempo" name="configTiempo" class="form-control" value="configTiempo" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label" >Martes</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="tiempoMartes" name="tiempoMartes" class="form-control tiempoDia" value='{{$usuario->tiempoMartes}}' placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label" for="email">Miercoles</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="tiempoMiercoles" name="tiempoMiercoles" class="form-control tiempoDia" value='{{$usuario->tiempoMiercoles}}' placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label" for="email">Jueves</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="tiempoJueves" name="tiempoJueves" class="form-control tiempoDia" value='{{$usuario->tiempoJueves}}' placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label" for="email">Viernes</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="tiempoViernes" name="tiempoViernes" class="form-control tiempoDia" value='{{$usuario->tiempoViernes}}' placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label" for="email">Sábado</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="tiempoSabado" name="tiempoSabado" class="form-control tiempoDia" value='{{$usuario->tiempoSabado}}' placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 col-form-label" for="email">Domingo</label>
                                                        <div class="col-md-9">
                                                            <input type="text" id="tiempoDomingo" name="tiempoDomingo" class="form-control tiempoDia" value='{{$usuario->tiempoDomingo}}' placeholder="">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group mb-0 mt-2 row justify-content-end">
                                                        <div class="col-md-9">
                                                            <button type="button" name="btnGuardarTimepoConfig" id="btnGuardarTimepoConfig" class="btn btn-info">Guardar</button>
                                                            <a href="{{url('/')}}" class="btn btn-warning">Volver</a>
                                                        </div>
                                                    </div>

                                            </form>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Ubicación y zona del comercio</h4>
                                    </div>
                                    <div class="card-body">
                                    <div class="form-group">
                                            <input type="text" id="address-input" name="address_address" value="{{$usuario->direccion}}" class="form-control map-input">
                                            <input type="hidden" name="address_latitude" id="address-latitude" value="{{$usuario->latitud}}" />
                                            <input type="hidden" name="address_longitude" id="address-longitude" value="{{$usuario->longitud}}" />
                                        </div>
                                        <div id="address-map-container" style="width:100%;height:600px; ">
                                            <div style="width: 100%; height: 100%" id="address-map"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="divBtnNuevoGrupo" class="row">
                            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group mb-0 mt-2 row justify-content-end">
                                            <div class="col-md-9" >
                                                <button onclick="removeLine();" type="button" name="btnRemoveLine" id="btnRemoveLine" class="btn btn-warning">Quitar áreas</button>
                                                <button type="button" name="btnGuardarAreas" id="btnGuardarAreas" class="btn btn-info">Guardar áreas</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


@csrf
<input type="hidden" name="urlEnviarCoordenadas" id="urlEnviarCoordenadas" value="{{action('CoordenadasController@update', ['id' => $usuario->id])}}">

{{-- Modal Emergencia --}}
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form class="form-horizontal" id="EmergenciaConfig" method="POST" action="{{route('perfil.cerradoemergencia')}}">
            @csrf
              <div class="modal-header">
                <div class="anchoModal row">
                    <div class="col-md-3">
                        <span class="ion-android-warning col-md-3 col-form-label" id="iconoE"></span>
                    </div>
                    <div class="col-md-9">
                        <h4 id="tituloE"class="modal-title" id="exampleModalLabel">Activar cerrado de emergencia</h4>
                    </div>
                </div>
              </div>
              <div class="modal-body">
                ...
              </div>
              <div class="modal-footer">
              <input type="hidden" id="idTinenda" name="idTinenda" value="{{$usuario->id}}" >
              <input type="hidden" id="cerradoEmergenciaActivo" name="cerradoEmergenciaActivo" value="1" >
                <button type="button" id="calcelarCerrado" class="btn btn-secondary" data-dismiss="modal">Calcelar</button>
                <button type="button" id="guardarEmergencia" class="btn btn-danger">Activar</button>
              </div>
        </form>
    </div>
  </div>
</div>
{{-- Modal Emergencia Desactivar --}}
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form class="form-horizontal" id="EmergenciaConfig2" method="POST" action="{{route('perfil.cerradoemergencia')}}">
            @csrf
              <div class="modal-header">
                <div class="anchoModal row">
                    <div class="col-md-3">
                        <span class="ion-android-warning col-md-3 col-form-label" id="iconoE"></span>
                    </div>
                    <div class="col-md-9">
                        <h4 id="tituloE"class="modal-title" id="exampleModalLabel">Desactivar cerrado de emergencia</h4>
                    </div>
                </div>
              </div>
              <div class="modal-body">
                ...
              </div>
              <div class="modal-footer">
              <input type="hidden" id="idTinenda" name="idTinenda" value="{{$usuario->id}}" >
              <input type="hidden" id="cerradoEmergenciaActivo" name="cerradoEmergenciaActivo" value="0" >
                <button type="button" id="calcelarCerrado2" class="btn btn-secondary" data-dismiss="modal">Calcelar</button>
                <button type="button" id="guardarEmergencia2" class="btn btn-danger">Aceptar</button>
              </div>
        </form>
    </div>
  </div>
</div>
{{-- Modal Emergencia activo --}}
<div class="modal fade bd-example-modal-sm" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <div class="anchoModal row">
                <div class="col-md-3">
                    <span class="ion-android-alert col-md-3 col-form-label" id="iconoA"></span>
                </div>
                <div class="col-md-9">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                          <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="mySmallModalLabel">Cerrado de emergencia activo</h4>
                </div>
            </div>         
      </div>

           <div class="modal-body">
                ...
        </div>
        </div>
    
    </div>
  </div>
</div>

                        @endif
                        
@if(session('actualizado'))
    <div style="display: none;" id="actualizado" name="actualizado" class="alert alert-success" data-mensaje="{{session('actualizado')}}"></div>
@endif
@if(session('error'))
    <div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{session('error')}}"></div>
@endif
@if($errors->has('name'))
    <div style="display: none;" id="errorName" name="errorName" class="alert alert-success" data-mensaje="{{$errors->first('name')}}"></div>
@endif
@if($errors->has('phone'))
    <div style="display: none;" id="errorPhone" name="errorPhone" class="alert alert-success" data-mensaje="{{$errors->first('phone')}}"></div>
@endif

@endsection

@section('libreriasjavascript')
<script src="{{url('assets/plugins/inputmask/jquery.inputmask.js')}}"></script>
<script src="{{url('assets/plugins/moment/moment.js')}}"></script>
<script src="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

    {{-- Formularios avanzados --}}
        <!--Select2 js-->
        <script src="{{url('assets/plugins/select2/select2.full.js')}}"></script>
        <!--Bootstrap-datepicker js-->
        <script src="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
        <!--Bootstrap-colorpicker js-->
        <script src="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
        <!--Bootstrap-timepicker js-->
        <script src="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
        <!--iCheck js-->
        <script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
        <!--forms js-->
        <script src="{{url('assets/js/forms.js')}}"></script>

    {{-- Wysiwing Editor --}}
        <!--ckeditor js-->
        <script src="{{url('assets/plugins/tinymce/tinymce.min.js')}}"></script>
        <!--Scripts js-->
        <script src="{{url('assets/js/formeditor.js')}}"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places,drawing,geometry&callback=initialize" async defer></script>
        <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5WO_983jAxhg6v-_AHH94FXMZzFUXDVo&callback=initMap" async defer></script> -->

        
@endsection

@section('javascript')

    <script type="text/javascript">
    var cerradoEmergenciaActivo = <?php echo $usuario->cerradoEmergenciaActivo; ?>;
        $(document).ready(function($) {
            if($('#actualizado').length){
                $mensaje = $('#actualizado').data('mensaje');
                notificacion($mensaje, 'success');
            }
            if($('#error').length){
                $mensaje = $('#error').data('mensaje');
                notificacion($mensaje, 'error');
            }
            if($('#errorName').length){
                $mensaje = $('#errorName').data('mensaje');
                notificacion($mensaje, 'error');
            }

            $("#btnGuardarConfig").click(function(){

                $url=$('#frmConfig').attr("action");
                $data=$('#frmConfig').serialize();
                $.ajax({
                  method: "PUT",
                  url: $url,
                  data: $data,
                })
                  .done(function( m ) {
                        if(m.exito){
                        notificacion('Configuración guardada correctamente', 'success');
                        }
                        if(m.error){
                        notificacion('Error al guardar configuración', 'error');
                        }
                        if(m.costoEnvio){
                            notificacion(m.costoEnvio, 'error');
                        }
                        if(m.tiempoEntrega){
                            notificacion(m.tiempoEntrega, 'error');
                        }
                        if(m.whatsapp){
                            notificacion(m.whatsapp, 'error');
                        }
                        if(m.terminos){
                            notificacion(m.terminos, 'error');
                        }
                  });
            })

            if($('#errorPhone').length){
                $mensaje = $('#errorPhone').data('mensaje');
                notificacion($mensaje, 'error');
            }

            $("#btnGuardarTimepoConfig").click(function(){

                if(cerradoEmergenciaActivo == 0){
                    $url=$('#TimepoConfig').attr("action");
                    $data=$('#TimepoConfig').serialize();
                    $.ajax({
                      method: "PUT",
                      url: $url,
                      data: $data,
                    })
                          .done(function( m ) {
                            if(m.exito){
                                    notificacion('Configuración guardada correctamente', 'success');
                            }
                            if(m.error){
                                notificacion('Error al guardar configuración', 'error');
                            }
                            if(m.errorDife){
                                notificacion('El rango de horas no es valido', 'error');
                            }

                          });
                }else{

                    $('#myModal2').appendTo("body");
                    $('#myModal2').modal('show')
                }
              
            })
        });

        $(document).ready(function() {
            var cerradoEmergenciaActivo = <?php echo $usuario->cerradoEmergenciaActivo; ?>;
            if(cerradoEmergenciaActivo != 0){
                
                $(".tiempoDia").attr('readonly','readonly');
                window.onload=function() {

                    $('#myModal2').appendTo("body");
                    $('#myModal2').modal('show')
                }
                    
            }

            
            $('.tiempoDia').daterangepicker({
                timePicker: true,
                timePicker12Hour: true,
                   timePickerIncrement: 1,
                timePickerSeconds: false,
                applyButtonClasses:("applyB"),
                   format: 'LT',
                   locale: {
                       format: 'h:mm A'
                   }
            }).on('show.daterangepicker', function (ev, picker) {
                $('.applyBtn').text("Guardar");
                $('.cancelBtn').text("Cancelar");
                picker.container.find(".calendar-table").hide();
                picker.container.find(".input-mini").hide();
                picker.container.find(".glyphicon-calendar").hide();
                if(cerradoEmergenciaActivo != 0){
            
                    $('.daterangepicker').hide();
                
                }
                
            })


        });


        $(document).ready(function() {

            $('#cerradoEmergencia[type="checkbox"]').on('change', function(e){
               if(e.target.checked){
                $('#myModal').appendTo("body");
                $('#myModal').modal('show')
               }else{
                $('#myModal3').appendTo("body");
                $('#myModal3').modal('show')
               }
            });



            $("#calcelarCerrado").click(function(){
                $('#cerradoEmergencia').prop('checked', false);
            })
            $("#calcelarCerrado2").click(function(){
                $('#cerradoEmergencia').prop('checked', true);
            })


            $("#guardarEmergencia").click(function(){
                $data=$('#EmergenciaConfig').serialize();
                $url=$('#EmergenciaConfig').attr("action");
                $.ajax({
                  method: "POST",
                  url: $url,
                  data: $data,
                })
                      .done(function( m ) {
                

                      });
              
            })
            $("#guardarEmergencia2").click(function(){
                $data=$('#EmergenciaConfig2').serialize();
                $url=$('#EmergenciaConfig2').attr("action");
                $.ajax({
                  method: "POST",
                  url: $url,
                  data: $data,
                })
                      .done(function( m ) {
                

                      });
              
            })

            $("#guardarEmergencia").click(function(){
                location.reload();
              
            })
            $("#guardarEmergencia2").click(function(){
                location.reload();
              
            })
        });


        $(document).ready(function($) {

            $("#btnGuardarAreas").click(function(){
                enviarCoordenadas();
            });
            
        });

        var areaDibujada = [];
        var coordinates = [];
        var coordTienda = [];
        const map = "";
        var zonaDibujada = "";

        function initialize() {

            $('form').on('keyup keypress', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });
            const locationInputs = document.getElementsByClassName("map-input");

            const autocompletes = [];
            const geocoder = new google.maps.Geocoder;
            for (let i = 0; i < locationInputs.length; i++) {

                const input = locationInputs[i];
                const fieldKey = input.id.replace("-input", "");
                const isEdit = document.getElementById(fieldKey + "-latitude").value != '' && document.getElementById(fieldKey + "-longitude").value != '';

                const latitude = parseFloat(document.getElementById(fieldKey + "-latitude").value) || -12.102535;
                const longitude = parseFloat(document.getElementById(fieldKey + "-longitude").value) || -77.025973;

                const map = new google.maps.Map(document.getElementById(fieldKey + '-map'), {
                    center: {lat: latitude, lng: longitude},
                    zoom: 13
                });
                
                const marker = new google.maps.Marker({
                    map: map,
                    position: {lat: latitude, lng: longitude},
                });

                marker.setVisible(isEdit);
                
                const autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.key = fieldKey;
                autocompletes.push({input: input, map: map, marker: marker, autocomplete: autocomplete});

                $.get("{{action('CoordenadasController@obtener', ['id' => $usuario->id])}}", function( data ) {
                    // coordTienda= JSON.stringify(data);
                    coordTienda= data;

                    var zonaCoord = coordTienda;
                    // coordinates = JSON.stringify(data);

                    zonaDibujada = new google.maps.Polygon({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35,
                        map: map
                        });
                    zonaDibujada.setPath(zonaCoord);
                });

            }

            for (let i = 0; i < autocompletes.length; i++) {
                const input = autocompletes[i].input;
                const autocomplete = autocompletes[i].autocomplete;
                const map = autocompletes[i].map;
                const marker = autocompletes[i].marker;

                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    marker.setVisible(false);
                    const place = autocomplete.getPlace();

                    geocoder.geocode({'placeId': place.place_id}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            const lat = results[0].geometry.location.lat();
                            const lng = results[0].geometry.location.lng();
                            setLocationCoordinates(autocomplete.key, lat, lng);
                        }
                    });

                    if (!place.geometry) {
                        window.alert("No details available for input: '" + place.name + "'");
                        input.value = "";
                        return;
                    }

                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);
                    }
                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);

                    marker.addListener('click', function() {
                    infoWindow.setContent(infowincontent);
                    infoWindow.open(map, marker);
                    });

                });

                //Listen for any clicks on the map.
                google.maps.event.addListener(map, 'click', function(event) {                
                    //Get the location that the user clicked.
                    var clickedLocation = event.latLng;
                    //If the marker hasn't been added.
                    if(marker === false){
                        //Create the marker.
                        marker = new google.maps.Marker({
                            position: clickedLocation,
                            map: map,
                            draggable: true //make it draggable
                        });
                        //Listen for drag events!
                        google.maps.event.addListener(marker, 'dragend', function(event){
                            markerLocation();
                        });
                    } else{
                        //Marker has already been added, so just change its location.
                        marker.setPosition(clickedLocation);
                    }
                    //Get the marker's location.
                    var currentLocation = marker.getPosition();
                    //Add lat and lng values to a field that we can save.
                    document.getElementById('address-latitude').value = currentLocation.lat(); //latitude
                    document.getElementById('address-longitude').value = currentLocation.lng(); //longitude
                    // getGeolocation($("#address-latitude").val(),$("#address-longitude").val())

                    var ubicacion = $("#address-latitude").val() + "," + $("#address-longitude").val()

                    $.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + ubicacion + "&key={{ env('GOOGLE_MAPS_API_KEY') }}", function(data){
                        // console.log(data.results);
                        $("#address-input").val(data.results[0]['formatted_address']);
                    });
                });

                var infoWindow = new google.maps.InfoWindow();
                var drawingManager = new google.maps.drawing.DrawingManager({
                    // drawingMode: google.maps.drawing.OverlayType.MARKER,
                    drawingControl: true,
                    drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: ['polygon']
                    },
                    // markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
                    // circleOptions: {
                    //     fillColor: '#ffff00',
                    //     fillOpacity: 1,
                    //     strokeWeight: 5,
                    //     clickable: false,
                    //     editable: true,
                    //     zIndex: 1
                    // }
                });
                drawingManager.setMap(map);
                $a=0;
                google.maps.event.addListener(drawingManager, 'polygoncomplete', function(rectangle) {
                        removeLine();
                    // console.log(rectangle.getPath());

                    var polygonBounds = rectangle.getPath();

                    for(var i = 0 ; i < polygonBounds.length ; i++)
                    {
                        coordinates.push(polygonBounds.getAt(i).lat(), polygonBounds.getAt(i).lng());
                    }


                    areaDibujada[$a] = rectangle;
                    $a++;
                    console.log(coordinates);
                    console.log(areaDibujada);

                    var point = new google.maps.LatLng($("#address-latitude").val(), $("#address-longitude").val());

                    if(google.maps.geometry.poly.containsLocation(point, rectangle) == true){
                        console.log("Adentro");
                    }else{
                        console.log("Afuera");
                    }
                });

            }
            

        }

        function removeLine() {
            coordinates = [];
            zonaDibujada.setMap(null);
            for($i=0; $i<areaDibujada.length; $i++){
                areaDibujada[$i].setMap(null);
            }
        }

        function setLocationCoordinates(key, lat, lng) {
            const latitudeField = document.getElementById(key + "-" + "latitude");
            const longitudeField = document.getElementById(key + "-" + "longitude");
            latitudeField.value = lat;
            longitudeField.value = lng;
        }


        function markerLocation(){
            //Get location.
            var currentLocation = marker.getPosition();
            //Add lat and lng values to a field that we can save.
            document.getElementById('address-latitude').value = currentLocation.lat(); //latitude
            document.getElementById('address-longitude').value = currentLocation.lng(); //longitude
        }

        function enviarCoordenadas(){
            data = coordinates;
            if(data.length){
                latitud = $("#address-latitude").val();
                longitud = $("#address-longitude").val();
                direccion = $("#address-input").val();
                $.ajax({
                        headers: {'X-CSRF-TOKEN':$("input[name=_token]").val()},
                        method: "PUT",
                        url: $("#urlEnviarCoordenadas").val(),
                        data: {coord:data, latitud:latitud, longitud:longitud, direccion:direccion}
                    })
                    .done(function( msg ) {
                        location.reload();
                    });
            }else{
                notificacion('Dibuje el area de servicio', 'warning');
            }
        }

    </script>
@endsection
