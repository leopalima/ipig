@extends('esquema.esquema')

@section('libreriascss')

@endsection

@section('css')

<style>

.yellow{
    background: #495057 !important;
    color: white;
}
.placeholderSortable{
    background: #f27474 !important;
    color: white;
}

</style>
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div style="display:scroll; position:fixed;bottom:100px;right:50px;">
                                        <a href="{{route('tienda.categorias.create')}}" class="btn btn-warning m-b-5 m-t-5">Nuevo</a>
                                    </div>
                                    <div class="card-header">
                                        <h4>Categorías</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="tableid" class="table table-bordered table-hover mb-0 text-nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nombre</th>
                                                        <th>Descripción</th>
                                                        <th>Estatus</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody class='moverP'>
                                                    @foreach($categorias as $categoria)
                                                    <tr>
                                                        <td></td>
                                                        <td style='display: none;'>{{$categoria->id}}</td>
                                                        <td>{{$categoria->name}}</td>
                                                        <td>{{$categoria->descripcion}}</td>
                                                        <td>
                                                            @if($categoria->estatus)
                                                            <span class="badge badge-success m-b-5">Activo</span>
                                                            @else
                                                            <span class="badge badge-warning m-b-5">Pausado</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-info" href="{{action('CategoriasProductoController@edit', ['id' => $categoria->id])}}">Editar</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </body>
                                            </table>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
@endsection

@section('libreriasjavascript')
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js" > </script>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            var myTable = document.getElementById('tableid');
                for(i=1; i < myTable.rows.length; i++){
                    myTable.rows[i].cells[0].innerHTML = i;
                }
        });
        
        $('.moverP').sortable({

            placeholder: 'placeholderSortable',
            
            start: function( event, ui ) { 
                $(ui.item).addClass("yellow");
            
            },

             stop:function( event, ui ) { 
            
                $(ui.item).removeClass("yellow");
            
            },




            update: function(event, ui) {
            
                var myTable = document.getElementById('tableid');
                for(i=1; i < myTable.rows.length; i++){
                    myTable.rows[i].cells[0].innerHTML = i;
                }
                data=[];
                myTable = document.getElementById('tableid');
                for(i=1; i < myTable.rows.length; i++){
                
                    var nombre=myTable.rows[i].cells[0].innerHTML;
                    var id=myTable.rows[i].cells[1].innerHTML;
                    data.push({
                        nombre: nombre,
                        id: id,
                    });

                }

                var _token = $('input[name="_token"]').val();

                $.ajax({
                 url:"{{ route('ordenar_categorias.guardar') }}",
                 method:"POST",
                 data: {_token:_token,data:data},
                
                      success:function(data)
                      {
                        if(data.grabado){
                            notificacion('Se guardo el orden de productos', 'success')
                        };

                    

                    
                      }
                })
            
            }

            });
            $(".moverP").disableSelection();

    </script>
@endsection