@extends('esquema.esquema')

@section('libreriascss')
    {{-- Formularios --}}
        <!--Morris css-->
        <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">

    {{-- Notificaciones emergentes --}}
        <!--Toastr css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">


@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Crear categoría</h4>
                                    </div>
                                    <div class="card-body">
                                        <form enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{route('tienda.categorias.store')}}">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Nombre</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}" placeholder="Nombre" autofocus>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="email">Descripción</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="descripcion" name="descripcion" class="form-control" value='{{old('descripcion')}}' placeholder="Descripcion">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Ordenamiento</label>
                                                <div class="col-md-9">
                                                    <select id="orden" name="orden" class="form-control">
                                                        @for($i=1; $i <= 20; $i++)
                                                        <option value="{{$i}}" {{20 == $i ? 'selected': ''}}>{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Estatus</label>
                                                <div class="col-md-9">
                                                    <select id="estatus" name="estatus" class="form-control">
                                                        <option value="1">Activo</option>
                                                        <option value="0">Pausa</option>
                                                    </select>
                                                </div>
											</div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="email">Imagen</label>
                                                <div class="col-md-9">
                                                    <input type="file" id="foto" name="foto" accept="image/*" class="form-control" placeholder="Descripcion">
                                                    <div id="divFoto">
                                                        <img src="" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mb-0 mt-2 row justify-content-end">
                                                <div class="col-md-9">
                                                    <button type="submit" name="btnGuardar" id="btnGuardar" class="btn btn-info">Guardar</button>
                                                    <a href="{{route('tienda.categorias.index')}}" class="btn btn-warning">Volver</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
@if(session('actualizado'))
    <div style="display: none;" id="actualizado" name="actualizado" class="alert alert-success" data-mensaje="{{session('actualizado')}}"></div>
@endif
@if(session('error'))
    <div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{session('error')}}"></div>
@endif
@if($errors->has('name'))
    <div style="display: none;" id="errorName" name="errorName" class="alert alert-success" data-mensaje="{{$errors->first('name')}}"></div>
@endif
@if($errors->has('descripcion'))
    <div style="display: none;" id="errorDescripcion" name="errorDescripcion" class="alert alert-success" data-mensaje="{{$errors->first('descripcion')}}"></div>
@endif

@endsection

@section('libreriasjavascript')
        
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            if($('#actualizado').length){
                $mensaje = $('#actualizado').data('mensaje');
                notificacion($mensaje, 'success');
            }
            if($('#error').length){
                $mensaje = $('#error').data('mensaje');
                notificacion($mensaje, 'error');
            }
            if($('#errorName').length){
                $mensaje = $('#errorName').data('mensaje');
                notificacion($mensaje, 'error');
            }
            if($('#errorDescripcion').length){
                $mensaje = $('#errorDescripcion').data('mensaje');
                notificacion($mensaje, 'error');
            }
            $("#foto").change(function () {
                filePreview(this);
            });
        });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                // $('#uploadForm + img').remove();
                $('#divFoto').html('<img src="'+e.target.result+'" width="500" height="300"/>');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    </script>
@endsection