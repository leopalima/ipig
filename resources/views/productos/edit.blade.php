@extends('esquema.esquema')

@section('libreriascss')
    {{-- Formularios --}}
        <!--Morris css-->
        <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">

    {{-- Notificaciones emergentes --}}
        <!--Toastr css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">


@endsection

@section('css')

<style>

.buttonTu{
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 1%;
}

</style>
@endsection

@section('contenido')
                        <div class="row">
							<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header">
										<h4>Editar producto</h4>
									</div>
									<div class="card-body">
										<form id="frmProducto" class="form-horizontal" method="POST" action="{{action('ProductosController@update', ['id' => $producto->id])}}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')
											<div class="form-group row">
												<label class="col-md-3 col-form-label">Nombre</label>
												<div class="col-md-9">
                                                    <input type="text" id="name" name="name" class="form-control" value="{{$producto->name}}" placeholder="Nombre" autofocus>
                                                    <div id="errorName" name="errorName" class="alert alert-danger mb-lg-0" hidden>
                                                            
                                                    </div>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 col-form-label" for="email">Descripción</label>
												<div class="col-md-9">
                                                    <input type="text" id="descripcion" name="descripcion" class="form-control" value='{{$producto->descripcion}}' placeholder="Descripcion">
                                                    <div id="errorDescripcion" name="errorDescripcion" class="alert alert-danger mb-lg-0" hidden>
                                                    </div>

												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 col-form-label">Precio</label>
												<div class="col-md-9">
                                                    <input type="text" id="precio" name="precio"class="form-control" value="{{$producto->precio}}" placeholder="Precio">
                                                    <div id="errorPrecio" name="errorPrecio" class="alert alert-danger mb-lg-0" hidden>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group row">
												<label class="col-md-3 col-form-label">Categoría</label>
												<div class="col-md-9">
													<select id="categoria" name="categoria" class="form-control">
                                                        @if(count($categorias) > 0)
                                                        @foreach($categorias as $categoria)
														<option value="{{$categoria->id}}" {{$categoria->id==$producto->categoria_id ? 'selected': ''}}>{{$categoria->name}}</option>
                                                        @endforeach
                                                        @else
                                                        <option value="">Para continuar agregue categorías</option>
                                                        @endif
                                                    </select>
                                                    <div id="errorCategoria" name="errorCategoria" class="alert alert-danger mb-lg-0" hidden>
                                                    </div>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 col-form-label">Ordenamiento</label>
												<div class="col-md-9">
													<select id="orden" name="orden" class="form-control">
                                                        @for($i=1; $i <= 20; $i++)
														<option value="{{$i}}" {{$producto->orden==$i ? 'selected': ''}}>{{$i}}</option>
                                                        @endfor
                                                    </select>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 col-form-label">Activo / Pausado</label>
												<div class="col-md-9">
                                                    <label class="custom-switch">
                                                        <input type="checkbox" value="{{$producto->estatus}}" id="estatus" name="estatus" class="custom-switch-input" {{$producto->estatus == 1 ? 'checked': ''}}>
                                                        <span class="custom-switch-indicator"></span>
                                                    </label>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 col-form-label">Foto</label>
												<div class="col-md-9">
                                                    <input type="file" accept="image/*" id="foto" name="foto" class="form-control">
                                                    <div id="errorFoto" name="errorFoto" class="alert alert-danger mb-lg-0" hidden>
                                                    </div>
												</div>
                                                <label class="col-md-3 col-form-label"></label>
                                                @if($producto->foto)
												<div class="col-md-9">
                                                    <img id="imgFoto" src="{{$producto->foto}}" alt="Smiley face" height="200" width="200">
                                                    <label class="col-md-9 col-form-label">
                                                        Imagen de proporción cuadrada <br>
                                                        Extensión JPG o PNG
                                                    </label>
                                                </div>
                                                @endif
                                            </div>
											<div class="form-group mb-0 mt-2 row justify-content-end">
												<div class="col-md-9">
													<button type="button" name="btnGuardar" id="btnGuardar" class="btn btn-info">Actualizar producto</button>
												</div>
                                            </div>
										</form>
									</div>
								</div>
							</div>
                        </div>

                        <div id="rowAdicionales" class="row">
                            <div id="div0">
                            </div>
                            
                        @if($producto->adicionales->count())
                        @php $countDiv = 1; @endphp
                        @foreach($producto->adicionales as $adicional)
                        <div id="div{{$countDiv}}" class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                        @php $countDiv++; @endphp
                            <div class="card">
                                <div class="card-header">
                                    <h4>Adicional</h4>
                                </div>
                                <div class="card-body">
                                    <form method="POST" action="{{action('AdicionalesController@update', ['id' => $adicional->id])}}">
                                    @csrf
                                    @method('PUT')
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                
                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <div class="form-group row mb-0">
                                                                <label class="col-md-3 col-form-label">Título</label>
                                                                    <div class="col-md-9">
                                                                        <input type="text" id="titulo" name="titulo" class="form-control" value="{{$adicional->titulo}}" placeholder="Título" autofocus>
                                                                        <input type="hidden" id="producto_id" name="producto_id" class="form-control" value="">
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group row mb-0">
                                                                <label class="col-md-3 col-form-label">Items seleccionables</label>
                                                                <div class="col-md-9">
                                                                    <select  name="maximo" class="form-control maximo">
                                                                        @php
                                                                        $max = count($adicional->items);
                                                                        @endphp
                                                                        @for($i=1; $i <= $max; $i++)
                                                                        <option value="{{$i}}" {{$i==$adicional->maximo ? "Selected" : ""}}>{{$i}}</option>
                                                                        @endfor
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group row mb-0">
                                                                <label class="col-md-3 col-form-label">Ordenamiento</label>
                                                                <div class="col-md-9">
                                                                    <select  name="orden" class="form-control maximo">
                                                                        @for($i=1; $i <= 20; $i++)
                                                                        <option value="{{$i}}" {{$i==$adicional->orden ? "Selected" : ""}}>{{$i}}</option>
                                                                        @endfor
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group row mb-0">
                                                                <label class="col-md-3 col-form-label">Activar / Desactivar </label>
                                                                <label class="custom-switch">
                                                                    <input type="checkbox" value="{{$adicional->estatus}}" id="estatus" name="estatus" class="custom-switch-input" {{$adicional->estatus == 1 ? 'checked': ''}}>
                                                                    <span class="custom-switch-indicator"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-12">

                                                            <div class="table-responsive">
                                                                <table class="table table-bordered mb-0 text-nowrap">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th>#</th>
                                                                            <th>Nombre</th>
                                                                            <th>Precio</th>
                                                                            <th>Mostrar / Ocultar</th>
                                                                        </tr>
                                                                        @foreach($adicional->items as $item)
                                                                        <tr id="nuevoTr">
                                                                            <td></td>
                                                                            <td>
                                                                            <input type="hidden" id="id" name="item[id][]" class="form-control" value="{{$item->id}}">
                                                                            <input type="text" id="item" name="item[name][]" class="form-control" value="{{$item->nombre}}" placeholder="Nombre de item">
                                                                            </td>
                                                                            <td><input type="text" id="precio" name="item[precio][]" class="form-control" value="{{$item->precio}}" placeholder="Precio de item"></td>
                                                                            <td>
                                                                                <label class="custom-switch">
                                                                                    <input type="checkbox" {{$item->estatus == 1 ? "checked" : ""}} value="{{$item->id}}" id="estatus" name="item[estatus][]" class="custom-switch-input">
                                                                                    <span class="custom-switch-indicator"></span>
                                                                                </label>
                                                                            </td>
                                                                        </tr>
                                                                        @endforeach
                                                                        <tr id="trAcciones">
                                                                            <td colspan="4">
                                                                            <button type="button" name="btnNuevo" id="btnNuevo" class="btn-sm btn-success">Nuevo item</button>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                        </div>

                        <div id="divBtnNuevoGrupo" class="row">
							<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row justify-content-end">
                                            <div class="col-lg-4 col-xl-4 col-md-6 col-sm-6 buttonTu" >
                                                <button type="button" name="btnGuardarAdicional" id="btnGuardarAdicional" class="btn btn-info">Actualizar adicionales</button>
                                            </div>
                                            <div class="col-lg-4 col-xl-4 col-md-6 col-sm-6 buttonTu" >
                                                <button type="button" name="btnNuevoGrupo" id="btnNuevoGrupo" class="btn btn-warning">Nuevo grupo de adicionales</button>
                                            </div> 
                                            <div class="col-lg-4 col-xl-4 col-md-12 col-sm-12  buttonTu" > 
                                                <a href="{{route('tienda.productos.index')}}" class="btn btn-success">Volver a la lista de productos</a>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div id="divBtnNuevoGrupo" class="row">
							<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group mb-0 mt-2 row justify-content-end">
                                            <div class="col-md-9" >
                                                <button type="button" name="btnNuevoGrupo" id="btnNuevoGrupo" class="btn btn-warning">Nuevo grupo de adicionales</button>
                                                <a href="{{route('tienda.productos.index')}}" class="btn btn-success">Volver a la lista de productos</a>
                                                <button type="button" name="btnGuardarAdicional" id="btnGuardarAdicional" class="btn btn-info">Actualizar adicionales</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->




{{--- Filas a copiar ---}}
<div id="trNuevo" style="display:none;">
<table>
    <tr id="nuevoTr">
        <td></td>
        <td>
        <input type="text" id="item" name="item[name][]" class="form-control" placeholder="Nombre de item">
        </td>
        <td><input type="text" id="precio" name="item[precio][]" class="form-control" placeholder="Precio de item"></td>
        <td>
        <button type="button" name="btnQuitar" id="btnQuitar" class="btn-sm btn-danger">Quitar</button>
        </td>
    </tr>
</table>
</div>
<div id="trExistente" style="display:none;">
<table>
    <tr>
        <td></td>
        <td>
        <select name="item[name][]" class="form-control">
        </select>
        </td>
        <td><input type="text" id="precio" name="item[precio][]" class="form-control" value='' placeholder="Precio de item"></td>
        <td>
        <button type="button" name="btnQuitar" id="btnQuitar" class="btn-sm btn-danger">Quitar</button>
        </td>
    </tr>
</table>
</div>

{{--- Grupos a copiar ---}}
<div id="divAdicionales" style="display:none">
    <div id="divAdicionalesModelo" class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-header">
            <div class="float-right">
                <button type="button" id="ocultarDiv" class="btn btn-icon"><i class="ion ion-close"></i></button>
            </div>
                <h4>Adicional</h4>
            </div>
            <div class="card-body">
                <form method="POST" action="{{route('tienda.adicionales.store')}}">
                @csrf
                    <div class="form-group row">
                        <div class="col-md-12">
                            
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Título</label>
                                        <div class="col-md-9">
                                            <input type="text" id="titulo" name="titulo" class="form-control" value="" placeholder="Título" autofocus>
                                            <input type="hidden" id="producto_id" name="producto_id" class="form-control" value="{{$producto->id}}">
                                        </div>
                                    </div>
                                </div>
                                <div  class="col-md-12 maximoItem" style="display: none;">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Items seleccionables</label>
                                        <div class="col-md-9">
                                            <select name="maximo" class="form-control maximo">
                                                <option value="" ></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div  class="col-md-12 maximoItem" style="display: none;">
                                    <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Ordenamiento</label>
                                        <div class="col-md-9">
                                            <select name="orden" class="form-control">
                                                @for($j=1; $j <= 20; $j++)
                                                <option value="{{$j}}" {{$j==20 ? "selected" : ""}}>{{$j}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">

                                        <div class="table-responsive">
                                            <table class="table table-bordered mb-0 text-nowrap">
                                                <tbody>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nombre</th>
                                                        <th>Precio</th>
                                                        <th></th>
                                                    </tr>
                                                    <tr id="trAcciones">
                                                        <td colspan="4">
                                                        <button type="button" name="btnNuevo" id="btnNuevo" class="btn-sm btn-success">Nuevo item</button>
                                                        <!-- <button type="button" name="btnExistente" id="btnExistente" class="btn-sm btn-info">Item existente</button> -->
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div style="display: none;" id="actualizado" name="actualizado" class="alert alert-success" data-mensaje="{{session('actualizado')}}" hidden></div>

<div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{session('error')}}" hidden></div>
<input type="hidden" id="countDiv" name="countDiv" value="{{$producto->adicionales->count() ? $producto->adicionales->count() : 0}}">



@endsection

@section('libreriasjavascript')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            $maximo=0;
            
            
            

            // $('#btnNuevo').click(function(){
            //     $('#trNuevo table tr').clone().insertBefore("#trAcciones");
            //     $.each($('#btnQuitar'), function(){
            //         $("table").delegate("#btnQuitar","click", function(event){
            //             $(this).parent().parent().remove();
            //         });
            //     });
            // });


            $divTotal = $('#countDiv').val();
            
            for($i=1; $i<=$divTotal; $i++){
                $("#div" + $i).delegate("#btnNuevo", "click", function(event){
                    $maximo=document.getElementById(event.delegateTarget.id).getElementsByTagName('tr').length;
                    var select="";
                        $maximo=$maximo-1;
                        
                            for($j=1 ; $j <$maximo ; $j++){
                                select = select+ "<option>"+$j+"</option>";
                                
                            }
                            select = select+ "<option Selected>"+$maximo+"</option>";
                        
                            $("#" + event.delegateTarget.id).find(".maximo").html(select);
                    
                    $('#trNuevo table tr').clone().insertBefore($(this).parent().parent());
                   
                });
                $.each($('#btnQuitar'), function(){
                    $("#div" + $i).delegate("#btnQuitar","click", function(event){
                        $maximo=document.getElementById(event.delegateTarget.id).getElementsByTagName('tr').length;
                        
                        var deleteSelect="";
                            $maximo=$maximo-3;

                            for($j=1 ; $j <$maximo ; $j++){
                                deleteSelect = deleteSelect+ "<option>"+$j+"</option>";
                            }
                            deleteSelect = deleteSelect+ "<option Selected>"+$maximo+"</option>";
                           
                            $("#" + event.delegateTarget.id).find(".maximo").html(deleteSelect);
                        $(this).parent().parent().remove();
                    });
                });
            }


            
            // $('#btnExistente').click(function(){
            //     $('#trExistente table tr').clone().insertBefore("#trAcciones");
            //     $.each($('#btnQuitar'), function(){
            //         console.log("hola3")
            //         $('#btnQuitar').click(function(event){
            //             $(this).parent().parent().remove();
            //         });
            //     });
            //     $.each($('#btnQuitar'), function(){
            //         console.log("hola4")
            //         $("table").delegate("#btnQuitar","click", function(event){
            //             $(this).parent().parent().remove();
            //         });
            //     });
            // });
            
            
            $('#btnNuevoGrupo').click(function(){
                $i = $("#countDiv").val();
                $i++;
                $('#divAdicionalesModelo').clone().insertAfter('#div' + ($i-1));
                $('#divAdicionalesModelo').attr('id', "div" + $i);
                $("#countDiv").val($i);
                $.each($('#btnNuevo'), function(){
                    $.each($('#btnQuitar'), function(){
                        
                        $("#div" + $i).delegate("#btnQuitar","click", function(event){
                            var deleteSelect="";
                            
                            $id=eval("this" + ".parentNode".repeat(13) + ".id")
                            $maximo=document.getElementById($id).getElementsByTagName('tr').length;
                            $maximo=$maximo-3;
                            if($maximo<1)
                            {
                                $("#" + $id).find(".maximoItem").fadeOut()
                            }
                            for($j=1 ; $j <$maximo ; $j++){
                                deleteSelect = deleteSelect+ "<option>"+$j+"</option>";
                            }
                            deleteSelect = deleteSelect+ "<option Selected>"+$maximo+"</option>";
                            $("#"+$id).find(".maximo").html(deleteSelect);

                            $(this).parent().parent().remove();
                        });
                    });
                    $("#div" + $i).delegate("#btnNuevo", "click", function(event){
                        var select="";
                        $maximo=document.getElementById(event.delegateTarget.id).getElementsByTagName('tr').length;
                        $maximo=$maximo-1;
                        if($maximo>0)
                            {
                                $("#" + event.delegateTarget.id).find(".maximoItem").fadeIn();
                            }
                            
                            for($j=1 ; $j <$maximo ; $j++){
                                select = select+ "<option>"+$j+"</option>";
                                
                            }
                            select = select+ "<option Selected>"+$maximo+"</option>";
                            $("#" + event.delegateTarget.id).find(".maximo").html(select);  

                        $('#trNuevo table tr').clone().insertBefore($(this).parent().parent());
                    });
                    // $("#div" + $i).delegate("#btnExistente", "click", function(event){
                    //     $('#trExistente table tr').clone().insertBefore($(this).parent().parent());
                    // });
                    });
                $.each($("#ocultarDiv"), function(){
                   
                    $("#div"+ $i).delegate("#ocultarDiv", "click", function(event){
                        $("#"+ event.delegateTarget.id).html("");
                        $("#"+ event.delegateTarget.id).html("<input type='hidden' id='producto_id' disabled>");
                    });
                });
                
                
                $("#btnGuardarAdicional").prop("hidden", false);
            });


            $("#btnGuardarAdicional").click(function(){
                $countDiv = $("#countDiv").val();
                for($i=1; $i<= $countDiv; $i++)
                {
                    if($("#div" + $i).find("#producto_id").prop("disabled") != true){
                        $divUrl = $("#div" + $i).find("form").attr("action");
                        $divFrm = $("#div" + $i).find("form").serialize();
                        var guardar = guardarAdicional($divUrl, $divFrm, $("#div" + $i));
                    }
                }
            });

            $("#btnGuardar").click(function(){
                $divUrl = $(this).parent().parent().parent().attr("action");
                // $frmData = $(this).parent().parent().parent().serialize();
                var frmData = new FormData(jQuery('#frmProducto')[0]);
                $.ajax({
                    type: "POST",
                    url: $divUrl,
                    data: frmData,
                    dataType: 'json',
                    success: function(data){
                        try {
                            if(data.errors.name){
                                var mensaje = data.errors.name[0];
                                $elemento = $('#errorName');
                                $elemento.prop('hidden', false)
                                $elemento.html(mensaje)
                                if($elemento.length){
                                    $mensaje = mensaje;
                                    notificacion($mensaje, 'error');
                                }
                            }else{
                                $('#errorName').prop('hidden', true);
                            }
                        
                            if(data.errors.descripcion){
                                var mensaje = data.errors.descripcion[0];
                                $elemento = $('#errorDescripcion');
                                $elemento.prop('hidden', false)
                                $elemento.html(mensaje)
                                if($elemento.length){
                                    $mensaje = mensaje;
                                    notificacion($mensaje, 'error');
                                }
                            }else{
                                $('#errorDescripcion').prop("hidden", true);
                            }
                            if(data.errors.precio){
                                var mensaje = data.errors.precio[0];
                                $elemento = $('#errorPrecio');
                                $elemento.prop('hidden', false)
                                $elemento.html(mensaje)
                                if($elemento.length){
                                    $mensaje = mensaje;
                                    notificacion($mensaje, 'error');
                                }
                            }else{
                                $('#errorPrecio').prop("hidden", true);
                            }
                            if(data.errors.categoria){
                                var mensaje = data.errors.categoria[0];
                                $elemento = $('#errorCategoria');
                                $elemento.prop('hidden', false)
                                $elemento.html(mensaje)
                                if($elemento.length){
                                    $mensaje = mensaje;
                                    notificacion($mensaje, 'error');
                                }
                            }else{
                                $('#errorCategoria').prop("hidden", true);
                            }
                        } catch (e) {
                            $('#errorName').prop('hidden', true);
                            $('#errorDescripcion').prop("hidden", true);
                            $('#errorPrecio').prop("hidden", true);
                            $('#errorCategoria').prop("hidden", true);
                        }

                        try {
                            if(data.grabado){
                                var mensaje = data.grabado;
                                $elemento = $('#actualizado');
                                $elemento.html(mensaje)
                                if($elemento.length){
                                    $mensaje = mensaje;
                                    notificacion($mensaje, 'success');
                                }
                                // $("#btnGuardar").prop('hidden', true);
                                // $("#divBtnNuevoGrupo").prop('hidden', false);
                                $("#producto_id").val(data.producto_id);
                                $("#imgFoto").attr('src', data.url_foto);
                            }
                        } catch (e) {
                            
                        }

                        try {
                            if(data.error){
                                var mensaje = data.error;
                                $elemento = $('#error');
                                $elemento.html(mensaje)
                                if($elemento.length){
                                    $mensaje = mensaje;
                                    notificacion($mensaje, 'error');
                                }
                            }
                        } catch (e) {
                            
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false,
                    async: true,
                });
            })            
        });

        function guardarAdicional($url, $formulario, $div){
            $.ajax({
                type: "POST",
                url: $url,
                data: $formulario,
                success: function(data){
                    try {
                        if(data.grabado){
                            var mensaje = data.grabado;
                            $elemento = $('#actualizado');
                            $elemento.html(mensaje)
                            if($elemento.length){
                                $mensaje = mensaje;
                                notificacion($mensaje, 'success');
                            }
                            $div.find("#trAcciones").remove();
                            $div.find("table :button").remove();
                            $div.find("form :input").attr("disabled", true);
                            $("#btnGuardarAdicional").prop('hidden', true);

                        }
                    } catch (e) {
                        
                    }

                    try {
                        if(data.error){
                            var mensaje = data.error;
                            $elemento = $('#error');
                            $elemento.html(mensaje)
                            if($elemento.length){
                                $mensaje = mensaje;
                                notificacion($mensaje, 'error');
                            }
                        }
                    } catch (e) {
                        return 0;
                    }

                    try {
                        if(data.errors.titulo){
                            var mensaje = data.errors.titulo;
                            $elemento = $('#error');
                            $elemento.html(mensaje)
                            if($elemento.length){
                                $mensaje = mensaje;
                                notificacion($mensaje, 'error');
                            }
                        }
                        if(data.errors.producto_id){
                            var mensaje = data.errors.producto_id;
                            $elemento = $('#error');
                            $elemento.html(mensaje)
                            if($elemento.length){
                                $mensaje = mensaje;
                                notificacion($mensaje, 'error');
                            }
                        }
                        if(data.errors.item){
                            var mensaje = data.errors.item;
                            $elemento = $('#error');
                            $elemento.html(mensaje)
                            if($elemento.length){
                                $mensaje = mensaje;
                                notificacion($mensaje, 'error');
                            }
                        }

                        jQuery.each(data.errors, function(i, val) {
                            var mensaje = val;
                            $elemento = $('#error');
                            $elemento.html(mensaje)
                            if($elemento.length){
                                $mensaje = mensaje;
                                notificacion($mensaje, 'error');
                            }
                        });

                    } catch (e) {
                        
                    }
                },
                error: function(xhr, status, errorThrown){
                    notificacion("Todos los adicionales están guardados", 'warning');
                }
            });
        }
    </script>
@endsection