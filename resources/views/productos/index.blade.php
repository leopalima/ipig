@extends('esquema.esquema')

@section('libreriascss')


<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">


@endsection

@section('css')
<style>

.buttonTu{
    display: flex;
    align-items: center;
    justify-content: center;

}
#nomalAtras{
    display: flex;
    align-items: center;
    justify-content: center;
    
 

}
.fa-arrows-v{

    padding: 0px 5% 0px 0px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 120%;

}

.yellow{
    background: #495057 !important;
    color: white;
}
.placeholderSortable{
    background: #f27474 !important;
    color: white;
}


</style>
@endsection

@section('contenido')
                        <div id="divBtnNuevoGrupo" class="row">
							<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                <div class="card-header">
                                        <h4>Ordena tus productos</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row " id="botonesLocos">
                                            
                                            @foreach($categorias as $ca)
                                                <div class="col-lg col-xl col-md col-sm buttonTu" >
                                                    <button type="button" id="" value="{{$ca->id}}" class="buttonTu badge badge-secondary m-b-5">{{$ca->name}}</button>
                                                </div>
                                            @endforeach
                                            <div class="col-lg col-xl col-md col-sm " id="nomalAtras"style="display: none;">
                                                    <button type="button"  id="btnGuardarAdicional"  class="badge badge-danger m-b-5">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="aquiNo">
                            <div class="col-lg-12">
                                <div class="card">
                                    {{--  <div style="display:scroll; position:fixed;bottom:100px;right:50px;">
                                        <a href="{{route('tienda.productos.create')}}" class="btn btn-warning m-b-5 m-t-5">Nuevo</a>
                                    </div>  --}}
                                    <div class="card-header">
                                        <h4>Productos</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tr>
                                                
                                                    <th>#</th>
                                                    <th>Nombre</th>
                                                    <th>Categoria</th>
                                                    <th>Precio</th>
                                                    <th>Adicionales</th>
                                                    <th>Estatus</th>
                                                    <th></th>
                                                </tr>
                                                <?php $pos=1 ?>
                                                @foreach($productos as $producto)
                                                <tr>
                                                    <td><?php print $pos++ ?></td>
                                                    <td>{{$producto->name}}</td>
                                                    <td>{{$producto->categoria->name}}</td>
                                                    <td>{{$producto->precio}}</td>
                                                    <td>{{$producto->adicionales->count()}}</td>
                                                    <td>
                                                        @if($producto->estatus)
                                                        <span class="badge badge-success m-b-5">Activo</span>
                                                        @else
                                                        <span class="badge badge-warning m-b-5">Pausado</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-info" href="{{action('ProductosController@edit', ['id' => $producto->id])}}">Editar</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                            {{ $productos->links() }}
                                            <a href="{{route('tienda.productos.create')}}" class="btn btn-warning m-b-5 m-t-5">Nuevo</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="aquiEs" style="display: none;">
                                <div class="col-lg-12">
                                    <div class="card" >
                                        {{--  <div style="display:scroll; position:fixed;bottom:100px;right:50px;">
                                            <a href="{{route('tienda.productos.create')}}" class="btn btn-warning m-b-5 m-t-5">Nuevo</a>
                                        </div>  --}}
                                        <div class='card-header'><h4 id="tituloAqui"></h4></div>
                                            <div class='card-body'>
                                                <div class='table-responsive'>
                                                    <table id="tableid" class='table table-bordered table-hover mb-0 text-nowrap'>
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Nombre</th>
                                                                <th>Precio</th>
                                                                <th>Adicionales</th>
                                                                <th>Estatus</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                    <tbody class='moverP'>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                        </div>
                        
<input type="hidden" value="{{ csrf_token()}}" name="_token">             
@endsection

@section('libreriasjavascript')

<script src = "https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js" > </script>

@endsection

@section('javascript')
    <script type="text/javascript">
        var movement=0;
        var static=0;
        $(document).ready(function() {
          
            
        });
        $('.moverP').sortable({

            // change: function(event, ui) {

            //     movement = ui.position.top - ui.originalPosition.top;
            //     var donde = movement > static ? "down" : "up";
            //     static=movement;
            //         if(donde=="up"){
            //             $(ui.item).removeClass("fa flechaLoca fa-chevron-down");
            //             $(ui.item).addClass("fa flechaLoca fa-chevron-up");
                        
                        
            //         }else{
            //             $(ui.item).removeClass("fa flechaLoca fa-chevron-up");
            //             $(ui.item).addClass("fa flechaLoca fa-chevron-down");

            //         }

            //     console.log(movement)
            //     console.log(static)
            //     console.log(donde)

            // },
            placeholder: 'placeholderSortable',
            
            start: function( event, ui ) { 
                $(ui.item).addClass("yellow");

            },

             stop:function( event, ui ) { 
               
                $(ui.item).removeClass("yellow");

            },

            


            update: function(event, ui) {
        
                var myTable = document.getElementById('tableid');
                for(i=1; i < myTable.rows.length; i++){
                    myTable.rows[i].cells[0].innerHTML = i;
                }
                data=[];
                myTable = document.getElementById('tableid');
                for(i=1; i < myTable.rows.length; i++){

                    var nombre=myTable.rows[i].cells[0].innerHTML;
                    var id=myTable.rows[i].cells[1].innerHTML;
                    data.push({
                        nombre: nombre,
                        id: id,
                    });
                    
                }
                
                var _token = $('input[name="_token"]').val();
                
                $.ajax({
                 url:"{{ route('ordenar_productos.guardar') }}",
                 method:"POST",
                 data: {_token:_token,data:data},

                      success:function(data)
                      {
                        if(data.grabado){
                            notificacion('Se guardo el orden de productos', 'success')
                        };
                        

                        

                      }
                })

            }
            
        });
        $(".moverP").disableSelection();


        $(document).on('click', '#nomalAtras', function(){
            
            $("#aquiEs").fadeOut();
            $("#nomalAtras").fadeOut();
			$("#aquiNo").fadeIn();
            

        });

        $(document).on('click', '.buttonTu', function(){

            var id = $(this).val();
            var URLactual = window.location.href
            
            

          $('#exampleModalLabel').text('Editar etiqueta');
            $.ajax({
                url:"/productos/ordenar/"+id,
                dataType:"json",
                success:function(html){
                    
                    html.sort(function(a, b) {
                        return a.orden - b.orden;
                    });
                    
                    if(html.length>0){
                        $('#tituloAqui').text("Ordena tus productos de "+html[0].categoria.name);
                    }else{
                        $('#tituloAqui').text("No hay productos en esta categoría");
                    }
                    var compraformato = "";
                    
                    for(i=0; i < html.length; i++){
                        var j=1+i
                        compraformato = compraformato + "<tr>";
                        compraformato = compraformato + "<td name='posicion'>"+ j +"</td>";
                        compraformato = compraformato + "<td nameP='idProducto'style='display: none;'>"+ html[i].id +"</td>";
                        compraformato = compraformato + "<td name='flecha'>"+ html[i].name +"</td>";
                        compraformato = compraformato + "<td>"+ html[i].precio +"</td>";
                        compraformato = compraformato + "<td>"+ html[i].adicionales.length +"</td>";
                        compraformato = compraformato + "<td>"+ (html[i].estatus ? "<span class='badge badge-success m-b-5'>Activo</span>" : "<span class='badge badge-warning m-b-5'>Pausado</span>") +"</td>";
                        compraformato = compraformato + "<td> <a class='btn btn-info' href='"+URLactual+"/"+html[i].id+"/edit'>Editar</a></td>";
                        compraformato = compraformato + "<td><span class='fa fa-arrows-v flechaIndicar'></span> </td>";
                        compraformato = compraformato + "</tr>";
                    }
                    $("#aquiEs").fadeIn();
                    $("#nomalAtras").fadeIn();
					$("#aquiNo").fadeOut();

                    $("#aquiEs").find(".moverP").html(compraformato);   
                    


                }
            })
        });


    </script>
@endsection