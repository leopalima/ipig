@extends('esquema.esquema')

@section('libreriascss')
    {{-- Formularios --}}
        <!--Morris css-->
        <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">

    {{-- Notificaciones emergentes --}}
        <!--Toastr css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">


@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
							<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
								<div class="card">
									<div class="card-header">
										<h4>Crear adicionales</h4>
									</div>
									<div class="card-body">
										<form class="form-horizontal" method="POST" action="{{route('tienda.adicionales.store')}}">
                                            @csrf
											<div class="form-group row">
												<label class="col-md-3 col-form-label">Título</label>
												<div class="col-md-9">
                                                    <input type="text" id="titulo" name="titulo" class="form-control" value="{{old('titulo')}}" placeholder="Título" autofocus>
                                                    @if($errors->has('titulo'))
                                                    <div id="errorTitulo" name="errorTitulo" class="alert alert-danger mb-lg-0">
                                                            {{$errors->first('titulo')}}
                                                    </div>
                                                    @endif
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 col-form-label" for="email">Máximos a elegir</label>
												<div class="col-md-9">
                                                    <input type="text" id="maximo" name="maximo" class="form-control" value='{{old('maximo')}}' placeholder="Máximos a elegir">
                                                    @if($errors->has('maximo'))
                                                    <div id="errorMaximo" name="errorMaximo" class="alert alert-danger mb-lg-0">
                                                            {{$errors->first('maximo')}}
                                                    </div>
                                                    @endif

												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 col-form-label">Items</label>
												<div class="col-md-9">

                                                <div class="table-responsive">
                                                    <table class="table table-bordered mb-0 text-nowrap">
                                                        <tbody>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Nombre</th>
                                                                <th>Precio</th>
                                                                <th></th>
                                                            </tr>
                                                            @if(old('item'))
                                                            @for($i = 0; $i < count(old('item.name')); $i++)
                                                            <tr>
                                                                <td></td>
                                                                <td>
                                                                <input type="text" id="item" name="item[name][]" class="form-control" value="{{old('item.name.'.$i)}}" placeholder="Nombre de item">
                                                                </td>
                                                                <td><input type="text" id="precio" name="item[precio][]" class="form-control" value="{{old('item.precio.'.$i)}}" placeholder="Precio de item"></td>
                                                                <td>
                                                                <button type="button" name="btnQuitar" id="btnQuitar" class="btn-sm btn-danger">Quitar</button>
                                                                </td>
                                                            </tr>
                                                            @endfor
                                                            @endif
                                                            <tr id="trAcciones">
                                                                <td colspan="4">
                                                                <button type="button" name="btnNuevo" id="btnNuevo" class="btn-sm btn-success">Nuevo</button>
                                                                <button type="button" name="btnExistente" id="btnExistente" class="btn-sm btn-info">Existente</button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                @if($errors->has('item'))
                                                    <div id="errorItem" name="errorItem" class="alert alert-danger mb-lg-0">
                                                            {{$errors->first('item')}}
                                                    </div>
                                                @endif
                                                @if($errors->has('item.name.*'))
                                                    <div id="errorItemName" name="errorItemName" class="alert alert-danger mb-lg-0">
                                                            {{$errors->first('item.name.*')}}
                                                    </div>
                                                @endif
                                                @if($errors->has('item.precio.*'))
                                                    <div id="errorItemPrecio" name="errorItemPrecio" class="alert alert-danger mb-lg-0">
                                                            {{$errors->first('item.precio.*')}}
                                                    </div>
                                                @endif

                                                </div>
                                            </div>
											<div class="form-group mb-0 mt-2 row justify-content-end">
												<div class="col-md-9">
													<button type="submit" name="btnGuardar" id="btnGuardar" class="btn btn-info">Guardar</button>
													<a href="{{route('tienda.adicionales.index')}}" class="btn btn-warning">Volver</a>
												</div>
                                            </div>
										</form>
									</div>
								</div>
							</div>
                        </div>

<div id="trNuevo" style="display:none;">
<table>
    <tr>
        <td></td>
        <td>
        <input type="text" id="item" name="item[name][]" class="form-control" placeholder="Nombre de item">
        </td>
        <td><input type="text" id="precio" name="item[precio][]" class="form-control" placeholder="Precio de item"></td>
        <td>
        <button type="button" name="btnQuitar" id="btnQuitar" class="btn-sm btn-danger">Quitar</button>
        </td>
    </tr>
</table>
</div>
<div id="trExistente" style="display:none;">
<table>
    <tr>
        <td></td>
        <td>
        <select name="item[name][]" class="form-control">
        @foreach($items as $item)
        <option>{{$item->nombre}}</option>
        @endforeach
        </select>
        </td>
        <td><input type="text" id="precio" name="item[precio][]" class="form-control" value='' placeholder="Precio de item"></td>
        <td>
        <button type="button" name="btnQuitar" id="btnQuitar" class="btn-sm btn-danger">Quitar</button>
        </td>
    </tr>
</table>
</div>
                        
@if(session('actualizado'))
	<div style="display: none;" id="actualizado" name="actualizado" class="alert alert-success" data-mensaje="{{session('actualizado')}}"></div>
@endif
@if(session('error'))
	<div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{session('error')}}"></div>
@endif

@endsection

@section('libreriasjavascript')
        
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            if($('#actualizado').length){
                $mensaje = $('#actualizado').data('mensaje');
                notificacion($mensaje, 'success');
            }
            if($('#error').length){
                $mensaje = $('#error').data('mensaje');
                notificacion($mensaje, 'error');
            }
            if($('#errorTitulo').length){
                $mensaje = $('#errorTitulo').html();
                notificacion($mensaje, 'error');
            }
            if($('#errorMaximo').length){
                $mensaje = $('#errorMaximo').html();
                notificacion($mensaje, 'error');
            }
            if($('#errorPrecio').length){
                $mensaje = $('#errorPrecio').html();
                notificacion($mensaje, 'error');
            }
            if($('#errorCategoria').length){
                $mensaje = $('#errorCategoria').html();
                notificacion($mensaje, 'error');
            }
            if($('#errorEstatus').length){
                $mensaje = $('#errorEstatus').html();
                notificacion($mensaje, 'error');
            }

            $('#btnNuevo').click(function(){
                $('#trNuevo table tr').clone().insertBefore("#trAcciones");
                $.each($('#btnQuitar'), function(){
                    $("table").delegate("#btnQuitar","click", function(event){
                        $(this).parent().parent().remove();
                    });
                });
            });

            $('#btnExistente').click(function(){
                $('#trExistente table tr').clone().insertBefore("#trAcciones");
                $.each($('#btnQuitar'), function(){
                    $('#btnQuitar').click(function(event){
                        $(this).parent().parent().remove();
                    });
                });
                $.each($('#btnQuitar'), function(){
                    $("table").delegate("#btnQuitar","click", function(event){
                        $(this).parent().parent().remove();
                    });
                });
            });


        });
    </script>
@endsection