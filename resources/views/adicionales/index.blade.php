@extends('esquema.esquema')

@section('libreriascss')

@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div style="display:scroll; position:fixed;bottom:100px;right:50px;">
                                        <a href="{{route('tienda.adicionales.create')}}" class="btn btn-warning m-b-5 m-t-5">Nuevo</a>
                                    </div>
                                    <div class="card-header">
                                        <h4>Adicionales</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tr>
                                                    <th>#</th>
                                                    <th>Titulo</th>
                                                    <th>Máximo</th>
                                                    <th>Items</th>
                                                    <th></th>
                                                </tr>
                                                @foreach($adicionales as $adicional)
                                                <tr>
                                                    <td></td>
                                                    <td>{{$adicional->titulo}}</td>
                                                    <td>{{$adicional->maximo}}</td>
                                                    <td>{{$adicional->items->count()}}</td>
                                                    <td>
                                                        <a class="btn btn-info" href="{{action('AdicionalesController@edit', ['id' => $adicional->id])}}">Editar</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
                                            {{ $adicionales->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
@endsection

@section('libreriasjavascript')

@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            
        });
    </script>
@endsection