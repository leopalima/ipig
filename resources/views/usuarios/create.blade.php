@extends('esquema.esquema')

@section('libreriascss')
    {{-- Formularios --}}
        <!--Morris css-->
        <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">

    {{-- Notificaciones emergentes --}}
        <!--Toastr css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">


@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Crear usuario</h4>
                                    </div>
                                    <div class="card-body">
                                        <form enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{route('usuarios.store')}}">
                                            @csrf
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Nombre</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}" placeholder="Nombre" autofocus>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="email">Email</label>
                                                <div class="col-md-9">
                                                    <input type="email" id="email" name="email" class="form-control" value='{{old('email')}}' placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="email">Teléfono</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="phone" name="phone" class="form-control" value="{{old('phone')}}" placeholder="Teléfono">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="email">Descripción</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="descripcion" name="descripcion" class="form-control" value='{{old('descripcion')}}' placeholder="Descripción">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Tiempo entrega (min)</label>
                                                <div class="col-md-9">
                                                    <select id="tiempoEntrega" name="tiempoEntrega" id="tipo"class="form-control">
                                                        <?php  for($i=5;$i<125;$i=$i+5) { echo "<option value='".$i."'>".$i."</option>"; } ?>
                                                    </select>
                                                </div>    
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Costo de envío</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="costoEnvio" name="costoEnvio"class="form-control" placeholder="Indique costo de envío">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Contraseña</label>
                                                <div class="col-md-9">
                                                    <input type="password" id="password" name="password"class="form-control" placeholder="Contraseña">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Tipo de usuario</label>
                                                <div class="col-md-9">
                                                    <select id="tipo" name="tipo" id="tipo"class="form-control">
                                                        @foreach($tipos as $tipo)
                                                            <option value="{{$tipo->id}}">{{$tipo->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Foto</label>
                                                <div class="col-md-9">
                                                    <input type="file" accept="image/*" id="foto" name="foto" class="form-control">
                                                    <div id="errorFoto" name="errorFoto" class="alert alert-danger mb-lg-0" hidden>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row classEtiqueta">
                                                <label class="col-md-3 col-form-label">Etiquetas</label>
                                                <div class="col-md-9">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped mb-0 text-nowrap">
                                                            <tr>
                                                                <th>Etiqueta</th>
                                                                <th></th>
                                                                <th>Etiqueta</th>
                                                                <th></th>
                                                            </tr>
                                                            @php
                                                                $parEtiqueta = $etiquetas->chunk(2);
                                                            @endphp
                                                            @foreach($parEtiqueta as $etq)
                                                            <tr>
                                                                @foreach($etq as $e)
                                                                <td>{{$e->etiqueta}}</td>
                                                                <td>
                                                                    <label class="custom-switch">
                                                                        <input name="etiquetas[]" value="{{$e->etiqueta}}" type="checkbox" name="custom-switch-checkbox" class="custom-switch-input">
                                                                        <span class="custom-switch-indicator"></span>
                                                                    </label>
                                                                </td>
                                                                @endforeach
                                                            </tr>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mb-0 mt-2 row justify-content-end">
                                                <div class="col-md-9">
                                                    <button type="submit" name="btnGuardar" id="btnGuardar" class="btn btn-info">Guardar</button>
                                                    <a href="{{route('usuarios.index')}}" class="btn btn-warning">Volver</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
@if(session('actualizado'))
    <div style="display: none;" id="actualizado" name="actualizado" class="alert alert-success" data-mensaje="{{session('actualizado')}}"></div>
@endif
@if(session('error'))
    <div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{session('error')}}"></div>
@endif
@if($errors->has('name'))
    <div style="display: none;" id="errorName" name="errorName" class="alert alert-success" data-mensaje="{{$errors->first('name')}}"></div>
@endif

@endsection

@section('libreriasjavascript')
        
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            if($('#actualizado').length){
                $mensaje = $('#actualizado').data('mensaje');
                notificacion($mensaje, 'success');
            }
            if($('#error').length){
                $mensaje = $('#error').data('mensaje');
                notificacion($mensaje, 'error');
            }
            if($('#errorName').length){
                $mensaje = $('#errorName').data('mensaje');
                notificacion($mensaje, 'error');
            }
        });
        $(document).ready(function($) {

            if($('#tipo option:selected').text() == 'tienda'){
                $(".classEtiqueta").show();
            }else
            {
                $(".classEtiqueta").hide();
            }

            $('#tipo').change(function(){

                if($('#tipo option:selected').text() == 'tienda'){
                    $(".classEtiqueta").fadeIn();
                }else{
                    $(".classEtiqueta").fadeOut();
                }

            });
        })
    </script>
@endsection