@extends('esquema.esquema')

@if (session()->has('back_previous_button'))
  {{session()->forget('back_previous_button')}}
@endif

@section('libreriascss')

@endsection

@section('css')

<style>

.masGrande{

max-width: 80%;

}

#Lunes{
	background-color: #f27272 !important;
    border-color: #f27272!important;
}

#Martes{
	background-color: rgb(90,98,104) !important;
    border-color:rgb(90,98,104)!important;
}

#Miercoles{
	background-color: rgb(25,217,54) !important;
    border-color:rgb(25,217,54)!important;
}

#Jueves{
	background-color: rgb(200,34,51) !important;
    border-color: rgb(200,34,51)!important;
}

#Viernes{
	background-color: rgb(248,200,17) !important;
    border-color:rgb(248,200,17)!important;
}

#Sabado{
	background-color: rgb(24,190,255) !important;
    border-color:rgb(24,190,255)!important;
}

#Domingo{
	background-color: rgb(34,39,43) !important;
    border-color:rgb(34,39,43)!important;
}


</style>

@endsection

@section('contenido')
                        <div class="row">
							<div class="col-12 ">
								<div class="card">
									<div class="card-header">
										<h4>Buscador de usuarios</h4>
									</div>
									<div class="card-body">
                                    <form class="form-horizontal" method="GET" action="{{route('usuarios.index')}}">
                                        <div class="form-group row">
                                            <div class="col-md-9">
										        <input id="txtBuscar" value="{{request()->get('txtBuscar') != null ? request()->get('txtBuscar') : ''}}" name="txtBuscar" class="form-control input-lg" type="text" placeholder="Buscar">
                                            </div>
                                            <label class="col-md-3"><button type="submit" class="btn btn-primary m-b-5 m-t-5">Buscar</button></label>
                                        </div>
                                    </form>
									</div>
								</div>
							</div>
						</div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Usuarios</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tr>
                                                    <th></th>
                                                    <th>Nombre</th>
                                                    <th>Email</th>
                                                    <th>Tipo</th>
                                                    <th>Fecha de registro</th>
                                                    <th>Dispositivo</th>
                                                    <th></th>
                                                </tr>
                                                
                                                @foreach($usuarios as $usuario)
                                                <tr>
                                                    <td>
														@switch($usuario->estatus)
															@case(1)
																<span class="badge badge-success m-b-5">I</span>
																@break
															@case(0)
																<span class="badge badge-danger m-b-5">O</span>
																@break
															@default
														@endswitch
													</td>
                                                    <td>
														{{$usuario->name}}
														@if($usuario->tipo->name=="cliente")
															@if($usuario->validate)
																<img src="{{url('assets/img')}}/check-icon.png" width="18px">
															@else
																<img src="{{url('assets/img')}}/warning-icon.png" width="18px">
															@endif
														@endif
													</td>
                                                    <td>{{$usuario->email}}</td>
                                                    <td>{{$usuario->tipo->name}}</td>
                                                    <td>{{\Carbon\Carbon::parse($usuario->created_at)->format('d/m/Y h:i A')}}</td>
													<td>
														{{$usuario->dispositivo}}
													</td>
                                                    <td>
                                                        <a class="btn btn-info" href="{{action('UsuariosController@edit', ['id' => $usuario->id])}}">Editar</a>
                                                        @if($usuario->tipo->name == 'tienda')
                                                        <a class="btn btn-warning" href="{{action('CoordenadasController@edit', ['id' => $usuario->id])}}">Coord.</a>
                                                        <button type="button" id="" value="{{$usuario->id}}" class="btn btn-success mostrarTienda">Detalles</button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>
											<div>
												{{ $usuarios->links() }}
											</div>
											<div>
												<a href="{{route('usuarios.create')}}" class="btn btn-warning m-b-5 m-t-5">Nuevo</a>
											</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog masGrande " role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<!-- grafica 1 -->
            <div class="row">
				<div class="col-12 col-sm-12">
					<div class="card botonesDias">

						<div class="row" >
							<div class="col-md">
								<button type="button" id="Lunes" class="btn btn-primary dias"  value="Lunes">Lunes</button>
							</div>
							<div class="col-md">
								<button type="button" id="Martes" class="btn btn-secondary dias" value="Martes">Martes</button>
							</div>
							<div class="col-md">
								<button type="button" id="Miercoles" class="btn btn-success dias" value="Miercoles">Miercoles</button>
							</div>
							<div class="col-md">
								<button type="button" id="Jueves" class="btn btn-danger dias"  value="Jueves">Jueves</button>
							</div>
							<div class="col-md">
								<button type="button" id="Viernes" class="btn btn-warning dias"  value="Viernes">Viernes</button>
							</div>
							<div class="col-md">
								<button type="button" id="Sabado" class="btn btn-info dias"  value="Sabado">Sábado</button>
							</div>
							<div class="col-md">
								<button type="button" id="Domingo" class="btn btn-dark dias"  value="Domingo">Domingo</button>
							</div>

						</div>
					</div>
				</div>
						
			</div>

            <div class="row LunesCol" >
				<div class="col-12 col-sm-12">
					<div class="card">
						<div class="card-header">
							<h4>Estadística de los pedidos por hora</h4>
						</div>
						<div class="card-body text-center">
						<canvas id="canvas" height="480" width="1200"></canvas>
						</div>
					</div>
				</div>
			</div>
		<!-- grafica 1 -->

		<!-- grafica 2 -->

			<div class="row" id="recarga">
				<div class="col-12 col-sm-12">
					<div class="card">
						<div class="card-header">
							<h4>Ventas del mes</h4>
						
							<div  class="row" >
								<div class="col-md-6">
            						<label>Año</label>
            						<select class="form-control dynamicAnio dynamicMes" id="anio_sel" >
										<?php $year = date("Y");  for($i=2016;$i<$year;$i++) { echo "<option value='".$i."'>".$i."</option>"; } echo "<option value='".$year."' selected>".$year."</option>" ;  ?>
            						</select>
								</div>
								<div class="col-md-6">
								                  <label>Mes</label>
								                  <select class="form-control dynamicMes" id="mes_sel" >
								                    <option value="1">ENERO</option>
								                    <option value="2">FEBRERO</option>
								                    <option value="3">MARZO</option>
								                    <option value="4">ABRIL</option>
								                    <option value="5">MAYO</option>
								                    <option value="6">JUNIO</option>
								                    <option value="7">JULIO</option>
								                    <option value="8">AGOSTO</option>
								                    <option value="9">SEPTIEMBRE</option>
								                    <option value="10">OCTUBRE</option>
								                    <option value="11">NOVIEMBRE</option>
								                    <option value="12">DICIEMBRE</option>
								                  </select>
								</div>
							</div>
						
						</div>
						<input type="hidden" value="" id="tiendaid">
						<div class="card-body text-center">
						<canvas id="canvas2" height="480" width="1200"></canvas>
						</div>
					</div>
				</div>
			</div>

		<!-- grafica 2 -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<!-- modal -->
<input type="hidden" value="{{ csrf_token()}}" name="_token">                       
@endsection

@section('libreriasjavascript')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script> -->
<!-- <script src="{{url('assets/plugins/Chart.js/dist/Chart.js')}}"></script> -->
<script src="{{url('assets/plugins/Chart.js/dist/char.edit.js')}}"></script>

@endsection

@section('javascript')
<script type="text/javascript">
var todosJunto=0;
var horaLunes=0;
var horaMartes = 0;
var horaMiercoles = 0;
var horaJueves = 0;
var horaViernes = 0;
var horaSabado = 0;
var horaDomingo = 0;
var mes = <?php echo date("m"); ?>;
var anio = <?php echo date("Y"); ?>;
document.getElementById("mes_sel").value = mes;

      
        
    $(document).ready(function($) {


		    //iniciar graficas dia



				var barChartData = {
    			    labels: ["12:00 AM","1:00 AM","2:00 AM","3:00 AM","4:00 AM","5:00 AM","6:00 AM","7:00 AM","8:00 AM","9:00 AM","10:00 AM","11:00 AM"
							,"12:00 AM","1:00 PM","2:00 PM","3:00 PM","4:00 PM","5:00 PM","6:00 PM","7:00 PM","8:00 PM","9:00 PM","10:00 PM","11:00 PM"],
    			    datasets: [],
						options: { scaleLabel: function (label) { return Math.round(label.value); } } 
    			};
				
    			var ctx = document.getElementById("canvas");
    			var canvas = new Chart(ctx, {
    			    type: 'bar',
    			    data: barChartData,
    			    options: {

    			        elements: {
    			            rectangle: {
    			                borderWidth: 2,
    			                borderColor: 'rgb(0, 255, 0)',
    			                borderSkipped: 'bottom'
    			            }
    			        },

    			        responsive: true,
    			        title: {
    			            display: true,
    			            text: 'Ventas '
    			        },

						scales: {
						    yAxes: [{
						        ticks: {
						            beginAtZero: true,
						            userCallback: function(label, index, labels) {
						                if (Math.floor(label) === label) {
						                    return label;
						                }
						            },
						        }
						    }],
						},

						//no
    			    }
    			});
			//iniciar graficas dia




			//graficas mes año


				var barChartData2 = {
    			    	labels: [],
    			    	datasets: [{
    			    	    label: 'Efectivo',
    			    	    backgroundColor: "rgba(242, 121, 121, 0.69)",
    			    	    data: []
    			    	},{
    			    	    label: 'POS',
    			    	    backgroundColor: "rgba(0, 0, 0, 0.69)",
    			    	    data: []
    			    	}]
    			};


    	    	var ctx2 = document.getElementById("canvas2").getContext("2d");
    	    	canvas2 = new Chart(ctx2, {
    	    	    type: 'line',
    	    	    data: barChartData2,
    	    	    options: {
    	    	        elements: {
    	    	            rectangle: {
    	    	                borderWidth: 2,
    	    	                borderColor: 'rgb(0, 255, 0)',
    	    	                borderSkipped: 'bottom'
    	    	            }
    	    	        },
    	    	        responsive: true,
    	    	        title: {
    	    	            display: true,
    	    	            text: 'Ventas Mensuales'
						}
						
						
    	    	    }//no
    	    	});


			//graficas mes año

	

		//var mes por anio

		//var mes por anio




        $('#myModal').appendTo("body");
        $(".mostrarTienda").click(function() { 

            $('#myModal').modal('show')
            var id=$(this).val();

            // var nombre= link.attr("id");
            // var link = $(e.relatedTarget);


                    var _token = $('input[name="_token"]').val();
                    
                    
                    
                        $.ajax({
                        url:"{{route('usuarios.modalDetalles')}}",
                        method:"POST",
                        data:{id:id, _token:_token},
                                success:function(data){
									canvas.config.data.datasets=[];
									
                                    todosJunto = $.parseJSON(data);
									horaLunes = todosJunto[0];
									horaMartes = todosJunto[1];
									horaMiercoles = todosJunto[2];
									horaJueves = todosJunto[3];
									horaViernes = todosJunto[4];
									horaSabado = todosJunto[5];
									horaDomingo = todosJunto[6];
									canvas.config.data.datasets=[]
									diasPos =Object.values(todosJunto[7]);
									diasEfectivo =Object.values(todosJunto[8]);
									nombreDias =Object.values(todosJunto[9]);
									barChartData2.datasets[0].data.length=[];
					 				barChartData2.datasets[1].data.length=[];
									barChartData2.labels.length=[];
									for(i=0; i < diasEfectivo.length; i++){

										barChartData2.datasets[0].data.push(diasEfectivo[i]);
										barChartData2.datasets[1].data.push(diasPos[i]);
										barChartData2.labels.push(nombreDias[i]);
										
									}
									canvas2.update();
									
									$('.modal-title').text(todosJunto[10]);
									$('#tiendaid').val(todosJunto[11])
									document.getElementById("mes_sel").value = mes;
									document.getElementById("anio_sel").value = anio;

                                    var hoy = new Date();
				                    var days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
				                    var d = new Date(hoy);
				                    var dayName = days[d.getDay()];
                                    
				                    mostrarDias(dayName); 


                                }       

                        });

          
          
		
			
		});

		//grafico dias
			var borrar=0;

				$(".dias").click(function() {
					
					var boton=$(this).val();
					mostrarDias(boton);
				});
			
			function mostrarDias(mostrar){
						var prefijo="hora";


						if(canvas.config.data.datasets.findIndex( item => item.label == mostrar) >= 0){

							borrar=canvas.config.data.datasets.findIndex( item => item.label == mostrar);
				    		canvas.config.data.datasets.splice(borrar,1);	
				    		canvas.update();
						}else{

							var barChartData = {  
   				    		        label: mostrar,
   				    		        backgroundColor: $("#"+mostrar).css("background-color"),
				    					borderColor: 'rgba(255, 99, 132,0)',
   				    		        data: eval(prefijo+mostrar) 
   				    			};

				    			canvas.config.data.datasets.push(barChartData);	
				    			canvas.update();


						}			

			};
		//grafico dias

		//graficas del mes dinamico
			$('.dynamicMes').change(function(){
			
				var mes= $("#mes_sel").val();
				var anio= $("#anio_sel").val();
				var id= $("#tiendaid").val();

				var _token = $('input[name="_token"]').val();

				$.ajax({
					url:"{{route('usuarios.graficasDinamicas')}}",
					method:"POST",
					data:{mes:mes ,anio:anio, id:id, _token:_token},
				
					success:function(result)
					{	
						
					 	var nombresDias=result[0];
					 	var pagosEfectivo=result[1];
					 	var pagosPos=result[2];
					 	nombresDias =Object.values(nombresDias)
					 	pagosEfectivo =Object.values(pagosEfectivo)
					 	pagosPos =Object.values(pagosPos)
					 	barChartData2.datasets[0].data.length=[];
					 	barChartData2.datasets[1].data.length=[];
					 	barChartData2.labels.length=[];
						
					 	for(i=0; i < nombresDias.length; i++){
							barChartData2.datasets[0].data.push(pagosEfectivo[i]);
					 		barChartData2.datasets[1].data.push(pagosPos[i]);
					  		barChartData2.labels.push(nombresDias[i]);
					 	}
						 canvas2.update();	 
					 
					}
				
				})


			});
		//graficas del mes dinamico


       

    })




</script>
@endsection