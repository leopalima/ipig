@extends('esquema.esquema')

@if (!session()->has('back_previous_button'))
  {{session()->put('back_previous_button', url()->previous())}}
@endif

@section('libreriascss')
    {{-- Formularios --}}
        <!--Morris css-->
        <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
    {{-- Formularios avanzados --}}
    <!--Bootstrap-daterangepicker css-->
    <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">

    <!--Bootstrap-datepicker css-->
    <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}">

    <!--iCheck css-->
    <link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}">

    <!--Bootstrap-colorpicker css-->
    <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}">

    <!--Bootstrap-timepicker css-->
    <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">

    <!--Select2 css-->
    <link rel="stylesheet" href="{{url('assets/plugins/select2/select2.css')}}">

    {{-- Notificaciones emergentes --}}
        <!--Toastr css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">


@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Editar usuario</h4>
                                    </div>
                                    <div class="card-body">
                                        <form enctype="multipart/form-data" class="form-horizontal" method="POST" action="{{action('UsuariosController@update', ['id' => $usuario->id])}}">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Nombre</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="name" name="name" class="form-control" value="{{old('name') ? old('name') : $usuario->name}}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="email">Email</label>
                                                <div class="col-md-9">
                                                    <input type="email" id="email" name="email" class="form-control" value='{{$usuario->email}}' placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="email">Teléfono</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="phone" name="phone" class="form-control" value='{{$usuario->phone}}' placeholder="Teléfono">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="descripcion">Descripción</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="descripcion" name="descripcion" class="form-control" value='{{$usuario->descripcion}}' placeholder="Descripción">
                                                </div>
                                            </div>
                                            @if($usuario->tipo->name=="tienda")
                                            
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Tiempo entrega (min)</label>
                                                <div class="col-md-9">
                                                    <select id="tiempoEntrega" name="tiempoEntrega" id="tipo"class="form-control">
                                                        <?php  for($i=5;$i<125;$i=$i+5) { echo "<option class='selectedTiempo' value='".$i."'>".$i."</option>"; }  ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Costo de envío</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="costoEnvio" name="costoEnvio"class="form-control" value="{{$usuario->costoEnvio}}" placeholder="Indique costo de envío">
                                                    <input type="hidden" id="configName" name="configName"class="form-control" value="configname">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">% Comisión</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="prctjcomision" name="prctjcomision"class="form-control" value="{{$usuario->prctjcomision}}" placeholder="Indique costo de envío">
                                                </div>
                                            </div>
                                            @endif
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Contraseña</label>
                                                <div class="col-md-9">
                                                    <input type="password" id="password" name="password"class="form-control" placeholder="Si desea cambiarla escriba la nueva contraseña">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Tipo de usuario</label>
                                                <div class="col-md-9">
                                                    <select id="tipo" name="tipo" id="tipo" class="form-control">
                                                        @foreach($tipos as $tipo)
                                                        <option value="{{$tipo->id}}" {{$tipo->id == $usuario->tipo_id ? 'selected' : ''}} >{{$tipo->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Estatus</label>
                                                <div class="col-md-9">
                                                    @if($usuario->tipo->name=='cliente')
                                                    <select id="validate" name="validate" class="form-control">
                                                        <option value="1" {{($usuario->validate) ? 'selected' : ''}} >Activo</option>
                                                        <option value="0" {{($usuario->validate) ? '' : 'selected'}} >Inactivo</option>
                                                    </select>    
                                                    @else
                                                    <select id="estatus" name="estatus" class="form-control">
                                                        <option value="1" {{($usuario->estatus) ? 'selected' : ''}} >Activo</option>
                                                        <option value="0" {{($usuario->estatus) ? '' : 'selected'}} >Inactivo</option>
                                                    </select>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Foto</label>
                                                <div class="col-md-9">
                                                    <input type="file" accept="image/*" id="foto" name="foto" class="form-control">
                                                    <div id="errorFoto" name="errorFoto" class="alert alert-danger mb-lg-0" hidden>
                                                    </div>
                                                </div>
                                                <label class="col-md-3 col-form-label"></label>
                                                @if($usuario->foto)
                                                <div class="col-md-9">
                                                    <img id="imgFoto" src="{{$usuario->foto}}" alt="Smiley face" height="200" width="300">
                                                </div>
                                                @endif
                                            </div>
                                            <div class="form-group row classEtiqueta">
                                                <label class="col-md-3 col-form-label">Etiquetas</label>
                                                <div class="col-md-9">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped mb-0 text-nowrap">
                                                            <tr>
                                                                <th>Etiqueta</th>
                                                                <th></th>
                                                                <th>Etiqueta</th>
                                                                <th></th>
                                                            </tr>
                                                            @php
                                                                $parEtiqueta = $etiquetas->chunk(2);
                                                            @endphp
                                                            @foreach($parEtiqueta as $etq)
                                                            <tr>
                                                                @foreach($etq as $e)
                                                                <td>{{$e->etiqueta}}</td>
                                                                <td>
                                                                    @php
                                                                        $usuarioEtiquetas = collect($usuario->etiquetas);
                                                                        $checked = $usuarioEtiquetas->contains($e->etiqueta) ? "checked" : "" ;
                                                                    @endphp
                                                                    <label class="custom-switch">
                                                                        <input name="etiquetas[]" value="{{$e->etiqueta}}" type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" {{$checked}}>
                                                                        <span class="custom-switch-indicator"></span>
                                                                    </label>
                                                                </td>
                                                                @endforeach
                                                            </tr>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mb-0 mt-2 row justify-content-end">
                                                <div class="col-md-9">
                                                    <button type="submit" name="btnGuardar" id="btnGuardar" class="btn btn-info">Guardar</button>
                                                    <a href="{{ session()->get('back_previous_button')}}" class="btn btn-warning">Volver</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- fsdjfndsjfnsdjkfnjsdkfnjksdnfjksdnfjksdnfjknsdkjfnsdjkfndsjkfnjksdnfsdjkfnsdjkfnksd -->
                        
                        <div class="row">
                            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Datos</h4>
                                    </div>
                                    <div class="card-body">
                                        <form enctype="multipart/form-data" class="form-horizontal" method="POST" action="">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">ID</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="name6" name="name6" class="form-control" value="{{$usuario->id}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="descripcion">Dirección</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="direccion" name="direccion" class="form-control" value='{{$usuario->direccion}}' readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="descripcion">Fecha de creación</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="dpcion3" name="despcion3" class="form-control" value="{{\Carbon\Carbon::parse($usuario->created_at)->format('d/m/Y H:m A')}}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="descripcion">Fecha de actualización</label>
                                                <div class="col-md-9">
                                                    <input type="text" id=cripcion3" name="dripcion3" class="form-control" value="{{\Carbon\Carbon::parse($usuario->updated_at)->format('d/m/Y H:m A')}}" readonly>
                                                </div>
                                            </div>        
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="descripcion">Token</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="dscripcion3" name="descrion3" class="form-control" value='{{$usuario->fcm_token}}' readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="email">Latitud</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="emai3" name="email3" class="form-control" value='{{$usuario->latitud}}' readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="email">Longitud</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="phone3" name="phone3" class="form-control" value='{{$usuario->longitud}}' readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label" for="descripcion">Ver Dirección</label>
                                                <div class="col-md-9">
                                                    <a href="http://www.google.com/maps/place/{{$usuario->latitud}},{{$usuario->longitud}}" target="_blank">Ver Dirección en Google maps</a>
                                                </div>
                                            </div>
                                            

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
@if(session('actualizado'))
    <div style="display: none;" id="actualizado" name="actualizado" class="alert alert-success" data-mensaje="{{session('actualizado')}}"></div>
@endif
@if(session('error'))
    <div style="display: none;" id="error" name="error" class="alert alert-success" data-mensaje="{{session('error')}}"></div>
@endif
@if($errors->has('name'))
    <div style="display: none;" id="errorName" name="errorName" class="alert alert-success" data-mensaje="{{$errors->first('name')}}"></div>
@endif

@endsection

@section('libreriasjavascript')
{{-- Formularios avanzados --}}
    <!--Select2 js-->
    <script src="{{url('assets/plugins/select2/select2.full.js')}}"></script>
    <!--Inputmask js-->
    <script src="{{url('assets/plugins/inputmask/jquery.inputmask.js')}}"></script>
    <!--Moment js-->
    <script src="{{url('assets/plugins/moment/moment.min.js')}}"></script>
    <!--Bootstrap-daterangepicker js-->
    <script src="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!--Bootstrap-datepicker js-->
    <script src="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
    <!--Bootstrap-colorpicker js-->
    <script src="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
    <!--Bootstrap-timepicker js-->
    <script src="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
    <!--iCheck js-->
    <script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
    <!--forms js-->
    <script src="{{url('assets/js/forms.js')}}"></script>
        
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {
            if($('#actualizado').length){
                $mensaje = $('#actualizado').data('mensaje');
                notificacion($mensaje, 'success');
            }
            if($('#error').length){
                $mensaje = $('#error').data('mensaje');
                notificacion($mensaje, 'error');
            }
            if($('#errorName').length){
                $mensaje = $('#errorName').data('mensaje');
                notificacion($mensaje, 'error');
            }
        });
        $(document).ready(function($) {
            var tiempoEntrega = <?php echo $usuario->tiempoEntrega; ?>;
            $('option[value=' + tiempoEntrega + ']').attr('selected',true);

            if($('#tipo option:selected').text() == 'tienda'){
                $(".classEtiqueta").show();
            }else
            {
                $(".classEtiqueta").hide();
            }

            $('#tipo').change(function(){
            
                if($('#tipo option:selected').text() == 'tienda'){
                    $(".classEtiqueta").fadeIn();
                }else{
                    $(".classEtiqueta").fadeOut();
                }
            
            });
        })

    </script>
@endsection