@extends('esquema.esquema')

@section('libreriascss')
    {{-- Formularios --}}
        <!--Morris css-->
        <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
    {{-- Formularios avanzados --}}
        <!--Bootstrap-daterangepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">

        <!--Bootstrap-datepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}">

        <!--iCheck css-->
        <link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}">

        <!--Bootstrap-colorpicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}">

        <!--Bootstrap-timepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">

        <!--Select2 css-->
        <link rel="stylesheet" href="{{url('assets/plugins/select2/select2.css')}}">

    {{-- Wysiwing Editor --}}
        <!--Bootstrap-wysihtml5 css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    {{-- Notificaciones emergentes --}}
        <!--Toastr css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">

@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Ubicación de pedidos</h4>
                                    </div>
                                    <div class="card-body">
                                        <form class="form-horizontal">
                                                <div class="form-group row">
                                                    <label class="col-md-2 col-form-label">Hasta</label>
                                                    <div class="col-md-3">
                                                        <input class="form-control" type="date" name="date" value="{{Carbon\Carbon::createFromFormat('d/m/Y', date('d/m/Y'))->format('Y-m-d')}}">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button class="btn btn-info" type="button" name="btnMostrar" onclick="mostrarPedidos()">Mostrar</button>
                                                    </div>
                                                </div>
                                            </form>
                                    <div class="form-group">
                                        <div id="address-map-container" style="width:100%;height:600px; ">
                                            <div style="width: 100%; height: 100%" id="map"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
@endsection

@section('libreriasjavascript')
    {{-- Formularios avanzados --}}
        <!--Select2 js-->
        <script src="{{url('assets/plugins/select2/select2.full.js')}}"></script>
        <!--Inputmask js-->
        <script src="{{url('assets/plugins/inputmask/jquery.inputmask.js')}}"></script>
        <!--Moment js-->
        <script src="{{url('assets/plugins/moment/moment.min.js')}}"></script>
        <!--Bootstrap-daterangepicker js-->
        <script src="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
        <!--Bootstrap-datepicker js-->
        <script src="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
        <!--Bootstrap-colorpicker js-->
        <script src="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
        <!--Bootstrap-timepicker js-->
        <script src="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
        <!--iCheck js-->
        <script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
        <!--forms js-->
        <script src="{{url('assets/js/forms.js')}}"></script>

    {{-- Wysiwing Editor --}}
        <!--ckeditor js-->
        <script src="{{url('assets/plugins/tinymce/tinymce.min.js')}}"></script>
        <!--Scripts js-->
        <script src="{{url('assets/js/formeditor.js')}}"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places,drawing,geometry&callback=initMap" async defer></script>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {

            $("#btnGuardarAreas").click(function(){
                enviarCoordenadas();
            });
            
        });

        // If you're adding a number of markers, you may want to drop them on the map
        // consecutively rather than all at once. This example shows how to use
        // window.setTimeout() to space your markers' animation.

        var motorizados = [];
        var tiendas = [];
        var pedidos = [];

        var markers = [];
        var markersTiendas = [];
        var markersPedidos = [];
        var map;
        var iconos = [
            '{{ asset("/assets/img/moto.png") }}',
            '{{ asset("/assets/img/comercio.png") }}',
            '{{ asset("/assets/img/cliente.png") }}',
        ];

        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 13,
                center: {lat: -12.102340, lng: -77.025824},
                mapTypeControl: false,
            });
            // var centerControlDiv = document.getElementById('floating-panel');
            // map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
        }

        function drop() {
            obtenerCoordenadasMotorizados();
            setTimeout(function() {
                clearMarkers();
                for (var i = 0; i < motorizados.length; i++) {
                    addMarkerWithTimeout(motorizados[i], i * 0, motorizados[i].name);
                }
            }, 0);
        }

        function addMarkerWithTimeout(position, timeout, name) {
            window.setTimeout(function() {
                markers.push(new google.maps.Marker({
                position: position,
                map: map,
                zIndex: 100,
                label: {
                    color: 'black',
                    fontWeight: 'bold',
                    text: name,
                  },
                icon: {
                    labelOrigin: new google.maps.Point(19, 0),
                    url: iconos[0],
                    size: new google.maps.Size(38, 38),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(11, 40),
                  }
                }));
            }, timeout);
        }

        function addMarkerWithTimeoutTiendas(position, timeout, name) {
            window.setTimeout(function() {
                markersTiendas.push(new google.maps.Marker({
                position: position,
                map: map,
                label: name.substring(0,1),
                title: name,
                icon: {
                    labelOrigin: new google.maps.Point(10, 10),
                    url: iconos[1],
                    size: new google.maps.Size(38, 38),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(11, 40),
                  }
                }));
            }, timeout);
        }

        function addMarkerWithTimeoutPedidos(position, timeout, name, index) {
            contenido = `
                Pedido: <b>${name.codigo}</b><br>
                Cliente: <b>${name.cliente}</b><br>
                Comercio: <b>${name.tienda}</b><br>
                Motorizado: <b>${name.direccion}</b>
            `
            var infowindow = new google.maps.InfoWindow({
                content: contenido
              });
            window.setTimeout(function() {
                markersPedidos.push(new google.maps.Marker({
                position: position,
                map: map,
                label: name.codigo.substring(0,1),
                title: name.cliente + "<br>" + name.motorizado,
                zIndex: 200,
                icon: {
                    labelOrigin: new google.maps.Point(19, 14),
                    url: iconos[2],
                    size: new google.maps.Size(38, 38),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(11, 40),
                  }
                }));
                markersPedidos[index].addListener('click', function() {
                    infowindow.open(map, markersPedidos[index]);
                  });
            }, timeout);
        }

        function clearMarkers() {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers = [];
        }

        function clearMarkersPedidos() {
            for (var i = 0; i < markersPedidos.length; i++) {
                markersPedidos[i].setMap(null);
            }
            markersPedidos = [];
        }

        // function obtenerCoordenadasMotorizados(){
        //    $.get("{{route('coordenadas.motorizados')}}", function(d){
        //         motorizados =  d;
        //    });
        // }

        function obtenerCoordenadasTienda(){
           $.get("{{route('ubicar.pedidos.coord.tienda')}}", function(d){
                tiendas =  d;
           });
        }

        function obtenerCoordenadasPedidos(){
           $.get("{{route('ubicar.pedidos.obtener')}}", function(d){
                pedidos =  d;
           });
        }

        function mostrarMotos(){
            drop();
            setInterval(drop, 3000);
        }

        function mostrarTienda(){
            obtenerCoordenadasTienda();
            setTimeout(function() {
                for (var i = 0; i < tiendas.length; i++) {
                    addMarkerWithTimeoutTiendas(tiendas[i], i * 200, tiendas[i].name);
                }
            }, 2500);
        }

        function mostrarPedidos(){
            mostrarTienda();
            dropPedidos();
            setInterval(dropPedidos, 10000);
        }

        function dropPedidos(){
            obtenerCoordenadasPedidos();
            setTimeout(function() {
                clearMarkersPedidos();
                for (var i = 0; i < pedidos.length; i++) {
                    addMarkerWithTimeoutPedidos(pedidos[i], i * 0, pedidos[i], i);
                }
            }, 500);
        }

    </script>
  
@endsection