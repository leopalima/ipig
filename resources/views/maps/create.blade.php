@extends('esquema.esquema')

@section('libreriascss')
    {{-- Formularios --}}
        <!--Morris css-->
        <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
    {{-- Formularios avanzados --}}
        <!--Bootstrap-daterangepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">

        <!--Bootstrap-datepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}">

        <!--iCheck css-->
        <link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}">

        <!--Bootstrap-colorpicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}">

        <!--Bootstrap-timepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">

        <!--Select2 css-->
        <link rel="stylesheet" href="{{url('assets/plugins/select2/select2.css')}}">

    {{-- Wysiwing Editor --}}
        <!--Bootstrap-wysihtml5 css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    {{-- Notificaciones emergentes --}}
        <!--Toastr css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">

@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
							<div class="col-md-12">
								<div class="card">
                                    <div class="card-header">
										<h4>Ubicación y zona del comercio</h4>
									</div>
									<div class="card-body">
                                    <div class="form-group">
                                            <input type="text" id="address-input" name="address_address" value="{{$tienda->direccion}}" class="form-control map-input">
                                            <input type="hidden" name="address_latitude" id="address-latitude" value="{{$tienda->latitud}}" />
                                            <input type="hidden" name="address_longitude" id="address-longitude" value="{{$tienda->longitud}}" />
                                        </div>
                                        <div id="address-map-container" style="width:100%;height:600px; ">
                                            <div style="width: 100%; height: 100%" id="address-map"></div>
                                        </div>
									</div>
								</div>
							</div>
                        </div>
                        <div id="divBtnNuevoGrupo" class="row">
							<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group mb-0 mt-2 row justify-content-end">
                                            <div class="col-md-9" >
                                                <button onclick="removeLine();" type="button" name="btnRemoveLine" id="btnRemoveLine" class="btn btn-warning">Quitar áreas</button>
                                                <button type="button" name="btnGuardarAreas" id="btnGuardarAreas" class="btn btn-info">Guardar áreas</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


@csrf
<input type="hidden" name="urlEnviarCoordenadas" id="urlEnviarCoordenadas" value="{{action('CoordenadasController@update', ['id' => $id])}}">

@endsection

@section('libreriasjavascript')
    {{-- Formularios avanzados --}}
        <!--Select2 js-->
        <script src="{{url('assets/plugins/select2/select2.full.js')}}"></script>
        <!--Inputmask js-->
        <script src="{{url('assets/plugins/inputmask/jquery.inputmask.js')}}"></script>
        <!--Moment js-->
        <script src="{{url('assets/plugins/moment/moment.min.js')}}"></script>
        <!--Bootstrap-daterangepicker js-->
        <script src="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
        <!--Bootstrap-datepicker js-->
        <script src="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
        <!--Bootstrap-colorpicker js-->
        <script src="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
        <!--Bootstrap-timepicker js-->
        <script src="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
        <!--iCheck js-->
        <script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
        <!--forms js-->
        <script src="{{url('assets/js/forms.js')}}"></script>

    {{-- Wysiwing Editor --}}
        <!--ckeditor js-->
        <script src="{{url('assets/plugins/tinymce/tinymce.min.js')}}"></script>
        <!--Scripts js-->
        <script src="{{url('assets/js/formeditor.js')}}"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places,drawing,geometry&callback=initialize" async defer></script>
        <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5WO_983jAxhg6v-_AHH94FXMZzFUXDVo&callback=initMap" async defer></script> -->
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($) {

            $("#btnGuardarAreas").click(function(){
                enviarCoordenadas();
            });
            
        });

        var areaDibujada = [];
        var coordinates = [];
        var coordTienda = [];
        const map = "";

        function initialize() {

            $('form').on('keyup keypress', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });
            const locationInputs = document.getElementsByClassName("map-input");

            const autocompletes = [];
            const geocoder = new google.maps.Geocoder;
            for (let i = 0; i < locationInputs.length; i++) {

                const input = locationInputs[i];
                const fieldKey = input.id.replace("-input", "");
                const isEdit = document.getElementById(fieldKey + "-latitude").value != '' && document.getElementById(fieldKey + "-longitude").value != '';

                const latitude = parseFloat(document.getElementById(fieldKey + "-latitude").value) || -12.102535;
                const longitude = parseFloat(document.getElementById(fieldKey + "-longitude").value) || -77.025973;

                const map = new google.maps.Map(document.getElementById(fieldKey + '-map'), {
                    center: {lat: latitude, lng: longitude},
                    zoom: 13
                });
                
                const marker = new google.maps.Marker({
                    map: map,
                    position: {lat: latitude, lng: longitude},
                });

                marker.setVisible(isEdit);
                
                const autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.key = fieldKey;
                autocompletes.push({input: input, map: map, marker: marker, autocomplete: autocomplete});

                $.get("{{action('CoordenadasController@obtener', ['id' => $id])}}", function( data ) {
                    // coordTienda= JSON.stringify(data);
                    coordTienda= data;

                    var zonaCoord = coordTienda;
                    // coordinates = JSON.stringify(data);

                    var zonaDibujada = new google.maps.Polygon({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35,
                        map: map
                        });
                    zonaDibujada.setPath(zonaCoord);
                });

            }

            for (let i = 0; i < autocompletes.length; i++) {
                const input = autocompletes[i].input;
                const autocomplete = autocompletes[i].autocomplete;
                const map = autocompletes[i].map;
                const marker = autocompletes[i].marker;

                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    marker.setVisible(false);
                    const place = autocomplete.getPlace();

                    geocoder.geocode({'placeId': place.place_id}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            const lat = results[0].geometry.location.lat();
                            const lng = results[0].geometry.location.lng();
                            setLocationCoordinates(autocomplete.key, lat, lng);
                        }
                    });

                    if (!place.geometry) {
                        window.alert("No details available for input: '" + place.name + "'");
                        input.value = "";
                        return;
                    }

                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);
                    }
                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);

                    marker.addListener('click', function() {
                    infoWindow.setContent(infowincontent);
                    infoWindow.open(map, marker);
                    });

                });

                //Listen for any clicks on the map.
                google.maps.event.addListener(map, 'click', function(event) {                
                    //Get the location that the user clicked.
                    var clickedLocation = event.latLng;
                    //If the marker hasn't been added.
                    if(marker === false){
                        //Create the marker.
                        marker = new google.maps.Marker({
                            position: clickedLocation,
                            map: map,
                            draggable: true //make it draggable
                        });
                        //Listen for drag events!
                        google.maps.event.addListener(marker, 'dragend', function(event){
                            markerLocation();
                        });
                    } else{
                        //Marker has already been added, so just change its location.
                        marker.setPosition(clickedLocation);
                    }
                    //Get the marker's location.
                    var currentLocation = marker.getPosition();
                    //Add lat and lng values to a field that we can save.
                    document.getElementById('address-latitude').value = currentLocation.lat(); //latitude
                    document.getElementById('address-longitude').value = currentLocation.lng(); //longitude
                    // getGeolocation($("#address-latitude").val(),$("#address-longitude").val())

                    var ubicacion = $("#address-latitude").val() + "," + $("#address-longitude").val()

                    $.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + ubicacion + "&key={{ env('GOOGLE_MAPS_API_KEY') }}", function(data){
                        // console.log(data.results);
                        $("#address-input").val(data.results[0]['formatted_address']);
                    });
                });

                var infoWindow = new google.maps.InfoWindow();
                var drawingManager = new google.maps.drawing.DrawingManager({
                    // drawingMode: google.maps.drawing.OverlayType.MARKER,
                    drawingControl: true,
                    drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: ['polygon']
                    },
                    // markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
                    // circleOptions: {
                    //     fillColor: '#ffff00',
                    //     fillOpacity: 1,
                    //     strokeWeight: 5,
                    //     clickable: false,
                    //     editable: true,
                    //     zIndex: 1
                    // }
                });
                drawingManager.setMap(map);
                $a=0;
                google.maps.event.addListener(drawingManager, 'polygoncomplete', function(rectangle) {

                    // console.log(rectangle.getPath());

                    var polygonBounds = rectangle.getPath();

                    for(var i = 0 ; i < polygonBounds.length ; i++)
                    {
                        coordinates.push(polygonBounds.getAt(i).lat(), polygonBounds.getAt(i).lng());
                    }


                    areaDibujada[$a] = rectangle;
                    $a++;
                    console.log(coordinates);
                    console.log(areaDibujada);

                    var point = new google.maps.LatLng($("#address-latitude").val(), $("#address-longitude").val());

                    if(google.maps.geometry.poly.containsLocation(point, rectangle) == true){
                        console.log("Adentro");
                    }else{
                        console.log("Afuera");
                    }
                });

            }
            

        }

        function removeLine() {
            for($i=0; $i<areaDibujada.length; $i++){
                areaDibujada[$i].setMap(null);
            }
        }

function setLocationCoordinates(key, lat, lng) {
    const latitudeField = document.getElementById(key + "-" + "latitude");
    const longitudeField = document.getElementById(key + "-" + "longitude");
    latitudeField.value = lat;
    longitudeField.value = lng;
}


function markerLocation(){
    //Get location.
    var currentLocation = marker.getPosition();
    //Add lat and lng values to a field that we can save.
    document.getElementById('address-latitude').value = currentLocation.lat(); //latitude
    document.getElementById('address-longitude').value = currentLocation.lng(); //longitude
}

function enviarCoordenadas(){
    data = coordinates;
    latitud = $("#address-latitude").val();
    longitud = $("#address-longitude").val();
    direccion = $("#address-input").val();
    $.ajax({
            headers: {'X-CSRF-TOKEN':$("input[name=_token]").val()},
            method: "PUT",
            url: $("#urlEnviarCoordenadas").val(),
            data: {coord:data, latitud:latitud, longitud:longitud, direccion:direccion}
        })
        .done(function( msg ) {
            console.log( msg );
            location.reload();
        });
}

    </script>
@endsection