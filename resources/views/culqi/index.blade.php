@extends('esquema.esquema')

@section('libreriascss')
    {{-- Formularios --}}
        <!--Morris css-->
        <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
    {{-- Formularios avanzados --}}
        <!--Bootstrap-daterangepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}">

        <!--Bootstrap-datepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css')}}">

        <!--iCheck css-->
        <link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}">

        <!--Bootstrap-colorpicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}">

        <!--Bootstrap-timepicker css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}">

        <!--Select2 css-->
        <link rel="stylesheet" href="{{url('assets/plugins/select2/select2.css')}}">

    {{-- Wysiwing Editor --}}
        <!--Bootstrap-wysihtml5 css-->
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

    {{-- Notificaciones emergentes --}}
        <!--Toastr css-->
        <link rel="stylesheet" href="{{url('assets/plugins/toastr/build/toastr.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/toaster/garessi-notif.css')}}">


@endsection

@section('css')
@endsection

@section('contenido')
                        <div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-body">
                                    <form>
                                        <div>
                                            <label>
                                            <span>Correo Electrónico</span>
                                            <input type="text" size="50" data-culqi="card[email]" id="card[email]" value="@php echo uniqid() . '@algo.com'; @endphp">
                                            </label>
                                        </div>
                                        <div>
                                            <label>
                                            <span>Número de tarjeta</span>
                                            <input type="text" size="20" data-culqi="card[number]" id="card[number]" value="4111111111111111">
                                            </label>
                                        </div>
                                        <div>
                                            <label>
                                            <span>CVV</span>
                                            <input type="text" size="4" data-culqi="card[cvv]" id="card[cvv]" value="123">
                                            </label>
                                        </div>
                                        <div>
                                            <label>
                                            <span>Fecha expiración (MM/YYYY)</span>
                                            <input size="2" data-culqi="card[exp_month]" id="card[exp_month]" value="09">
                                            <span>/</span>
                                            <input size="4" data-culqi="card[exp_year]" id="card[exp_year]" value="2025">
                                            </label>
                                        </div>
                                        <div>
                                            <button id="buyButton"> PAGAR</button>
                                        </div>
                                    </form>

									</div>
								</div>
							</div>
						</div>
@endsection

@section('libreriasjavascript')
    {{-- Formularios avanzados --}}
        <!--Select2 js-->
        <script src="{{url('assets/plugins/select2/select2.full.js')}}"></script>
        <!--Inputmask js-->
        <script src="{{url('assets/plugins/inputmask/jquery.inputmask.js')}}"></script>
        <!--Moment js-->
        <script src="{{url('assets/plugins/moment/moment.min.js')}}"></script>
        <!--Bootstrap-daterangepicker js-->
        <script src="{{url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
        <!--Bootstrap-datepicker js-->
        <script src="{{url('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
        <!--Bootstrap-colorpicker js-->
        <script src="{{url('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
        <!--Bootstrap-timepicker js-->
        <script src="{{url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js')}}"></script>
        <!--iCheck js-->
        <script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
        <!--forms js-->
        <script src="{{url('assets/js/forms.js')}}"></script>

    {{-- Wysiwing Editor --}}
        <!--ckeditor js-->
        <script src="{{url('assets/plugins/tinymce/tinymce.min.js')}}"></script>
        <!--Scripts js-->
        <script src="{{url('assets/js/formeditor.js')}}"></script>

        <!-- Incluyendo .js de Culqi JS -->
        <script src="https://checkout.culqi.com/v2"></script>

        <script>
            Culqi.publicKey = 'pk_test_XG0pzDxJ21rrgcw4';
            Culqi.init();
        </script>

        <script>
        $('#buyButton').on('click', function(e) {
            // Crea el objeto Token con Culqi JS
            Culqi.createToken();
            e.preventDefault();
        });
        </script>


@endsection

@section('javascript')
    <script type="text/javascript">
        function culqi() {
            if (Culqi.token) { // ¡Objeto Token creado exitosamente!
                var token = Culqi.token.id;
                // alert('Se ha creado un token:' + JSON.stringify(Culqi.token));
                console.log(token)
                pasarToken(token);
                
            } else { // ¡Hubo algún problema!
                // Mostramos JSON de objeto error en consola
                console.log(Culqi.error);
                alert(Culqi.error.user_message);
            }
        };
{{-- Quitar comentario
        function pasarToken(token)
        {
            $.ajax({
                method: "POST",
                url: "http://ipig.test/api/tarjeta/guardar",
                data: { token_culqi: token }
                })
                .done(function( msg ) {
                    console.log( "Data Saved: " + JSON.stringify(msg) );
                });
        };
--}}

        $(document).ready(function($) {





        });
    </script>
@endsection