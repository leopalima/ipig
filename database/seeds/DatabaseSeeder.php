<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        /*
        * Crea los tipos de usuarios y los usuarios asociados
        */
        factory('App\Users_tipo', 'cliente', 1)->create()->each(function($u){
            $u->usuarios()->save(factory('App\User', 'cliente')->make());
        });

        factory('App\Users_tipo', 'tienda', 1)->create()->each(function($u){
            
            $u->usuarios()->save(factory('App\User', 'tienda')->make());

            for($i=0; $i<11;$i++){
                $u->usuarios()->save(factory('App\User', 'tiendasVarias')->make());
            }
            
        });
        // factory('App\Users_tipo', 'tiendasVarias', 10)->create()->each(function($u){
        //     $u->usuarios()->save(factory('App\User', 'tiendasVarias')->make());
        // });
        factory('App\Users_tipo', 'motorizado', 1)->create()->each(function($u){
            $u->usuarios()->save(factory('App\User', 'motorizado')->make());
        });
        factory('App\Users_tipo', 'root', 1)->create()->each(function($u){
            $u->usuarios()->save(factory('App\User', 'root')->make());
        });
        factory('App\Users_tipo', 'admin', 1)->create()->each(function($u){
            $u->usuarios()->save(factory('App\User', 'admin')->make());
        });

        /*
        * Crea las categorias de productos
        */
        factory('App\Producto_categorias', 'alimentos', 10)->create();
        factory('App\Producto_categorias', 'helados', 10)->create();
        factory('App\Producto_categorias', 'bebidas', 10)->create();
        
        /*
        * Crea las categorias de productos
        */
        factory('App\Productos', 'alimentos', 100)->create();

        /*
        * Crea los adicionales
        */
        factory('App\Adicionales', 20)->create();

        /*
        * Crea los items asociados a los adicionales
        */
        factory('App\Adicionales_items', 1000)->create();


    }
}
