<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaAdicionales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adicionales', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('titulo');
            $table->integer('maximo')->default(1);
            $table->boolean('estatus')->default(1);
            $table->char('tienda_id', 36);
            $table->foreign('tienda_id')
                    ->references('id')->on('users');
            $table->char('producto_id', 36);
            $table->foreign('producto_id')
                    ->references('id')->on('productos');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adicionales');
    }
}
