<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TMensajesayuda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensajes_ayuda', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('mensaje');
            $table->char('user_id', 36);
            $table->foreign('user_id')
                    ->references('id')->on('users');
            $table->char('atendido_por', 36)->nullable();
            $table->char('estatus', 120)->nullable()->default('abierto');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensajes_ayuda');
    }
}
