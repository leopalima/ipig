<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class COrdenamientoTProductoCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('producto_categorias', function (Blueprint $table) {
            $table->dropColumn('orden');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('producto_categorias', function (Blueprint $table) {
            $table->integer('orden')->default(20)->after('foto');
        });
    }
}
