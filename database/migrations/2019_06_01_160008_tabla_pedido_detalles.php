<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaPedidoDetalles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedido_detalles', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('pedido_id', 36);
            $table->foreign('pedido_id')
                ->references('id')->on('pedidos');
            $table->char('producto_id', 36);
            $table->foreign('producto_id')
                ->references('id')->on('productos');
            $table->char('tienda_id', 36);
            $table->foreign('tienda_id')
                ->references('id')->on('users');
            $table->char('cliente_id', 36);
            $table->foreign('cliente_id')
                ->references('id')->on('users');
            $table->char('tipo_producto', 120);
            $table->double('precio', 10, 4);
            $table->integer('cantidad');
            $table->char('estatus', 120);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_detalles');
    }
}
