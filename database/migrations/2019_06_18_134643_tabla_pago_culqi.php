<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaPagoCulqi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pago_culqi', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('pedido_id', 36);
            $table->foreign('pedido_id', 36)
                    ->references('id')->on('pedidos');
            $table->char('cliente_id', 36);
            $table->foreign('cliente_id')
                    ->references('id')->on('users');
            $table->char('tienda_id', 36);
            $table->foreign('tienda_id')
                    ->references('id')->on('users');
            $table->char('culqi_id', 120);
            $table->char('tarjeta_marca', 120);
            $table->char('tarjeta_numero', 120);
            $table->double('monto', 10, 4);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pago_culqi');
    }
}
