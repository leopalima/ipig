<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaAdicionalesItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adicionales_items', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('nombre');
            $table->double('precio', 8, 2);
            $table->char('tienda_id', 36);
            $table->boolean('estatus')->default(1);
            $table->foreign('tienda_id')
                    ->references('id')->on('users');
            $table->char('adicionales_id', 36);
            $table->foreign('adicionales_id')
                    ->references('id')->on('adicionales');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adicionales_items');
    }
}
