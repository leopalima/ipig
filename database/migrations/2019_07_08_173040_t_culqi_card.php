<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TCulqiCard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('culqi_card', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('user_id', 36);
            $table->foreign('user_id')
                    ->references('id')->on('users');
            $table->string('culqi_id')->nullable();
            $table->string('date')->nullable();
            $table->string('customer_id')->nullable();
            $table->string('card_number')->nullable();
            $table->string('last_four')->nullable();
            $table->string('active')->nullable();
            $table->string('card_brand')->nullable();
            $table->string('card_type')->nullable();
            $table->string('card_category')->nullable();
            $table->string('name')->nullable();
            $table->string('country')->nullable();
            $table->string('country_code')->nullable();
            $table->string('ip')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('culqi_card');
    }
}
