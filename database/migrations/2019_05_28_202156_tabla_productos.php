<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('name');
            $table->text('descripcion')->nullable();
            $table->double('precio', 10, 4);
            $table->char('tienda_id', 36);
            $table->char('categoria_id', 36);
            $table->foreign('tienda_id')
                ->references('id')->on('users');
            $table->foreign('categoria_id')
                ->references('id')->on('producto_categorias');
            $table->string('foto')->nullable();
            $table->boolean('estatus');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
