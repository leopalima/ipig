<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClaveForaneaTablaAdicItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adicionales_items', function (Blueprint $table) {
            $table->foreign('id')
                    ->references('id')->on('global_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adicionales_items', function (Blueprint $table) {
            $table->dropForeign(['id']);
        });
    }
}
