<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TUsuariosValidados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_validate', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo');
            $table->char('user_id', 36);
            $table->foreign('user_id')
                    ->references('id')->on('users');
            $table->boolean('validate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_validate');
    }
}
