<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->timestamp('fecha');
            $table->char('tienda_id', 36);
            $table->foreign('tienda_id')
                ->references('id')->on('users');
            $table->char('cliente_id', 36);
            $table->foreign('cliente_id')
                ->references('id')->on('users');
            $table->string('direccion');
            $table->double('latitud');
            $table->double('longitud');
            $table->char('estatus', 120);
            $table->char('tipo_pago', 120);
            $table->string('comentario')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
