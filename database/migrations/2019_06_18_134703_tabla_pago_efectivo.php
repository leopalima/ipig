<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaPagoEfectivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pago_efectivo', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('pedido_id', 36);
            $table->foreign('pedido_id')
                    ->references('id')->on('pedidos');
            $table->char('cliente_id', 36);
            $table->foreign('cliente_id')
                    ->references('id')->on('users');
            $table->char('tienda_id', 36);
            $table->foreign('tienda_id')
                    ->references('id')->on('users');
            $table->double('efectivo', 10, 4);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pago_efectivo');
    }
}
