<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TMotoPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moto_pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('pedido_id', 36);
            $table->foreign('pedido_id')
                    ->references('id')->on('pedidos');
            $table->char('estatus');
            $table->integer('tiempopreparacion');
            $table->timestamp('fecha')->nullable();
            $table->char('motorizado_id', 36)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moto_pedidos');
    }
}
