<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaProdcutosAdiconales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('productos_adiconales', function (Blueprint $table) {
        //     $table->char('producto_id', 36);
        //     $table->foreign('producto_id')
        //         ->references('id')->on('productos');
        //     $table->char('adicionales_id', 36);
        //     $table->foreign('adicionales_id')
        //         ->references('id')->on('adicionales');
        //     $table->char('tienda_id', 36);
        //     $table->foreign('tienda_id')
        //         ->references('id')->on('users');
        //     $table->boolean('estatus')->default(1);
        //     $table->timestamps();
        //     $table->softDeletes();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos_adiconales');
    }
}
