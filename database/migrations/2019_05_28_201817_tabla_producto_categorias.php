<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaProductoCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_categorias', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->char('tienda_id');
            $table->foreign('tienda_id')
                ->references('id')->on('users');
            $table->string('name');
            $table->text('descripcion')->nullable();
            $table->string('foto')->nullable();
            $table->integer('orden');
            $table->boolean('estatus');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_categorias');
    }
}
