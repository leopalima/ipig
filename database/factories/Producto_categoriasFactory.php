<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Producto_categorias;
use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;

$factory->defineAs(Producto_categorias::class, 'alimentos', function (Faker $faker) {
    return [
        'id' => Uuid::generate()->string,
        'name' => 'Alimentos',
        'descripcion' => 'Tipo de usuario cliente',
        'foto' => 'Aqui va una foto',
        'orden' => 1,
        'estatus' => 1,
        'tienda_id' => function() {
            $tienda = App\User::whereHas('tipo', function($q){
                                 $q->where('name', 'tienda');
                            })
                            ->inRandomOrder()
                            ->first();
            return $tienda->id;
        },
    ];
});

$factory->defineAs(Producto_categorias::class, 'helados', function (Faker $faker) {
    return [
        'id' => Uuid::generate()->string,
        'name' => 'Helados',
        'descripcion' => 'Tipo de usuario cliente',
        'foto' => 'Aqui va una foto',
        'orden' => 1,
        'estatus' => 1,
        'tienda_id' => function() {
            $tienda = App\User::whereHas('tipo', function($q){
                                 $q->where('name', 'tienda');
                            })
                            ->inRandomOrder()
                            ->first();
            return $tienda->id;
        },
    ];
});

$factory->defineAs(Producto_categorias::class, 'bebidas', function (Faker $faker) {
    return [
        'id' => Uuid::generate()->string,
        'name' => 'Bebidas',
        'descripcion' => 'Tipo de usuario cliente',
        'foto' => 'Aqui va una foto',
        'orden' => 1,
        'estatus' => 1,
        'tienda_id' => function() {
            $tienda = App\User::whereHas('tipo', function($q){
                                 $q->where('name', 'tienda');
                            })
                            ->inRandomOrder()
                            ->first();
            return $tienda->id;
        },
    ];
});