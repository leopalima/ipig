<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Productos;
use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;

$factory->defineAs(Productos::class, 'alimentos', function (Faker $faker) {

    $image = [
        '490437cf-32a0-48a6-bde8-88ebfabb5478.jpeg',
        '490437cf-32a0-48a6-bde8-88ebfabb5478.jpeg',
        '02f67b8b-93be-4b4e-b944-21c8a972bb0d.jpeg',
        'c4d3e293-396f-4d2b-9260-3a9f55358f63.jpeg',
        '2f6ed3af-5989-47f9-8b6e-febe0ee41b32.jpeg',
        '5adeccf9-9796-4ae9-a003-f06d7fb0ccf8.jpeg',
        ];
    
        $imageSelected = $image[rand(1,5)];



    return [
        'id' => Uuid::generate()->string,
        'name' => $faker->name,
        'descripcion' => 'Tipo de usuario cliente',
        'precio' => $faker->randomFloat(2,25,75),
        'foto' => url('storage/productos') . '/' . $imageSelected,
        'estatus' => 1,
        'tienda_id' => function() {
            $tienda = App\User::whereHas('tipo', function($q){
                                 $q->where('name', 'tienda');
                            })
                            ->inRandomOrder()
                            ->first();
            return $tienda->id;
        },
        'categoria_id' => function() {
            $categoria = App\Producto_categorias::all()->random();
            return $categoria->id;
        },
    ];
});
