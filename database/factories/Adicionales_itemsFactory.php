<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Adicionales_items;
use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;

$factory->define(Adicionales_items::class, function (Faker $faker) {
    return [
        'id' => Uuid::generate()->string,
        'nombre' => $faker->name,
        'precio' => $faker->randomFloat(2,1,10),
        'estatus' => 1,
        'tienda_id' => function() {
            $tienda = App\User::whereHas('tipo', function($q){
                                 $q->where('name', 'tienda');
                            })
                            ->inRandomOrder()
                            ->first();
            return $tienda->id;
        },
        'adicionales_id' => function() {
            $adicional = App\Adicionales::all()->random();
            return $adicional->id;
        },
    ];
});
