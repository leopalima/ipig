<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Users_tipo;
use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;

$factory->defineAs(Users_tipo::class, 'cliente', function (Faker $faker) {
    return [
        'id' => Uuid::generate()->string,
        'name' => 'cliente',
        'descripcion' => 'Tipo de usuario cliente',
    ];
});

$factory->defineAs(Users_tipo::class, 'tienda', function (Faker $faker) {
    return [
        'id' => Uuid::generate()->string,
        'name' => 'tienda',
        'descripcion' => 'Tipo de usuario tienda',
    ];
});

$factory->defineAs(Users_tipo::class, 'tiendasVarias', function (Faker $faker) {
    return [
        'id' => Uuid::generate()->string,
        'name' => 'tienda',
        'descripcion' => 'Tipo de usuario tienda',
    ];
});

$factory->defineAs(Users_tipo::class, 'motorizado', function (Faker $faker) {
    return [
        'id' => Uuid::generate()->string,
        'name' => 'motorizado',
        'descripcion' => 'Tipo de usuario motorizado',
    ];
});

$factory->defineAs(Users_tipo::class, 'root', function (Faker $faker) {
    return [
        'id' => Uuid::generate()->string,
        'name' => 'root',
        'descripcion' => 'Tipo de usuario SuperUsuario root',
    ];
});

$factory->defineAs(Users_tipo::class, 'admin', function (Faker $faker) {
    return [
        'id' => Uuid::generate()->string,
        'name' => 'admin',
        'descripcion' => 'Tipo de usuario administrador',
    ];
});
