<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Adicionales;
use Faker\Generator as Faker;
use Webpatser\Uuid\Uuid;

$factory->define(Adicionales::class, function (Faker $faker) {
    return [
        'id' => Uuid::generate()->string,
        'titulo' => $faker->name,
        'maximo' => $faker->randomDigit(1,10),
        'tienda_id' => function() {
            $tienda = App\User::whereHas('tipo', function($q){
                                 $q->where('name', 'tienda');
                            })
                            ->inRandomOrder()
                            ->first();
            return $tienda->id;
        },
        'producto_id' => function() {
            $producto = App\Productos::all()->random();
            return $producto->id;
        },
    ];
});
