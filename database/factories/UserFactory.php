<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Users_tipo;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->defineAs(User::class, 'cliente', function (Faker $faker) {
    return [
        'name' => $faker->name,
        'descripcion' => $faker->sentence,
        'email' => 'cliente@cliente.com',
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->defineAs(User::class, 'tienda', function (Faker $faker) {
    return [
        'name' => "TIENDA",
        'email' => 'tienda@tienda.com',
        'descripcion' => $faker->sentence,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'tipo_id' => function() {
            $tipo = Users_tipo::where('name', 'tienda')->first();
            return $tipo->id;
        },
    ];
});

$factory->defineAs(User::class, 'motorizado', function (Faker $faker) {
    return [
        'name' => $faker->name,
        'descripcion' => $faker->sentence,
        'email' => 'motorizado@motorizado.com',
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'tipo_id' => function() {
            $tipo = Users_tipo::where('name', 'motorizado')->first();
            return $tipo->id;
        },
    ];
});

$factory->defineAs(User::class, 'root', function (Faker $faker) {
    return [
        'name' => "ROOT",
        'descripcion' => $faker->sentence,
        'email' => 'root@root.com',
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'tipo_id' => function() {
            $tipo = Users_tipo::where('name', 'root')->first();
            return $tipo->id;
        },
    ];
});

$factory->defineAs(User::class, 'admin', function (Faker $faker) {
    return [
        'name' => "ADMIN",
        'descripcion' => $faker->sentence,
        'email' => 'admin@admin.com',
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'tipo_id' => function() {
            $tipo = Users_tipo::where('name', 'admin')->first();
            return $tipo->id;
        },
    ];
});


$factory->defineAs(User::class, 'tiendasVarias', function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'email' => 'tienda@'. $faker->lastName . random_int(20,30) .'.com',
        'descripcion' => $faker->sentence,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'tipo_id' => function() {
            $tipo = Users_tipo::where('name', 'tienda')->first();
            return $tipo->id;
        },
    ];
});