<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAPa5qXBY:APA91bHCI-GOzLFFukGy4MNUUm1em9ZrXTcJt_z83SDqOPgQC9ER7L8ii7FBFablckTAwOaW4utpA3jRsutx6yy2AkznyC8AxoDXwkq8RigZoYpvbxEarDNnLkZNyDptxGGC52RirEBH'),
        'sender_id' => env('FCM_SENDER_ID', '264919211030'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
