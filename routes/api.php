<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



/*
| Ruta de prueba Culqi
*/
// Route::get('culqi/creartoken', 'API\PagoCulqiController@crearToken');






/*
| Ruta para mostrar las tiendas
*/
Route::get('/tiendas', 'API\TiendasController@all');

/*
| Ruta para mostrar una tienda
*/
Route::get('/tiendas/{id}', 'API\TiendasController@one');

/*
| Ruta para mostrar una tienda
*/
Route::get('/tiendas/multimarcas/{id}', 'API\TiendasController@multiMarcas');

/*
| Ruta para buscar tiendas
*/
Route::get('/buscar/tiendas', 'API\TiendasController@buscar');

/*
| Ruta para buscar tiendas
*/
Route::get('/busqueda/{etiqueta}', 'API\TiendasController@buscarEtiqueta');

/*
| Ruta para mostrar las categorias una tienda
*/
Route::get('/tiendas/{id}/categorias', 'API\TiendasController@categorias');

/*
| Ruta para mostrar los productos de una categoria de una tienda
*/
Route::get('/tiendas/{id}/categorias/{categoria_id}/productos', 'API\TiendasController@categoriaProductos');

/*
| Ruta para iniciar sesión
*/
Route::post('login', 'API\AuthController@login');
Route::post('signup', 'API\AuthController@signup');
Route::post('password/recovery', 'API\AuthController@passwordRecovery');

/*
| Ruta para validar código de verificacion
*/
Route::get('validacion/codigo/{user_id}', 'API\AuthController@reenviarCodigo');
Route::put('validacion/codigo', 'API\AuthController@validarCodigo');

/*
| Ruta para guardar las calificaciones
*/
Route::get('etiquetas', 'API\EtiquetasController@obtenerEtiquetas');

/*
| Ruta que muestra el número de la última versión
*/
Route::get('version/control/{so}', 'API\VersionController@muestraVersion');

/*
| Ruta que muestra los banner de promoción
*/
Route::get('promocion/banner/', 'API\PromocionController@banner');


/*
| Rutas que necesitan autorizacion
*/
Route::group(['middleware' => 'auth:api'], function() {

    /*
    | Ruta para desloguearse
    */
    Route::get('logout', 'API\AuthController@logout');

    /*
    | Ruta que trae información del usuario logueado.
    */
    Route::get('usuario', 'API\AuthController@usuario');

    /*
    | Ruta que trae información del usuario logueado.
    */
    Route::put('usuario', 'API\AuthController@update');

    /*
    | Ruta que recibe el pedido
    */
    Route::post('pedido/recibir', 'API\PedidosController@recibirPedido');
    Route::put('pedido/cancelar/{id}', 'API\PedidosController@cancelarPedido');
    Route::put('pedido/aceptar/{id}', 'API\PedidosController@aceptarPedido');
    Route::get('pedido/mostrar/{id}', 'API\PedidosController@mostrarPedido');
    Route::put('pedido/encamino/{id}', 'API\PedidosController@encaminoPedido');
    Route::put('pedido/entregado/{id}', 'API\PedidosController@entregadoPedido');
    Route::get('cliente/pedidos', 'API\PedidosController@misPedidos');
    route::get('moto/pedidos/activos', 'API\PedidosController@motoPedidosActivos');

    /*
    | Ruta para guardar la tarjeta
    */
    Route::post('tarjeta/guardar', 'API\CulqiController@guardarTarjeta');
    Route::put('tarjeta/eliminar', 'API\CulqiController@eliminarTarjeta');

    /*
    | Ruta para guardar los mensajes de ayuda
    */
    Route::post('mensajes', 'API\MensajesAyudaController@recibirMensaje');

    /*
    | Ruta para guardar las calificaciones
    */
    Route::post('calificacion', 'API\CalificacionesController@recibirCalificacion');

    /*
    | Ruta para actualizar las coordenas
    */
    Route::put('coordenadas', 'API\CoordenadasController@actualizarCoordenadas');
    Route::get('coordenadas/consultar/{usuario_id}', 'API\CoordenadasController@consultarCoordenadas');


    // /*
    // | Ruta para mostrar las tiendas
    // */
    // Route::get('/tiendas', 'API\TiendasController@all');

    // /*
    // | Ruta para mostrar una tienda
    // */
    // Route::get('/tiendas/{id}', 'API\TiendasController@one');

    // /*
    // | Ruta para mostrar las categorias una tienda
    // */
    // Route::get('/tiendas/{id}/categorias', 'API\TiendasController@categorias');

    // /*
    // | Ruta para mostrar los productos de una categoria de una tienda
    // */
    // Route::get('/tiendas/{id}/categorias/{categoria_id}/productos', 'API\TiendasController@categoriaProductos');

});



