<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/password', function(){
    return redirect()->route('password.update');
});
Route::get('/resetcomplete', function(){
    Auth::logout();
    return view('auth.passwords.resetcomplete');
});

Route::get('terminos', function(){
    return response()->view('terminos.index', [], 200);
});

// Route::get('/', function(){
//     Route::auth();
// });

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

/*
|--------------------------------------------------------------------------
| Ruta para probar el esquema principal de las vistas
|--------------------------------------------------------------------------
*/
Route::get('/esquema', function(){
    return view('esquema.index');
});

/*
|--------------------------------------------------------------------------
| Rutas para usuarios autenticados
|--------------------------------------------------------------------------
*/
Route::middleware(['auth'])->group(function(){

    /*
    | Ruta para el dashboard
    |*/
    // Route::get('/', function(){
    //     return view('esquema.index');
    // });
    // Route::get('/dashboard', function(){
    //     return view('dashboard.index');
    // });
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');
    Route::post('/dashboard', 'DashboardController@graficasDatos')->name('dashboard.graficasDatos');

    /*
    | Ruta para el perfil del usuario
    |*/
    Route::resource('/perfil', 'PerfilController')->only(['index', 'show', 'edit', 'update']);
    Route::post('/perfil/emergencia', 'PerfilController@cerradoEmergencia')->name('perfil.cerradoemergencia');


    /*
    | CRUD para editar a los usuarios y sus tipos
    |*/
    Route::resource('/usuarios', 'UsuariosController');
    Route::resource('/tipos', 'UsuariosTipoController');

    /*
    | Ruta para los detalles de usuario tienda
    |*/
    Route::post('/usuarios/modaldetalles', 'UsuariosController@modalDetalles')->name('usuarios.modalDetalles');
    Route::post('/usuarios/graficos', 'UsuariosController@graficasDinamicas')->name('usuarios.graficasDinamicas');

    /*
    | CRUD para editar a los productos y sus categorias
    |*/
    Route::resource('/categorias', 'CategoriasProductoController', ['as' => 'tienda']);
    Route::resource('/productos', 'ProductosController', ['as' => 'tienda']);
    Route::resource('/adicionales', 'AdicionalesController', ['as' => 'tienda']);
     /*
    | ordenar productos y categorias
    |*/
    Route::get('/productos/ordenar/{id}', 'ProductosController@productosDeCategoria')->name('ordenar_productos.edit');
    Route::post('/productos/ordenar', 'ProductosController@productosDeCategoriaGuardar')->name('ordenar_productos.guardar');
    Route::post('/categorias/ordenar', 'CategoriasProductoController@CategoriaGuardarOrden')->name('ordenar_categorias.guardar');

    Route::resource('/coordenadas', 'CoordenadasController');
    Route::get('/coordenadas/obtener/{id}', 'CoordenadasController@obtener');

    /*
    |   Ruta que recibe el token para las notificaciones web
    */
    Route::post('tokenfcm/recibir', 'FCMtoken@recibirToken')->name('recibir.token');

    /*
    |   Ruta que muestra los pedidos
    */
    
    Route::get('pedidos/activos/{estatus?}', 'PedidosController@activos')->name('tienda.pedidos.activos');


    // Route::get('pedidos/historico', 'PedidosController@historico')->name('tienda.pedidos.historico');
    Route::get('pedidos', 'PedidosController@historico')->name('tienda.pedidos.historico');
    Route::get('pedidos/{id?}', 'PedidosController@pedido')->name('tienda.pedidos.pedido');
    Route::get('pedidos/aceptar/{id?}', 'PedidosController@aceptarPedido')->name('tienda.pedidos.aceptar');
    Route::get('pedidos/cancelar/{id?}', 'PedidosController@cancelarPedido')->name('tienda.pedidos.cancelar');
    Route::get('pedidos/enviar/{id?}', 'PedidosController@enviarPedido')->name('tienda.pedidos.enviar');
    Route::get('pedidos/entregar/{id?}', 'PedidosController@entregarPedido')->name('tienda.pedidos.entregar');
    Route::get('pedidos/listo/{id?}', 'PedidosController@listoEnviar')->name('tienda.pedidos.listo');
    
    /*
    |   Ruta que muestra los errores del sistema
    */
    Route::get('error', 'ErrorController@index')->name('error.index');
    Route::get('error/{id?}', 'ErrorController@buscar')->name('error.buscar');

    /*
    | CRUD para mensajes de ayuda
    |*/
    Route::get('/mensaje', 'MensajesAyudaController@index')->name('error.mensajes_ayuda');
    Route::get('/mensaje/{id}', 'MensajesAyudaController@edit')->name('mensajes_ayuda.edit');
    Route::post('/mensaje/{id}', 'MensajesAyudaController@update')->name('mensajes_ayuda.update');

    /*
    | CRUD para etiquetas
    |*/
    Route::get('/etiqueta', 'EtiquetasController@index')->name('error.etiquetas');
    Route::post('/etiqueta', 'EtiquetasController@store')->name('etiquetas.store');
    Route::get('/etiqueta/{id}', 'EtiquetasController@edit')->name('etiquetas.edit');
    Route::post('/etiqueta/{id}', 'EtiquetasController@update')->name('etiquetas.update');
    /*
    | Productos orden
    |*/
    // Route::get('/orden', 'ProductoCategoriaController@index')->name('productoCategoriaOrden.index');

    /*
    | Control de versiones
    |*/
    Route::resource('/version', 'VersionController');

    /*
    | Ubicación de motos
    |*/
    Route::get('/ubicacionmotos', function(){
        return view("maps.ubicamotos");
    })->name('ubicacion.motos');
    Route::get('/coordenadas/motorizados/todos', 'CoordenadasController@coordenadasMotorizados')->name('coordenadas.motorizados');
    Route::get('/coordenadas/tiendas/todos', 'CoordenadasController@coordenadasTiendas')->name('coordenadas.tiendas');
    Route::get('/coordenadas/pedidos/todos', 'CoordenadasController@coordenadasPedidos')->name('coordenadas.pedidos');

    /*
    | Ubicación de pedidos pendiente para cada tienda
    |*/
    Route::get('/ubicar/pedidos', 'UbicarPedidosController@index')->name('ubicar.pedidos.index');
    Route::get('/ubicar/pedidos/obtener', 'UbicarPedidosController@obtener')->name('ubicar.pedidos.obtener');
    Route::get('/ubicar/pedidos/coord/tienda', 'UbicarPedidosController@coordenadasTienda')->name('ubicar.pedidos.coord.tienda');

    Route::get('/administrar/pedido', 'Administrador\PedidosController@obtener')->name('administrar.pedidos');
    Route::post('/administrar/pedido', 'Administrador\PedidosController@modificar')->name('administrar.pedidos.modificar');
    
    Route::get('culqi', 'CulqiController@index')->name('culqi');

    /*
    | Promociones
    |*/
    Route::resource('promocion/banner', 'BannerController');
    Route::resource('promocion/notificaciones', 'NotificacionesController');

    /*
    | Emojis
    |*/
    Route::resource('emojis', 'EmojisController');

    /*
    | Ruta de Cambio de login
    |*/
    Route::get('/cambio/login/{id}', 'CambioLoginController@index')->name('cambio.login');

    /*
    | Ruta de Pruebas
    |*/
    Route::get('/pruebas', function(){

        // $mapsApiKey="AIzaSyC5WO_983jAxhg6v-_AHH94FXMZzFUXDVo";
        // $coordTienda = [
        //     "lat" => -12.102584,
        //     "long" => -77.025892
        // ];

        // $coordCliente = [
        //     "lat" => -12.099347,
        //     "long" => -76.998089
        // ];

        // // $coordCliente = [
        // //     "lat" => -12.102179,
        // //     "long" => -77.021896
        // // ];

        // $ruta= "https://maps.googleapis.com/maps/api/directions/json?origin=". $coordTienda['lat'] .",". $coordTienda['long'] ."&destination=". $coordCliente['lat'] .",". $coordCliente['long'] ."&units=metric&key=". $mapsApiKey ."&alternatives=true";
        
        // $json = file_get_contents($ruta);
        // $obj = json_decode($json);

        // $cantRutasAlternativas = count($obj->routes);
        // $rutaMenor = 99999;

        // for($i = 0; $i < $cantRutasAlternativas; $i++){
        //     $j = $i;
        //     echo "Ruta " . ($j+1) . " => ". $obj->routes[$i]->legs[0]->distance->text . "<br> ";
        //     $explodeDistacia = explode(" ", $obj->routes[$i]->legs[0]->distance->text);
        //     $cantRecorrida = $explodeDistacia[0]; 
            

        //     if($cantRecorrida <= $rutaMenor){
        //         $rutaMenor = $cantRecorrida;
        //         $unidadMedida = $explodeDistacia[1];
        //     }
            
        // }

        // echo $rutaMenor . " - " . $unidadMedida;

        $horaActual = now();
        $horaEnBBDD = now()->addMinutes(45);


        $respuesta = array(
            "Hora actual" => $horaActual,
            "+ 45 minutos" => $horaEnBBDD,
            "Diferencia" => $horaActual->diffInMinutes($horaEnBBDD),
        );

        return response()->json($respuesta);
    });


});
