<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Pedidos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fecha', 'tienda_id', 'cliente_id', 'direccion', 'estatus', 'costoenvio', 'comentario', 'distancia', 'distmedida', 'prctjcomision', 'motospropias', 'delivery', 'prctjdescuento'
    ];

     /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'pedidos';

    /**
     * Quitar el autoincremento para trabajar con Uuid.
     */
    
    public $incrementing = false;

    /**
     * Función que se ejecuta al llamar el modelo.
     */

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $uuid = Uuid::generate()->string;
            $model->{$model->getKeyName()} = $uuid;
        });

    }

    public function cliente(){
        return $this->hasOne('App\User', 'id', 'cliente_id');
    }

    public function tienda(){
        return $this->hasOne('App\User', 'id', 'tienda_id');
    }

    public function motorizado(){
        return $this->hasOne('App\User', 'id', 'motorizado_id');
    }

    public function detalles(){
        return $this->hasMany('App\Pedido_detalles', 'pedido_id', 'id');
    }

    public function pagoefectivo(){
        return $this->hasOne('App\Pago_efectivo', 'pedido_id', 'id');
    }
}
