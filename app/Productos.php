<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
use App\Global_id;

class Productos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'descripcion', 'precio', 'tienda_id', 'categoria_id', 'foto', 'estatus',
    ];

    /**
     * Quitar el autoincremento para trabajar con Uuid.
     */

    public $incrementing = false;

    /**
     * Establece el nombre de la tabla.
     */

     protected $table = 'productos';

    /**
     * Función que se ejecuta al llamar el modelo.
     */

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            // $uuid = Uuid::generate()->string;
            // $model->{$model->getKeyName()} = $uuid;
            $id_generado = Global_id::create(['tipo' => 'producto']);
            $model->{$model->getKeyName()} = $id_generado->id;

        });

    }

    public function categoria(){
        return $this->hasOne('App\Producto_categorias', 'id', 'categoria_id');
    }

    public function adicionales(){
        return $this->hasMany('App\Adicionales', 'producto_id', 'id');
    }

    public function getFotoAttribute($value)
    {
        if(empty($value))
            $value = "https://tienda.activatunegocio.com/storage/productos/21a28eb2-c36d-417b-a36d-7dbb172a663e.png";

        return ucfirst($value);
    }
}
