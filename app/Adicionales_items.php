<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
use App\Global_id;

class Adicionales_items extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'precio', 'tienda_id', 'adicionales_id',
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'adicionales_items';

    /**
     * Quitar el autoincremento para trabajar con Uuid.
     */

    public $incrementing = false;

    /**
     * Función que se ejecuta al llamar el modelo.
     */

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            // $uuid = Uuid::generate()->string;
            // $model->{$model->getKeyName()} = $uuid;
            $id_generado = Global_id::create(['tipo' => 'adicional_item']);
            $model->{$model->getKeyName()} = $id_generado->id;
        });

    }
}
