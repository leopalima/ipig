<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etiquetas extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'etiqueta' ,
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'etiquetas';
}
