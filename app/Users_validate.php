<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_validate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'codigo', 'user_id', 'validate',
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'users_validate';
}
