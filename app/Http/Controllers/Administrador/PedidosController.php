<?php

namespace App\Http\Controllers\Administrador;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pedidos;
use App\User;
use App\Moto_pedidos;
use App\Http\Controllers\FCMnotificacion as FCM;

class PedidosController extends Controller
{
    public function obtener(Request $r)
    {
        $respuesta = [
            'estatus',
            'data',
        ];

        try {
            $pedido = Pedidos::with(['cliente', 'tienda', 'motorizado'])
                            ->where('id', $r->pedido)->firstOrFail();

            $motorizados = User::whereHas('tipo', function($q){
                                    $q->where('name', 'motorizado');
                                })
                                ->select('id', 'name')
                                ->get();
            $respuesta = array(
                'estatus' => "success",
                'data'  => array(
                    'pedido' => $pedido,
                    'motorizados' => $motorizados,
                ),
            );            
        } catch (\Exception $e) {
            $respuesta = array(
                'estatus' => "error",
                'data'  => $e
            );
        }
        
        return response()->json($respuesta);
    }

    public function modificar(Request $r)
    {
        $datos = array(
            'cliente' => $r->admCliente,
            'estatus' => $r->admEstatus,
            'pedido_id' => $r->admId,
            'motorizado_id' => $r->admMotorizado,
            'tiempoentrega' => $r->admTiempo,
            'tiempopreparacion' => $r->admTiempo,
            'tienda_id' => $r->admTienda,
        );

        $respuesta = [
            'estatus',
            'data',
        ];

        try {
            switch ($datos['estatus']) {
                case 'aceptado':
                    $data = $this->aceptado($datos);
                    if($data){
                        $respuesta = [
                            'estatus' => "success",
                            'data' => "",
                        ];
                    }else{
                        throw new \Exception("Error Processing Request", 1);
                    }
                    break;

                case 'buscando':
                    $data = ($datos['motorizado_id'] == null) ? 0 : $this->buscando($datos);
                    if($data){
                        $respuesta = [
                            'estatus' => "success",
                            'data' => "",
                        ];
                    }else{
                        throw new \Exception("Error Processing Request", 1);
                    }
                    break;

                case 'encamino':
                    $data = ($datos['motorizado_id'] == null) ? 0 : $this->encamino($datos);
                    if($data){
                        $respuesta = [
                            'estatus' => "success",
                            'data' => "",
                        ];
                    }else{
                        throw new \Exception("Error Processing Request", 1);
                    }
                    break;

                case 'entregado':
                    $data = ($datos['motorizado_id'] == null) ? 0 : $this->entregado($datos);
                    if($data){
                        $respuesta = [
                            'estatus' => "success",
                            'data' => "",
                        ];
                    }else{
                        throw new \Exception("Error Processing Request", 1);
                    }
                    break;

                case 'cancelado':
                    $data = $this->cancelado($datos);
                    if($data){
                        $respuesta = [
                            'estatus' => "success",
                            'data' => "",
                        ];
                    }else{
                        throw new \Exception("Error Processing Request", 1);
                    }
                    break;
                
                default:
                    throw new \Exception("Error Processing Request", 1);
                    break;
            }
        } catch (\Exception $e) {
            $respuesta = [
                'estatus' => "error",
                'data' => "",
            ];
        }


        return response()->json($respuesta);
    }

    public function aceptado($d)
    {
        $respuesta = "";
        try {
            Pedidos::where('id', $d['pedido_id'])
                                ->update([
                                    'estatus' => $d['estatus'],
                                    'motorizado_id' => null,
                                    'tiempoentrega' => $d['tiempoentrega'],
                                    'aceptado' => now(),
                                    ]);
            Moto_pedidos::where('pedido_id', $d['pedido_id'])
                            ->update([
                                'estatus' => 'cancelado',
                            ]);
            Moto_pedidos::create([
                'pedido_id' => $d['pedido_id'],
                'estatus' => $d['estatus'],
                'tiempopreparacion' => $d['tiempopreparacion'],
                'fecha' => now(),
                'motorizado_id' => null,
            ]);
            $respuesta = 1;
        } catch (\Exception $e) {
            $respuesta = 0;
        }

        return $respuesta;
    }

    public function buscando($d)
    {
        $respuesta = "";
        try {
            Pedidos::where('id', $d['pedido_id'])
                                ->update([
                                    'estatus' => $d['estatus'],
                                    'motorizado_id' => $d['motorizado_id'],
                                    'tiempoentrega' => $d['tiempoentrega'],
                                    ]);
            Moto_pedidos::where('pedido_id', $d['pedido_id'])
                            ->update([
                                'estatus' => 'cancelado',
                            ]);
            Moto_pedidos::create([
                'pedido_id' => $d['pedido_id'],
                'estatus' => $d['estatus'],
                'tiempopreparacion' => $d['tiempopreparacion'],
                'fecha' => now(),
                'motorizado_id' => $d['motorizado_id'],
            ]);
            $respuesta = 1;
        } catch (\Exception $e) {
            $respuesta = 0;
        }

        try {

            $pedido = Pedidos::where('id', $d['pedido_id'])->first();

            $cliente = $this->cliente($pedido->cliente_id);
            $tienda = $this->tienda($pedido->tienda_id);

            User::where('id', $pedido->motorizado_id)
                    ->update([
                        'latitud' => $tienda->latitud,
                        'longitud' => $tienda->longitud,
                        ]);

            $pedidoNotificable = [
                'id' => $pedido->id,
                'estatus' => $pedido->estatus,
                'motorizado_id' => $pedido->motorizado_id,
            ];

            $notificacion = new FCM;
            $notificacion->enviarNotificacion($cliente->fcm_token, '', 'Estan recogiendo tu pedido en el comercio.', $pedidoNotificable);
        } catch (\Exception $e) {
        }


        return $respuesta;
    }

    public function encamino($d)
    {
        $respuesta = "";
        try {
            Pedidos::where('id', $d['pedido_id'])
                                ->update([
                                    'estatus' => $d['estatus'],
                                    'motorizado_id' => $d['motorizado_id'],
                                    'tiempoentrega' => $d['tiempoentrega'],
                                    'enviado' => now(),
                                    ]);
            Moto_pedidos::where('pedido_id', $d['pedido_id'])
                            ->update([
                                'estatus' => 'cancelado',
                            ]);
            Moto_pedidos::create([
                'pedido_id' => $d['pedido_id'],
                'estatus' => $d['estatus'],
                'tiempopreparacion' => $d['tiempopreparacion'],
                'fecha' => now(),
                'motorizado_id' => $d['motorizado_id'],
            ]);
            $respuesta = 1;
        } catch (\Exception $e) {
            $respuesta = 0;
        }

        try {

            $pedido = Pedidos::where('id', $d['pedido_id'])->first();

            $cliente = $this->cliente($pedido->cliente_id);
            $tienda = $this->cliente($pedido->tienda_id);

            User::where('id', $pedido->motorizado_id)
                    ->update([
                        'latitud' => $tienda->latitud,
                        'longitud' => $tienda->longitud,
                        ]);

            $pedidoNotificable = [
                'id' => $pedido->id,
                'estatus' => $pedido->estatus,
                'motorizado_id' => $pedido->motorizado_id,
            ];

            $notificacion = new FCM;
            $notificacion->enviarNotificacion($cliente->fcm_token, '', 'Tu pedido va en camino hacia ti.', $pedidoNotificable);
        } catch (\Exception $e) {
        }

        return $respuesta;
    }

    public function entregado($d)
    {
        $respuesta = "";
        try {
            Pedidos::where('id', $d['pedido_id'])
                                ->update([
                                    'estatus' => $d['estatus'],
                                    'motorizado_id' => $d['motorizado_id'],
                                    'tiempoentrega' => $d['tiempoentrega'],
                                    'entregado' => now(),
                                    ]);
            Moto_pedidos::where('pedido_id', $d['pedido_id'])
                            ->update([
                                'estatus' => 'cancelado',
                            ]);
            Moto_pedidos::create([
                'pedido_id' => $d['pedido_id'],
                'estatus' => $d['estatus'],
                'tiempopreparacion' => $d['tiempopreparacion'],
                'fecha' => now(),
                'motorizado_id' => $d['motorizado_id'],
            ]);
            $respuesta = 1;
        } catch (\Exception $e) {
            $respuesta = 0;
        }

        try {

            $pedido = Pedidos::where('id', $d['pedido_id'])->first();

            $cliente = $this->cliente($pedido->cliente_id);
            $tienda = $this->cliente($pedido->tienda_id);

            User::where('id', $pedido->motorizado_id)
                    ->update([
                        'latitud' => $tienda->latitud,
                        'longitud' => $tienda->longitud,
                        ]);

            $pedidoNotificable = [
                'id' => $pedido->id,
                'estatus' => $pedido->estatus,
                'motorizado_id' => $pedido->motorizado_id,
            ];

            $notificacion = new FCM;
            $notificacion->enviarNotificacion($cliente->fcm_token, '', 'Recibiste tu pedido', $pedidoNotificable);
        } catch (\Exception $e) {
        }

        return $respuesta;
    }

    public function cancelado($d)
    {
        $respuesta = "";
        try {
            Pedidos::where('id', $d['pedido_id'])
                                ->update([
                                    'estatus' => $d['estatus'],
                                    'tiempoentrega' => $d['tiempoentrega'],
                                    ]);
            Moto_pedidos::where('pedido_id', $d['pedido_id'])
                            ->update([
                                'estatus' => 'cancelado',
                            ]);
            $respuesta = 1;
        } catch (\Exception $e) {
            $respuesta = 0;
        }

        return $respuesta;
    }

    public function cliente($cliente_id)
    {
        return User::where('id', $cliente_id)->first();
    }

    public function tienda($tienda_id)
    {
        return User::where('id', $tienda_id)->first();
    }
}
