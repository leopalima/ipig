<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Error;

class ErrorController extends Controller
{
    public function index(){
        $listaError = Error::orderBy("id", "desc")->paginate(15);

        return view("error.index", compact('listaError'));
    }

    public function buscar($id){
        try {
            $buscarError = Error::where('id', $id)->first();
        } catch (\Exception $e) {
            return ["error" => 0];
        }

        return response()->json($buscarError);
    }
}
