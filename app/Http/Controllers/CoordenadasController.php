<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Coordenadas;
use App\Pedidos;


class CoordenadasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tienda = User::where('id', $id)->first();
        return view('maps.create', compact('tienda', 'id', 'coord'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $coordenadas = Coordenadas::where('tienda_id', $id)->update(['deleted_at' => now()]);
        $tienda = User::where('id', $id)
                    ->update(
                        [
                            'latitud' => $request->latitud,
                            'longitud' => $request->longitud,
                            'direccion' => $request->direccion,
                        ]);
        $lat = [];
        $long = [];
        for($i = 0; $i < count($request->coord); $i++){
            if($i % 2 == 0){
                $lat[] = $request["coord"][$i];
            }else{
                $long[] = $request["coord"][$i];
            }
        }

        for($j=0; $j < count($lat); $j++){
            $coordenada = new Coordenadas;
            $coordenada->tienda_id = $id;
            $coordenada->latitud = $lat[$j];
            $coordenada->longitud = $long[$j];
            $coordenada->save();
        }
        return $this->checkLatLng($lat, $long);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
				
    public function checkLatLng($y, $x){

        $vertices_y = $y;    // x-coordinates of the vertices of the polygon (LATITUDES)
        $vertices_x = $x; // y-coordinates of the vertices of the polygon (LONGITUDES)
        $points_polygon = count($vertices_x)-1; 
        $longitude_x = -77.0215741772156;  // Your Longitude
        $latitude_y = -12.097877232227845;    // Your Latitude

        if ($this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)){
            echo "Is in polygon!";
        }else{
            echo "Is not in polygon";
        }
    }

    public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y){
        $i = $j = $c = $point = 0;
        for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
                $point = $i;
            if( $point == $points_polygon )
                $point = 0;
            if ( (($vertices_y[$point]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) && ($longitude_x < ($vertices_x[$j] - $vertices_x[$point]) * ($latitude_y - $vertices_y[$point]) / ($vertices_y[$j] - $vertices_y[$point]) + $vertices_x[$point]) ) )
                $c = !$c;
        }
        return $c;
    }

    public function obtener($id){
        $coord = [];
        $tienda = User::where('id', $id)->first();
        if($tienda->coordenadas->count()){
            foreach($tienda->coordenadas as $c){
                $coord[] = [
                    'lat' => $c->latitud,
                    'lng' => $c->longitud,
                ];
            }
        }
        return $coord;
    }

    public function coordenadasMotorizados(){
        $coordenadas = array();
        $motorizados = User::where('estatus', 1)
                                ->whereHas('tipo', function($q){
                                    $q->where('name', 'motorizado');
                                })
                                ->get();
        foreach($motorizados as $m){
            $coordenadas[] = [
                'lat' => $m->latitud,
                'lng' => $m->longitud,
                'name' => $m->name,
            ];
        }

        return response()->json($coordenadas);
    }

    public function coordenadasTiendas(){
        $coordenadas = array();
        $motorizados = User::whereHas('tipo', function($q){
                                    $q->where('name', 'tienda');
                                })
                                ->get();
        foreach($motorizados as $m){
            $coordenadas[] = [
                'lat' => $m->latitud,
                'lng' => $m->longitud,
                'name' => $m->name,
            ];
        }

        return response()->json($coordenadas);
    }

    public function coordenadasPedidos(){
        $coordenadas = array();
        $pedidos = Pedidos::where(function($q){
                                    $q->orWhere('estatus', 'aceptado')
                                    ->orWhere('estatus', 'listo')
                                    ->orWhere('estatus', 'buscando')
                                    ->orWhere('estatus', 'encamino');
                                })
                                ->get();
        foreach($pedidos as $p){
            $coordenadas[] = [
                'lat' => $p->latitud,
                'lng' => $p->longitud,
                'codigo' => $p->codigo,
                'cliente' => $p->cliente->name,
                'tienda' => $p->tienda->name,
                'motorizado' => $p->motorizado ? $p->motorizado->name : "",
            ];
        }

        return response()->json($coordenadas);
    }

}
