<?php

namespace App\Http\Controllers\CRON;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\modelos\PedidosController as PC;
use App\Http\Controllers\modelos\UsersController as USERS;
use App\Http\Controllers\modelos\MotoPedidosController as MotoPedido;
use App\Http\Controllers\FCMnotificacion as FCM;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use Carbon\Carbon;
use App\User;
use DB;

class PedidosCron extends Controller
{
    public function listoEnviar($pedido_id, $motoPedido){

        $buscarPedido = new PC;
        $pedidoEncontrado = 1;
        try{
            $pedido = $buscarPedido
                        ->one($pedido_id)
                        ->where('estatus', 'aceptado')
                        ->where('motospropias', 0)
                        ->firstOrFail();            
        }catch(\Exception $e){
            $pedidoEncontrado = 0;
        }

        if($pedidoEncontrado){
            $tienda = $this->tienda($pedido->tienda_id);

            try {
                $rango = $this->getBoundaries($tienda->latitud, $tienda->longitud, 1000);
                
                $tokenMotorizados = $this->motorizados()
                                    ->select(DB::raw('(6371 * ACOS( 
                                        SIN(RADIANS(latitud)) 
                                        * SIN(RADIANS(' . $tienda->latitud . ')) 
                                        + COS(RADIANS(longitud - ' . $tienda->longitud . ')) 
                                        * COS(RADIANS(latitud)) 
                                        * COS(RADIANS(' . $tienda->latitud . '))
                                        )
                                        ) AS distance, fcm_token, id'))
                                    ->whereBetween('latitud', [$rango['min_lat'], $rango['max_lat']])
                                    ->whereBetween('longitud', [$rango['min_lng'], $rango['max_lng']])
                                    ->orderBy('distance', 'ASC')
                                    // ->having('distance', '<', 1)
                                    ->get();

                $listarMotoPedidos = new MotoPedido;
                $motoPedidos = $listarMotoPedidos->all()
                                                ->orWhere(function($q){
                                                    $q->orWhere('estatus', 'listo');
                                                    $q->orWhere('estatus', 'buscando');
                                                    $q->orWhere('estatus', 'encamino');
                                                })
                                                ->get()->pluck('motorizado_id');
                
                $motorizados = $tokenMotorizados->whereNotIn('id', $motoPedidos)->flatten();
    
                $tokenMotorizados = $motorizados->first();

                if($tokenMotorizados){
                    $pedido->enviado = now();
                    $pedido->estatus = "listo";
                    $pedido->save();

                    $tokenMotorizados2 = $tokenMotorizados->fcm_token;
                    $pedidoNotificable = [
                        'id' => $pedido->id,
                        'estatus' => $pedido->estatus
                    ];

                    $asignarMotorizado = new MotoPedido;
                    $asignarMotorizado->asignarMotorizado($motoPedido, $tokenMotorizados->id);


                    $notificacion = new FCM;
                    $notificacion->enviarNotificacion($tokenMotorizados2, '', 'Hay un pedido disponible para recoger.', $pedidoNotificable);
                }
            } catch (\Exception $e) {
                $formato = new JSON;
                $formato->error(["error" => "Error al notificar motorizados"], $e);
            }
        }


        // return response()->json($respuesta);

    }

    public function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371){
        $return = array();
         
        // Los angulos para cada dirección
        $cardinalCoords = array('north' => '0',
                                'south' => '180',
                                'east' => '90',
                                'west' => '270');
        $rLat = deg2rad($lat);
        $rLng = deg2rad($lng);
        $rAngDist = $distance/$earthRadius;
        foreach ($cardinalCoords as $name => $angle)
        {
            $rAngle = deg2rad($angle);
            $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
             $return[$name] = array('lat' => (float) rad2deg($rLatB), 
                                    'lng' => (float) rad2deg($rLonB));
        }
        return array('min_lat'  => $return['south']['lat'],
                     'max_lat' => $return['north']['lat'],
                     'min_lng' => $return['west']['lng'],
                     'max_lng' => $return['east']['lng']);
    }

    public function tienda($id){
        $buscarTienda = new USERS;
        $tienda = $buscarTienda->one($id)->first();
        return $tienda;
    }

    public function asignarMoto(){
        $traerMotoPedidos = new MotoPedido;

        $motoPedidos = $traerMotoPedidos->all()
                                        ->where('estatus', 'aceptado')
                                        ->whereNull('motorizado_id')
                                        ->get();

        foreach($motoPedidos as $m){
            // $tiempoPreparacion = Carbon::parse($m->created_at)->addMinutes($m->tiempopreparacion );
            $tiempoPreparacion = Carbon::parse($m->created_at);
            $tiempoActual = now();
            $diferencia = $tiempoActual->diffInMinutes($tiempoPreparacion, false);
            if($diferencia <= 5){
                $this->listoEnviar($m->pedido_id, $m->id);
            }
        }
    }

    public function motorizados(){
        $motorizados = User::where('estatus', 1)
                            ->whereHas('tipo', function($q){
                                $q->where('name', 'motorizado');
                            });
        return $motorizados;
    }

    public function reasignarMoto()
    {
        $traerMotoPedidos = new MotoPedido;

        $motoPedidos = $traerMotoPedidos->all()
                                        ->where('estatus', 'listo')
                                        ->whereNotNull('motorizado_id')
                                        ->get();
        foreach($motoPedidos as $m){
            $tiempoAsignado = Carbon::parse($m->updated_at);
            $tiempoActual = now();
            $diferencia = $tiempoActual->diffInMinutes($tiempoAsignado, false);
            
            if($diferencia < 0){
                $this->penalizarDuplicar($m);
            }
            //return $diferencia;
        }

    }

    public function penalizarDuplicar($moto_pedido)
    {
        $motoPedido = new MotoPedido;
        $penalizar = $motoPedido->penalizar($moto_pedido);

    }

    public function pedidoEntregadoMotosPropias()
    {
        $horaActualMenos45 = now()->subMinutes(45);
        $pedidos = new PC;
        try {
            $losPedidos = $pedidos->all()
                            ->where('motospropias', 1)
                            ->where('estatus', 'encamino')
                            ->where('created_at', '<', $horaActualMenos45)
                            ->update(['estatus' => 'entregado', 'entregado' => now()]);
        } catch (\Exception $e) {}

    }

}
