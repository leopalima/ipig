<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class FCMtoken extends Controller
{
    public function recibirToken(Request $r){
        $tienda = User::find($this->tienda()->id);
        $tienda->update(['fcm_token' => $r->data]);
        return response()->json($tienda->id);
    }

    public function tienda(){
        return Auth::user();
    }

}
