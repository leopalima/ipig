<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Pedidos;
use App\User;

class UbicarPedidosController extends Controller
{
    public function index()
    {
        return view('maps.ubicar-pedidos-index');
    }

    public function obtener()
    {
        $coordenadas = array();
        $pedidos = Pedidos::where(function($q){
                                    $q->orWhere('estatus', 'recibido')
                                    ->orWhere('estatus', 'aceptado')
                                    ->orWhere('estatus', 'listo')
                                    ->orWhere('estatus', 'buscando')
                                    ->orWhere('estatus', 'encamino');
                                })
                            ->where('tienda_id', auth::user()->id)
                            ->get();
        foreach($pedidos as $p){
            $coordenadas[] = [
                'lat' => $p->latitud,
                'lng' => $p->longitud,
                'codigo' => $p->codigo,
                'cliente' => $p->cliente->name,
                'tienda' => $p->tienda->name,
                'direccion' => $p->direccion,
            ];
        }

        return response()->json($coordenadas);
    }

    public function coordenadasTienda(){
        $coordenadas = array();
        $tienda = User::where('id', auth::user()->id)
                                ->firstOrFail();
        $coordenadas[] = [
            'lat' => $tienda->latitud,
            'lng' => $tienda->longitud,
            'name' => $tienda->name,
        ];

        return response()->json($coordenadas);
    }
}
