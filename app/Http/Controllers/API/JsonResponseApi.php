<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Error;

class JsonResponseApi extends Controller
{

    public function success($data){
        $success = [
            'status' => 'success',
            'data' => $data,
        ];
        return $success;
    }

    public function fail($data, $error=""){
        $fail = [
            'status' => 'fail',
            'data' => $data,
        ];

        if($error!=""){
            $this->grabarError($error);
        }

        return $fail;
    }

    public function error($data, $error=""){
        $respuesta = [
            'status' => 'error',
            'data' => $data,
            'message' => $data['error'],
        ];

        if($error!=""){
            $this->grabarError($error);
        }

        return $respuesta;
    }

    public function grabarError($exception){

        // Checks if a user has logged in to the system, so the error will be recorded with the user id
        $userId = 0;
        if (Auth::user()) {
            $userId = Auth::user()->id;
        }

        $data = array(
            'user_id' => $userId,
            'code' => $exception->getCode(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'message' => $exception->getMessage(),
            'trace' => $exception->getTraceAsString(),
        );

        Error::create($data);

    }
}
