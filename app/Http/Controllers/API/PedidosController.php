<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Http\Controllers\modelos\PedidosController as PC;
use App\Http\Controllers\modelos\PedidosDetallesController as PCD;
use App\Http\Controllers\modelos\PagoPosController as POS;
use App\Http\Controllers\modelos\PagoEfectivoController as EFECTIVO;
use App\Http\Controllers\modelos\PagoCulqiController as CULQI;
use App\Http\Controllers\modelos\MotoPedidosController as MotoPedido;
use App\Http\Controllers\FCMnotificacion as FCM;
use Carbon\Carbon;
use App\Pedidos;
use Auth;
use App\User;
use App\Coordenadas;
use Exception;

class PedidosController extends Controller
{
    public function recibirPedido(Request $request){
        $formato = new JSON;

        $tienda = $this->tienda($request->tienda_id);

        try {
            $dentroArea = $this->estaEnAreaTienda($request->tienda_id, $request->latitud, $request->longitud);
            $mensaje = "Usted se encuentra fuera del área de cobertura del comercio.";

            if(!$dentroArea){
                throw new \Exception($mensaje);
            }

        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => $mensaje], $e);
            return response()->json($respuesta);
        }

        // try {
        //     $distancia = $this->obtenerDistanciaRecta($tienda->latitud, $tienda->longitud, $request->latitud, $request->longitud);
        //     $mensaje = "Usted se encuentra fuera del área de cobertura del comercio.";

        //     if($distancia > 7000){
        //         throw new \Exception($mensaje);
        //     }

        // } catch (\Exception $e) {
        //     $respuesta = $formato->error(['error' => $mensaje], $e);
        //     return response()->json($respuesta);
        // }

        // try {
        //     $cerrado = $this->cerrado($tienda);
        //     if($cerrado){
        //         $mensaje = "En este momento el comercio no está disponible para atender tu pedido.";
        //         throw new \Exception($mensaje);
        //     }
        // } catch (\Exception $e) {
        //     $respuesta = $formato->error(['error' => $mensaje], $e);
        //     return response()->json($respuesta);
        // }

        $monto = $this->montoPedido($request);
        $request["monto"] = round($monto, 2);
        $request["tienda_nombre"] = $tienda->name;

        $request["motospropias"] = 0;
        $request["prctjcomision"] = $tienda->prctjcomision;

        switch (strtoupper($request->tipo_pago['tipo'])) {
            case 'TENGO LA CANTIDAD EXACTA':
            case 'EFECTIVO':
            case 'POS VISA':
            case 'POSVISA':
            case 'POS MASTERCARD':
            case 'POSMASTERCARD':
                $request["prctjcomision"] = 0;
            break;
        }

        // if($tienda->motospropias){
        //     $request["motospropias"] = 1;
        //     $request["prctjcomision"] = 10;
        // }

        $grabarPedido = new PC;
        $pedido = $grabarPedido->store($request);

        if($pedido){

            $grabarDetalles = new PCD;
            $detalles = $grabarDetalles->store($request, $pedido->id);

            if($detalles == 0){
                $pedido->estatus = "fallido";
                $pedido->save();
                $respuesta = $formato->error(['error' => 'Error al grabar el pedido el detalle'], '');
                return response()->json($respuesta);
            }else{

                switch (strtoupper($request->tipo_pago['tipo'])) {
                    case 'TENGO LA CANTIDAD EXACTA':
                    case 'EFECTIVO':
                        $grabarPagoEfe = new EFECTIVO;
                        $pagoGrabado = $grabarPagoEfe->store($request, $pedido->id);
                        break;
                    case 'POS VISA':
                    case 'POSVISA':
                        $grabarPagoPOS = new POS;
                        $pagoGrabado = $grabarPagoPOS->store($request, $pedido->id);
                        break;
                    case 'POS MASTERCARD':
                    case 'POSMASTERCARD':
                        $grabarPagoPOS = new POS;
                        $pagoGrabado = $grabarPagoPOS->store($request, $pedido->id);
                        break;
                    case 'CULQI':
                        $grabarPagoCulqi = new CULQI;
                        $pagoGrabado = $grabarPagoCulqi->store($request, $pedido->id);
            
                        if(array_key_exists("error", $pagoGrabado)){
                            $pedido->estatus = "fallido";
                            $pedido->save();
                            $respuesta = $formato->error(['error' => $pagoGrabado['mensaje']], $pagoGrabado['e']);
                            return response()->json($respuesta);
                        }
                        break;
                    default:
                        try {
                            throw new \Exception("Error en el método de pago seleccionado.");
                        } catch (\Exception $e) {
                            $pedido->estatus = "fallido";
                            $pedido->save();
                            $respuesta = $formato->error(['error' => 'Error en el método de pago seleccionado'], $e);
                            return response()->json($respuesta);
                        }
                        break;
                }
            }

            $obteniendoDistancia = $this->obtenerDistancia($pedido->id, $tienda->latitud, $tienda->longitud, $request->latitud, $request->longitud);
            
            $usuarioDireccion = User::find($this->cliente()->id);
            $usuarioDireccion->direccion = $request->direccion;
            $usuarioDireccion->latitud = $request->latitud;
            $usuarioDireccion->longitud = $request->longitud;
            $usuarioDireccion->save();

            $pedidoNotificable = [
                'id' => $pedido->id,
                'estatus' => $pedido->estatus
            ];    

            try {
                $notificacion = new FCM;
                $notificacion->enviarNotificacion($this->cliente()->fcm_token, 'Ipig', 'Tienes un nuevo pedido', $pedidoNotificable); 
            } catch (\Exception $e) {
                $respuesta = $formato->error(['error' => 'Error FCM'], $e);
            }

            try {
                $notificacion = new FCM;
                $notificacion->enviarNotificacion($this->tienda($request->tienda_id)->fcm_token, '', 'Tienes un nuevo pedido', $pedidoNotificable);
            } catch (\Exception $e) {
                $respuesta = $formato->error(['error' => 'Error FCM'], $e);
            }
            
            try {
                $administradores = array(
                    '56e1e200-a826-11e9-b342-89b9645ba0b2',
                    '2fea0820-a806-11e9-80d8-e9adaaa996de',
                    '7a882ae0-ab68-11e9-99bc-e1920e2a613b',
                    '226ea870-b496-11e9-a5ff-510e17f2f61c',
                    '7df8acf0-cabf-11e9-a05f-43d668b371cc',
                );

                for($i=0; $i<count($administradores); $i++){
                    $tienda_id_angela = $this->tienda($administradores[$i])->id;
                    try {
                        $notificacion = new FCM;
                        if(
                            $administradores[$i] == '7df8acf0-cabf-11e9-a05f-43d668b371cc' and
                            (
                                $tienda_id_angela == 'ec2180b0-a8ec-11e9-8779-1dc68164808e' or
                                $tienda_id_angela == '79d6b360-a829-11e9-8649-1710509afd54' or
                                $tienda_id_angela == 'e364da50-b926-11e9-bd68-9736deeb80de'
                            )
                            ){
                            $notificacion->enviarNotificacion($this->tienda($administradores[$i])->fcm_token, '', 'PEDIDO: ' . $this->tienda($request->tienda_id)->name . ' #' . $pedido->codigo, $pedidoNotificable);
                        }else{
                            $notificacion->enviarNotificacion($this->tienda($administradores[$i])->fcm_token, '', 'PEDIDO: ' . $this->tienda($request->tienda_id)->name . ' #' . $pedido->codigo, $pedidoNotificable);
                        }
                    } catch (\Exception $e) {}
                }
            } catch (\Exception $e) {}

            $respuesta = $formato->success($pedido);
            return response()->json($respuesta);    
        }

        $respuesta = $formato->error(['error' => 'Error al grabar el pedido'], '');
        return response()->json($respuesta);

    }

    public function misPedidos(){
        $pedidos = new PC;
        $misPedidos = $pedidos->all()
                            ->where('cliente_id', $this->cliente()->id)
                            ->with(['detalles.producto', 'detalles.adicional', 'tienda', 'motorizado'])
                            ->get();
        
        // return $misPedidos;
        $hasta = count($misPedidos);
        $pedido=[];
        for($i=0; $i<$hasta; $i++){
            $monto=0;
            $pedido[$i] = [
                "id"=>$misPedidos[$i]['id'],
                "codigo"=>$misPedidos[$i]['codigo'],
                "fecha"=>Carbon::parse($misPedidos[$i]['fecha'])->format('d-F-Y'),
                "tienda_id"=>$misPedidos[$i]['tienda_id'],
                "cliente_id"=>$misPedidos[$i]['cliente_id'],
                "direccion"=>$misPedidos[$i]['direccion'],
                "latitud"=>$misPedidos[$i]['latitud'],
                "longitud"=>$misPedidos[$i]['longitud'],
                "estatus"=>$misPedidos[$i]['estatus'],
                "tiempoentrega"=>$misPedidos[$i]['tiempoentrega'],
                "costoenvio"=>$misPedidos[$i]['costoenvio'],
                "tipo_pago"=>$misPedidos[$i]['tipo_pago'],
                "comentario"=>$misPedidos[$i]['comentario'],
                "enviado"=>$misPedidos[$i]['enviado'],
                "aceptado"=>$misPedidos[$i]['aceptado'],
                "entregado"=>$misPedidos[$i]['entregado'],
                "descuento"=>$misPedidos[$i]['prctjdescuento'],
                "detalles" => "",
                "tienda" => "",
            ];

            $hasta2 = count($misPedidos[$i]['detalles']);
            $detalles = [];
            $c=-1;
            $x=0;
            $id = $misPedidos[0]['detalles'][0]['tipo_producto'];
            for($j=0; $j<$hasta2; $j++){
                
                if($id != $misPedidos[$i]['detalles'][$j]['tipo_producto'] || $misPedidos[$i]['detalles'][$j]['producto']['name'] != null){
                    $id = $misPedidos[$i]['detalles'][$j]['tipo_producto'];
                    $c++;
                    $x=0;
                }

                if($misPedidos[$i]['detalles'][$j]['producto']['name'] != null){
                    $detalles[$c] = [
                        "producto_id"=>$misPedidos[$i]['detalles'][$j]['tipo_producto'],
                        "pedido_id"=>$misPedidos[$i]['detalles'][$j]['pedido_id'],
                        "precio"=>$misPedidos[$i]['detalles'][$j]['precio'],
                        "cantidad"=>$misPedidos[$i]['detalles'][$j]['cantidad'],
                        "estatus"=>$misPedidos[$i]['detalles'][$j]['estatus'],
                        "nombre"=>$misPedidos[$i]['detalles'][$j]['producto']['name'],
                        "adicionales" => [],
                    ];
                    $monto = $monto + (round($misPedidos[$i]['detalles'][$j]['precio'], 2) * $misPedidos[$i]['detalles'][$j]['cantidad']);
                }

                if($misPedidos[$i]['detalles'][$j]['adicional']['nombre'] != null){
                    $detalles[$c]["adicionales"][$x] = [
                        "adicional_id"=> $misPedidos[$i]['detalles'][$j]['adicional']['id'],
                        "nombre"=> $misPedidos[$i]['detalles'][$j]['adicional']['nombre'],
                        "precio"=> $misPedidos[$i]['detalles'][$j]['precio'],
                    ];
                    $x++;
                    $monto = $monto + (round($misPedidos[$i]['detalles'][$j]['precio'], 2) * $misPedidos[$i]['detalles'][$j]['cantidad']);
                }
            }

            $pedido[$i]["detalles"] = [$detalles];
            $pedido[$i]["tienda"] = [
                    "id" => $misPedidos[$i]['tienda']['id'],
                    "name" => $misPedidos[$i]['tienda']['name'],
                    "descripcion" => $misPedidos[$i]['tienda']['descripcion'],
                    "email" => $misPedidos[$i]['tienda']['email'],
                    "phone" => $misPedidos[$i]['tienda']['phone'],
                    "email_verified_at" => $misPedidos[$i]['tienda']['email_verified_at'],
                    "tipo_id" => $misPedidos[$i]['tienda']['tipo_id'],
                    "direccion" => $misPedidos[$i]['tienda']['direccion'],
                    "latitud" => $misPedidos[$i]['tienda']['latitud'],
                    "longitud" => $misPedidos[$i]['tienda']['longitud'],
                    "foto" => $misPedidos[$i]['tienda']['foto'],
            ];
            if($misPedidos[$i]['motorizado']['id']==null)
            {
                $pedido[$i]["motorizado"] = null;
            }else{
                $pedido[$i]["motorizado"] = [
                        "id" => $misPedidos[$i]['motorizado']['id'],
                        "name" => $misPedidos[$i]['motorizado']['name'],
                        "descripcion" => $misPedidos[$i]['motorizado']['descripcion'],
                        "email" => $misPedidos[$i]['motorizado']['email'],
                        "phone" => $misPedidos[$i]['motorizado']['phone'],
                        "email_verified_at" => $misPedidos[$i]['motorizado']['email_verified_at'],
                        "tipo_id" => $misPedidos[$i]['motorizado']['tipo_id'],
                        "direccion" => $misPedidos[$i]['motorizado']['direccion'],
                        "latitud" => $misPedidos[$i]['motorizado']['latitud'],
                        "longitud" => $misPedidos[$i]['motorizado']['longitud'],
                        "foto" => $misPedidos[$i]['motorizado']['foto'],
                ];
            }
            $descontado = 0;
            if($misPedidos[$i]['prctjdescuento']){
                $descontado = $monto * ($misPedidos[$i]['prctjdescuento'] / 100);
            }
            $pedido[$i]["montoproductos"] = round($monto - $descontado, 2);

        }


        return $pedido;

    }

    public function cliente(){
        return Auth::user();
    }

    public function tienda($tienda_id){
        return User::find($tienda_id);
    }

    public function cancelarPedido(Request $r, $id){
        $formato = new JSON;
        try {
            $buscarPedido = new PC;
            $pedido = $buscarPedido->one($id)
                        ->where('cliente_id', $this->cliente()->id)
                        ->where(function($q){
                            $q->orWhere('estatus', 'recibido');
                            $q->orWhere('estatus', 'aceptado');
                            $q->orWhere('estatus', 'listo');
                            $q->orWhere('estatus', 'buscando');
                            $q->orWhere('estatus', 'encamino');
                        })
                        ->firstOrFail();

            try {

                $pedidoNotificable = [
                    'id' => $pedido->id,
                    'estatus' => "cancelado"
                ];
    

                if($pedido->estatus === "encamino"){
                    try {
                        $notificacion = new FCM;
                        $notificacion->enviarNotificacion($this->cliente()->fcm_token, '', 'Tu pedido esta en camino y no se puede cancelar.' . $pedido->codigo, $pedidoNotificable);
                    } catch (\Exception $e) {
                        $formato->error(['error' => 'Error FCM'], $e);
                    }

                    $respuesta = $formato->error(['error' => 'Tu pedido esta en camino y no se puede cancelar.'], $e);
                    return response()->json($respuesta);
                }

                switch ($pedido->estatus) {
                    case 'listo':
                    case 'buscando':
                        $motoPedido = new MotoPedido;
                        $motopedido = $motoPedido->buscar($pedido->id, $pedido->estatus);
                        $motopedidoCancelar = $motoPedido->cancelar($pedido->id, $motopedido->motorizado_id);
                        try {
                            $notificacion = new FCM;
                            $notificacion->enviarNotificacion($this->tienda($motopedido->motorizado_id)->fcm_token, '', 'Se ha cancelado el pedido ' . $pedido->codigo, $pedidoNotificable);
                        } catch (\Exception $e) {
                            $formato->error(['error' => 'Error FCM'], $e);
                        }
                        break;
                    
                    default:
                        break;
                }
            } catch (\Throwable $th) {}
            
            $pedido->estatus = "cancelado";
            $pedido->save();

            $respuesta = $formato->success($pedido);

            $pedidoNotificable = [
                'id' => $pedido->id,
                'estatus' => $pedido->estatus
            ];

            try {
                $notificacion = new FCM;
                $notificacion->enviarNotificacion($this->cliente()->fcm_token, '', 'Se cancelado el pedido ' . $pedido->codigo, $pedidoNotificable);
            } catch (\Exception $e) {
                $formato->error(['error' => 'Error FCM'], $e);
            }
    
            try {
                $notificacion = new FCM;
                $notificacion->enviarNotificacion($this->tienda($pedido->tienda_id)->fcm_token, '', 'Se ha cancelado el pedido ' . $pedido->codigo, $pedidoNotificable);
            } catch (\Exception $e) {
                $formato->error(['error' => 'Error FCM'], $e);
            }


        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Pedido no encontrado'], $e);
        }

        return response()->json($respuesta);
    }

    public function montoPedido($r){

        $monto = 0;
        $descuento = 0;
        if(strtoupper($r->tipo_pago['tipo']) == "EFECTIVO" || strtoupper($r->tipo_pago['tipo']) == "TENGO LA CANTIDAD EXACTA")
            $descuento = $this->tienda($r->tienda_id)->descuento;
        foreach($r->detalles as $d){

            $precio = $d['precio'];
            $cantidad = $d['cantidad'];
            $multiplica = $precio * $cantidad;

            $descontado = 0;
            if($descuento){
                $descontado = $multiplica * ($descuento/100);
            }
            $monto = $monto + ($multiplica - $descontado);

            if(count($d['adicionales'])){
                foreach($d['adicionales'] as $items){
                    $precio = $items['precio'];
                    $cantidad = $d['cantidad'];
                    $multiplica = $precio * $cantidad;
                    
                    $descontado = 0;
                    if($descuento){
                        $descontado = $multiplica * ($descuento/100);
                    }
                    $monto = $monto + ($multiplica - $descontado);
                }
            }
        }

        $tipoEnvio = $r['delivery'];
        $costoEnvio = $this->tienda($r->tienda_id)->costoEnvio;
        if(strtolower($tipoEnvio) === 'recojo'){
            $costoEnvio = 0;
        }

        return $monto + $costoEnvio;
    }

    public function mostrarPedido($id){
        $formato = new JSON;
        try {
            $buscarPedido = new PC;
            $misPedidos = $buscarPedido->one($id)
                                    // ->where('estatus', 'buscando')
                                    ->where('motorizado_id', $this->motorizado()->id)
                                    ->with(['detalles.producto', 'detalles.adicional', 'tienda', 'cliente'])
                                    ->firstOrFail();

            $monto=0;
            $misPedidos = $misPedidos->toArray();
            $pedido = [
                "id"=>$misPedidos['id'],
                "codigo"=>$misPedidos['codigo'],
                "fecha"=>Carbon::parse($misPedidos['fecha'])->format('d-F-Y'),
                "tienda_id"=>$misPedidos['tienda_id'],
                "cliente_id"=>$misPedidos['cliente_id'],
                "direccion"=>$misPedidos['direccion'],
                "latitud"=>$misPedidos['latitud'],
                "longitud"=>$misPedidos['longitud'],
                "estatus"=>$misPedidos['estatus'],
                "tiempoentrega"=>$misPedidos['tiempoentrega'],
                "costoenvio"=>$misPedidos['costoenvio'],
                "tipo_pago"=>$misPedidos['tipo_pago'],
                "comentario"=>$misPedidos['comentario'],
                "enviado"=>$misPedidos['enviado'],
                "aceptado"=>$misPedidos['aceptado'],
                "entregado"=>$misPedidos['entregado'],
                "detalles" => "",
                "tienda" => "",
            ];

            $hasta2 = count($misPedidos['detalles']);
            $detalles = [];
            $c=-1;
            $x=0;
            $id = $misPedidos['detalles'][0]['tipo_producto'];
            for($j=0; $j<$hasta2; $j++){
                
                if($id != $misPedidos['detalles'][$j]['tipo_producto'] || $misPedidos['detalles'][$j]['producto']['name'] != null){
                    $id = $misPedidos['detalles'][$j]['tipo_producto'];
                    $c++;
                    $x=0;
                }

                if($misPedidos['detalles'][$j]['producto']['name'] != null){
                    $detalles[$c] = [
                        "producto_id"=>$misPedidos['detalles'][$j]['tipo_producto'],
                        "pedido_id"=>$misPedidos['detalles'][$j]['pedido_id'],
                        "precio"=>$misPedidos['detalles'][$j]['precio'],
                        "cantidad"=>$misPedidos['detalles'][$j]['cantidad'],
                        "estatus"=>$misPedidos['detalles'][$j]['estatus'],
                        "nombre"=>$misPedidos['detalles'][$j]['producto']['name'],
                        "adicionales" => [],
                    ];
                    $monto = $monto + (round($misPedidos['detalles'][$j]['precio'], 2) * $misPedidos['detalles'][$j]['cantidad']);
                }

                if($misPedidos['detalles'][$j]['adicional']['nombre'] != null){
                    $detalles[$c]["adicionales"][$x] = [
                        "adicional_id"=> $misPedidos['detalles'][$j]['adicional']['id'],
                        "nombre"=> $misPedidos['detalles'][$j]['adicional']['nombre'],
                        "precio"=> $misPedidos['detalles'][$j]['adicional']['precio'],
                    ];
                    $x++;
                    $monto = $monto + (round($misPedidos['detalles'][$j]['adicional']['precio'], 2) * $misPedidos['detalles'][$j]['cantidad']);
                }
            }

            switch (strtoupper($misPedidos['tipo_pago'])) {
                case 'TENGO LA CANTIDAD EXACTA':
                case 'EFECTIVO':
                    $modo = "EFECTIVO";
                    $costoTotal = round($monto + $misPedidos['costoenvio'], 2);
                    break;

                case 'POS VISA':
                case 'POSVISA':
                    $modo = "VISA";
                    $costoTotal = round($monto + $misPedidos['costoenvio'], 2);
                    break;

                case 'POS MASTERCARD':
                case 'POSMASTERCARD':
                    $modo = "MASTERCARD";
                    $costoTotal = round($monto + $misPedidos['costoenvio'], 2);
                    break;

                case 'CULQI':
                    $modo = "ONLINE";
                    $costoTotal = "PAGADO";
                    break;
                
                default:
                    $modo = "PREGUNTAR";
                    $costoTotal = "PREGUNTAR";
                    break;
            }

            $metodoDePago = array(
                "producto_id"=>"",
                "pedido_id"=>"",
                "precio"=>0,
                "cantidad"=>1,
                "estatus"=>"recibido",
                "nombre"=> "MODO DE PAGO: " . $modo,
                "adicionales" => array([
                    "adicional_id" => "",
                    "nombre" =>  "Costo total: " . $costoTotal,
                    "precio" => 0,
                ])
            );

            array_push($detalles, $metodoDePago);

            $pedido["detalles"] = [$detalles];
            $pedido["tienda"] = [
                    "id" => $misPedidos['tienda']['id'],
                    "name" => $misPedidos['tienda']['name'],
                    "descripcion" => $misPedidos['tienda']['descripcion'],
                    "email" => $misPedidos['tienda']['email'],
                    "phone" => $misPedidos['tienda']['phone'],
                    "email_verified_at" => $misPedidos['tienda']['email_verified_at'],
                    "tipo_id" => $misPedidos['tienda']['tipo_id'],
                    "direccion" => $misPedidos['tienda']['direccion'],
                    "latitud" => $misPedidos['tienda']['latitud'],
                    "longitud" => $misPedidos['tienda']['longitud'],
                    "foto" => $misPedidos['tienda']['foto'],
            ];
            $pedido["cliente"] = [
                "id" => $misPedidos['cliente']['id'],
                "name" => $misPedidos['cliente']['phone'] . " " . $misPedidos['cliente']['name'],
                "descripcion" => $misPedidos['cliente']['descripcion'],
                "email" => $misPedidos['cliente']['email'],
                "phone" => $misPedidos['cliente']['phone'],
                "email_verified_at" => $misPedidos['cliente']['email_verified_at'],
                "tipo_id" => $misPedidos['cliente']['tipo_id'],
                "direccion" => $misPedidos['cliente']['direccion'],
                "latitud" => $misPedidos['cliente']['latitud'],
                "longitud" => $misPedidos['cliente']['longitud'],
                "foto" => $misPedidos['cliente']['foto'],
            ];
            $pedido["montoproductos"] = round($monto, 2);

            $respuesta = $formato->success($pedido);
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Pedido no encontrado' . $e], $e);
        }

        return response()->json($respuesta);

    }

    public function aceptarPedido($id){
        $formato = new JSON;
        try {

            $buscarPedido = new PC;
            $pedido = $buscarPedido->one($id)
                                    ->where('estatus', 'listo')
                                    ->whereNull('motorizado_id')
                                    ->with(['tienda', 'cliente'])
                                    ->firstOrFail();
            $pedido->motorizado_id = $this->motorizado()->id;
            $pedido->estatus = "buscando";
            $pedido->save();

            $motoPedido = new MotoPedido;
            $motoPedido->buscando($id, $this->motorizado()->id);

            $nuevoNombreCliente = $pedido->cliente->phone . " " . $pedido->cliente->name;

            $pedido->cliente->name = $nuevoNombreCliente;

            $respuesta = $formato->success($pedido);

        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Pedido no encontrado'], $e);
        }

        return response()->json($respuesta);
    }

    public function encaminoPedido($id){
        $formato = new JSON;
        try {

            $buscarPedido = new PC;
            $pedido = $buscarPedido->one($id)
                                    ->where('estatus', 'buscando')
                                    ->where('motorizado_id', $this->motorizado()->id)
                                    ->with(['tienda', 'cliente'])
                                    ->firstOrFail();
            $pedido->motorizado_id = $this->motorizado()->id;
            $pedido->estatus = "encamino";
            $pedido->save();

            $motoPedido = new MotoPedido;
            $motoPedido->encamino($id, $this->motorizado()->id);

            $nuevoNombreCliente = $pedido->cliente->phone . " " . $pedido->cliente->name;

            $pedido->cliente->name = $nuevoNombreCliente;

            $respuesta = $formato->success($pedido);

        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Pedido no encontrado'.$e], $e);
        }

        $pedidoNotificable = [
            'id' => $pedido->id,
            'estatus' => $pedido->estatus,
            'motorizado_id' => $pedido->motorizado_id,
        ];    

        try {
            $notificacion = new FCM;
            $notificacion->enviarNotificacion($this->motorizado()->fcm_token, '', 'Lleva el pedido al destino.', $pedidoNotificable);
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error FCM'], $e);
        }

        try {
            $cliente = User::find($pedido->cliente_id);
            $notificacion = new FCM;
            $notificacion->enviarNotificacion($cliente->fcm_token, '', 'Tu pedido va en camino hacia ti.', $pedidoNotificable);
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error FCM'], $e);
        }

        return response()->json($respuesta);
    }

    public function entregadoPedido($id){
        $formato = new JSON;
        try {

            $buscarPedido = new PC;
            $pedido = $buscarPedido->one($id)
                                    ->where('estatus', 'encamino')
                                    ->where('motorizado_id', $this->motorizado()->id)
                                    ->with(['tienda', 'cliente'])
                                    ->firstOrFail();
            $pedido->motorizado_id = $this->motorizado()->id;
            $pedido->estatus = "entregado";
            $pedido->entregado = now();
            $pedido->save();

            $motoPedido = new MotoPedido;
            $motoPedido->entregado($id, $this->motorizado()->id);

            $nuevoNombreCliente = $pedido->cliente->phone . " " . $pedido->cliente->name;

            $pedido->cliente->name = $nuevoNombreCliente;

            $respuesta = $formato->success($pedido);

        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Pedido no encontrado'], $e);
        }

        $pedidoNotificable = [
            'id' => $pedido->id,
            'estatus' => $pedido->estatus
        ];    

        try {
            $notificacion = new FCM;
            $notificacion->enviarNotificacion($this->motorizado()->fcm_token, '', 'Haz entregado el pedido', $pedidoNotificable);
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error FCM'], $e);
        }

        try {
            $cliente = User::find($pedido->cliente_id);
            $notificacion = new FCM;
            $notificacion->enviarNotificacion($cliente->fcm_token, '', 'Recibiste tu pedido', $pedidoNotificable);
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error FCM'], $e);
        }

        return response()->json($respuesta);
    }

    public function motoPedidosActivos()
    {
        $formato = new JSON;
        $mostrarMotoPedido = new MotoPedido;
        $motoPedido = $mostrarMotoPedido->all()
                                        ->where('motorizado_id', $this->motorizado()->id)
                                        ->where(function($q){
                                            $q->orWhere('estatus', 'listo');
                                            $q->orWhere('estatus', 'buscando');
                                            $q->orWhere('estatus', 'encamino');
                                        })
                                        ->first();
        
        $respuesta = $formato->success($motoPedido);

        return response()->json($respuesta);
    }

    public function motorizado(){
        return Auth::user();
    }

    public function estaEnAreaTienda($tienda_id, $lat_user, $long_user)
    {
        $tiendaCoordenadas = Coordenadas::where('tienda_id', $tienda_id)->get();

        $lat = [];
        $long = [];
        foreach($tiendaCoordenadas as $c){
            $lat[] = $c->latitud;
            $long[] = $c->longitud;
        }

        $lat[] = $c->latitud[0];
        $long[] = $c->longitud[0];

        return $this->checkLatLng($lat, $long, $lat_user, $long_user);

    }


    public function checkLatLng($y, $x, $lat_user, $long_user){

        $vertices_y = $y;    // x-coordinates of the vertices of the polygon (LATITUDES)
        $vertices_x = $x; // y-coordinates of the vertices of the polygon (LONGITUDES)
        $points_polygon = count($vertices_x)-1; 
        $latitude_y = $lat_user;    // Your Latitude
        $longitude_x = $long_user;  // Your Longitude

        if ($this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)){
            return 1;
        }else{
            return 0;
        }
    }

    public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y){
        $i = $j = $c = $point = 0;
        for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
                $point = $i;
            if( $point == $points_polygon )
                $point = 0;
            if ( (($vertices_y[$point]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) && ($longitude_x < ($vertices_x[$j] - $vertices_x[$point]) * ($latitude_y - $vertices_y[$point]) / ($vertices_y[$j] - $vertices_y[$point]) + $vertices_x[$point]) ) )
                $c = !$c;
        }
        return $c;
    }

    public function obtenerDistancia($pedido_id, $latTienda, $longTienda, $latPedido, $longPedido)
    {
        $mapsApiKey="AIzaSyC5WO_983jAxhg6v-_AHH94FXMZzFUXDVo";
        $coordTienda = [
            "lat" => $latTienda,
            "long" => $longTienda
        ];

        $coordPedido = [
            "lat" => $latPedido,
            "long" => $longPedido
        ];


        $ruta = "https://maps.googleapis.com/maps/api/directions/json?origin=". $coordTienda['lat'] .",". $coordTienda['long'] ."&destination=". $coordPedido['lat'] .",". $coordPedido['long'] ."&units=metric&key=". $mapsApiKey ."&alternatives=true";
        
        try {

            $json = file_get_contents($ruta);
            $obj = json_decode($json);
    
            $cantRutasAlternativas = count($obj->routes);
            $rutaMenor = 99999;
    
            for($i = 0; $i < $cantRutasAlternativas; $i++){
                $j = $i;
                // echo "Ruta " . ($j+1) . " => ". $obj->routes[$i]->legs[0]->distance->text . "<br> ";
                $explodeDistancia = explode(" ", $obj->routes[$i]->legs[0]->distance->text);
                $cantRecorrida = $explodeDistancia[0]; 
                
    
                if($cantRecorrida <= $rutaMenor){
                    $rutaMenor = $cantRecorrida;
                    $unidadMedida = $explodeDistancia[1];
                }
                
            }

            Pedidos::where('id', $pedido_id)
                    ->update([
                        'distancia' => $rutaMenor,
                        'distmedida' => $unidadMedida
                    ]);
            
        } catch (\Exception $e) {
            return 1;
        }

        return 1;
    }

    public function obtenerDistanciaRecta($latTienda, $longTienda, $latPedido, $longPedido)
    {
        $latFrom = deg2rad($latTienda);
        $lonFrom = deg2rad($longTienda);
        $latTo = deg2rad($latPedido);
        $lonTo = deg2rad($longPedido);
      
        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;
      
        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
          cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        $distancia = round($angle * 6371000, 2);
        
        return $distancia;
    }

    public function cerrado($t)
    {
        $hoyEs = date('l');
            switch ($hoyEs) {
                case 'Monday':
                    $horario = $t->tiempoLunes;
                    break;
                case 'Tuesday':
                    $horario = $t->tiempoMartes;
                    break;
                case 'Wednesday':
                    $horario = $t->tiempoMiercoles;
                    break;
                case 'Thursday':
                    $horario = $t->tiempoJueves;
                    break;
                case 'Friday':
                    $horario = $t->tiempoViernes;
                    break;
                case 'Saturday':
                    $horario = $t->tiempoSabado;
                    break;
                case 'Sunday':
                    $horario = $t->tiempoDomingo;
                    break;
                
                default:
                    return 0;
                    break;
            }
        $horaActual = now();
        $tiempo = explode('-', $horario);
        $abre = Carbon::parse(date('Y-m-d') . " " . $tiempo[0]);
        $cierra = Carbon::parse(date('Y-m-d') . " " . $tiempo[1]);

        if($horaActual->gte($abre) && $horaActual->lte($cierra)){
            return 0;
        }
        return 1;

    }

}
