<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Notifications\registerCliente;
use App\User;
use Auth;
use Carbon\Carbon;
use App\Users_tipo;
use App\Users_validate;
use App\Http\Controllers\FCMnotificacion as FCM;
use App\Notifications\CodigoValidacion;
use App\Notifications\PasswordRecoveryNotificaction;
use Illuminate\Support\Str;

class AuthController extends Controller
{

    // use RegistersUsers;

    public function login(Request $request)
    {
        $formato = new JSON;

        $request->validate([
            'email'       => 'required|string|email',
            'password'    => 'required|string',
            'tienda_id'    => 'required|string',
        ]);

        $credentials = request(['email', 'password', 'tienda_id']);

        // $credentials['deleted_at'] = null;
        
        if (!Auth::attempt($credentials)) {
            $respuesta = $formato->error(['error' => 'Usuario o contraseña inválidos.']);
            return response()->json($respuesta);
        }

        try{
            $user = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            if($request->has('fcm_token')){
                $user->fcm_token = $request->fcm_token;
            }
            if($request->has('dispositivo')){
                $user->dispositivo = $request->dispositivo;
            }else{
                $user->dispositivo = "Android";
            }
            $user->estatus = 1;
            $user->save();
            $token->save();
        }catch(\Exception $e){
            $respuesta = $formato->error(['error' => 'Error al registrar el token.' . $e], $e);
            return response()->json($respuesta);
        }

        // Aquí comprobamos si el usuario activo su cuenta
        if($user->validate==0){
            $respuesta = $formato->error([
                'error' => 'Valida tu cuenta para iniciar sesión',
                'user_id' => $user->id,
            ], '');

            try {
                $data=[
                    'codigo' => $user->usuariovalidate->codigo,
                    'user_id' => $user->id,
                ];
                $notificacion = new FCM;
                $notificacion->enviarNotificacion($user->fcm_token, '', 'Valida tu usuario, código de validación: ' . $data['codigo'], $data);
            } catch (\Exception $e) {
                $formato->error(['error' => 'error FCM'], $e);
            }

            return response()->json($respuesta);
        }

        $data = [
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => carbon::parse(
                $tokenResult->token->expires_at)
                    ->toDateTimeString(),
            'type' => $user->tipo->name,
            'name' => $user->name,
            'direccion' => $user->direccion,
            'latitud' => $user->latitud,
            'longitud' => $user->longitud,
            'email' => $user->email,
            'id' => $user->id,
            'phone' => $user->phone,
            'validate' => $user->validate,
            'mistarjetas' => $user->mistarjetas,
        ];

        $respuesta = $formato->success($data);
        return response()->json($respuesta);
    }

    public function usuario(Request $request)
    {
        $formato = new JSON;
        $usuario= [
            'id' => $request->user()->id,
            'name' => $request->user()->name,
            'email' => $request->user()->email,
            'tipo' => $request->user()->tipo->name,
            'validate' => $request->user()->validate,
            'ultima_conexion' => $request->user()->updated_at->format("d/m/Y h:i A"),
        ];
        $respuesta = $formato->success($usuario);
        return response()->json($respuesta);
    }

    public function logout(Request $request)
    {
        User::where('id', $request->user()->id)
            ->update(['estatus' => false]);

        $request->user()->token()->revoke();
        return response()->json(['message' => 
            'Successfully logged out']);
    }

    public function signup(Request $request)
    {
        $formato = new JSON;
        
        $valido = validator()->make($request->all(), [
            'name'     => 'required|min:2|max:50',
            'email'    => 'required|email',
            'password' => 'required|min:6|confirmed',
        ],[
            'name.required' => "El nombre es requerido.",
            'name.min' => "Mínimo 2 carácteres.",
            'name.max' => "Máximo 50 carácteres.",
            'email.required' => 'Ingrese el email.',
            'email.email' => 'El correo que ingresaste tiene formato no válido.',
            'password.required' => 'Ingrese la contraseña.',
            'password.min' => 'Mínimo 6 carácteres.',
            'password.confirmed' => 'La contraseña no coincide.',
        ]);

        if($valido->fails()){
            
            if($valido->errors()->first() === 'El correo que ingresaste ya está registrado.'){
                $respuesta = $formato->error(['error' => 'El correo ya existe.'], '');
                return response()->json($respuesta);
            }
            $respuesta = $formato->error(['error' => $valido->errors()->first()], '');
            return response()->json($respuesta);
        }


        $tipo = Users_tipo::where('name', 'cliente')->first();
        $tipo_usuario = $tipo->id;
            
        
        try{
            $user = new User([
                'name'     => $request->name,
                'email'    => $request->email,
                'phone'    => str_replace(" ", "", $request->phone),
                'estatus'  => true,
                'password' => bcrypt($request->password),
                'tipo_id' => $tipo_usuario,
                'tienda_id' => $request->tienda_id
            ]);

            if($request->has('fcm_token')){
                $user->fcm_token = $request->fcm_token;
            }

            if($request->has('dispositivo')){
                $user->dispositivo = $request->dispositivo;
            }else{
                $user->dispositivo = "Android";
            }
            
            $user->save();

        }catch(\Exception $e){
            $respuesta = $formato->error(['error' => 'El correo que ingresaste ya está registrado.'], $e);
            return response()->json($respuesta);
        }


            $codigo_validacion = Users_validate::create([
                "codigo" => random_int(0,9) . random_int(0,9) . random_int(0,9) . random_int(0,9),
                "user_id" => $user->id,
                "validate" => 1,
            ]);

            // try {
            //     $data=[
            //         'codigo' => $codigo_validacion->codigo,
            //         'user_id' => $codigo_validacion->user_id,
            //     ];
            //     $notificacion = new FCM;
            //     $notificacion->enviarNotificacion($user->fcm_token, '', 'Código de validación: ' . $data['codigo'], $data);
            // } catch (\Exception $e) {
            //     $formato->error(['error' => 'Notificacion de registro de usuario sin enviar'], $e);
            // }

            // try {
            //     $user->codigo = $codigo_validacion->codigo;
            //     $user->notify(new CodigoValidacion($user));
            // } catch (\Exception $e) {}

            // Todo listo para enviar correo electronico.
            // $user->notify(new registerCliente($user));
            $respuesta = $formato->success([
                "user_id" => $user->id,
                "mensaje" => 'Registro exitoso',
                ]);
            return response()->json($respuesta);
        
    }

    public function reenviarCodigo($user_id){
        $formato = new JSON;
        try {
            $usuario = User::findOrFail($user_id);
            $respuesta = $formato->success("Código de validación: " . $usuario->usuariovalidate->codigo);

            try {
                $data=[
                    'codigo' => $usuario->usuariovalidate->codigo,
                    'user_id' => $usuario->id,
                ];
                $notificacion = new FCM;
                $notificacion->enviarNotificacion($usuario->fcm_token, '', 'Código de validación: ' . $data['codigo'], $data);
            } catch (\Exception $e) {
                $formato->error(['error' => 'Notificacion de registro de usuario sin enviar'], $e);
            }

        } catch (\Throwable $e) {
            $respuesta = $formato->error(["error" => "Usuario no válido"], $e);
        }

        return response()->json($respuesta);

    }

    public function validarCodigo(Request $r){
        $formato = new JSON;
        try {

            $actualizar= Users_validate::where('user_id', $r->user_id)
                            ->where('codigo', $r->codigo)
                            ->update([
                                "validate" => 1
                            ]);
            
            if(!$actualizar){
                throw new \Exception();
            }

            $usuario = User::where('id', $r->user_id)->first();

            try {
                $notificacion = new FCM;
                $notificacion->enviarNotificacion($usuario->fcm_token, '', 'Validación exitosa', []);
            } catch (\Exception $e) {
                $formato->error(['error' => 'Notificacion de registro de usuario sin enviar'], $e);
            }

            $respuesta = $formato->success("Validación exitosa");
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Validación fallida'], $e);
        }

        return response()->json($respuesta);

    }

    public function update(Request $r){

        $formato = new JSON;

        $user = User::where('id', Auth::user()->id)->first();
        $user->name = $r->name;
        $user->email = $r->email;
        $user->phone = $r->phone;
        if($r->password != "")
            $user->password = bcrypt($r->password);
        $user->save();

        $respuesta = $formato->success("Usuario actualizado");

        if(!$user){
            $respuesta = $formato->error(["error" => "Error en la actualización"], "");
        }


        return response()->json($respuesta);
    }

    public function passwordRecovery(Request $request)
    {

        $formato = new JSON;

        try {
            $user = User::where('email', $request->email)
                ->where('tienda_id', $request->tienda_id)
                ->firstOrFail();

            $tienda = User::where('id', $request->tienda_id)->firstOrFail();

            $nuevoPassword = strtoupper(Str::random(8));
            $user->password = bcrypt($nuevoPassword);
            $user->save();

            $data = [
                'tienda' => $tienda,
                'usuario' => $user,
                'nuevoPassword' => $nuevoPassword,
            ];

            $respuesta = $formato->success("Se ha enviado una nueva contraseña al email ");
            $this->enviarEmailPasswordRecovery($user, $data);

        } catch (\Exception $e) {
            $respuesta = $formato->error(["error" => $e->getmessage()], "");
        }

        return response()->json($respuesta);
    }

    public function enviarEmailPasswordRecovery($usuario, $data)
    {
        $usuario->notify(new PasswordRecoveryNotificaction($data));
    }

}
