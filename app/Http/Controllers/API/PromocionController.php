<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banner;
use App\Http\Controllers\API\JsonResponseApi as JSON;

class PromocionController extends Controller
{
    public function banner()
    {
        $formato = new JSON;

        $banners = Banner::with('tienda')
                            ->activo()
                            ->orderBy('updated_at', 'desc')
                            ->get();

        $respuesta = $formato->success($banners);

        return response()->json($respuesta);
    }
}
