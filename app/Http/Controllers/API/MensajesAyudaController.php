<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\modelos\MensajesAyudaController as MENSAJES;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use Auth;

class MensajesAyudaController extends Controller
{
    public function recibirMensaje(Request $r){

        $cliente = $this->cliente();
        $formato = new JSON;

        $r['cliente'] = $cliente;
        // return $r;

        try {
            $grabarMensaje = new MENSAJES;
            $mensaje = $grabarMensaje->store($r);

            if($mensaje){
                $respuesta = $formato->success("Mensaje recibido.");
            }else{
                throw new \Exception("Error al guardar el mensaje de ayuda");
            }

        } catch (\Exception $e) {
            $respuesta = $formato->error(["error" => "Error al recibir el mensaje" . $mensaje], $e);
        }

        return response()->json($respuesta);

    }

    public function cliente(){
        return Auth::user();
    }
}
