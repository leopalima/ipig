<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\User;
use App\Prueba;
use Auth;

class CoordenadasController extends Controller
{
    public function actualizarCoordenadas(Request $r){
        $x = $this->usuario()->id;
        $y = $this->usuario()->email;
        Prueba::create([
            'texto' => $r->latitud . ',' . $r->longitud . ' - ' . $x . ' - ' . $y
        ]);

        $formato = new JSON;
        try {
            
            $coordUser = User::where('id', $this->usuario()->id)
                            ->select('id', 'latitud', 'longitud')
                            ->firstOrfail();

            $coordUser->latitud = $r->latitud;
            $coordUser->longitud = $r->longitud;
            $coordUser->save();

            $latFrom = deg2rad($r->latitud);
            $lonFrom = deg2rad($r->longitud);
            $latTo = deg2rad($r->latitud_destino);
            $lonTo = deg2rad($r->longitud_destino);
          
            $latDelta = $latTo - $latFrom;
            $lonDelta = $lonTo - $lonFrom;
          
            $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
              cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

            $distancia = round($angle * 6371000, 2);
            
            $respuesta = $formato->success([
                'success' =>'Coordenadas actualizadas',
                "distancia" => $distancia
                ]);

        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error al actualizar las coordenas'], $e);
        }

        return response()->json($respuesta);
    }

    public function consultarCoordenadas($id){
        $formato = new JSON;
        try {
            $coordenadas = User::where('id', $id)
                                ->select('id', 'latitud', 'longitud')
                                ->firstOrFail();
            $usuarioCoordenadas = [
                "id" => $coordenadas->id,
                "latitud" => $coordenadas->latitud,
                "longitud" => $coordenadas->longitud,
            ];
            $respuesta = $formato->success($usuarioCoordenadas);
        } catch (\Exception $e) {
            $respuesta = $formato(['error' => "Coordenadas no encontrada"], $e);
        }

        return response()->json($respuesta);
    }

    public function usuario(){
        return Auth::user();
    }
}
