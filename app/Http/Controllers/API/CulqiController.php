<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Culqi_customer;
use App\Culqi_card;
use Auth;
use Culqi;

class CulqiController extends Controller
{

    // protected $secret_key = "sk_test_UhAGiWJ3n8iTLhkj";
    protected $secret_key = "sk_live_QrGWbBnVQgFQyI3s";

    public function crearCliente(){

        $formato = new JSON;
        $cliente = $this->cliente();

        try {
            // Configurar tu API Key y autenticación
            $SECRET_KEY = $this->secret_key;
            $culqi = new Culqi\Culqi(array('api_key' => $SECRET_KEY));

            // Creando Cargo a una tarjeta
            $cliente = $culqi->Customers->create(
                array(
                    "address" => $cliente->direccion,
                    "address_city" => "Lima",
                    "country_code" => "PE",
                    "email" => rand() . "-" . $cliente->email,
                    "first_name" => $cliente->name,
                    "last_name" => $cliente->name,
                    "phone_number" => $cliente->phone,
                )
            );

            $cliente = Culqi_customer::create([
                'user_id' => $this->cliente()->id,
                'object' => $cliente->object,
                'culqi_id' => $cliente->id,
                'creation_date' => $cliente->creation_date,
                'email' => $cliente->email,
                'country_code' => $cliente->antifraud_details->country_code,
                'first_name' => $cliente->antifraud_details->first_name,
                'last_name' => $cliente->antifraud_details->last_name,
                'address_city' => $cliente->antifraud_details->address_city,
                'address' => $cliente->antifraud_details->address,
                'phone' => $cliente->antifraud_details->phone,
            ]);

        } catch (\Exception $e) {
            $mensaje = json_decode($e->getMessage());
            $respuesta = [
                "error" => $this->capturaError($e),
                "e" => $e,
            ];

            return $respuesta;
        }

        return $cliente;
    }

    public function crearTarjeta($culqi_id, $token){
        $formato = new JSON;
        try {
            // Configurar tu API Key y autenticación
            $SECRET_KEY = $this->secret_key;
            $culqi = new Culqi\Culqi(array('api_key' => $SECRET_KEY));
    
            // Creando Cargo a una tarjeta
            $card = $culqi->Cards->create(
                array(
                "customer_id" => $culqi_id,
                "token_id" => $token,
                )
            );

            $tarjeta = Culqi_card::create([
                'user_id' => $this->cliente()->id,
                'culqi_id' => $card->id,
                'date' => $card->creation_date,
                'customer_id' => $card->customer_id,
                'card_number' => $card->source->card_number,
                'last_four' => $card->source->last_four,
                'active' => $card->active,
                'card_brand' => $card->source->iin->card_brand,
                'card_type' => $card->source->iin->card_type,
                'card_category' => $card->source->iin->card_category,
                'name' => $card->source->iin->issuer->name,
                'country' => $card->source->iin->issuer->country,
                'country_code' => $card->source->iin->issuer->country_code,
                'ip' => $card->source->client->ip,
            ]);

        } catch (\Exception $e) {
            $mensaje = json_decode($e->getMessage());
            $respuesta = [
                "error" => $this->capturaError($e),
                "e" => $e,
            ];
            return $respuesta;
        }

        return $card;

    }

    public function guardarTarjeta(Request $r){

        $formato= new JSON;

        if($this->cliente()->culqiCustomer){
            $culqi_id = $this->cliente()->culqiCustomer->culqi_id;
        }else{
            
            $culqi_id = $this->crearCliente();

            if(array_key_exists("error", $culqi_id)){
                $respuesta = $formato->error(["error" => $culqi_id["error"]], $culqi_id["e"]);
                return response()->json($respuesta);
            }else{
                $culqi_id = $culqi_id->culqi_id;
            }
        }

        $crearTarjeta = $this->crearTarjeta($culqi_id, $r->token_culqi);

        if(array_key_exists("error", $crearTarjeta)){
            $respuesta = $formato->error(["error" => $crearTarjeta["error"]], $crearTarjeta["e"]);
        }else{
            $respuesta = $formato->success($crearTarjeta);
        }




        return response()->json($respuesta);

    }

    public function cliente(){
        return Auth::user();
    }

    public function eliminarTarjeta(Request $r){

        $formato = new JSON;
        try {
            // Configurar tu API Key y autenticación
            $SECRET_KEY = $this->secret_key;
            $culqi = new Culqi\Culqi(array('api_key' => $SECRET_KEY));

            $cardDelete = $culqi->Cards->delete($r->card_culqi);

            $cardDelete = Culqi_card::where('culqi_id', $r->card_culqi)
                                        ->where('user_id', $this->cliente()->id)
                                        ->delete();

        } catch (\Exception $e) {
            
            $mensaje = json_decode($e->getMessage());
            $data = [
                "error" => $this->capturaError($e),
                "e" => $e,
            ];

            $respuesta = $formato->error(["error" => $data["error"]], $data["e"]);

            return response()->json($respuesta);
        }
        $respuesta = $formato->success("Tarjeta eliminada.");
        return response($respuesta);


    }

    public function capturaError($e)
    {
        $error = json_decode($e->getMessage(), true);
        $tipo = $error['type'];
        $mensaje = "";

        switch ($tipo) {
            case 'invalid_request_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'authentication_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'parameter_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'card_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'limit_api_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'resource_error  ':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'api_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;

            default:
                $mensaje = "ERROR.";
                break;
        }
        return $mensaje;
    }


}
