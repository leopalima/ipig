<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Http\Controllers\modelos\UsersController as Tiendas;
use App\Http\Controllers\modelos\ProductosCategoriasController as Categorias;
use App\Http\Controllers\modelos\ProductosController as Productos;
use Carbon\Carbon;
use App\Logs;
use Auth;
use Log;

class TiendasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $formato = new JSON;
        try {
            $listarTiendas = new  Tiendas;
            $tiendas = $listarTiendas->all()
                                        ->whereHas('tipo', function($q){
                                            $q->where('name', 'tienda');
                                        })
                                        ->select('id', 'name', 'email', 'foto', 'latitud', 'longitud', 'direccion')
                                        ->get();
            
            $tiendas2 = collect($this->tiendaArray($tiendas))->toArray();

            $respuesta = $formato->success($tiendas2);
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error en la consulta'], $e);
        }

        try {
            $log = array(
                'user_id' => Auth::check() ? Auth::user()->id : 0,
                'descripcion' => 'Pantalla de todas las tiendas',
            );
            Logs::create($log);
        } catch (\Exception $e) {
            Log::error($e);
        }

        return response()->json($respuesta);
    }
    public function multiMarcas($id)
    {
        $formato = new JSON;
        try {
            $listarTiendas = new  Tiendas;
            $tiendas = $listarTiendas->all()
                                        ->whereHas('tipo', function($q){
                                            $q->where('name', 'tienda');
                                        })
                                        ->where('multimarca_id', $id)
                                        ->select('id', 'name', 'email', 'foto', 'latitud', 'longitud', 'direccion')
                                        ->get();
            
            $tiendas2 = collect($this->tiendaArray($tiendas))->toArray();

            $respuesta = $formato->success($tiendas2);
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error en la consulta'], $e);
        }

        try {
            $log = array(
                'user_id' => Auth::check() ? Auth::user()->id : 0,
                'descripcion' => 'Pantalla de todas las tiendas',
            );
            Logs::create($log);
        } catch (\Exception $e) {
            Log::error($e);
        }

        return response()->json($respuesta);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function one($id)
    {
        $formato = new JSON;
        try {
            $buscarTienda = new  Tiendas;
            $tienda = $buscarTienda->one($id)
                                        ->whereHas('tipo', function($q){
                                            $q->where('name', 'tienda');
                                        })
                                        ->get();
            $respuesta = $formato->success($tienda);
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error en la consulta'], $e);
        }

        try {
            $log = array(
                'user_id' => Auth::check() ? Auth::user()->id : 0,
                'descripcion' => 'Visualiza la tienda: ' . $tienda->first()->name,
            );
            Logs::create($log);
        } catch (\Exception $e) {
            Log::error($e);;
        }
        
        return response()->json($respuesta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function categorias($id)
    {
        $formato = new JSON;
        try {
            $buscarCategorias = new Categorias;
            $categorias = $buscarCategorias->all()
                                    ->where('tienda_id', $id)
                                    ->where('estatus', 1)
                                    ->with(['productos' => function($q){
                                        $q->where('estatus', 1);
                                        $q->orderBy('orden', 'asc');
                                        $q->select('id', 'name', 'descripcion', 'precio', 'tienda_id', 'categoria_id', 'foto', 'estatus', 'orden');
                                    }, 'productos.adicionales' => function($q){
                                        $q->where('estatus', 1);
                                        $q->orderBy('orden', 'asc');
                                        $q->select('id', 'titulo', 'maximo', 'tienda_id', 'producto_id', 'estatus', 'orden');
                                    }, 'productos.adicionales.items' => function($q){
                                        $q->where('estatus', 1);
                                        $q->select('id', 'nombre', 'precio', 'tienda_id', 'adicionales_id');
                                    }])
                                    ->whereHas('productos', function($q){
                                        $q->where('id', '!=', '');
                                    })
                                    ->select(['id', 'name', 'orden', 'foto'])
                                    ->get();
            $respuesta = $formato->success($categorias);
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error en la consulta'], $e);
        }

        try {
            $tienda = new Tiendas;
            $log = array(
                'user_id' => Auth::check() ? Auth::user()->id : 0,
                'descripcion' => 'Visualiza la tienda: ' . $tienda->one($id)->first()->name,
            );
            Logs::create($log);
        } catch (\Exception $e) {
            Log::error($e);;
        }

        
        return response()->json($respuesta);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function categoriaProductos($id, $categoria_id)
    {
        $formato = new JSON;
        try {
            $buscarProductos = new Productos;
            $productos = $buscarProductos->all()
                                    ->where('tienda_id', $id)
                                    ->where('categoria_id', $categoria_id)
                                    ->with('adicionales')
                                    ->get();
            $respuesta = $formato->success($productos);    
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error en la consulta'], $e);
        }
        
        return response()->json($respuesta);

    }

    public function buscar(Request $r){
        $formato = new JSON;
        try {
            $buscarTiendas = new Tiendas;
            $listaTiendas = $buscarTiendas->all()
                                            ->whereHas('tipo', function($q){
                                                $q->where('name', 'tienda');
                                            })
                                            ->where('name', 'like', '%'. $r->string .'%')
                                            ->select('id', 'name', 'email', 'foto', 'latitud', 'longitud', 'direccion')
                                            ->get();

            $respuesta = $formato->success($listaTiendas);

        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error en la busqueda'], $e);
        }

        return response()->json($respuesta);
    }

    public function buscarEtiqueta($etiqueta){
        $formato = new JSON;
        try {
            $buscarTiendas = new Tiendas;
            $listaTiendas = $buscarTiendas->all()
                                            ->whereHas('tipo', function($q){
                                                $q->where('name', 'tienda');
                                            })
                                            ->whereHas('configuracion', function($q) use ($etiqueta){
                                                $q->where('valor', 'like', '%'. $etiqueta .'%');
                                            })
                                            ->select('id', 'name', 'email', 'foto', 'latitud', 'longitud', 'direccion')
                                            ->get();

            $tiendas2 = collect($this->tiendaArray($listaTiendas))->toArray();

            $respuesta = $formato->success($tiendas2);

        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error en la busqueda'. $e], $e);
        }

        return response()->json($respuesta);
    }

    public function tiendaArray($tiendas)
    {
        $tiendaAbierta = Array();
        $tiendaCerrada = Array();
        $todasLasTiendas = Array();
        foreach($tiendas as $t){
            $hoyEs = date('l');
            switch ($hoyEs) {
                case 'Monday':
                    $cerrado = $this->cerrado($t->tiempoLunes);
                    break;
                case 'Tuesday':
                    $cerrado = $this->cerrado($t->tiempoMartes);
                    break;
                case 'Wednesday':
                    $cerrado = $this->cerrado($t->tiempoMiercoles);
                    break;
                case 'Thursday':
                    $cerrado = $this->cerrado($t->tiempoJueves);
                    break;
                case 'Friday':
                    $cerrado = $this->cerrado($t->tiempoViernes);
                    break;
                case 'Saturday':
                    $cerrado = $this->cerrado($t->tiempoSabado);
                    break;
                case 'Sunday':
                    $cerrado = $this->cerrado($t->tiempoDomingo);
                    break;
                
                default:
                    $cerrado = "";
                    break;
            }

            if($cerrado === ""){

                $datosTienda = Array(
                    "id" => $t->id,
                    "name" => $t->name,
                    "email" => $t->email,
                    "foto" => $t->foto,
                    "latitud" => $t->latitud,
                    "longitud" => $t->longitud,
                    "direccion" => $t->direccion,
                    "tiempoEntrega" => $t->tiempoEntrega,
                    "costoEnvio" => $t->costoEnvio,
                    "valoracion" => $t->valoracion,
                    "etiquetas" => $t->etiquetas,
                    "validate" => $t->validate,
                    "motospropias" => $t->motospropias,
                    "mistarjetas" => $t->mistarjetas,
                    "pagosaceptados" => [
                        // "efectivo" => ($t->motospropias==0) ? 1 : $t->pagosaceptados["efectivo"],
                        "efectivo" => $t->pagosaceptados["efectivo"],
                        "visa" => $t->pagosaceptados["visa"],
                        "master" => $t->pagosaceptados["master"],
                    ],
                    "tiempoLunes" => $t->tiempoLunes,
                    "tiempoMartes" => $t->tiempoMartes,
                    "tiempoMiercoles" => $t->tiempoMiercoles,
                    "tiempoJueves" => $t->tiempoJueves,
                    "tiempoViernes" => $t->tiempoViernes,
                    "tiempoSabado" => $t->tiempoSabado,
                    "tiempoDomingo" => $t->tiempoDomingo,
                    "cerradoEmergenciaActivo" => $t->cerradoEmergenciaActivo,
                    "calificacion" => $t->calificacion,
                    "descuento" => $t->descuento,
                    "whatsapp" => $t->whatsapp,
                    "recojoentienda" => $t->recojoentienda,
                    "terminos" => $t->terminos,
                    "abierto" => $t->abierto,
                    "esMultimarcas" => $t->esMultimarcas,

            );

            if($t->id==""){
                array_unshift($tiendaAbierta, $datosTienda);
            }else{
                $tiendaAbierta[] = $datosTienda;
            }

            }else{
                $fotoGris = $this->fotoGris($t->foto);
                $tiendaCerrada[] = Array(
                        "id" => $t->id,
                        "name" => $cerrado . $t->name,
                        "email" => $t->email,
                        "foto" => $fotoGris,
                        "latitud" => $t->latitud,
                        "longitud" => $t->longitud,
                        "direccion" => $t->direccion,
                        "tiempoEntrega" => $t->tiempoEntrega,
                        "costoEnvio" => $t->costoEnvio,
                        "valoracion" => $t->valoracion,
                        "etiquetas" => $t->etiquetas,
                        "validate" => $t->validate,
                        "motospropias" => $t->motospropias,
                        "mistarjetas" => $t->mistarjetas,
                        "pagosaceptados" => [
                            "efectivo" => $t->pagosaceptados["efectivo"],
                            "visa" => $t->pagosaceptados["visa"],
                            "master" => $t->pagosaceptados["master"],
                        ],
                        "tiempoLunes" => $t->tiempoLunes,
                        "tiempoMartes" => $t->tiempoMartes,
                        "tiempoMiercoles" => $t->tiempoMiercoles,
                        "tiempoJueves" => $t->tiempoJueves,
                        "tiempoViernes" => $t->tiempoViernes,
                        "tiempoSabado" => $t->tiempoSabado,
                        "tiempoDomingo" => $t->tiempoDomingo,
                        "cerradoEmergenciaActivo" => $t->cerradoEmergenciaActivo,
                        "calificacion" => $t->calificacion,
                        "descuento" => $t->descuento,
                        "whatsapp" => $t->whatsapp,
                        "recojoentienda" => $t->recojoentienda,
                        "terminos" => $t->terminos,
                        "abierto" => $t->abierto,
                        "EsMultimarcas" => $t->EsMultimarcas,
                );
            }

            $todasLasTiendas = array_merge($tiendaAbierta, $tiendaCerrada);

        }
        return $todasLasTiendas;
    }

    public function cerrado($tiempos)
    {
        $horaActual = now();
        $tiempo = explode('-', $tiempos);
        $abre = Carbon::parse(date('Y-m-d') . " " . $tiempo[0]);
        $cierra = Carbon::parse(date('Y-m-d') . " " . $tiempo[1]);

        if($horaActual->gte($abre) && $horaActual->lte($cierra)){
            return "";
        }
        return "Cerrado - ";

    }

    public function fotoGris($foto)
    {
        $arrayFotoJPEG = explode('.jpeg', $foto);
        $arrayFotoPNG = explode('.png', $foto);
        $arrayFotoGIF = explode('.gif', $foto);

        if(count($arrayFotoJPEG) > 1){
            return $arrayFotoJPEG[0] . "-gris" . ".jpeg";
        }
        if(count($arrayFotoPNG) > 1){
            return $arrayFotoPNG[0] . "-gris" . ".png";
        }
        if(count($arrayFotoGIF) > 1){
            return $arrayFotoGIF[0] . "-gris" . ".gif";
        }

        return "";

    }
}

