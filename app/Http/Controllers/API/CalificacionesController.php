<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Http\Controllers\modelos\CalificacionesController as Calificaciones;
use Auth;

class CalificacionesController extends Controller
{
    public function recibirCalificacion(Request $r){

        $formato = new JSON;
        try {

            $r['cliente'] = $this->cliente();

            $grabarCalificacion = new Calificaciones;
            $calificacion = $grabarCalificacion->store($r);
            
            if($calificacion){
                $respuesta = $formato->success("Calificación realizada");
            }else{
                throw new \Exception("Error al calificar");
            }

        } catch (\Exception $e) {
            $respuesta = $formato->error(["error" => "Error al calificar". $e], $e);
        }

        return response()->json($respuesta);
        
    }

    public function cliente(){
        return Auth::user();
    }


}
