<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Http\Controllers\modelos\EtiquetasController as Etiquetas;

class EtiquetasController extends Controller
{
    public function obtenerEtiquetas(){

        $formato = new JSON;

        $listarEtiquetas = new Etiquetas;
        $etiquetas = $listarEtiquetas->all()->select('etiqueta')->get();

        $respuesta = $formato->success($etiquetas);

        return response()->json($respuesta);

    }
}
