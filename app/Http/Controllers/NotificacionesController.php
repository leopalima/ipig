<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notificaciones;
use App\Emojis;
use App\Http\Controllers\FCMnotificacion as FCM;
use App\User;

class NotificacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notificaciones = Notificaciones::orderBy('created_at', 'desc')->paginate(15);
        $emojis = Emojis::all();
        
        return view('promocion.notificaciones', compact('notificaciones', 'emojis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $respuesta = array();
        try {
            $noti = new Notificaciones;
            $noti->titulo = $request->titulo;
            $noti->mensaje = $request->mensaje;
            $noti->save();

            $respuesta = array(
                'estatus' => 'success',
                'data' => $noti,

            );

            try {
                $usuarios = User::whereHas('tipo', function($q){
                                        $q->where('name', 'cliente');
                                    })
                                    ->get();
                $tokens = $usuarios->pluck('fcm_token')->toArray();
                try {
                    $notificacion = new FCM;
                    $notificacion->enviarNotificacion($tokens, $request->titulo, $request->mensaje, []);    
                } catch (\Exception $e) {}
            } catch (\Exception $e) {}

        } catch (\Exception $e) {
            $respuesta = array(
                'estatus' => 'error',
                'data' => $e,

            );
        }

        return response()->json($respuesta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $respuesta = array();
        try {
            $noti = Notificaciones::where('id', $id)->firstOrFail();
            $respuesta = array(
                "estatus" => "success",
                "data" => $noti,
            );
        } catch (\Exception $e) {
            $respuesta = array(
                "estatus" => "error",
                "data" => $e,
            );
        }
        return response()->json($respuesta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
