<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pedido_detalles;
use App\User;
use DB;
use Auth;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DateTime;
use App\Pedidos;
use Illuminate\Support\Collection;

use App\Pago_efectivo;

class DashboardController extends Controller
{

    public function getUltimoDiaMes($elAnio,$elMes) {
        return date("d",(mktime(0,0,0,$elMes+1,1,$elAnio)-1));
    }

    //cantidad de dias de semana
        public function getMondays()
        {
            return new \DatePeriod(
                Carbon::parse("first monday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first monday of next month")
            );
        }
        public function getTuesday()
        {
            return new \DatePeriod(
                Carbon::parse("first tuesday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first tuesday of next month")
            );
        }
        public function getWednesday()
        {
            return new \DatePeriod(
                Carbon::parse("first wednesday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first wednesday of next month")
            );
        }
        public function getThursday()
        {
            return new \DatePeriod(
                Carbon::parse("first thursday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first thursday of next month")
            );
        }
        public function getFriday()
        {
            return new \DatePeriod(
                Carbon::parse("first friday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first friday of next month")
            );
        }
        public function getSaturday()
        {
            return new \DatePeriod(
                Carbon::parse("first saturday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first saturday of next month")
            );
        }
        public function getSunday()
        {
            return new \DatePeriod(
                Carbon::parse("first sunday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first sunday of next month")
            );
        }
    //cantidad de dias de semana

    public function index(){

        $usuarioAqui=Auth::user()->id;
        
       
            //estadistica del dia por hora
                $estadisticaDiaHora=$this->estadisticaDiaHora($usuarioAqui);
                $horaLunes=$estadisticaDiaHora[0];
                $horaMartes=$estadisticaDiaHora[1];
                $horaMiercoles=$estadisticaDiaHora[2];
                $horaJueves=$estadisticaDiaHora[3];
                $horaViernes=$estadisticaDiaHora[4];
                $horaSabado=$estadisticaDiaHora[5];
                $horaDomingo=$estadisticaDiaHora[6];

            //estadistica del dia por hora
                
            //ventas del mes por año

                $ventasMesPorAnio=$this->ventasMesPorAnio($usuarioAqui);
                $diasEfectivo=$ventasMesPorAnio[0];
                $diasPos=$ventasMesPorAnio[1];
                $nombreDias=$ventasMesPorAnio[2];

            //ventas del mes por año

            //fechas comparar

                $todofechasComparar=$this->fechasComparar($usuarioAqui);
                $fechasGraficasA=$todofechasComparar[0];
                $mejorTiempoA=$todofechasComparar[1];
                $fechasGraficasB=$todofechasComparar[2];
                $mejorTiempoB=$todofechasComparar[3];
                $fechasGraficasC=$todofechasComparar[4];
                $mejorTiempoC=$todofechasComparar[5];


            //fechas comparar


            //productos repetido
                $todoRepetido=$this->productoRepetido($usuarioAqui);  
                $productoName=$todoRepetido[0];
                $repetido=$todoRepetido[1];
            //productos repetido
        if(Auth::user()->tipo->name == 'tienda'){

            return view('dashboard.index-tienda');
            
            return view('dashboard.index')
            ->with('repetido',json_encode($repetido,JSON_NUMERIC_CHECK))
            ->with('fechasGraficasA',json_encode($fechasGraficasA,JSON_NUMERIC_CHECK))
            ->with('fechasGraficasB',json_encode($fechasGraficasB,JSON_NUMERIC_CHECK))
            ->with('fechasGraficasC',json_encode($fechasGraficasC,JSON_NUMERIC_CHECK))
            ->with('diasEfectivo',json_encode($diasEfectivo,JSON_NUMERIC_CHECK))
            ->with('diasPos',json_encode($diasPos,JSON_NUMERIC_CHECK))
            ->with('nombreDias',json_encode($nombreDias, JSON_UNESCAPED_UNICODE))
            ->with('horaLunes',json_encode($horaLunes, JSON_NUMERIC_CHECK))
            ->with('horaMartes',json_encode($horaMartes, JSON_NUMERIC_CHECK))
            ->with('horaMiercoles',json_encode($horaMiercoles, JSON_NUMERIC_CHECK))
            ->with('horaJueves',json_encode($horaJueves, JSON_NUMERIC_CHECK))
            ->with('horaViernes',json_encode($horaViernes, JSON_NUMERIC_CHECK))
            ->with('horaSabado',json_encode($horaSabado, JSON_NUMERIC_CHECK))
            ->with('horaDomingo',json_encode($horaDomingo, JSON_NUMERIC_CHECK))
            ->with('productoName',$productoName)
            ->with('mejorTiempoC',$mejorTiempoC)
            ->with('mejorTiempoB',$mejorTiempoB)
            ->with('mejorTiempoA',$mejorTiempoA);
        }
        if(Auth::user()->tipo->name == 'root'){
            
            return view('dashboardAdmin.index')
            ->with('repetido',json_encode($repetido,JSON_NUMERIC_CHECK))
            ->with('fechasGraficasA',json_encode($fechasGraficasA,JSON_NUMERIC_CHECK))
            ->with('fechasGraficasB',json_encode($fechasGraficasB,JSON_NUMERIC_CHECK))
            ->with('fechasGraficasC',json_encode($fechasGraficasC,JSON_NUMERIC_CHECK))
            ->with('diasEfectivo',json_encode($diasEfectivo,JSON_NUMERIC_CHECK))
            ->with('diasPos',json_encode($diasPos,JSON_NUMERIC_CHECK))
            ->with('nombreDias',json_encode($nombreDias, JSON_UNESCAPED_UNICODE))
            ->with('horaLunes',json_encode($horaLunes, JSON_NUMERIC_CHECK))
            ->with('horaMartes',json_encode($horaMartes, JSON_NUMERIC_CHECK))
            ->with('horaMiercoles',json_encode($horaMiercoles, JSON_NUMERIC_CHECK))
            ->with('horaJueves',json_encode($horaJueves, JSON_NUMERIC_CHECK))
            ->with('horaViernes',json_encode($horaViernes, JSON_NUMERIC_CHECK))
            ->with('horaSabado',json_encode($horaSabado, JSON_NUMERIC_CHECK))
            ->with('horaDomingo',json_encode($horaDomingo, JSON_NUMERIC_CHECK))
            ->with('productoName',$productoName)
            ->with('mejorTiempoC',$mejorTiempoC)
            ->with('mejorTiempoB',$mejorTiempoB)
            ->with('mejorTiempoA',$mejorTiempoA);
        }

    }

    public function graficasDatos(Request $request){

            //fechas de pagos
                $anio=$request->anio;
                $mes=$request->mes;
                $primerDia=1;
                $ultimoDia=$this->getUltimoDiaMes($anio,$mes);
                $usuarioAqui=Auth::user()->id;


                $fecha_inicial=date("Y-m-d H:i:s", strtotime($anio."-".$mes."-".$primerDia) );
                $fecha_final=date("Y-m-d H:i:s", strtotime($anio."-".$mes."-".$ultimoDia) );
            //fechas de pagos

            //buscar
            
                if(Auth::user()->tipo->name == 'root'){
                    $usuarios=DB::table('pedidos')
                    ->join('pedido_detalles', 'pedidos.id', '=', 'pedido_detalles.pedido_id')
                    ->whereBetween('pedidos.created_at', [$fecha_inicial,  $fecha_final])
                    ->where('pedidos.estatus','=', 'entregado' )
                    ->get();
                }
                if(Auth::user()->tipo->name == 'tienda'){
                    $usuarios=DB::table('pedidos')
                    ->join('pedido_detalles', 'pedidos.id', '=', 'pedido_detalles.pedido_id')
                    ->whereBetween('pedidos.created_at', [$fecha_inicial,  $fecha_final])
                    ->where('pedidos.tienda_id','=', $usuarioAqui )
                    ->where('pedidos.estatus','=', 'entregado' )
                    ->get();
                }

            //buscar

            //sumar por dia

                for($d=1;$d<=$ultimoDia;$d++){
                    $diasEfectivo[$d]=0;
                    $diasPos[$d]=0;
                    $nombreDias[$d]=""; 
                };


                for($d=1;$d<=$ultimoDia;$d++){
                
                    foreach($usuarios as $usuario){
                    
                        if( date("Y-m-d", strtotime($usuario->created_at) ) == date("Y-m-d", strtotime($anio."-".$mes."-".$d) ) && $usuario->tipo_pago == "efectivo"){
                            if(Auth::user()->tipo->name == 'root'){
                                $diasEfectivo[$d]+=($usuario->precio * $usuario->cantidad);
                            }
                            if(Auth::user()->tipo->name == 'tienda'){
                                if(Auth::user()->motospropias == 0){
                                    $descuento[$d]= 0.13*($usuario->precio * $usuario->cantidad);
                                    $descuento2[$d]= 0.18*($usuario->precio * $usuario->cantidad);
                                    $descuento3[$d]= $descuento[$d]+$descuento2[$d];
                                    $diasEfectivo[$d]=$diasEfectivo[$d]+ (($usuario->precio * $usuario->cantidad)-$descuento3[$d]); 
                                }else{                                   
                                    $descuento[$d]= 0.1*($usuario->precio * $usuario->cantidad);
                                    $descuento2[$d]= 0.18*($usuario->precio * $usuario->cantidad);
                                    $descuento3[$d]= $descuento[$d]+$descuento2[$d];
                                    $diasEfectivo[$d]=$diasEfectivo[$d]+ (($usuario->precio * $usuario->cantidad)-$descuento3[$d]); 
                                }  
                            }
                        }elseif( date("Y-m-d", strtotime($usuario->created_at) ) == date("Y-m-d", strtotime($anio."-".$mes."-".$d) ) && $usuario->tipo_pago == "POS"){
                            if(Auth::user()->tipo->name == 'root'){
                                $diasPos[$d]=$diasPos[$d]+ ($usuario->precio * $usuario->cantidad);
                            }
                            if(Auth::user()->tipo->name == 'tienda'){
                                if(Auth::user()->motospropias == 0){
                                    $descuento[$d]= 0.13*($usuario->precio * $usuario->cantidad);
                                    $descuento2[$d]= 0.18*($usuario->precio * $usuario->cantidad);
                                    $descuento3[$d]= $descuento[$d]+$descuento2[$d];
                                    $diasPos[$d]=$diasPos[$d]+ (($usuario->precio * $usuario->cantidad)-$descuento3[$d]);
                                }else{
                                    $descuento[$d]= 0.1*($usuario->precio * $usuario->cantidad);
                                    $descuento2[$d]= 0.18*($usuario->precio * $usuario->cantidad);
                                    $descuento3[$d]= $descuento[$d]+$descuento2[$d];
                                    $diasPos[$d]=$diasPos[$d]+ (($usuario->precio * $usuario->cantidad)-$descuento3[$d]);
                                }
                            } 
                        }
                    
                    }
                    $dato= new DateTime(date("Y-m-d", strtotime($anio."-".$mes."-".$d) ));
                    if($dato->format('l') == "Monday"){

                        $nombreDias[$d]="Lunes"." ".$d;

                    }if($dato->format('l') == "Tuesday"){

                        $nombreDias[$d]="Martes"." ".$d;

                    }if($dato->format('l') == "Wednesday"){

                        $nombreDias[$d]="Miercoles"." ".$d;

                    }if($dato->format('l') == "Thursday"){

                        $nombreDias[$d]="Jueves"." ".$d;

                    }if($dato->format('l') == "Friday"){

                        $nombreDias[$d]="Viernes"." ".$d;

                    }if($dato->format('l') == "Saturday"){

                        $nombreDias[$d]="Sábado"." ".$d;

                    }if($dato->format('l') == "Sunday"){

                        $nombreDias[$d]="Domingo"." ".$d;

                    }

                
                }

            //sumar por dia

        $todoUnido=[$nombreDias, $diasEfectivo,$diasPos];

       return $todoUnido;
                        
           

    }

    function productoRepetido($usuarioAqui){

        
            $buscarRepetido = DB::table('pedido_detalles')
            ->where('tienda_id','=', $usuarioAqui )
            ->select(DB::raw('count(id) as user_count, producto_id')) 
            ->groupBy('producto_id')
            ->orderBy('user_count', 'DESC')
            ->limit(15)
            ->get();
           
           
            for($i=0 ; $i < 15; $i++){
                $repetido[$i]=0;
            }
            for($i=0 ; $i < count($buscarRepetido); $i++){
            
                $repetido[$i]=$buscarRepetido[$i]->user_count;
            }
            
            
            if(!$buscarRepetido->isEmpty()){
                $productoName= DB::table('productos')
                ->where('id', '=', $buscarRepetido[0]->producto_id )
                ->get();
                if($productoName->isEmpty())
                {         
                    $productoName="No Tienes producos repetidos";
                    
                    for($i=0 ; $i < 15; $i++){
                        $repetido[$i]=0;
                    }
                }else{
                    $productoName=$productoName[0]->name;
                }

            }else{
                $productoName="No Tienes producos repetidos";
                for($i=0 ; $i < 15; $i++){
                    $repetido[$i]=0;
                }
            }

            $todoRepetido=[$productoName,$repetido];

        return $todoRepetido;

    }


    function fechasComparar($usuarioAqui){


        $fechasDeTodos= Pedidos::where('tienda_id','=', $usuarioAqui )->get();
        $fechaCompararA=[];
        $fechaCompararE=[];
        $fechaCompararEntrega=[];
        for($i=0 ; $i < 12 ; $i++){

        
        }

        for($i=0 ; $i < count($fechasDeTodos); $i++){

            $fechaCreado[$i]=Carbon::createFromDate($fechasDeTodos[$i]->created_at->toDateTimeString());
            $fechaAceptado[$i]=Carbon::createFromDate($fechasDeTodos[$i]->aceptado);
            $fechaEnviado[$i]=Carbon::createFromDate($fechasDeTodos[$i]->enviado);
            $fechaEntregado[$i]=Carbon::createFromDate($fechasDeTodos[$i]->entregado);

            if($fechasDeTodos[$i]->aceptado != null){
                $fechaCompararA[$i]=$fechaCreado[$i]->diffInSeconds($fechaAceptado[$i]);
            }
            if($fechasDeTodos[$i]->enviado != null){
                $fechaCompararE[$i]=$fechaAceptado[$i]->diffInSeconds($fechaEnviado[$i]);
            }
            if($fechasDeTodos[$i]->entregado != null){
                $fechaCompararEntrega[$i]=$fechaEnviado[$i]->diffInSeconds($fechaEntregado[$i]);
            }
        }


    

        
        // rsort($fechaCompararA);
        // rsort($fechaCompararE);
        // rsort($fechaCompararEntrega);
        
        
        if($fechaCompararA != []){
            $fechasGraficasA=array_slice($fechaCompararA, 0, 12);

            $mejorTiempoA=date('i:s', mktime(0, 0, min($fechaCompararA)));
        }else{
            $mejorTiempoA='00:00';
            for($i=0 ; $i < 12 ; $i++){
                $fechasGraficasA[$i]=0;
            }
        }
        if($fechaCompararE != []){
            $fechasGraficasB=array_slice($fechaCompararE, 0, 12);
            $mejorTiempoB=date('i:s', mktime(0, 0, min($fechaCompararE)));
        }else{
            $mejorTiempoB='00:00';
            for($i=0 ; $i < 12 ; $i++){
                $fechasGraficasB[$i]=0;
            }
        }
        if($fechaCompararEntrega != []){
            $fechasGraficasC=array_slice($fechaCompararEntrega, 0, 12);
            $mejorTiempoC=date('i:s', mktime(0, 0, min($fechaCompararEntrega)));
        }else{
            $mejorTiempoC='00:00';
            for($i=0 ; $i < 12 ; $i++){
                $fechasGraficasC[$i]=0;
            }
        }

        $todofechasComparar=[$fechasGraficasA, $mejorTiempoA, $fechasGraficasB, $mejorTiempoB, $fechasGraficasC, $mejorTiempoC];

        return $todofechasComparar;


    }

    function ventasMesPorAnio($usuarioAqui){


                //Ventas del mes
                    //fechas de pagos
                    $now = Carbon::now();
                    $anio=$now->year;
                    $mes=$now->month;
                    $primerDia=1;
                    $ultimoDia=$this->getUltimoDiaMes($anio,$mes);

                    $fecha_inicial=date("Y-m-d H:i:s", strtotime($anio."-".$mes."-".$primerDia) );
                    $fecha_final=date("Y-m-d H:i:s", strtotime($anio."-".$mes."-".$ultimoDia) );
                    //fechas de pagos

                    //buscar
                    
                        if(Auth::user()->tipo->name == 'root'){
                            $usuarios=DB::table('pedidos')
                            ->join('pedido_detalles', 'pedidos.id', '=', 'pedido_detalles.pedido_id')
                            ->whereBetween('pedidos.created_at', [$fecha_inicial,  $fecha_final])
                            ->where('pedidos.estatus','=', 'entregado' )
                            ->get();
                        }
                        if(Auth::user()->tipo->name == 'tienda'){
                            $usuarios=DB::table('pedidos')
                            ->join('pedido_detalles', 'pedidos.id', '=', 'pedido_detalles.pedido_id')
                            ->whereBetween('pedidos.created_at', [$fecha_inicial,  $fecha_final])
                            ->where('pedidos.tienda_id','=', $usuarioAqui )
                            ->where('pedidos.estatus','=', 'entregado' )
                            ->get();
                        }

                       

                            $ct=count($usuarios);
                    //buscar
                //Ventas del mes

                //sumar por dia
                    for($d=1;$d<=$ultimoDia;$d++){
                        $diasEfectivo[$d]=0;
                        $diasPos[$d]=0;
                        $descuento[$d]=0;
                        $nombreDias[$d]=""; 
                    }

                    for($d=1;$d<=$ultimoDia;$d++){

                        foreach($usuarios as $usuario){

                            if( date("Y-m-d", strtotime($usuario->created_at) ) == date("Y-m-d", strtotime($anio."-".$mes."-".$d) ) && $usuario->tipo_pago == "efectivo"){
                                if(Auth::user()->tipo->name == 'root'){
                                    $diasEfectivo[$d]+=($usuario->precio * $usuario->cantidad);
                                }
                                if(Auth::user()->tipo->name == 'tienda'){
                                    if(Auth::user()->motospropias == 0){
                                        $descuento[$d]= 0.13*($usuario->precio * $usuario->cantidad);
                                        $descuento2[$d]= 0.18*($usuario->precio * $usuario->cantidad);
                                        $descuento3[$d]= $descuento[$d]+$descuento2[$d];
                                        $diasEfectivo[$d]=$diasEfectivo[$d]+ (($usuario->precio * $usuario->cantidad)-$descuento3[$d]); 
                                    }else{                                   
                                        $descuento[$d]= 0.1*($usuario->precio * $usuario->cantidad);
                                        $descuento2[$d]= 0.18*($usuario->precio * $usuario->cantidad);
                                        $descuento3[$d]= $descuento[$d]+$descuento2[$d];
                                        $diasEfectivo[$d]=$diasEfectivo[$d]+ (($usuario->precio * $usuario->cantidad)-$descuento3[$d]); 
                                    }
                                }
                            }elseif( date("Y-m-d", strtotime($usuario->created_at) ) == date("Y-m-d", strtotime($anio."-".$mes."-".$d) ) && $usuario->tipo_pago == "POS"){
                                if(Auth::user()->tipo->name == 'root'){
                                    $diasPos[$d]=$diasPos[$d]+ ($usuario->precio * $usuario->cantidad);
                                }
                                if(Auth::user()->tipo->name == 'tienda'){
                                    if(Auth::user()->motospropias == 0){
                                        $descuento[$d]= 0.13*($usuario->precio * $usuario->cantidad);
                                        $descuento2[$d]= 0.18*($usuario->precio * $usuario->cantidad);
                                        $descuento3[$d]= $descuento[$d]+$descuento2[$d];
                                        $diasPos[$d]=$diasPos[$d]+ (($usuario->precio * $usuario->cantidad)-$descuento3[$d]);
                                    }else{
                                        $descuento[$d]= 0.1*($usuario->precio * $usuario->cantidad);
                                        $descuento2[$d]= 0.18*($usuario->precio * $usuario->cantidad);
                                        $descuento3[$d]= $descuento[$d]+$descuento2[$d];
                                        $diasPos[$d]=$diasPos[$d]+ (($usuario->precio * $usuario->cantidad)-$descuento3[$d]);
                                    }
                                } 
                            }

                        }

                        $dato= new DateTime(date("Y-m-d", strtotime($anio."-".$mes."-".$d) ));
                        if($dato->format('l') == "Monday"){

                            $nombreDias[$d]="Lunes"." ".$d;

                        }if($dato->format('l') == "Tuesday"){

                            $nombreDias[$d]="Martes"." ".$d;

                        }if($dato->format('l') == "Wednesday"){

                            $nombreDias[$d]="Miercoles"." ".$d;

                        }if($dato->format('l') == "Thursday"){

                            $nombreDias[$d]="Jueves"." ".$d;

                        }if($dato->format('l') == "Friday"){

                            $nombreDias[$d]="Viernes"." ".$d;

                        }if($dato->format('l') == "Saturday"){

                            $nombreDias[$d]="Sábado"." ".$d;

                        }if($dato->format('l') == "Sunday"){

                            $nombreDias[$d]="Domingo"." ".$d;

                        }


                    }


                //sumar por dia


                $ventasMesPorAnio=[$diasEfectivo, $diasPos, $nombreDias];

                return $ventasMesPorAnio;

        

    }

    function estadisticaDiaHora($usuarioAqui){


            // var
                $mondays = $this->getMondays();
                $mondays2 = $this->getTuesday();
                $mondays3 = $this->getWednesday();
                $mondays4 = $this->getThursday();
                $mondays5 = $this->getFriday();
                $mondays6 = $this->getSaturday();
                $mondays7 = $this->getSunday();
                $l=0;
                $lunesPedidos=[];
                $ma=0;
                $martesPedidos=[];
                $mi=0;
                $miercolesPedidos=[];
                $j=0;
                $juevesPedidos=[];
                $v=0;
                $viernesPedidos=[];
                $s=0;
                $sabadoPedidos=[];
                $d=0;
                $domingoPedidos=[];
                $numeroPaVe=0;
            // var

            //gurdar dias
                foreach ($mondays as $monday)
                {
                    $lunes[$l]=date("Y-m-d", strtotime($monday) ) ;
                    $l++;
                }
                foreach ($mondays2 as $monday)
                {
                    $martes[$ma]=date("Y-m-d", strtotime($monday) ) ;
                    $ma++;
                }
                foreach ($mondays3 as $monday)
                {
                    $miercoles[$mi]=date("Y-m-d", strtotime($monday) ) ;
                    $mi++;
                }
                foreach ($mondays4 as $monday)
                {
                    $jueves[$j]=date("Y-m-d", strtotime($monday) ) ;
                    $j++;
                }
                foreach ($mondays5 as $monday)
                {
                    $viernes[$v]=date("Y-m-d", strtotime($monday) ) ;
                    $v++;
                }
                foreach ($mondays6 as $monday)
                {
                    $sabado[$s]=date("Y-m-d", strtotime($monday) ) ;
                    $s++;
                }
                foreach ($mondays7 as $monday)
                {
                    $domingo[$d]=date("Y-m-d", strtotime($monday) ) ;
                    $d++;
                }
            //gurdar dias
                if(Auth::user()->tipo->name == 'root'){
                    $pedidos=DB::table('pedidos')
                    ->where('estatus','!=', 'fallido')
                    ->get();
                }
                if(Auth::user()->tipo->name == 'tienda'){
                    $pedidos=DB::table('pedidos')
                    ->where('pedidos.tienda_id','=', $usuarioAqui )
                    ->where('estatus','!=', 'fallido')
                    ->get();
                }
        //                 http_response_code(500);
        // dd(  $pedidos);
            //busqueda de pedidos ese dia
                for($d=0;$d<$l;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($lunes[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($lunesPedidos,$p);
                        }
                    }
                }
                for($d=0;$d<$ma;$d++){
                    foreach ($pedidos as $p)
                    {  
                        if($martes[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($martesPedidos,$p);
                        }
                    }
                }
                for($d=0;$d<$mi;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($miercoles[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($miercolesPedidos,$p);
                        }
                    }
                }
                for($d=0;$d<$j;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($jueves[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($juevesPedidos,$p);   
                        }
                    }
                }
                for($d=0;$d<$v;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($viernes[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($viernesPedidos,$p);  
                        }
                    }
                }
                for($d=0;$d<$s;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($sabado[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($sabadoPedidos,$p);  
                        }
                    }
                }
                for($d=0;$d<$d;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($domingo[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($domingoPedidos,$p);  
                        }
                    }
                }
            //busqueda de pedidos ese dia

            //agregando a dias
                for($d=0;$d<24;$d++){
                    $horaLunes[$d]=0;
                    $horaMartes[$d]=0;
                    $horaMiercoles[$d]=0;
                    $horaJueves[$d]=0;
                    $horaViernes[$d]=0;
                    $horaSabado[$d]=0;
                    $horaDomingo[$d]=0;
                }
                foreach ($lunesPedidos as $pl)
                {
                    $dl=date("H", strtotime($pl->created_at));
                    settype($dl , "integer");
                    $horaLunes[$dl]++;
                }
                foreach ($martesPedidos as $pma)
                {
                    $dma=date("H", strtotime($pma->created_at));
                    settype($dma , "integer");
                    $horaMartes[$dma]++;
                }
                foreach ($miercolesPedidos as $pmi)
                {
                    $dmi=date("H", strtotime($pmi->created_at));
                    settype($dmi , "integer");
                    $horaMiercoles[$dmi]++;
                }
                foreach ($juevesPedidos as $pj)
                {
                    $dj=date("H", strtotime($pj->created_at));
                    settype($dj , "integer");
                    $horaJueves[$dj]++;
                }
                foreach ($viernesPedidos as $pv)
                {
                    $dv=date("H", strtotime($pv->created_at));
                    settype($dv , "integer");
                    $horaViernes[$dv]++;
                }
                foreach ($sabadoPedidos as $ps)
                {
                    $ds=date("H", strtotime($ps->created_at));
                    settype($ds , "integer");
                    $horaSabado[$ds]++;
                }
                foreach ($domingoPedidos as $pd)
                {
                    $dd=date("H", strtotime($pd->created_at));
                    settype($dd , "integer");
                    $horaDomingo[$dd]++;
                }
            //agregando a dias
                    
    
                    // http_response_code(500);
                    // dd($horaDomingo);
                $estadisticaDiaHora=[$horaLunes, $horaMartes, $horaMiercoles, $horaJueves, $horaViernes, $horaSabado, $horaDomingo];

                return $estadisticaDiaHora;


    }


}
