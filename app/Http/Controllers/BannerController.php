<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\User;
use Image;
use Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::orderBy('updated_at', 'DESC')->paginate(15);

        $tiendas = User::whereHas('tipo', function($q){
                            $q->where('name', 'tienda');
                        })
                        ->get();

        return view('promocion.banner', compact('banners', 'tiendas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $respuesta = array();
        try {

            $banner = new Banner;

            if($request->file('image')){
                try {
                    $nombreImagenUnico = Str::uuid();

                    $imagen = Image::make($request->file('image'));
                    $nombreImagen = $nombreImagenUnico . "." . $request->image->extension();

                    if (!Storage::disk('public')->exists('banner')) {
                        Storage::disk('public')->makeDirectory('banner');
                    }

                    $imagen->save('storage/banner/' . $nombreImagen);

                    $banner->imagen_url = url('storage/banner') . '/' . $nombreImagen;

                } catch (\Exception $e) {
                    Log::error($e);
                }

                $banner->tienda_id = $request->comercio;
                $banner->estatus = $request->estatus;
                $banner->save();

                $respuesta = array(
                    "estatus" => "success",
                    "data" => $banner,
                );
            }else{
                $respuesta = array(
                    "estatus" => "error",
                    "data" => "Falta imagen",
                );
            }
            
            

        } catch (\Exception $e) {
            Log::error($e);
            $respuesta = array(
                "estatus" => "error",
                "data" => $e,
            );
        }

        return response()->json($respuesta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $respuesta = array();
        try {
            $banner = Banner::where('id', $id)->firstOrFail();
            $respuesta = array(
                "estatus" => "success",
                "data" => $banner,
            );
        } catch (\Exception $e) {
            $respuesta = array(
                "estatus" => "error",
                "data" => $e,
            );

        }
        return response()->json($respuesta);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $respuesta = array();
        try {

            $banner = Banner::where('id', $id)->firstOrFail();            

            if($request->file('image')){
                try {
                    $nombreImagenUnico = Str::uuid();

                    $imagen = Image::make($request->file('image'));
                    $nombreImagen = $nombreImagenUnico . "." . $request->image->extension();

                    if (!Storage::disk('public')->exists('banner')) {
                        Storage::disk('public')->makeDirectory('banner');
                    }

                    $imagen->save('storage/banner/' . $nombreImagen);

                    $banner->imagen_url = url('storage/banner') . '/' . $nombreImagen;

                } catch (\Exception $e) {
                    Log::error($e);
                }
            }

            $banner->tienda_id = $request->comercio;
            $banner->estatus = $request->estatus;
            $banner->save();

            $respuesta = array(
                "estatus" => "success",
                "data" => $banner,
            );

        } catch (\Exception $e) {
            Log::error($e);
            $respuesta = array(
                "estatus" => "error",
                "data" => $e,
            );
        }

        return response()->json($respuesta);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
