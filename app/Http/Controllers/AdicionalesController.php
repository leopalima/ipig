<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\modelos\AdicionalesController as Adicionales;
use App\Http\Controllers\modelos\AdicionalesItemsController as Items;
use Auth;
use Validator;

class AdicionalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listarAdicionales = new Adicionales;
        $adicionales = $listarAdicionales->all()->where('tienda_id', $this->usuario()->id)->paginate(10);
        return view('adicionales.index', compact('adicionales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listarItems = new Items;
        $items = $listarItems->all()->select('nombre', 'created_at')->where('tienda_id', $this->usuario()->id)->distinct()->get();
        return view('adicionales.create', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make(
            $request->all(), [
                'titulo' => 'required|min:3',
                'producto_id' => 'required',
                'item' => 'required|array',
                'item.name.*' => 'required|min:3',
                'item.precio.*' => 'required|numeric',
            ],
            [
                'titulo.required' => 'Indique el titulo',
                'producto_id.required' => 'No hay un producto asociado',
                'titulo.min' => 'Mínimo tres caracteres para el titulo.',
                'item.required' => 'No hay items.',
                'item.name.*.required' => 'Indique el nombre del adicional.',
                'item.precio.*.required' => 'Indique el precio del adicional, puede colocar cero.',
                'item.name.*.min' => 'Mínimo 3 caracteres para el nombre.',
                'item.precio.*.numeric' => 'El precio debe ser númerico.',
            ]
        );

        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()]);
        }

        $request->tienda = $this->usuario()->id;

        $almacenarAdicional = new Adicionales;
        $adicional = $almacenarAdicional->store($request);

        if($adicional){
            $request->adicionales_id = $adicional->id;
            $almacenarItems = new Items;
            $items = $almacenarItems->store($request);
            if($items){
                return response()->json(['grabado'=> 'Adicional registrado correctamente.']);
            }else{
                return response()->json(['error'=> 'Error al guardar el adicional.']);
            }
            return response()->json(['grabado'=> 'Adicional registrado correctamente.']);
        }else{
            return response()->json(['error'=> 'Error al guardar el adicional.']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buscarAdicional = new Adicionales;
        $adicional = $buscarAdicional->one($id)->where('tienda_id', $this->usuario()->id)->first();
        $listarItems = new Items;
        $items = $listarItems->all()->select('nombre', 'created_at')->where('tienda_id', $this->usuario()->id)->distinct()->get();
        return view('adicionales.edit', compact('adicional', 'items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
                'titulo' => 'required|min:3',
                'item' => 'required|array',
                'item.name.*' => 'required|min:3',
                'item.precio.*' => 'required|numeric',
            ],
            [
                'titulo.required' => 'Indique el titulo',
                'titulo.min' => 'Mínimo tres caracteres para el titulo.',
                'item.required' => 'No hay items.',
                'item.name.*.required' => 'Indique el nombre del adicional.',
                'item.precio.*.required' => 'Indique el precio del adicional, puede colocar cero.',
                'item.name.*.min' => 'Mínimo 3 caracteres para el nombre.',
                'item.precio.*.numeric' => 'El precio debe ser númerico.',
            ]
        );

        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()]);
        }

        $request->tienda = $this->usuario()->id;

        $editarAdicional = new Adicionales;
        $adicional = $editarAdicional->update($request, $id);

        if($adicional){
            // $request->adicionales_id = $adicional->id;
            $almacenarItems = new Items;
            $items = $almacenarItems->update($request, $id);
            if($items){
                return response()->json(['grabado'=> 'Adicional registrado correctamente.']);
            }else{
                return response()->json(['error'=> 'Error al guardar el adicional.']);
            }
            return response()->json(['grabado'=> 'Adicional registrado correctamente.']);
        }else{
            return response()->json(['error'=> 'Error al guardar el adicional.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function usuario(){
        return Auth::user();
    }
}
