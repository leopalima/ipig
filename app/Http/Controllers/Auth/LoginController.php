<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, User $user)
    {
        session(['numero_marcas' => 0]);
        if($user->mismarcas->count() > 1){
            session(['numero_marcas' => $user->mismarcas->count()]);
            foreach ($user->mismarcas as $marcas) {
                $lasmarcas[] = [
                    'id' => $marcas->id,
                    'name' => $marcas->name,
                ];
            }
            session(['las_marcas' => $lasmarcas]);
        }

        $user->dispositivo = "Web";
        $user->save();
        $tipodeUsuario = $user->tipo->name;
        if(
            $tipodeUsuario != "tienda" &&
            $tipodeUsuario != "root" &&
            $tipodeUsuario != "admin"
            ){
            Auth::logout();
            return redirect('/');
        }
    }
}
