<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\modelos\ProductosCategoriasController as Categorias;
use Auth;
use Validator;

class CategoriasProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $listarCategorias = new Categorias;
        $categorias = $listarCategorias->all()->where('tienda_id', $this->usuario()->id)->get();
        return view('producto_categorias.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('producto_categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
                'estatus' => 'required',
            ],
            [
                'name.required' => 'Indique el nombre',
                'name.min' => 'Mínimo tres caracteres para el nombre',
                'estatus.required' => 'Seleccione el estatus',
            ]
        );

        if($validator->fails()) {
            return redirect('categorias/create')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $request->tienda = $this->usuario()->id;

        $nuevaCategoria = new Categorias;
        $categoria = $nuevaCategoria->store($request);

        if($categoria){
            return redirect('categorias');
            return redirect('categorias/create')->with('actualizado', 'Actualizado');
        }else{
            return redirect('categorias/create')
                        ->with('error', 'Error en la actualizacion de los datos');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buscarCategoria = new Categorias;
        $categoria = $buscarCategoria->one($id)->where('tienda_id', $this->usuario()->id)->first();

        if($categoria){
            return view('producto_categorias.edit', compact('categoria'));
        }else{
            return redirect('dashboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
                // 'descripcion' => 'required|min:3',
            ],
            [
                'name.required' => 'Indique el nombre',
                'name.min' => 'Mínimo tres caracteres para el nombre',
                'descripcion.required' => 'Indique la descripcion',
                'descripcion.min' => 'Mínimo 8 caracteres para la descripción',
            ]
        );

        if($validator->fails()) {
            return redirect('categorias/'. $id .'/edit')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $request->tienda = $this->usuario()->id;

        $nuevaCategoria = new Categorias;
        $categoria = $nuevaCategoria->update($request, $id);

        if($categoria){
            return redirect('categorias/'. $id .'/edit')->with('actualizado', 'Actualizado');
        }else{
            return redirect('categorias/'. $id .'/edit')
                        ->with('error', 'Error en la actualizacion de los datos');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function usuario(){
        $usuario = Auth::user();
        return $usuario;
    }

    public function CategoriaGuardarOrden(Request $request){


        $categoriaPosicion = new Categorias;
        $categorias = $categoriaPosicion->updatePosicion($request);  

   
        if($categorias){
            return response()->json(['grabado'=> 'Grabado exitosamente.',]);
            
        }else{
            return response()->json(['error'=>'Error al guardar el registro.']);

        }
            
    
}
}
