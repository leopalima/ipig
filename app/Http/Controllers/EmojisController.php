<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emojis;

class EmojisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emojis = Emojis::orderBy('created_at', 'desc')->paginate(15);
        $clasificacion = Emojis::select('clasificacion')->distinct()->get();

        return view('emojis.index', compact('emojis', 'clasificacion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $respuesta = array();
        try {
            $emoji = new Emojis;
            if(filled($request->nvaClasif)){
                $emoji->clasificacion = $request->nvaClasif;
            }else{
                $emoji->clasificacion = $request->clasificacion;
            }
            $emoji->emoji = $request->emoji;
            $emoji->save();

            $respuesta = array(
                'estatus' => 'success',
                'data' => $emoji,

            );
        } catch (\Exception $e) {
            $respuesta = array(
                'estatus' => 'error',
                'data' => $e,

            );
        }
        return response()->json($respuesta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $respuesta = array();
        try {
            $emoji = Emojis::where('id', $id)->firstOrFail();

            $respuesta = array(
                'estatus' => 'success',
                'data' => $emoji,

            );
        } catch (\Exception $e) {
            $respuesta = array(
                'estatus' => 'error',
                'data' => $e,

            );
        }
        return response()->json($respuesta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $respuesta = array();
        try {
            $emoji = Emojis::where('id', $id)->firstOrFail();
            if(filled($request->nvaClasif)){
                $emoji->clasificacion = $request->nvaClasif;
            }else{
                $emoji->clasificacion = $request->clasificacion;
            }
            $emoji->emoji = $request->emoji;
            $emoji->save();

            $respuesta = array(
                'estatus' => 'success',
                'data' => $emoji,

            );
        } catch (\Exception $e) {
            $respuesta = array(
                'estatus' => 'error',
                'data' => $e,

            );
        }
        return response()->json($respuesta);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
