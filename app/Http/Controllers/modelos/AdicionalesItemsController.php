<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Adicionales_items as Items;

class AdicionalesItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $items = Items::orderBy('created_at', 'desc');
        return $items;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            for($i=0; $i < count($request->item['name']); $i++)
            {
                $items = new Items;
                $items->nombre = $request->item['name'][$i];   
                $items->precio = $request->item['precio'][$i];
                $items->tienda_id = $request->tienda;
                $items->adicionales_id = $request->adicionales_id;
                $items->save();
            }
        } catch (\Exception $e) {
            
            return 0;
        }

        return $items;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function one($id)
    {
        try{
            $item = Items::where('id', $id);
        }catch(\Exception $e){
            return 0;
        }
        return $item;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            for($i=0; $i < count($request->item['name']); $i++)
            {
                if(isset($request->item['id'][$i])){
                    $items = $this->one($request->item['id'][$i])->first();

                    if(isset($request->item['estatus']) && in_array($request->item['id'][$i], $request->item['estatus'])){
                        $items->estatus = 1;
                    }else{
                        $items->estatus = 0;
                    }

                }else{
                    $items = new Items;
                    $items->tienda_id = $request->tienda;
                    $items->adicionales_id = $id;
                }

                $items->nombre = $request->item['name'][$i];   
                $items->precio = $request->item['precio'][$i];

                

                if(isset($request->item['mostrar'][$i]))
                    $items->estatus = $request->item['mostrar'][$i];
                $items->save();
            }
        } catch (\Exception $e) {
            return 0;
        }

        return $items;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
