<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Users_tipo;

class UsersTipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $tipos = Users_tipo::orderBy('id', 'desc');
        return $tipos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $tipo = new Users_tipo;
            $tipo->name = $request->name;
            $tipo->descripcion = $request->descripcion;
            $tipo->save();
        }catch(\Exception $e){
            return 0;
        }

        return $tipo;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function one($id)
    {
        try{
            $tipo = Users_tipo::where('id', '=', $id);
        }catch(\Exception $e){
            return 0;
        }
        return $tipo;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $tipo = $this->one($id)->first();
            $tipo->name = $request->name;
            $tipo->descripcion = $request->descripcion;
            $tipo->save();
        }catch(\Exception $e){
            return 0;
        }

        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
