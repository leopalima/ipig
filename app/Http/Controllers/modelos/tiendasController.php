<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tienda_configuracion;
use Illuminate\Support\Arr;

class tiendasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if($request->has("tiempoEntrega")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "tiempoEntrega";
            $configuracion->valor = $request->tiempoEntrega;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }
        if($request->has("costoEnvio")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "costoEnvio";
            $configuracion->valor = $request->costoEnvio;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }
        if($request->has("tiempoLunes")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "lunes";
            $configuracion->valor = $request->tiempoLunes;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }
        if($request->has("tiempoMartes")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "martes";
            $configuracion->valor = $request->tiempoMartes;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }
        if($request->has("tiempoMiercoles")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "miercoles";
            $configuracion->valor = $request->tiempoMiercoles;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }
        if($request->has("tiempoJueves")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "jueves";
            $configuracion->valor = $request->tiempoJueves;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }
        if($request->has("tiempoViernes")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "viernes";
            $configuracion->valor = $request->tiempoViernes;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }
        if($request->has("tiempoSabado")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "sabado";
            $configuracion->valor = $request->tiempoSabado;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }
        if($request->has("tiempoDomingo")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "domingo";
            $configuracion->valor = $request->tiempoDomingo;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }
        if($request->has("cerradoEmergenciaActivo")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "cerrado";
            $configuracion->valor = $request->cerradoEmergenciaActivo;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }
        if($request->has("whatsapp")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "whatsapp";
            $configuracion->valor = $request->whatsapp;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }
        if($request->has("descuento")){
            $configuracion = new Tienda_configuracion;
            $configuracion->clave = "descuento";
            $configuracion->valor = $request->descuento;
            $configuracion->tienda_id = $id;
            $configuracion->save();
        }

            // $configuracion = new Tienda_configuracion;
            // $configuracion->clave = "etiquetas";
            // $configuracion->valor = serialize(['Burguer', 'Pastas', 'Carne']);
            // $configuracion->tienda_id = $id;
            // $configuracion->save();
        // if($request->has("motospropias")){
        //     $configuracion = new Tienda_configuracion;
        //     $configuracion->clave = "motospropias";
        //     $configuracion->valor = $request->motospropias;
        //     $configuracion->tienda_id = $id;
        //     $configuracion->save();
        // }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function one($id)
    {
        $configuracion=Tienda_configuracion::where("tienda_id", "=" , $id);
        return $configuracion;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->configName){
            $checkbox = [
                'recojoentienda',
                'motospropias',
                'efectivo',
                'visa',
                'master',
            ];

            foreach($checkbox as $c)
            {
                $clave = Tienda_configuracion::where('tienda_id', $id)
                                        ->where('clave', $c)
                                        ->first();

                if($clave){
                    $clave->valor = ($request->$c == null) ? 0 : $request->$c;
                    $clave->save();
                }else{
                    $clave = new Tienda_configuracion;
                    $clave->tienda_id = $id;
                    $clave->clave = $c;
                    $clave->valor = 0;
                    $clave->save();
                }
            }

            $textbox = [
                'tiempoEntrega' => 25,
                'costoEnvio' => 5,
                'whatsapp' => 999999999,
                'descuento' => 0,
                'terminos' => "https://tienda.activatunegocio.com/terminos",
                'prctjcomision' => 8,
            ];

            foreach($textbox as $t => $c){
                $clave = Tienda_configuracion::where('tienda_id', $id)
                                        ->where('clave', $t)
                                        ->first();

                if($clave){
                    $clave->valor = ($request->$t == null) ? $clave->valor : $request->$t;
                    $clave->save();
                }else{
                    $clave = new Tienda_configuracion;
                    $clave->tienda_id = $id;
                    $clave->clave = $t;
                    $clave->valor = ($request->$t == null) ? $c : $request->$t;
                    $clave->save();
                }
            }
        }elseif($request->configTiempo){

            $dias = [
                'tiempoLunes',
                'tiempoMartes',
                'tiempoMiercoles',
                'tiempoJueves',
                'tiempoViernes',
                'tiempoSabado',
                'tiempoDomingo',
            ];

            foreach($dias as $d){
                $clave = Tienda_configuracion::where('tienda_id', $id)
                                        ->where('clave', $d)
                                        ->first();

                if($clave){
                    $clave->valor = ($request->$d == null) ? 0 : $request->$d;
                    $clave->save();
                }else{
                    $clave = new Tienda_configuracion;
                    $clave->tienda_id = $id;
                    $clave->clave = $d;
                    $clave->valor = ($request->$d == null) ? 0 : $request->$d;
                    $clave->save();
                }
            }
        }

        if($request->has("cerradoEmergenciaActivo")){
            $clave = Tienda_configuracion::where('tienda_id', $id)
                                        ->where('clave', 'cerrado')
                                        ->first();

                if($clave){
                    $clave->valor = ($request->cerradoEmergenciaActivo == null) ? 0 : $request->cerradoEmergenciaActivo;
                    $clave->save();
                }else{
                    $clave = new Tienda_configuracion;
                    $clave->tienda_id = $id;
                    $clave->clave = 'cerrado';
                    $clave->valor = 0;
                    $clave->save();
                }
        }

        return 1;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateEtiquetas($request, $id){
        $configuracionTienda = Tienda_configuracion::where('tienda_id', $id)
                                    ->where('clave', 'etiquetas')
                                    ->first();

        if($configuracionTienda){
            $configuracionTienda->valor = serialize($request->etiquetas);
            $configuracionTienda->save();
        }else{
                $configuracion = new Tienda_configuracion;
                $configuracion->clave = "etiquetas";
                $configuracion->valor = serialize($request->etiquetas);
                $configuracion->tienda_id = $id;
                $configuracion->save();
            }
        }
}
