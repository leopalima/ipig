<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Adicionales;

class AdicionalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $adicionales = Adicionales::orderBy('created_at', 'desc');
        return $adicionales;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        try {
            $adicional = new Adicionales;
            $adicional->titulo = $request->titulo;
            $adicional->tienda_id = $request->tienda;
            $adicional->producto_id = $request->producto_id;
            $adicional->maximo = $request->maximo;
            $adicional->orden = $request->orden;
            $adicional->save();
        } catch (\Exception $e) {
            return 0;
        }

        return $adicional;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function one($id)
    {
        $adicional = Adicionales::where('id', $id);
        return $adicional;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $adicional = $this->one($id)->first();
            $adicional->titulo = $request->titulo;
            $adicional->tienda_id = $request->tienda;
            if(isset($request->estatus)){
                $adicional->estatus=1;
            }else{
                $adicional->estatus=0;
            }
            $adicional->maximo = $request->maximo;
            $adicional->orden = $request->orden;
            $adicional->save();
        } catch (\Exception $e) {
            return 0;
        }

        return $adicional;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
