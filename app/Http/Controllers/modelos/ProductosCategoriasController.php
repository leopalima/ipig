<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Producto_categorias as Categorias;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Image;

class ProductosCategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $categorias = Categorias::orderBy('orden', 'asc');
        return $categorias;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $categoria = new Categorias;
            $categoria->name = $request->name;
            $categoria->descripcion = $request->descripcion;

            if($request->foto){
                if (!Storage::disk('public')->exists('categorias')) {
                    Storage::disk('public')->makeDirectory('categorias');
                }
                $photo = $request->file('foto');
                $filename = time() . Str::random(10) . '.' . strtolower($photo->getClientOriginalExtension());
                Image::make($photo)->resize(500, 300)->save('storage/categorias/' . $filename);
                $categoria->foto = $filename;
            }

            $orden = $this->all()->where('tienda_id', $request->tienda)->count();
                $categoria->orden = $orden++;

            if($request->estatus){
                
                $categoria->estatus = 1;
            }else{
                
                $categoria->estatus = 0;
               
            }

            $categoria->orden = $request->orden;

            $categoria->tienda_id = $request->tienda;
            $categoria->save();
        }catch(\Exception $e){
            return 0;
        }

        return $categoria;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function one($id)
    {
        try{
            $categoria = Categorias::where('id', '=', $id);
        }catch(\Exception $e){
            return 0;
        }
        return $categoria;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $categoria = $this->one($id)->where('tienda_id', $request->tienda)->first();
            $categoria->name = $request->name;
            $categoria->descripcion = $request->descripcion;

            if($request->foto){
                if (!Storage::disk('public')->exists('categorias')) {
                    Storage::disk('public')->makeDirectory('categorias');
                }
                $photo = $request->file('foto');
                $filename = time() . Str::random(10) . '.' . strtolower($photo->getClientOriginalExtension());
                Image::make($photo)->resize(500, 300)->save('storage/categorias/' . $filename);
                $categoria->foto = $filename;
            }

            $orden = $this->all()->where('tienda_id', $request->tienda)->count();
                $categoria->orden = $orden++;

            $categoria->orden = $request->orden;
            $categoria->estatus = $request->estatus;

            $categoria->tienda_id = $request->tienda;
            $categoria->save();
        }catch(\Exception $e){
            return 0;
        }

        return $categoria;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function updatePosicion(Request $request)
    {
 
        try{

            foreach($request->data as $r){
                $categorias = $this->one($r['id'])->first();
                $categorias->orden = $r['nombre'];

                 $categorias->save();
            }


        }catch(\Exception $e){
            return 0;
        }

        return $categorias;

    }
}
