<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Productos;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $productos = Productos::orderBy('id', 'desc');
        return $productos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $producto = new Productos;
            $producto->name = $request->name;
            $producto->descripcion = $request->descripcion;
            if($request->descripcion == "")
                $producto->descripcion = "";
            $producto->precio = $request->precio;
            $producto->categoria_id = $request->categoria;
            $producto->tienda_id = $request->tienda;
            $producto->orden = $request->orden;

            if($request->file('foto')){

                $imagen = Image::make($request->file('foto'))->resize(200, 200);

                $nombreImagen = Str::uuid() . "." . $request->foto->extension();

                if (!Storage::disk('public')->exists('productos')) {
                  Storage::disk('public')->makeDirectory('productos');
                }

                $imagen->save('storage/productos/' . $nombreImagen);

                $producto->foto = url('storage/productos') . '/' . $nombreImagen;
            }else{
                $producto->foto = '';
            }

            $producto->estatus = 1;
            $producto->save();
        }catch(\Exception $e){
            return 0;
        }

        return $producto;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function one($id)
    {
        try{
            $producto = Productos::where('id', $id);
        }catch(\Exception $e){
            return 0;
        }
        return $producto;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $producto = $this->one($id)->first();
            $producto->name = $request->name;
            $producto->descripcion = $request->descripcion;
            if($request->descripcion == "")
                $producto->descripcion = "";
            $producto->precio = $request->precio;
            $producto->categoria_id = $request->categoria;
            $producto->orden = $request->orden;

            if($request->file('foto')){

                $imagen = Image::make($request->file('foto'))->resize(200, 200);

                $nombreImagen = Str::uuid() . "." . $request->foto->extension();

                if (!Storage::disk('public')->exists('productos')) {
                  Storage::disk('public')->makeDirectory('productos');
                }

                $imagen->save('storage/productos/' . $nombreImagen);

                $producto->foto = url('storage/productos') . '/' . $nombreImagen;
            }

            if(isset($request->estatus)){
                $producto->estatus = 1;
            }else{
                $producto->estatus = 0;
            }
            
            $producto->save();
        }catch(\Exception $e){
            return 0;
        }

        return $producto;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function updatePosicion(Request $request)
    {
        
        try{

            foreach($request->data as $r){
                $producto = $this->one($r['id'])->first();
                $producto->orden = $r['nombre'];

                 $producto->save();
            }


        }catch(\Exception $e){
            return 0;
        }

        return $producto;

    }
}
