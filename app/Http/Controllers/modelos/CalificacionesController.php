<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Calificaciones;

class CalificacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $editarCalificacion = $this->update($request, $request->tienda_id);

        if($editarCalificacion){
            return 1;
        }else{

            try {

                $calificaciones = new Calificaciones;
                $calificaciones->tienda_id = $request->tienda_id;
                $calificaciones->cliente_id = $request->cliente->id;
                $calificaciones->calificacion = $request->calificacion;
                $calificaciones->save();

                return 1;

            } catch (\Exception $e) {
                return $e;
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $buscarCalificacion = Calificaciones::where('tienda_id', $request->tienda_id)
                                            ->where('cliente_id', $request->cliente->id)
                                            ->update(['calificacion' => $request->calificacion]);
            return 1;
        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
