<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Pago_efectivo;
use Auth;

class PagoEfectivoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $formato = new JSON;
        try {
            $pago = new Pago_efectivo;
            $pago->pedido_id = $id;
            $pago->cliente_id = $this->cliente()->id;
            $pago->tienda_id = $request->tienda_id;
            $pago->efectivo = $request->tipo_pago['monto'];
            $pago->save();
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error al registrar el pago con efectivo'], $e);
            return 0;
        }
        return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cliente(){
        return Auth::user();
    }
}
