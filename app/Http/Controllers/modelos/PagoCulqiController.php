<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Pago_culqi;
use Culqi;
use Auth;

class PagoCulqiController extends Controller
{
    // protected $secret_key = "sk_test_UhAGiWJ3n8iTLhkj";
    protected $secret_key = "sk_live_QrGWbBnVQgFQyI3s";
    protected $charge;
    protected $cliente;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $pedido_id)
    {
        $registrarPago = $this->registrarPagoTDC($request);
        if(array_key_exists("error", $registrarPago)){
            return $registrarPago;
        }

        try {

            $pago = new Pago_culqi;
            $pago->pedido_id = $pedido_id;
            $pago->cliente_id = $this->cliente()->id;
            $pago->tienda_id = $request->tienda_id;
            $pago->culqi_id = $request->tipo_pago["token_culqi"];
            $pago->tarjeta_marca = $this->charge->source->source->iin->card_brand;
            $pago->tarjeta_numero = $this->charge->source->source->card_number;
            $pago->monto = $request->monto;
            $pago->save();
            
        } catch (\Exception $e) {
            $respuesta= [
                "error" => $e,
                "e" => $e,
                "mensaje" => "Error al registrar el pago."
            ];
            return $respuesta;
        }

        return $this->charge;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function registrarPagoTDC($request){

        $formato = new JSON;

        try {
            $SECRET_KEY = $this->secret_key;
            $culqi = new Culqi\Culqi(array('api_key' => $SECRET_KEY));

            // Creando Cargo a una tarjeta
            $this->charge = $culqi->Charges->create(
                array(
                    "amount" => round(round($request["monto"],2) * 100),
                    "capture" => true,
                    "currency_code" => "PEN",
                    "description" => $request['tienda_nombre'],
                    "installments" => 0,
                    "email" => $this->cliente()->email,
                    "metadata" => array("test"=>"test"),
                    "source_id" => $request->tipo_pago['token_culqi'],
                )
            );

          } catch (\Exception $e) {
                $respuesta = [
                    "error" => "Error al crear el cargo",
                    "e" => $e,
                    "mensaje" => $this->capturaError($e)

                ];

                return $respuesta;
          }

          return [];
    }

    public function cliente(){
        return Auth::user();
    }

    public function capturaError($e)
    {
        $error = json_decode($e->getMessage(), true);
        $tipo = $error['type'];
        $mensaje = "";

        switch ($tipo) {
            case 'invalid_request_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'authentication_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'parameter_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'card_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'limit_api_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'resource_error  ':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;
            case 'api_error':
                if(array_key_exists('user_message', $error)){
                    $mensaje = $error['user_message'];
                }else{
                    $mensaje = $error['merchant_message'];
                }
                break;

            default:
                $mensaje = "ERROR.";
                break;
        }
        return $mensaje;
    }




}
