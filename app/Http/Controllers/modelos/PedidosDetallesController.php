<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Pedido_detalles;
use Auth;

class PedidosDetallesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $formato = new JSON;
        try {
            foreach($request->detalles as $d){

                $detalle = new Pedido_detalles;
                $detalle->pedido_id = $id;
                $detalle->producto_id = $d['producto_id'];
                $detalle->tipo_producto = $d['producto_id'];
                $detalle->precio = $d['precio'];
                $detalle->cantidad = $d['cantidad'];
                $detalle->estatus = 'recibido';
                $detalle->tienda_id = $request->tienda_id;
                $detalle->cliente_id = $this->cliente()->id;
                $detalle->save();

                if(count($d['adicionales'])){
                    foreach($d['adicionales'] as $items){
                        $detalle = new Pedido_detalles;
                        $detalle->pedido_id = $id;
                        $detalle->producto_id = $items['adicional_id'];
                        $detalle->tipo_producto = $d['producto_id'];
                        $detalle->precio = $items['precio'];
                        $detalle->cantidad = $d['cantidad'];
                        $detalle->estatus = 'recibido';
                        $detalle->tienda_id = $request->tienda_id;
                        $detalle->cliente_id = $this->cliente()->id;
                        $detalle->save();
                    }
                }

            }
        } catch (\Exception $e) {
            $respuesta = $formato->error(['error' => 'Error al grabar los detalles'], $e);
            return 0;
        }

        return  1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cliente(){
        return Auth::user();
    }
}
