<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Pedidos;
use App\User;
use Carbon\Carbon;
use Auth;

class PedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $pedidos = Pedidos::orderBy('created_at', 'desc');
        return $pedidos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formato = new JSON;
        try{
            $pedido = new Pedidos;
            $pedido->fecha = now(); //Carbon::createFromFormat('d/m/Y', now())->format('Y/m/d');
            $pedido->tienda_id = $request->tienda_id;
            $pedido->codigo = strtoupper(Str::random(7));
            $pedido->cliente_id = $this->cliente()->id;
            $pedido->direccion = $request->direccion;
            $pedido->latitud = $request->latitud;
            $pedido->longitud = $request->longitud;

            $costoEnvio = $this->tienda($request->tienda_id)->costoEnvio;
            $prctjdescuento = 0;
            if(strtoupper($request->tipo_pago['tipo']) == "EFECTIVO" || strtoupper($request->tipo_pago['tipo']) == "TENGO LA CANTIDAD EXACTA")
                $prctjdescuento = $this->tienda($request->tienda_id)->descuento;
            if(strtolower($request->delivery) === "recojo")
            {
                $costoEnvio = 0;
            }

            $pedido->costoenvio = $costoEnvio;
            $pedido->tiempoentrega = $this->tienda($request->tienda_id)->tiempoEntrega;
            $pedido->tipo_pago = $request->tipo_pago['tipo'];
            $pedido->delivery = $request->delivery;
            // $pedido->distancia = $request->distancia;
            // $pedido->distmedida = $request->distmedida;
            // // $pedido->estatus = $request->estatus;
            $pedido->estatus = 'recibido';
            $pedido->motospropias = $request["motospropias"];
            $pedido->prctjcomision = $request["prctjcomision"];
            $pedido->prctjdescuento = $prctjdescuento;
            $pedido->comentario = $request->comentario;
            $pedido->save();
        }catch(\Exception $e){
            dd($e);
            $respuesta = $formato->error(['error' => 'Error al grabar el pedido', $e], $e);
            return 0;
        }

        return $pedido;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function one($id)
    {
        try{
            $pedido = Pedidos::where('id', '=', $id);
        }catch(\Exception $e){
            return 0;
        }
        return $pedido;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cliente(){
        return Auth::user();
    }
    
    public function tienda($id){
        return User::where('id', $id)->first();
    }

}
