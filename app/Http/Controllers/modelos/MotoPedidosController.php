<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Moto_pedidos;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Pedidos;

class MotoPedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $moto_pedidos = Moto_pedidos::orderBy('created_at', 'asc');
        return $moto_pedidos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        try {
            $moto_pedido = new Moto_pedidos;
            $moto_pedido->pedido_id = $request['pedido_id'];
            $moto_pedido->estatus = $request['estatus'];
            $moto_pedido->tiempopreparacion = $request['tiempopreparacion'];
            $moto_pedido->fecha = now();
            $moto_pedido->save();
            return 1;
        } catch (\Exception $e) {
            $formato = new JSON;
            $formato->error(["error" => "error al grabar el motopedido"], $e);
            return 0;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function one($id)
    {
        try{
            $moto_pedido = Moto_pedidos::where('id', '=', $id);
        }catch(\Exception $e){
            return 0;
        }
        return $moto_pedido;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $moto_pedido = Moto_pedidos::where('id', $id);
            $moto_pedido->pedido_id = $request->pedido_id;
            $moto_pedido->estatus = $request->estatus;
            $moto_pedido->tiempopreparacion = $request->tiempopreparacion;
            $moto_pedido->fecha = now();
            $moto_pedido->motorizado_id = $request->motorizado_id;
            $moto_pedido->save();
            return 1;
        } catch (\Exception $e) {
            return 0;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function asignarMotorizado($id, $motorizado)
    {
        try {
            $moto_pedido = Moto_pedidos::where('id', $id)->first();
            $moto_pedido->estatus = 'listo';
            $moto_pedido->motorizado_id = $motorizado;
            $moto_pedido->save();
            return 1;
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function penalizar($m)
    {
        Moto_pedidos::where('id', $m->id)->update(['estatus' => 'penalizado']);

        Moto_pedidos::create([
            'pedido_id' => $m->pedido_id,
            'estatus' => "aceptado",
            'tiempopreparacion' => $m->tiempopreparacion,
            'fecha' => $m->fecha,
        ]);

        Pedidos::where('id', $m->pedido_id)->update(['estatus' => 'aceptado']);
    }

    public function listo($l, $moto)
    {
        try {
            Moto_pedidos::where('pedido_id', $l)
                        ->where('estatus', 'aceptado')
                        ->whereNull('motorizado_id')
                        ->update([
                            'motorizado_id' => $moto,
                            'estatus' => 'listo'
                            ]);
        } catch (\Exception $e) {
            return 1;
        }
        return 1;
    }

    public function buscando($l, $moto)
    {
        try {
            Moto_pedidos::where('pedido_id', $l)
                        ->where('estatus', 'listo')
                        ->where('motorizado_id', $moto)
                        ->update([
                            'estatus' => 'buscando'
                            ]);
        } catch (\Exception $e) {
            return 1;
        }
        return 1;
    }

    public function encamino($l, $moto)
    {
        try {
            Moto_pedidos::where('pedido_id', $l)
                        ->where('estatus', 'buscando')
                        ->where('motorizado_id', $moto)
                        ->update([
                            'estatus' => 'encamino'
                            ]);
        } catch (\Exception $e) {
            return 1;
        }
        return 1;
    }

    public function entregado($l, $moto)
    {
        try {
            Moto_pedidos::where('pedido_id', $l)
                        ->where('estatus', 'encamino')
                        ->where('motorizado_id', $moto)
                        ->update([
                            'estatus' => 'entregado'
                            ]);
        } catch (\Exception $e) {
            return 1;
        }
        return 1;
    }

    public function cancelar($l, $moto)
    {
        try {
            Moto_pedidos::where('pedido_id', $l)
                        ->where('motorizado_id', $moto)
                        ->update([
                            'estatus' => 'cancelado'
                            ]);
        } catch (\Exception $e) {
            return 1;
        }
        return 1;
    }

    public function buscar($pedido_id, $estatus)
    {
        try {
            $motoPedido = Moto_pedidos::where(function($q) use($pedido_id, $estatus){
                                            $q->where('pedido_id', $pedido_id);
                                            $q->where('estatus', $estatus);
                                        })
                                        ->first();
        } catch (\Exception $e) {
            return 0;
        }

        return $motoPedido;
    }

}
