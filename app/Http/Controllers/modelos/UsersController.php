<?php

namespace App\Http\Controllers\modelos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Coordenadas;
use App\Users_validate;
use App\Http\Controllers\modelos\tiendasController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Auth;
use Log;
use Image;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        // $usuarios = User::orderBy('id', 'desc');
        $usuarios = User::inRandomOrder();
        return $usuarios;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $usuario = new User;
            $usuario->name = $request->name;
            if($request->descripcion)
                $usuario->descripcion = $request->descripcion;
            $usuario->email = $request->email;
            $usuario->phone = str_replace(" ", "", $request->phone);
            $usuario->password = bcrypt($request->password);
            $usuario->tipo_id = $request->tipo;
            $usuario->tienda_id = "tienda";

            if($request->file('foto')){
                try {
                    $nombreImagenUnico = Str::uuid();

                    $imagen = Image::make($request->file('foto'));
                    $nombreImagen = $nombreImagenUnico . "." . $request->foto->extension();

                    $imagenGris = Image::make($request->file('foto'))->greyscale();
                    $nombreImagenGris = $nombreImagenUnico . "-gris." . $request->foto->extension();

                    $imagen->save('storage/productos/' . $nombreImagen);
                    $imagenGris->save('storage/productos/' . $nombreImagenGris);

                    $usuario->foto = url('storage/productos') . '/' . $nombreImagen;
                } catch (\Exception $e) {
                    Log::error($e);
                }
            }

            $usuario->save();

            $tiendasController = new tiendasController();
            $tiendasController->updateEtiquetas($request, $usuario->id);

            if($usuario->tipo->name=="tienda"){
                $this->grabarCoordenadas($usuario->id);
            }



        }catch(\Exception $e){
            return 0;
        }

        return $usuario;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function one($id)
    {
        try{
            $usuario = User::where('id', '=', $id);
        }catch(\Exception $e){
            return 0;
        }
        return $usuario;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        try{
            $usuario = $this->one($id)->first();
            $usuario->name = $request->name;
            $usuario->email = $request->email;
            $usuario->phone = str_replace(" ", "", $request->phone);
            if($request->has("estatus")){
                $usuario->estatus = $request->estatus;
            }
            if($request->has("validate")){
                $actualizaValidate = Users_validate::where('user_id', $usuario->id)->update(['validate' => $request->validate]);
                if(!$actualizaValidate){
                    Users_validate::create([
                        'codigo' => "0000",
                        'user_id' => $usuario->id,
                        'validate' => 1,
                    ]);
                }
            }
            if($request->descripcion)
                $usuario->descripcion = $request->descripcion;
            if($request->password)
                $usuario->password = bcrypt($request->password);
            if($request->tipo)
                $usuario->tipo_id = $request->tipo;


            if($request->file('foto')){
                try {
                    $nombreImagenUnico = Str::uuid();

                    $imagen = Image::make($request->file('foto'));
                    $nombreImagen = $nombreImagenUnico . "." . $request->foto->extension();

                    $imagenGris = Image::make($request->file('foto'))->greyscale();
                    $nombreImagenGris = $nombreImagenUnico . "-gris." . $request->foto->extension();

                    $imagen->save('storage/productos/' . $nombreImagen);
                    $imagenGris->save('storage/productos/' . $nombreImagenGris);

                    $usuario->foto = url('storage/productos') . '/' . $nombreImagen;
                } catch (\Exception $e) {
                    Log::error($e);
                }
            }
            
            $usuario->save();

            if(Auth::user()->tipo->name=="tienda"){
                
                $tiendasController = new tiendasController();
                $tiendasController->update($request, $id);
            }else{
                
                $tiendasController = new tiendasController();
                $tiendasController->updateEtiquetas($request, $id);
                $tiendasController->update($request, $id);
            }

            if($usuario->tipo->name=="tienda"){
                $this->grabarCoordenadas($usuario->id);
            }
            
        }catch(\Exception $e){
            return 0;
        }

        return 1;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function grabarCoordenadas($tienda_id)
    {
        $tienda_coordenadas = array(
            array('tienda_id' => $tienda_id, 'latitud' => -12.150387257276085, 'longitud' => -77.02478604769999),
            array('tienda_id' => $tienda_id, 'latitud' => -12.14963208462212, 'longitud' => -77.01985078311259),
            array('tienda_id' => $tienda_id, 'latitud' => -12.147744143612533, 'longitud' => -77.02006535983378),
            array('tienda_id' => $tienda_id, 'latitud' => -12.14535273244406, 'longitud' => -77.01903539157206),
            array('tienda_id' => $tienda_id, 'latitud' => -12.141702642405251, 'longitud' => -77.01779084658915),
            array('tienda_id' => $tienda_id, 'latitud' => -12.136416216416452, 'longitud' => -77.0179625079661),
            array('tienda_id' => $tienda_id, 'latitud' => -12.135629988201917, 'longitud' => -77.01789888505289),
            array('tienda_id' => $tienda_id, 'latitud' => -12.14502803404582, 'longitud' => -76.99755701188394),
            array('tienda_id' => $tienda_id, 'latitud' => -12.139070382745858, 'longitud' => -76.99373754624673),
            array('tienda_id' => $tienda_id, 'latitud' => -12.141042296171628, 'longitud' => -76.99210676316568),
            array('tienda_id' => $tienda_id, 'latitud' => -12.122581258917453, 'longitud' => -76.9856265461857),
            array('tienda_id' => $tienda_id, 'latitud' => -12.119098692678417, 'longitud' => -76.98408159379312),
            array('tienda_id' => $tienda_id, 'latitud' => -12.11569999987352, 'longitud' => -76.98390993241617),
            array('tienda_id' => $tienda_id, 'latitud' => -12.110622859640804, 'longitud' => -76.98433908585855),
            array('tienda_id' => $tienda_id, 'latitud' => -12.110161296650194, 'longitud' => -76.97815927628824),
            array('tienda_id' => $tienda_id, 'latitud' => -12.088424972931525, 'longitud' => -76.9812491810734),
            array('tienda_id' => $tienda_id, 'latitud' => -12.086200886489824, 'longitud' => -76.98399576310464),
            array('tienda_id' => $tienda_id, 'latitud' => -12.085781245465359, 'longitud' => -76.98863062028238),
            array('tienda_id' => $tienda_id, 'latitud' => -12.086872310760047, 'longitud' => -76.99378046159097),
            array('tienda_id' => $tienda_id, 'latitud' => -12.086620526856152, 'longitud' => -76.99682745103189),
            array('tienda_id' => $tienda_id, 'latitud' => -12.087333913968456, 'longitud' => -76.99845823411295),
            array('tienda_id' => $tienda_id, 'latitud' => -12.090732967613693, 'longitud' => -77.02279123429605),
            array('tienda_id' => $tienda_id, 'latitud' => -12.073581908079568, 'longitud' => -77.03036351600929),
            array('tienda_id' => $tienda_id, 'latitud' => -12.074798920694224, 'longitud' => -77.03813119331642),
            array('tienda_id' => $tienda_id, 'latitud' => -12.075218578934344, 'longitud' => -77.04220815101905),
            array('tienda_id' => $tienda_id, 'latitud' => -12.075050715717198, 'longitud' => -77.04761548439308),
            array('tienda_id' => $tienda_id, 'latitud' => -12.07207112610685, 'longitud' => -77.05122037330909),
            array('tienda_id' => $tienda_id, 'latitud' => -12.101823568041828, 'longitud' => -77.07559628883644),
        );
        try {
            Coordenadas::where('tienda_id', $tienda_id)->delete();

            foreach($tienda_coordenadas as $t){
                Coordenadas::insert($t);
            }

        } catch (\Exception $e) {
            Log::error($e);
        }
    }
}
