<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Etiquetas;
use Validator;

class EtiquetasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $etiquetas=Etiquetas::orderBy("id", "asc")->paginate(10);
        return view('error.etiquetas',compact('etiquetas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {




        $rules = array(
            'etiquetaNombre'    =>  'required',
            
        );

        $error = Validator::make($request->all(), $rules);


        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }



        $form_data = array(
            'etiqueta'        =>  $request->etiquetaNombre,

        );



        Etiquetas::create($form_data);

        return response()->json(['success' => 'Los datos fueron guardados.']);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = Etiquetas::findOrFail($id);
        
        return response()->json(['data' => $data]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = array(
            'etiquetaNombre'    =>  'required',
            
        );

        $error = Validator::make($request->all(), $rules);


        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'etiqueta'        =>  $request->etiquetaNombre,

        );

        Etiquetas::whereId($id)->update($form_data);

        return response()->json(['success' => 'Los cambios fueron guardados.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
