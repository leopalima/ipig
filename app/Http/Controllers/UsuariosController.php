<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\modelos\UsersController as Usuarios;
use App\Http\Controllers\modelos\UsersTipoController as UsuariosTipo;
use App\Http\Controllers\modelos\EtiquetasController as Etiquetas;
use Validator;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use DB;
use DateTime;
use App\User;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $r)
    {
        $listarUsuarios = User::orderBy('created_at', 'desc');
        if($r->has('txtBuscar')){
            $usuarios = $listarUsuarios
                                ->whereHas('tipo', function($q) use ($r){
                                    $q->where('name', 'like', '%'. $r->txtBuscar .'%');
                                })
                                ->orWhere('email', 'like', '%'. $r->txtBuscar .'%')
                                ->orWhere('name', 'like', '%'. $r->txtBuscar .'%')
                                ->orWhere('phone', 'like', '%'. $r->txtBuscar .'%')
                                ->orWhere('direccion', 'like', '%'. $r->txtBuscar .'%')
                                ->orWhere('descripcion', 'like', '%'. $r->txtBuscar .'%')
                                ->paginate(15);
        }else{
            $usuarios = $listarUsuarios->paginate(15);
        }

        $usuarios->appends([
            'txtBuscar' => $r->txtBuscar,
        ]);

        return view('usuarios.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $traerTipos = new UsuariosTipo;
        $tipos = $traerTipos->all()->get();
        $tipos = $tipos->splice(2);
        $traerEtiquetas = new Etiquetas;
        $etiquetas = $traerEtiquetas->all()->select('etiqueta')->get();

        return view('usuarios.create', compact('tipos', 'etiquetas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
                'email' => 'required|email',
                'descripcion' => 'nullable|min:3',
                'password' => 'required|min:8',
            ],
            [
                'name.required' => 'Indique el nombre',
                'name.min' => 'Mínimo tres caracteres para el nombre',
                'descripcion.min' => 'Mínimo tres caracteres para la descripción',
                'email.required' => 'Indique el email',
                'email.email' => 'No es un email válido',
                'password.required' => 'Indique la contraseña',
                'password.min' => 'Mínimo 8 caracteres para la contraseña',
            ]
        );

        if ($validator->fails()) {
            return redirect('usuarios/create')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $traerUsuario = new Usuarios;
        $usuario = $traerUsuario->store($request);

        if($usuario){
            return redirect('usuarios/create')->with('actualizado', 'Usuario actualizado');
        }else{
            return redirect('usuarios/create')
                        ->with('error', 'Error en la actualizacion de los datos');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $traerUsuario = new Usuarios;
        $usuario = $traerUsuario->one($id)->first();
        $traerTipos = new UsuariosTipo;
        $tipos = $traerTipos->all()->get();
        $tipos = $tipos->splice(2);
        $traerEtiquetas = new Etiquetas;
        $etiquetas = $traerEtiquetas->all()->select('etiqueta')->get();

        if($usuario){
            return view('usuarios.edit', compact('usuario', 'tipos', 'etiquetas'));
        }else{
            return redirect('dashboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
                'descripcion' => 'nullable|min:3',
                'email' => 'required|email',
                'password' => 'nullable|min:8',
            ],
            [
                'name.required' => 'Indique el nombre',
                'name.min' => 'Mínimo tres caracteres para el nombre',
                'descripcion.min' => 'Mínimo tres caracteres para la descripción',
                'email.required' => 'Indique el email',
                'email.email' => 'No es un email válido',
                'password.min' => 'Mínimo 8 caracteres para la contraseña',
            ]
        );

        if ($validator->fails()) {
            return redirect('usuarios/'. $id .'/edit')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $traerUsuario = new Usuarios;
        $usuario = $traerUsuario->update($request, $id);

        if($usuario){
            return redirect('usuarios/'. $id .'/edit')->with('actualizado', 'Usuario actualizado');
        }else{
            return redirect('usuarios/'. $id .'/edit')
                        ->with('error', 'Error en la actualizacion de los datos');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function modalDetalles(Request $request)
    {

        //grafica N1 de dias 

            // var
                $mondays = $this->getMondays();
                $mondays2 = $this->getTuesday();
                $mondays3 = $this->getWednesday();
                $mondays4 = $this->getThursday();
                $mondays5 = $this->getFriday();
                $mondays6 = $this->getSaturday();
                $mondays7 = $this->getSunday();

                $l=0;
                $lunesPedidos=[];
                $ma=0;
                $martesPedidos=[];
                $mi=0;
                $miercolesPedidos=[];
                $j=0;
                $juevesPedidos=[];
                $v=0;
                $viernesPedidos=[];
                $s=0;
                $sabadoPedidos=[];
                $d=0;
                $domingoPedidos=[];
                $numeroPaVe=0;
            // var

            //gurdar dias
                foreach ($mondays as $monday)
                {
                    $lunes[$l]=date("Y-m-d", strtotime($monday) ) ;
                    $l++;
                }
                foreach ($mondays2 as $monday)
                {
                    $martes[$ma]=date("Y-m-d", strtotime($monday) ) ;
                    $ma++;
                }
                foreach ($mondays3 as $monday)
                {
                    $miercoles[$mi]=date("Y-m-d", strtotime($monday) ) ;
                    $mi++;
                }
                foreach ($mondays4 as $monday)
                {
                    $jueves[$j]=date("Y-m-d", strtotime($monday) ) ;
                    $j++;
                }
                foreach ($mondays5 as $monday)
                {
                    $viernes[$v]=date("Y-m-d", strtotime($monday) ) ;
                    $v++;
                }
                foreach ($mondays6 as $monday)
                {
                    $sabado[$s]=date("Y-m-d", strtotime($monday) ) ;
                    $s++;
                }
                foreach ($mondays7 as $monday)
                {
                    $domingo[$d]=date("Y-m-d", strtotime($monday) ) ;
                    $d++;
                }
               
            //gurdar dias


                $pedidos=DB::table('pedidos')
                ->where('pedidos.tienda_id','=', $request->id )
                ->get();
                

            //busqueda de pedidos ese dia
                for($d=0;$d<$l;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($lunes[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($lunesPedidos,$p);
                        }
                    }
                }
                for($d=0;$d<$ma;$d++){
                    foreach ($pedidos as $p)
                    {  
                        if($martes[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($martesPedidos,$p);
                        }
                    }
                }
                for($d=0;$d<$mi;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($miercoles[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($miercolesPedidos,$p);
                        }
                    }
                }
                for($d=0;$d<$j;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($jueves[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($juevesPedidos,$p);   
                        }
                    }
                }
                for($d=0;$d<$v;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($viernes[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($viernesPedidos,$p);  
                        }
                    }
                }
                for($d=0;$d<$s;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($sabado[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($sabadoPedidos,$p);  
                        }
                    }
                }
                for($d=0;$d<$d;$d++){
                    foreach ($pedidos as $p)
                    {
                        if($domingo[$d] == date("Y-m-d", strtotime($p->created_at)) ){
                            array_push($domingoPedidos,$p);  
                        }
                    }
                }
            //busqueda de pedidos ese dia
            
            //agregando a dias
                for($d=0;$d<24;$d++){
                    $horaLunes[$d]=0;
                    $horaMartes[$d]=0;
                    $horaMiercoles[$d]=0;
                    $horaJueves[$d]=0;
                    $horaViernes[$d]=0;
                    $horaSabado[$d]=0;
                    $horaDomingo[$d]=0;
                }
                foreach ($lunesPedidos as $pl)
                {
                    $dl=date("H", strtotime($pl->created_at));
                    settype($dl , "integer");
                    $horaLunes[$dl]++;
                }
                foreach ($martesPedidos as $pma)
                {
                    $dma=date("H", strtotime($pma->created_at));
                    settype($dma , "integer");
                    $horaMartes[$dma]++;
                }
                foreach ($miercolesPedidos as $pmi)
                {
                    $dmi=date("H", strtotime($pmi->created_at));
                    settype($dmi , "integer");
                    $horaMiercoles[$dmi]++;
                }
                foreach ($juevesPedidos as $pj)
                {
                    $dj=date("H", strtotime($pj->created_at));
                    settype($dj , "integer");
                    $horaJueves[$dj]++;
                }
                foreach ($viernesPedidos as $pv)
                {
                    $dv=date("H", strtotime($pv->created_at));
                    settype($dv , "integer");
                    $horaViernes[$dv]++;
                }
                foreach ($sabadoPedidos as $ps)
                {
                    $ds=date("H", strtotime($ps->created_at));
                    settype($ds , "integer");
                    $horaSabado[$ds]++;
                }
                foreach ($domingoPedidos as $pd)
                {
                    $dd=date("H", strtotime($pd->created_at));
                    settype($dd , "integer");
                    $horaDomingo[$dd]++;
                }
            //agregando a dias

            


               
        //grafica N1 de dias 
        
        

        //grafica N2 mes por año

            //Ventas del mes
                    //fechas de pagos
                    $now = Carbon::now();
                    $anio=$now->year;
                    $mes=$now->month;
                    $primerDia=1;
                    $ultimoDia=$this->getUltimoDiaMes($anio,$mes);

                    $fecha_inicial=date("Y-m-d H:i:s", strtotime($anio."-".$mes."-".$primerDia) );
                    $fecha_final=date("Y-m-d H:i:s", strtotime($anio."-".$mes."-".$ultimoDia) );
                    //fechas de pagos

                    //buscar
                    
                            $usuarios=DB::table('pedidos')
                            ->join('pedido_detalles', 'pedidos.id', '=', 'pedido_detalles.pedido_id')
                            ->whereBetween('pedidos.created_at', [$fecha_inicial,  $fecha_final])
                            ->where('pedidos.tienda_id','=', $request->id )
                            ->get();
     
                    //buscar

            //Ventas del mes  
            
            

            //sumar por dia
                    for($d=1;$d<=$ultimoDia;$d++){
                        $diasEfectivo[$d]=0;
                        $diasPos[$d]=0;
                        $descuento[$d]=0;
                        $nombreDias[$d]=""; 
                    }
                
                    for($d=1;$d<=$ultimoDia;$d++){
                    
                        foreach($usuarios as $usuario){
                        
                            if( date("Y-m-d", strtotime($usuario->created_at) ) == date("Y-m-d", strtotime($anio."-".$mes."-".$d) ) && $usuario->tipo_pago == "efectivo"){
                           
                                    $diasEfectivo[$d]+=($usuario->precio * $usuario->cantidad);
                                
                            }elseif( date("Y-m-d", strtotime($usuario->created_at) ) == date("Y-m-d", strtotime($anio."-".$mes."-".$d) ) && $usuario->tipo_pago == "POS"){
                    
                                    $diasPos[$d]=$diasPos[$d]+ ($usuario->precio * $usuario->cantidad);
                                
                            }
                        
                        }
                    
                        $dato= new DateTime(date("Y-m-d", strtotime($anio."-".$mes."-".$d) ));
                        if($dato->format('l') == "Monday"){
                        
                            $nombreDias[$d]="Lunes"." ".$d;
                        
                        }if($dato->format('l') == "Tuesday"){
                        
                            $nombreDias[$d]="Martes"." ".$d;
                        
                        }if($dato->format('l') == "Wednesday"){
                        
                            $nombreDias[$d]="Miercoles"." ".$d;
                        
                        }if($dato->format('l') == "Thursday"){
                        
                            $nombreDias[$d]="Jueves"." ".$d;
                        
                        }if($dato->format('l') == "Friday"){
                        
                            $nombreDias[$d]="Viernes"." ".$d;
                        
                        }if($dato->format('l') == "Saturday"){
                        
                            $nombreDias[$d]="Sábado"." ".$d;
                        
                        }if($dato->format('l') == "Sunday"){
                        
                            $nombreDias[$d]="Domingo"." ".$d;
                        
                        }
                    
                    
                    }
                
            //sumar por dia


        //grafica N2 mes por año

            $nombre=DB::table('users')
            ->where('id','=', $request->id)
            ->get('name');
            $nombre=$nombre[0]->name;
            $id=$request->id;


                return json_encode(array($horaLunes, $horaMartes, $horaMiercoles, $horaJueves, $horaViernes, $horaSabado, $horaDomingo, $diasPos, $diasEfectivo, $nombreDias, $nombre, $id));
                    

    }

    //cantidad de dias de semana
        public function getMondays()
        {
            return new \DatePeriod(
                Carbon::parse("first monday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first monday of next month")
            );
        }
        public function getTuesday()
        {
            return new \DatePeriod(
                Carbon::parse("first tuesday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first tuesday of next month")
            );
        }
        public function getWednesday()
        {
            return new \DatePeriod(
                Carbon::parse("first wednesday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first wednesday of next month")
            );
        }
        public function getThursday()
        {
            return new \DatePeriod(
                Carbon::parse("first thursday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first thursday of next month")
            );
        }
        public function getFriday()
        {
            return new \DatePeriod(
                Carbon::parse("first friday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first friday of next month")
            );
        }
        public function getSaturday()
        {
            return new \DatePeriod(
                Carbon::parse("first saturday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first saturday of next month")
            );
        }
        public function getSunday()
        {
            return new \DatePeriod(
                Carbon::parse("first sunday of this month"),
                CarbonInterval::week(),
                Carbon::parse("first sunday of next month")
            );
        }
    //cantidad de dias de semana

    public function getUltimoDiaMes($elAnio,$elMes) {
        return date("d",(mktime(0,0,0,$elMes+1,1,$elAnio)-1));
    }

    public function graficasDinamicas(Request $request){



        //fechas de pagos
            $anio=$request->anio;
            $mes=$request->mes;
            $primerDia=1;
            $ultimoDia=$this->getUltimoDiaMes($anio,$mes);


            $fecha_inicial=date("Y-m-d H:i:s", strtotime($anio."-".$mes."-".$primerDia) );
            $fecha_final=date("Y-m-d H:i:s", strtotime($anio."-".$mes."-".$ultimoDia) );
        //fechas de pagos

        //buscar
        
                $usuarios=DB::table('pedidos')
                ->join('pedido_detalles', 'pedidos.id', '=', 'pedido_detalles.pedido_id')
                ->whereBetween('pedidos.created_at', [$fecha_inicial,  $fecha_final])
                ->where('pedidos.tienda_id','=', $request->id )
                ->get();
        

        //buscar

        //sumar por dia

            for($d=1;$d<=$ultimoDia;$d++){
                $diasEfectivo[$d]=0;
                $diasPos[$d]=0;
                $nombreDias[$d]=""; 
            };


            for($d=1;$d<=$ultimoDia;$d++){
            
                foreach($usuarios as $usuario){
                
                    if( date("Y-m-d", strtotime($usuario->created_at) ) == date("Y-m-d", strtotime($anio."-".$mes."-".$d) ) && $usuario->tipo_pago == "efectivo"){

                            $diasEfectivo[$d]+=($usuario->precio * $usuario->cantidad);
            
                    }elseif( date("Y-m-d", strtotime($usuario->created_at) ) == date("Y-m-d", strtotime($anio."-".$mes."-".$d) ) && $usuario->tipo_pago == "POS"){
  
                            $diasPos[$d]=$diasPos[$d]+ ($usuario->precio * $usuario->cantidad);

                    }
                
                }
                $dato= new DateTime(date("Y-m-d", strtotime($anio."-".$mes."-".$d) ));
                if($dato->format('l') == "Monday"){

                    $nombreDias[$d]="Lunes"." ".$d;

                }if($dato->format('l') == "Tuesday"){

                    $nombreDias[$d]="Martes"." ".$d;

                }if($dato->format('l') == "Wednesday"){

                    $nombreDias[$d]="Miercoles"." ".$d;

                }if($dato->format('l') == "Thursday"){

                    $nombreDias[$d]="Jueves"." ".$d;

                }if($dato->format('l') == "Friday"){

                    $nombreDias[$d]="Viernes"." ".$d;

                }if($dato->format('l') == "Saturday"){

                    $nombreDias[$d]="Sábado"." ".$d;

                }if($dato->format('l') == "Sunday"){

                    $nombreDias[$d]="Domingo"." ".$d;

                }

            
            }


        //sumar por dia

        $todoUnido=[$nombreDias, $diasEfectivo, $diasPos];

        return $todoUnido;
                    
       

    }

}
