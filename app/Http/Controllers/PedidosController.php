<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\modelos\PedidosController as PC;
use App\Http\Controllers\modelos\UsersController as USUARIOS;
use App\Http\Controllers\modelos\MotoPedidosController as MOTOPEDIDOS;
use App\Http\Controllers\API\JsonResponseApi as JSON;
use App\Http\Controllers\FCMnotificacion as FCM;
use App\Moto_pedidos;
use App\User;
use Auth;
use DB;

class PedidosController extends Controller
{
    public function index(){
        $listarPedidos = new PC;
        $pedidos = $listarPedidos->all()
                            ->where('tienda_id', $this->tienda()->id)
                            ->where('estatus', '!=', 'fallido')
                            ->orderBy('fecha', 'desc')
                            ->paginate(15);
        return view('pedidos.index', compact('pedidos'));

    }

    public function tienda(){
        return Auth::user();
    }

    public function pedido($id){
        $buscarPedido = new PC;
        $pedido = $buscarPedido
                        ->one($id)
                        ->with(['cliente', 'detalles.producto', 'detalles.adicional', 'pagoefectivo'])
                        ->first();
        return response()->json($pedido);
    }

    public function aceptarPedido(Request $request, $id){
        $buscarPedido = new PC;
        try{
            $pedido = $buscarPedido
                        ->one($id)
                        // ->where('tienda_id', $this->tienda()->id)
                        ->where('estatus', 'recibido')
                        ->firstOrFail();
            $pedido->aceptado = now();
            $pedido->tiempoentrega = $request->data['tiempoentrega'];
            $pedido->costoenvio = $this->tienda()->costoEnvio;
            $pedido->estatus = "aceptado";
            $pedido->save();
            $respuesta = ['exito' => 'Pedido aceptado'];
        }catch(\Exception $e){
            $respuesta = ['error' => 'Pedido no encontrado'];
        }

        if($pedido->motospropias == 0){
            try {
                $grabarMotoPedido = new MOTOPEDIDOS;
                $data = [
                    'pedido_id' => $pedido->id,
                    'estatus' => $pedido->estatus,
                    'tiempopreparacion' => $pedido->tiempoentrega
                ];

                $motoPedido = $grabarMotoPedido->store($data);

                if(!$motoPedido){
                    throw new \Exception("Error Processing Request");
                }
                
            } catch (\Exception $e) {
                $formato = new JSON;
                $formato->error(['error' => 'Error al grabar el moto pedido' . $motoPedido], $e);
            }
        }

        try {
            $tokenCliente = $this->cliente($pedido->cliente_id)->fcm_token;
            $pedidoNotificable = [
                'id' => $pedido->id,
                'estatus' => $pedido->estatus
            ];
            $notificacion = new FCM;
            $notificacion->enviarNotificacion($tokenCliente, '', 'Tu pedido lo están preparando', $pedidoNotificable);
        } catch (\Exception $th) {
            //$respuesta = ['exito' => $th];
        }

        return response()->json($respuesta);

    }

    public function cancelarPedido(Request $r, $id){
        try {
            $buscarPedido = new PC;
            $pedido = $buscarPedido->one($id)
                        ->where(function($q){
                            $q->orWhere('estatus', 'recibido');
                            $q->orWhere('estatus', 'aceptado');
                            $q->orWhere('estatus', 'listo');
                            $q->orWhere('estatus', 'buscando');
                            $q->orWhere('estatus', 'encamino');
                        })
                        ->firstOrFail();

            $pedido->estatus = "cancelado";
            $pedido->save();

            $pedidoNotificable = [
                'id' => $pedido->id,
                'estatus' => $pedido->estatus
            ];

        } catch (\Exception $e) {
            // $respuesta = $formato->error(['error' => 'Pedido no encontrado'], $e);
            $respuesta = ['error' => 'Pedido no encontrado'];
        }

        try {
            $tokenCliente = $this->cliente($pedido->cliente_id)->fcm_token;
            $pedidoNotificable = [
                'id' => $pedido->id,
                'estatus' => $pedido->estatus
            ];
            $notificacion = new FCM;
            $notificacion->enviarNotificacion($tokenCliente, '', 'Tu pedido lo están preparando', $pedidoNotificable);
        } catch (\Exception $th) {
            //$respuesta = ['exito' => $th];
        }

        $respuesta = ['exito' => 'Pedido cancelado'];

        return response()->json($respuesta);
    }

    public function enviarPedido(Request $request, $id){
        $buscarPedido = new PC;
        try{
            $pedido = $buscarPedido
                        ->one($id)
                        ->where('tienda_id', $this->tienda()->id)
                        ->where('estatus', 'aceptado')
                        ->firstOrFail();
            $pedido->enviado = now();
            $pedido->estatus = "encamino";
            $pedido->save();
            $respuesta = ['exito' => 'Pedido enviado'];
        }catch(\Exception $e){
            $respuesta = ['error' => 'Pedido no encontrado---'];
        }

        try {
            $tokenCliente = $this->cliente($pedido->cliente_id)->fcm_token;
            $pedidoNotificable = [
                'id' => $pedido->id,
                'estatus' => $pedido->estatus
            ];
            $notificacion = new FCM;
            $notificacion->enviarNotificacion($tokenCliente, '', 'Tu pedido se ha enviado', $pedidoNotificable);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json($respuesta);

    }

    public function entregarPedido(Request $request, $id){
        $buscarPedido = new PC;
        try{
            $pedido = $buscarPedido
                        ->one($id)
                        ->where('tienda_id', $this->tienda()->id)
                        ->where('estatus', 'encamino')
                        ->firstOrFail();
            $pedido->enviado = now();
            $pedido->estatus = "entregado";
            $pedido->save();
            $respuesta = ['exito' => 'Pedido entregado'];
        }catch(\Exception $e){
            $respuesta = ['error' => 'Pedido no encontrado---'];
        }

        try {
            $tokenCliente = $this->cliente($pedido->cliente_id)->fcm_token;
            $pedidoNotificable = [
                'id' => $pedido->id,
                'estatus' => $pedido->estatus
            ];
            $notificacion = new FCM;
            $notificacion->enviarNotificacion($tokenCliente, '', 'Has recibido tu pedido', $pedidoNotificable);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return response()->json($respuesta);

    }

    public function listoEnviar(Request $request, $id){

        $buscarPedido = new PC;
        try{
            $pedido = $buscarPedido
                        ->one($id)
                        ->where('tienda_id', $this->tienda()->id)
                        ->where('estatus', 'aceptado')
                        ->firstOrFail();
            $pedido->enviado = now();
            $pedido->estatus = "encamino";
            $pedido->save();
            $respuesta = ['exito' => 'Pedido enviado'];

            //$rango = $this->getBoundaries($this->tienda()->latitud, $this->tienda()->longitud, 1);

        }catch(\Exception $e){
            $respuesta = ['error' => 'Pedido no encontrado---'];
        }

        $pedidoNotificable = [
            'id' => $pedido->id,
            'estatus' => $pedido->estatus,
        ];  

        try {
            $cliente = User::find($pedido->cliente_id);
            $notificacion = new FCM;
            $notificacion->enviarNotificacion($cliente->fcm_token, '', 'Tu pedido va en camino hacia ti.', $pedidoNotificable);
        } catch (\Exception $e) {}


        return response()->json($respuesta);

    }

    public function cliente($id){
        $cliente = new USUARIOS;
        return $cliente->one($id)->first();
    }

    public function activos($estatus = ""){
        $listarPedidos = new PC;

        switch (strtolower($estatus)) {
            case 'activos':
                if(Auth::user()->tipo->name == "tienda"){
                $pedidos = $listarPedidos->all()
                                    ->where('tienda_id', $this->tienda()->id)
                                    ->where('estatus', '!=', 'fallido')
                                    ->orderBy('fecha', 'desc')
                                    ->paginate(15);
                }else{
                    $pedidos = $listarPedidos->all()
                                    ->where('estatus', '!=', 'fallido')
                                    ->orderBy('fecha', 'desc')
                                    ->paginate(15);
                }
                break;

            case '':
                if(Auth::user()->tipo->name == "tienda"){
                    $pedidos = $listarPedidos->all()
                                    ->where('tienda_id', $this->tienda()->id)
                                    ->where('estatus', '!=', 'fallido')
                                    ->orderBy('fecha', 'desc')
                                    ->paginate(15);
                }else{
                    $pedidos = $listarPedidos->all()
                                    ->where('estatus', '!=', 'fallido')
                                    ->orderBy('fecha', 'desc')
                                    ->paginate(15);
                }
                break;
            
            default:
                return abort(404);
                break;
        }
        return view('pedidos.activos', compact('pedidos'));
    }

    public function historico(){
        $listarPedidos = new PC;
        $pedidos = $listarPedidos->all()
                            ->where('tienda_id', $this->tienda()->id)
                            ->where('estatus', '!=', 'fallido')
                            ->orderBy('fecha', 'desc')
                            ->paginate(15);
        return view('pedidos.historico', compact('pedidos'));
    }

    public function motorizados(){
        $motorizados = User::whereHas('tipo', function($q){
                                $q->where('name', 'motorizado');
                            });
        return $motorizados;
    }

    public function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371)
    {
        $return = array();
         
        // Los angulos para cada dirección
        $cardinalCoords = array('north' => '0',
                                'south' => '180',
                                'east' => '90',
                                'west' => '270');
        $rLat = deg2rad($lat);
        $rLng = deg2rad($lng);
        $rAngDist = $distance/$earthRadius;
        foreach ($cardinalCoords as $name => $angle)
        {
            $rAngle = deg2rad($angle);
            $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
             $return[$name] = array('lat' => (float) rad2deg($rLatB), 
                                    'lng' => (float) rad2deg($rLonB));
        }
        return array('min_lat'  => $return['south']['lat'],
                     'max_lat' => $return['north']['lat'],
                     'min_lng' => $return['west']['lng'],
                     'max_lng' => $return['east']['lng']);
    }

}
