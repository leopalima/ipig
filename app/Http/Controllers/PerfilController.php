<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\modelos\UsersController as Usuario;
use App\Http\Controllers\modelos\tiendasController as Configuracion;
use Validator;
use Carbon\Carbon;

class PerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = new Usuario;
        $todosLosUsuarios = $usuarios->all()->get();
        return $todosLosUsuarios;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $traerUsuario = new Usuario;
        $usuario = $traerUsuario->one($id)->first();

        if($usuario){
            return view('perfil.edit', compact('usuario'));
        }else{
            return redirect('dashboard');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $traerUsuario = new Usuario;
        $usuario = $traerUsuario->one($id)->first();

        if($usuario){
            return view('perfil.edit', compact('usuario'));
        }else{
            return redirect('dashboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        // return response()->json($request);
        if($request->has("configName")){

            $validator = Validator::make(
                $request->all(), [
                    'tiempoEntrega' => 'required|numeric',
                    'costoEnvio' => 'required|numeric',
                    'whatsapp' => 'numeric',
                    'descuento' => 'required|numeric',
                    'terminos' => 'nullable|url',
                ],
                [
                    'tiempoEntrega.required' => 'Indique el tiempo de preparación de los pedidos.',
                    'tiempoEntrega.numeric' => 'Este campo es tipo númerico.',
                    'costoEnvio.required' => 'Indique el costo de envío de los pedidos.',
                    'costoEnvio.numeric' => 'Este campo es tipo númerico.',
                    'whatsapp.numeric' => 'El campo whatsapp solo acepta número.',
                    'descuento.required' => 'Indique el costo de envío de los pedidos.',
                    'descuento.numeric' => 'Este campo es tipo númerico.',
                    'terminos.url' => 'No es una URL válida.'
                ]
            );

            if ($validator->fails()) {

                return response()->json($validator->errors());

            }


            $traerConfiguracion= new Configuracion;
            $configuracion=$traerConfiguracion->update($request,$id);
            if($configuracion){
                return response()->json(['exito'=>1]);
            }

            return response()->json(['error'=>1]);

        }elseif($request->has("configTiempo")){



            $validator = Validator::make(
                $request->all(), [
                    'tiempoLunes' => 'required',
                    'tiempoMartes' => 'required',
                    'tiempoMiercoles' => 'required',
                    'tiempoJueves' => 'required',
                    'tiempoViernes' => 'required',
                    'tiempoSabado' => 'required',
                    'tiempoDomingo' => 'required',
                ],
                [
                    'tiempoLunes.required' => 'Indique el rango de tiempo de trabajo',
                    'tiempoMartes.required' => 'Indique el rango de tiempo de trabajo',
                    'tiempoMiercoles.required' => 'Indique el rango de tiempo de trabajo',
                    'tiempoJueves.required' => 'Indique el rango de tiempo de trabajo',
                    'tiempoViernes.required' => 'Indique el rango de tiempo de trabajo',
                    'tiempoSabado.required' => 'Indique el rango de tiempo de trabajo',
                    'tiempoDomingo.required' => 'Indique el rango de tiempo de trabajo',
                ]
            );

            if ($validator->fails()) {

                return response()->json($validator->errors());

            }

            $lunes=explode( '-', $request->tiempoLunes );
            $martes=explode( '-', $request->tiempoMartes );
            $miercoles=explode( '-', $request->tiempoMiercoles );
            $jueves=explode( '-', $request->tiempoJueves );
            $viernes=explode( '-', $request->tiempoViernes );
            $sabado=explode( '-', $request->tiempoSabado );
            $damingo=explode( '-', $request->tiempoDomingo );

            if(strtotime($lunes[1])>strtotime($lunes[0]) && strtotime($martes[1])>strtotime($martes[0]) && strtotime($miercoles[1])>strtotime($miercoles[0]) && strtotime($jueves[1])>strtotime($jueves[0]) && strtotime($viernes[1])>strtotime($viernes[0]) && strtotime($sabado[1])>strtotime($sabado[0]) && strtotime($damingo[1])>strtotime($damingo[0])){

                $traerConfiguracion= new Configuracion;
                $configuracion=$traerConfiguracion->update($request,$id);
                if($configuracion){
                    return response()->json(['exito'=>1]);
                }

                return response()->json(['error'=>1]);

            }else{

                return response()->json(['errorDife'=>1]);
            }
            
        
        
        }else{

            $validator = Validator::make(
                $request->all(), [
                    'name' => 'required|min:3',
                    'email' => 'required|email',
                    'descripcion' => 'nullable|min:3',
                ],
                [
                    'name.required' => 'Indique el nombre',
                    'name.min' => 'Mínimo tres caracteres para el nombre',
                    'email.required' => 'Indique el email',
                    'email.email' => 'No es un email válido',
                    'descripcion.min' => 'Mínimo tres caracteres para la descripción',
                ]
            );
    
            if ($validator->fails()) {
                return redirect('perfil/'. $id .'/edit')
                            ->withErrors($validator)
                            ->with('error', 'Error en la validación del formulario')
                            ->withInput();
            }
    
            $traerUsuario = new Usuario;
            $usuario = $traerUsuario->update($request, $id);
    
            if($usuario){
                return redirect('perfil/'. $id .'/edit')->with('actualizado', 'Rol actualizado');
            }else{
                return redirect('perfil/'. $id .'/edit')
                            ->with('error', 'Error en la actualizacion de los datos');
            }

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cerradoEmergencia(Request $request)
    {

        $id=$request->idTinenda;
        $traerConfiguracion= new Configuracion;
        $configuracion=$traerConfiguracion->update($request,$id);
        if($configuracion){
            return response()->json(['exito'=>1]);
        }

        return response()->json(['error'=>1]);




    }

}
