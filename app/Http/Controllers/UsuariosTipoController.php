<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\modelos\UsersTipoController as UsuariosTipo;
use Validator;

class UsuariosTipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listarTipos = new UsuariosTipo;
        $tipos = $listarTipos->all()->paginate(10);
        return view('usuarios_tipo.index', compact('tipos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios_tipo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
                'descripcion' => 'required|min:3',
            ],
            [
                'name.required' => 'Indique el nombre',
                'name.min' => 'Mínimo tres caracteres para el nombre',
                'descripcion.required' => 'Indique la descripción',
                'descripcion.min' => 'Mínimo tres caracteres para la descripción',
            ]
        );

        if ($validator->fails()) {
            return redirect('tipos/create')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $buscarTipo = new UsuariosTipo;
        $tipo = $buscarTipo->store($request);

        if($tipo){
            return redirect('tipos/create')->with('actualizado', 'Tipo agregado');
        }else{
            return redirect('tipos/create')
                        ->with('error', 'Error en la actualizacion de los datos');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buscarTipo = new UsuariosTipo;
        $tipo = $buscarTipo->one($id)->first();
        if($tipo){
            return view('usuarios_tipo.edit', compact('tipo'));
        }else{
            return redirect('dasboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
                'descripcion' => 'required|min:3',
            ],
            [
                'name.required' => 'Indique el nombre',
                'name.min' => 'Mínimo tres caracteres para el nombre',
                'descripcion.required' => 'Indique la descripción',
                'descripcion.min' => 'Mínimo tres caracteres para la descripción',
            ]
        );

        if ($validator->fails()) {
            return redirect('tipos/'. $id .'/edit')
                        ->withErrors($validator)
                        ->with('error', 'Error en la validación del formulario')
                        ->withInput();
        }

        $buscarTipo = new UsuariosTipo;
        $tipo = $buscarTipo->update($request, $id);

        if($tipo){
            return redirect('tipos/'. $id .'/edit')->with('actualizado', 'Tipo actualizado');
        }else{
            return redirect('tipos/'. $id .'/edit')
                        ->with('error', 'Error en la actualizacion de los datos');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
