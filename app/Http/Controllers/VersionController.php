<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Version;
use Log;
use Validator;

class VersionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $version = Version::orderBy('id', 'desc')->paginate(15);

        return view('version.index', compact('version'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $respuesta = array();

        $validator = Validator::make(
            $request->all(), [
                'txtNumero' => 'required',
                'so' => 'required',
                'txtAutor' => 'required',
                'txtCambios' => 'required',
            ],
            [
                'txtNumero.required' => 'Indique el numero de versión',
                'so.required' => 'Seleccione el sistema operativo',
                'txtAutor.required' => 'Indique el autor',
                'txtCambios.required' => 'Indique los cambios',
            ]
        );

        if ($validator->fails()) {
            $respuesta = array(
                "estatus" => "error",
                "data" => $validator->errors()->first(),
            );
        }else{

            try {
                $version = new Version;

                $version->autor = $request->txtAutor;
                $version->cambios = $request->txtCambios;
                $version->so = $request->so;
                $version->fecha = now();
                $version->version_num = $request->txtNumero;
                $version->save();

                $respuesta = array(
                    "estatus" => "success",
                    "data" => $version,
                );

            } catch (\Exception $e) {
                Log::error($e);
                $respuesta = array(
                    "estatus" => "error",
                    "data" => $e,
                );
            }
        }

        return response()->json($respuesta);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $respuesta = array();
        try {
            $version = Version::where('id', $id)->first();
            $respuesta = array(
                "estatus" => "success",
                "data" => $version,
            );
        } catch (\Exception $e) {
            $respuesta = array(
                "estatus" => "error",
                "data" => $e,
            );
        }

        return response()->json($respuesta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $respuesta = array();

        $validator = Validator::make(
            $request->all(), [
                'txtNumero' => 'required',
                'so' => 'required',
                'txtAutor' => 'required',
                'txtCambios' => 'required',
            ],
            [
                'txtNumero.required' => 'Indique el numero de versión',
                'so.required' => 'Seleccione el sistema operativo',
                'txtAutor.required' => 'Indique el autor',
                'txtCambios.required' => 'Indique los cambios',
            ]
        );

        if ($validator->fails()) {
            $respuesta = array(
                "estatus" => "error",
                "data" => $validator->errors()->first(),
            );
        }else{

            try {
                $version = Version::where('id', $id)->firstOrFail();

                $version->autor = $request->txtAutor;
                $version->cambios = $request->txtCambios;
                $version->so = $request->so;
                $version->version_num = $request->txtNumero;
                $version->save();

                $respuesta = array(
                    "estatus" => "success",
                    "data" => $version,
                );

            } catch (\Exception $e) {
                Log::error($e);
                $respuesta = array(
                    "estatus" => "error",
                    "data" => $e,
                );
            }
        }

        return response()->json($respuesta);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
