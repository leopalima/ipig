<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\User;

class CambioLoginController extends Controller
{
    public function index($id)
    {
        $usuario = User::find($id);

        Auth::login($usuario);

        return redirect()->route('dashboard.index');

        return response()->json($usuario);
    }
}
