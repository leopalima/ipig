<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\modelos\ProductosController as Productos;
use App\Http\Controllers\modelos\ProductosCategoriasController as Categorias;
use Auth;
use Validator;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listarProductos = new Productos;
        $productos = $listarProductos->all()->where('tienda_id', $this->usuario()->id)->paginate(10);

        $listarCategorias = new Categorias;
        $categorias = $listarCategorias->all()
        ->where('tienda_id', $this->usuario()->id)
        ->get();


        return view('productos.index', compact('productos','categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listarCategorias = new Categorias;
        $categorias = $listarCategorias->all()->where('tienda_id', $this->usuario()->id)->get();
        return view('productos.create', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
                'descripcion' => 'nullable|min:1|max:200',
                'precio' => 'required|numeric',
                'categoria' => 'required',
            ],
            [
                'name.required' => 'Indique el nombre del producto.',
                'name.min' => 'Mínimo tres caracteres para el nombre.',
                'descripcion.min' => 'Mínimos tres caracteres para la descripción.',
                'descripcion.max' => 'Máximo 200 caracteres para la descripción.',
                'precio.required' => 'Indique el precio.',
                'categoria.required' => 'Indique la categoría.',
            ]
        );

        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()]);
        }

        $request->tienda = $this->usuario()->id;

        $almacenarProducto = new Productos;
        $producto = $almacenarProducto->store($request);

        if($producto){
            return response()->json(
                [
                    'grabado'=> 'Grabado exitosamente.',
                    'producto_id'=> $producto->id,
                    'url_foto'=> $producto->foto,
                    ]
            );
            // return redirect('productos/create')->with('actualizado', 'Producto registrado');
        }else{
            return response()->json(['error'=>'Error al guardar el registro.']);
            // return redirect('productos/create')
            //             ->with('error', 'Error en la actualizacion de los datos')
            //             ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $buscarProducto = new Productos;
            $producto = $buscarProducto->one($id)->where('tienda_id', $this->usuario()->id)->first();
        }catch(\Exception $e){
            return 0;
        }

        $listarCategorias = new Categorias;
        $categorias = $listarCategorias->all()->where('tienda_id', $this->usuario()->id)->get();

        if($producto){
            return view('productos.edit', compact('producto', 'categorias'));
        }else{
            return redirect(route('tienda.productos.index'));
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(), [
                'name' => 'required|min:3',
                'descripcion' => 'nullable|min:1|max:200',
                'precio' => 'required|numeric',
                'categoria' => 'required',
            ],
            [
                'name.required' => 'Indique el nombre del producto.',
                'name.min' => 'Mínimo tres caracteres para el nombre.',
                'descripcion.min' => 'Mínimos tres caracteres para la descripción.',
                'descripcion.max' => 'Máximo 200 caracteres para la descripción.',
                'precio.required' => 'Indique el precio.',
                'categoria.required' => 'Indique la categoría.',
            ]
        );

        if($validator->fails()){
            return response()->json(['errors'=>$validator->errors()]);
        }

        $almacenarProducto = new Productos;
        $producto = $almacenarProducto->update($request, $id);

        if($producto){
            return response()->json(
                [
                    'grabado'=> 'Grabado exitosamente.',
                    'producto_id'=> $producto->id,
                    'url_foto' => $producto->foto,
                    ]
            );
            // return redirect('productos/'. $id .'/edit')->with('actualizado', 'Producto registrado');
        }else{
            return response()->json(['error'=>'Error al guardar el registro.']);
            // return redirect('productos/'. $id .'/edit')
            //             ->with('error', 'Error en la actualizacion de los datos')
            //             ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function usuario(){
        $usuario = Auth::user();
        return $usuario;
    }

    public function productosDeCategoria($id){

        $listarProductos = new Productos;
        $productos = $listarProductos->all()
        ->orderBy('orden', 'DESC')
        ->where('tienda_id', $this->usuario()->id)
        ->where('categoria_id', $id)
        ->with('categoria','adicionales')  
        ->get();
       


        return  $productos;
        
                
        
    }
    public function productosDeCategoriaGuardar(Request $request){

     
            $almacenarProducto = new Productos;
            $producto = $almacenarProducto->updatePosicion($request);  

       
            if($producto){
                return response()->json(['grabado'=> 'Grabado exitosamente.',]);
                
            }else{
                return response()->json(['error'=>'Error al guardar el registro.']);
 
            }
                
        
    }
}
