<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mensajes_ayuda;
use Auth;

class MensajesAyudaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mensajesAyuda=Mensajes_ayuda::orderBy("id", "asc")->paginate(10);

        
        return view('error.mensajes_ayuda',compact('mensajesAyuda'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {     


        if(request()->ajax())
        {
            
            $data = Mensajes_ayuda::findOrFail($id);
            $cliente=$data->cliente;
            return response()->json(['data' => $data,'cliente' => $cliente]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $atendido_por=Auth::user()->id;

        $form_data = array(
            'estatus'        =>  'cerrado',
            'atendido_por' => $atendido_por,
        );

        Mensajes_ayuda::whereId($id)->update($form_data);
        
        return response()->json(['success' => 'Los cambios fueron guardados.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
