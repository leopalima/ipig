<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensajes_ayuda extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id' , 'mensaje', 'atendido_por', 'estatus',
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'mensajes_ayuda';

    public function cliente(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

}
