<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Banner extends Model
{
    protected $fillable = [
        'imagen_url', 'tienda_id', 'estatus',
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'banner';

    /**
     * Quitar el autoincremento para trabajar con Uuid.
     */

    public $incrementing = false;

    /**
     * Función que se ejecuta al llamar el modelo.
     */

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $uuid = Uuid::generate()->string;
            $model->{$model->getKeyName()} = $uuid;
        });

    }

    public function tienda()
    {
        return $this->hasOne('App\User', 'id', 'tienda_id');
    }

    public function scopeActivo($q)
    {
        return $q->where('estatus', 1);
    }

    public function getActivo()
    {
        return $this->activo()->get();
    }
}
