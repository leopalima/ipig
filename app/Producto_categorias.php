<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;

class Producto_categorias extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tienda_id', 'name', 'descripcion', 'foto', 'orden', 'estatus', 
    ];

    /**
     * Quitar el autoincremento para trabajar con Uuid.
     */

    public $incrementing = false;

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'producto_categorias';

    /**
     * Función que se ejecuta al llamar el modelo.
     */

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $uuid = Uuid::generate()->string;
            $model->{$model->getKeyName()} = $uuid;
        });

    }

    public function productos(){
        return $this->hasMany('App\Productos', 'categoria_id', 'id');
    }

    public function getFotoAttribute($v){
        if($v !== null && $v !== "")
            return url('/') . Storage::url('categorias/'.$v);

            return asset('categorias-alimentos.jpg');
    }
}
