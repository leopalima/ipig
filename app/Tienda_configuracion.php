<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tienda_configuracion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clave', 'valor', 'tienda_id',
    ];


    /**
     * Establece el nombre de la tabla.
     */

     protected $table = 'tienda_configuracion';
}
