<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emojis extends Model
{
    protected $fillable = [
        'clasificacion', 'emoji',
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'emojis';
}
