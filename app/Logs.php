<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id' , 'descripcion',
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'logs';

    public function usuario(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
