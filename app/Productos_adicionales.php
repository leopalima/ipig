<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos_adicionales extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'producto_id', 'adicionales_id', 'tienda_id',
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'productos_adicionales';

    /**
     * Quitar el autoincremento para trabajar con Uuid.
     */

    public $incrementing = false;

    /**
     * Función que se ejecuta al llamar el modelo.
     */

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $uuid = Uuid::generate()->string;
            $model->{$model->getKeyName()} = $uuid;
        });

    }
}
