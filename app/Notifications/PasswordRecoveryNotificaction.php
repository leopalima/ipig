<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordRecoveryNotificaction extends Notification
{
    use Queueable;

    protected $nuevoPassword;
    protected $tienda;
    protected $usuario;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->nuevoPassword = $data['nuevoPassword'];
        $this->tienda = $data['tienda'];
        $this->usuario = $data['usuario'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $password = $this->nuevoPassword;
        $tienda = $this->tienda;
        $usuario = $this->usuario;
        return (new MailMessage)
            ->from($tienda->email, $tienda->name)
            ->subject('Nueva contraseña')
            ->view('notification/passwordRecovery', compact('password', 'tienda', 'usuario'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
