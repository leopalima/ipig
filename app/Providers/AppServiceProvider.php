<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Auth;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Passport::ignoreMigrations();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('usuario', function ($valor) {
            if(Auth::user()->tipo->name === $valor){
                return true;
            }
            return false;
        });
        Blade::if('usuarios', function ($valor=[]) {
            
            $cantidad=count($valor);
            if($cantidad){
                for($i=0 ; $i<$cantidad ; $i++)
                {
                    if(Auth::user()->tipo->name == $valor[$i]){
                        return true;
                    }
                }
            }
            
            return false;
        });
    }
}
