<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moto_pedidos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pedido_id' , 'estatus', 'tiempopreparacion', 'motorizado_id', 'fecha'
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'moto_pedidos';
}
