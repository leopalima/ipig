<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prueba extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'texto',
    ];

    protected $table = "prueba";
}
