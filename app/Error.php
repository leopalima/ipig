<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Error extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id' , 'code' , 'file' , 'line' , 'message' , 'trace',
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'error';

    public function usuario(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
