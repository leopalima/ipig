<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Adicionales extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo', 'tienda_id', 'producto_id', 'maximo', 'estatus', 'orden'
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'adicionales';

    /**
     * Quitar el autoincremento para trabajar con Uuid.
     */

    public $incrementing = false;

    /**
     * Función que se ejecuta al llamar el modelo.
     */

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $uuid = Uuid::generate()->string;
            $model->{$model->getKeyName()} = $uuid;
        });

    }

    public function items(){
        return $this->hasMany('App\Adicionales_items', 'adicionales_id', 'id');
    }
}
