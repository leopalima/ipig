<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Error;
use Auth;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        // Checks if a user has logged in to the system, so the error will be recorded with the user id
        // $userId = 0;
        // if (Auth::user()) {
        //     $userId = Auth::user()->id;
        // }

        // $data = array(
        //     'user_id' => $userId,
        //     'code' => $exception->getCode(),
        //     'file' => $exception->getFile(),
        //     'line' => $exception->getLine(),
        //     'message' => $exception->getMessage(),
        //     'trace' => $exception->getTraceAsString(),
        // );

        // Error::create($data);

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }
}
