<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Webpatser\Uuid\Uuid;
use App\Users_tipo;
Use Carbon\Carbon;
use App\Notifications\EmailResetPasswordNotification;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'tipo_id', 'fcm_token', 'phone', 'estatus', 'tienda_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Quitar el autoincremento para trabajar con Uuid.
     */

    public $incrementing = false;

    /**
     * Función que se ejecuta al llamar el modelo.
     */

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $uuid = Uuid::generate()->string;
            $model->{$model->getKeyName()} = $uuid;

            // if($model->email == "motorizado@motorizado.com" ||
            //    $model->email == "root@root.com" ||
            //    $model->email == "tienda@tienda.com" ||
            //    $model->email == "cliente@cliente.com" ||
            //    $model->email == "admin@admin.com")
            // {
            //     $tipo = Users_tipo::where('name', 'cliente')->first();
            //     $model->tipo_id = $tipo->id;
            // }

        });

    }

    /**
     * Campos personalizados.
     */
    protected $appends = [
        'tiempoEntrega',
        'costoEnvio',
        'valoracion',
        'etiquetas',
        'validate',
        'motospropias',
        'mistarjetas',
        'pagosaceptados',
        'tiempoLunes',
        'tiempoMartes',
        'tiempoMiercoles',
        'tiempoJueves',
        'tiempoViernes',
        'tiempoSabado',
        'tiempoDomingo',
        'cerradoEmergenciaActivo',
        'calificacion',
        'descuento',
        'whatsapp',
        'recojoentienda',
        'terminos',
        'abierto',
        'esMultimarcas',
        'prctjcomision'
    ];

    /**
     * Establece los campos personalizados.
     */
    public function getTiempoEntregaAttribute(){
        $tiempoEntrega =  $this->configuracion()->where('clave', 'tiempoEntrega')->first();
        return $tiempoEntrega != null ? (int) $tiempoEntrega->valor : 25;
    }
    public function getCostoEnvioAttribute(){
        $costoEnvio =  $this->configuracion()->where('clave', 'costoEnvio')->first();
        return $costoEnvio != null ? (float) round($costoEnvio->valor,2) : round(5,2);
    }
    public function getTiempoLunesAttribute(){
        $tiempoLunes =  $this->configuracion()->where('clave', 'tiempoLunes')->first();
        return $tiempoLunes != null ? $tiempoLunes->valor : "8:00 AM - 10:00 PM";
    }
    public function getTiempoMartesAttribute(){
        $tiempoMartes =  $this->configuracion()->where('clave', 'tiempoMartes')->first();
        return $tiempoMartes != null ? $tiempoMartes->valor : "8:00 AM - 10:00 PM";
    }
    public function getTiempoMiercolesAttribute(){
        $tiempoMiercoles =  $this->configuracion()->where('clave', 'tiempoMiercoles')->first();
        return $tiempoMiercoles != null ? $tiempoMiercoles->valor : "8:00 AM - 10:00 PM";
    }
    public function getTiempoJuevesAttribute(){
        $tiempoJueves =  $this->configuracion()->where('clave', 'tiempoJueves')->first();
        return $tiempoJueves != null ? $tiempoJueves->valor : "8:00 AM - 10:00 PM";
    }
    public function getTiempoViernesAttribute(){
        $tiempoViernes =  $this->configuracion()->where('clave', 'tiempoViernes')->first();
        return $tiempoViernes != null ? $tiempoViernes->valor : "8:00 AM - 10:00 PM";
    }
    public function getTiempoSabadoAttribute(){
        $tiempoSabado =  $this->configuracion()->where('clave', 'tiempoSabado')->first();
        return $tiempoSabado != null ? $tiempoSabado->valor : "8:00 AM - 10:00 PM";
    }
    public function getTiempoDomingoAttribute(){
        $tiempoDomingo =  $this->configuracion()->where('clave', 'tiempoDomingo')->first();
        return $tiempoDomingo != null ? $tiempoDomingo->valor : "8:00 AM - 10:00 PM";
    }
    public function getCerradoEmergenciaActivoAttribute(){
        $cerradoEmergenciaActivo =  $this->configuracion()->where('clave', 'cerrado')->first();
        return $cerradoEmergenciaActivo != null ? $cerradoEmergenciaActivo->valor : "0";
    }
    public function getValoracionAttribute(){
        return 4;
    }
    public function getEtiquetasAttribute(){
        $etiquetas =  $this->configuracion()->where('clave', 'etiquetas')->first();
        return $etiquetas != null ? unserialize($etiquetas->valor) : [];
    }
    public function getMotospropiasAttribute(){
        $motospropias =  $this->configuracion()->where('clave', 'motospropias')->first();
        return $motospropias != null ? (int) $motospropias->valor : 0;
    }
    public function getPrctjcomisionAttribute(){
        $prctjcomision =  $this->configuracion()->where('clave', 'prctjcomision')->first();
        return $prctjcomision != null ? (int) $prctjcomision->valor : 8;
    }

    public function getValidateAttribute(){
        $valido =  $this->usuariovalidate()->where('user_id', $this->id)->first();
        return $valido != null ? $valido->validate : 0;
    }

    public function getMistarjetasAttribute(){
        $listarTarjetas = $this->culqiCard()->where('user_id', $this->id)->get();

        $tarjetas = [];

        if($listarTarjetas){
            foreach($listarTarjetas as $l){
                $tarjetas[] = [
                    "card_id" => $l->culqi_id,
                    "card_number" => $l->card_number,
                    "card_brand" => $l->card_brand,
                ];
            }
        }

        return $tarjetas;
    }

    public function getPagosaceptadosAttribute(){
        
        $efectivo = $this->configuracion()->where('clave', 'efectivo')->first();
        $visa = $this->configuracion()->where('clave', 'visa')->first();
        $master = $this->configuracion()->where('clave', 'master')->first();

        $pagosAceptados = [
            'efectivo' => ($efectivo != null) ? (int) $efectivo->valor : 0,
            'visa' => ($visa != null) ? (int) $visa->valor : 0,
            'master' => ($master != null) ? (int) $master->valor : 0,
        ];

        return $pagosAceptados;
    }

    public function getCalificacionAttribute(){
        return 5;
        $cantidadCalificaciones = $this->calificaciones()->count();
        $sumaCalificaciones = $this->calificaciones()->sum('calificacion');
        // return $sumaCalificaciones;

        if($cantidadCalificaciones){
            try {
                return round($sumaCalificaciones/$cantidadCalificaciones, 2);
            } catch (\Exception $e) {
                return 0;
            }
        }else{
            return 0;
        }

    }

    public function getDescuentoAttribute()
    {
        $descuento = $this->configuracion()->where('clave', 'descuento')->first();
        return ($descuento != null) ? (int) $descuento->valor : 0;
    }

    public function getWhatsappAttribute()
    {
        $whatsapp = $this->configuracion()->where('clave', 'whatsapp')->first();
        return ($whatsapp != null) ? (int) $whatsapp->valor : 0;
    }

    public function getRecojoentiendaAttribute()
    {
        $recojoentienda = $this->configuracion()->where('clave', 'recojoentienda')->first();
        return ($recojoentienda != null) ? (int) $recojoentienda->valor : 0;
    }

    public function getTerminosAttribute()
    {
        $urlTerminos = $this->configuracion()->where('clave', 'terminos')->first();
        return ($urlTerminos != null) ? ($urlTerminos->valor === "0") ? "" : $urlTerminos->valor : "";
    }

    public function getAbiertoAttribute()
    {
        $hoyEs = date('l');
            switch ($hoyEs) {
                case 'Monday':
                    $horario = $this->tiempoLunes;
                    break;
                case 'Tuesday':
                    $horario = $this->tiempoMartes;
                    break;
                case 'Wednesday':
                    $horario = $this->tiempoMiercoles;
                    break;
                case 'Thursday':
                    $horario = $this->tiempoJueves;
                    break;
                case 'Friday':
                    $horario = $this->tiempoViernes;
                    break;
                case 'Saturday':
                    $horario = $this->tiempoSabado;
                    break;
                case 'Sunday':
                    $horario = $this->tiempoDomingo;
                    break;
                
                default:
                    return 0;
                    break;
            }
        $horaActual = Carbon::createFromFormat('Y-m-d H:i:s', now());
        $tiempo = explode('-', $horario);
        // $abre = Carbon::parse(date('Y-m-d') . " " . $tiempo[0]);
        // $cierra = Carbon::parse(date('Y-m-d') . " " . $tiempo[1]);
        $abre = Carbon::parse(date('Y-m-d') . " " . trim($tiempo[0]))->format('Y-m-d H:i');
        $cierra = Carbon::parse(date('Y-m-d') . " " . trim($tiempo[1]))->format('Y-m-d H:i');

// return $cierra;
        if($horaActual->gte($abre) && $horaActual->lte($cierra)){
            return 1;
        }
        return 0;
    }

    public function getEsMultimarcasAttribute()
    {
        $mismarcas = 0;
        return $mismarcas;
    }
    /**
     * Devuelve con el tipo de usuario.
     */
    public function tipo(){
        return $this->hasOne('App\Users_tipo', 'id', 'tipo_id');
    }

    /**
     * Devuelve con las categorias del usuario.
     */
    public function categorias(){
        return $this->hasMany('App\Producto_categorias', 'tienda_id', 'id');
    }

    public function coordenadas(){
        return $this->hasMany('App\Coordenadas', 'tienda_id', 'id')->orderBy('id', 'asc');
    }

    public function configuracion(){
        return $this->hasMany('App\Tienda_configuracion', 'tienda_id', 'id');
    }

    public function usuariovalidate(){
        return $this->hasOne('App\Users_validate', 'user_id', 'id');
    }

    public function culqiCustomer(){
        return $this->hasOne('App\Culqi_customer', 'user_id', 'id');
    }

    public function culqiCard(){
        return $this->hasMany('App\Culqi_card', 'user_id', 'id');
    }

    public function calificaciones(){
        return $this->hasMany('App\Calificaciones', 'tienda_id', 'id');
    }

    public function mismarcas()
    {
        return $this->hasMany('App\User', 'multimarca_id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new EmailResetPasswordNotification($token));
    }
    
}
