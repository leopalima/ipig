<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;

class Pedido_detalles extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pedido_id', 'producto_id', 'tipo_producto', 'precio', 'cantidad', 'estatus', 'tienda_id', 'cliente_id',
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'pedido_detalles';

    /**
     * Quitar el autoincremento para trabajar con Uuid.
     */

    public $incrementing = false;

    /**
     * Función que se ejecuta al llamar el modelo.
     */

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $uuid = Uuid::generate()->string;
            $model->{$model->getKeyName()} = $uuid;
        });

    }

    public function producto(){
        return $this->hasOne('App\Productos', 'id', 'producto_id');
    }

    public function adicional(){
        return $this->hasOne('App\Adicionales_items', 'id', 'producto_id');
    }
}
