<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificaciones extends Model
{
    protected $fillable = [
        'titulo', 'mensaje',
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'notificaciones';
}
