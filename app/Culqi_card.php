<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Culqi_card extends Model
{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'culqi_id',
        'date',
        'customer_id',
        'card_number',
        'last_four',
        'active',
        'card_brand',
        'card_type',
        'card_category',
        'name',
        'country',
        'country_code',
        'ip',
    ];

    /**
     * Establece el nombre de la tabla.
     */

     protected $table = 'culqi_card';
}
