<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coordenadas extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tienda_id', 'maximo', 'latitud', 'longitud',
	];

	/**
    * Establece el nombre de la tabla.
    */

	protected $table = 'coordenadas';

    /**
    * Establece el uso de softDelete.
    */

	use SoftDeletes;

}
