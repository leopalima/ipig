<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificaciones extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tienda_id' , 'cliente_id', 'calificacion',
    ];

    /**
     * Establece el nombre de la tabla.
     */

    protected $table = 'calificaciones';
}
