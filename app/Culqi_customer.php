<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Culqi_customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'object', 'culqi_id', 'creation_date', 'email', 'country_code', 'first_name', 'last_name', 'address_city', 'address', 'email', 'phone', 'object',
    ];

    /**
     * Establece el nombre de la tabla.
     */

     protected $table = 'culqi_customer';

    /**
     * Función que se ejecuta al llamar el modelo.
     */

}
